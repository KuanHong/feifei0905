﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : MonoBehaviour {
  public GameObject meteorObj = null;
  public GameObject explosionObj = null;
  public float elapsedTime = 15.0f;
  public int cannonId =-1;
  public int ballIdCounter = 0;
  public bool host = false;

  public delegate void BallEventHandler(int cid, int bid, string collide_obj);

  public class CannonBallInfo {
    public GameObject instance = null;
    public GameObject expInstance = null;
    public float elapsedTime = 5.0f;
    public float duration = 0.0f;
    public Vector3 velocity = Vector3.forward;
    public Vector3 position = Vector3.zero;

    public bool playExplosionAnimation = false;
    public bool isHost = false;

    BallEventHandler pHandler = null;

    float destroyTime = -1;

    int cannon_id = -1;
    public int ball_id = -1;

    public CannonBallInfo(bool host, int cid, int bid) {
      cannon_id = cid;
      ball_id = bid;
      isHost = host;
    }

    public bool iterateNextPosition() {
      duration += Time.deltaTime;
      if (duration >= destroyTime) {
        GameObject.Destroy(instance);
        GameObject.Destroy(expInstance);
        return true;
      }

      if (duration >= elapsedTime) {
        if (playExplosionAnimation == false) {
          playExplosionAnimation = true;

          expInstance.transform.position = instance.transform.position;
          expInstance.SetActive(true);

          GameObject.Destroy(instance.transform.Find("Meteo").gameObject);

        }
        return false;
      }

      Vector3 gravity = new Vector3(0.0f, -9800.0f, 0.0f);
      velocity = velocity + gravity * Time.deltaTime * Time.deltaTime;
      position = position + velocity * Time.deltaTime;

      instance.transform.position = position;

      return false;
    }

    public void init(GameObject meteorPrefab, GameObject explosionPrefab, Vector3 initPosi, Vector3 initVel, float lifeTime, BallEventHandler ph) {
      pHandler = ph;
      instance = GameObject.Instantiate(meteorPrefab);
      expInstance = GameObject.Instantiate(explosionPrefab);
      expInstance.SetActive(false);
      instance.transform.position = initPosi;
      position = initPosi;
      velocity = initVel;
      duration = 0.0f;
      elapsedTime = lifeTime;

      destroyTime = elapsedTime + 15.0f;

      instance.transform.Find("MeteoCollider").GetComponent<CannonCollisionReporter>().parentBall = this;
    }

    public void collideWithGameObject(string object_name) {
      if (isHost == true) {
        if (duration < elapsedTime && playExplosionAnimation == false) {
          duration = elapsedTime;
        }
      }

      //notify playercontroller
      if (pHandler != null) {
        pHandler(cannon_id, ball_id, object_name);
      }

    }
  }
  List<CannonBallInfo> ballList = new List<CannonBallInfo>();

  float counter = 3.0f;
  public float countDownTime = 20.0f;
  public delegate void CannonEventHandler(int cannon_id, CannonBall.CannonEvent event_type, object param);
  CannonEventHandler pHandler = null;

  // Use this for initialization
  void Start() {
    

  }

  public enum CannonEvent {
    FIRE,
    COLLIDE_WITH_PLAYER
  }

  // Update is called once per frame
  bool mInited = false;
  void Update() {
    if (mInited == false)
      return;

    if (host == true) {
      counter -= Time.deltaTime;
      if (counter <= 0.0f) {
        //launch meteor
        CannonBallInfo cbi = new CannonBallInfo(host, cannonId, ballIdCounter);
        cbi.init(meteorObj, explosionObj, gameObject.transform.position, gameObject.transform.forward * 250.0f, elapsedTime, handleEventFromCannonBall);
        ballList.Add(cbi);
        ballIdCounter++;

        counter = countDownTime;

        //send event
        if (pHandler != null) {
          pHandler(cannonId, CannonBall.CannonEvent.FIRE, new object[] { cbi.ball_id });
        }
      }
    }

    for (int i = 0; i < ballList.Count;/* ++i*/) {
      if (ballList[i].iterateNextPosition() == true) {
        ballList.RemoveAt(i);
      } else {
        ++i;
      }
    }

  }

  public void manuallyFireCannon(int ball_id) {
    if (host == true)
      return;

    CannonBallInfo cbi = new CannonBallInfo(host, cannonId, ball_id);
    cbi.init(meteorObj, explosionObj, gameObject.transform.position, gameObject.transform.forward * 250.0f, elapsedTime, handleEventFromCannonBall);
    ballList.Add(cbi);

  }

  public void manuallyExplodeCannonBall(int ballid) {
    bool foundball = false;
    for (int i = 0; i < ballList.Count; ++i) {
      if (ballList[i].ball_id == ballid) {
        ballList[i].duration = ballList[i].elapsedTime; //explode cannon ball
        foundball = true;
        break;
      }
    }

    if (foundball == false) {
      Debug.LogWarning("173 - ball id " + ballid + " not found");
    }
  }

  public void init(int id, bool isHost, CannonEventHandler handler) {
    host = isHost;
    pHandler = handler;
    cannonId = id;
    counter = UnityEngine.Random.Range(0.0f, countDownTime);

    mInited = true;
  }

  public void handleEventFromCannonBall(int cannon_id, int ball_id, string collide_obj) {
    if (pHandler != null) {
      pHandler(cannon_id, CannonBall.CannonEvent.COLLIDE_WITH_PLAYER, new object[] { ball_id, collide_obj });
    }
  }
}
