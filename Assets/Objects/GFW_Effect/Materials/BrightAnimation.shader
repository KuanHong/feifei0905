﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "FeiFei/BrightAnimation"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_Speed("Speed",Range(1,10))=1
		_Strength("Strength",Range(1,100)) = 40
		_Color("Color Tint",Color) = (1,1,1,1)
	}
		SubShader
	{
		Tags{ "RenderType" = "Transparent" "Queue" = "Transparent" }
		LOD 100

		Blend srcAlpha oneMinusSrcAlpha

		Pass
		{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float3 worldPos : TEXCOORD1;
			};

			half4 _Color;
			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _Speed;
			float _Strength;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				// sample the texture
				half4 col = tex2D(_MainTex, i.uv)* _Color;
				float time = _Time.y % 2 * _Speed;
				float vertexV = i.uv.y / _MainTex_ST.y;
				float timeFactor = abs(vertexV - time);
				col.a *= pow(1- timeFactor,2);
				col.rgb *= _Strength *col.a;
				//col.a = vertexV;
				return col;
			}
			ENDCG
		}
	}
}