﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureAnimation : MonoBehaviour
{
    public int FPS = 24;
    public Texture2D[] textureArray;
    public MeshRenderer render;
    Material mat;
    int index;
    float lastFrameTime;
    // Use this for initialization
    void Start()
    {
        mat = render.material;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > (lastFrameTime + 1f / FPS)) {
            if (index > textureArray.Length - 2)
                index = 0;
            else
                index++;
            lastFrameTime = Time.time;
        }
        mat.SetTexture("_AlphaTex", textureArray[index]);
    }
}
