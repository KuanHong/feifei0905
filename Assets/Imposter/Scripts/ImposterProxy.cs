﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ImposterProxy : MonoBehaviour {
  [HideInInspector]
  public float maxSize = 0.0f;
  [HideInInspector]
  public Bounds bound, shadowBound;
  [HideInInspector]
  public float shadowZOffset = 0.35f;
  [HideInInspector]
  public float zOffset = 0.35f;
  [HideInInspector]
  public float maxShadowDistance = 1.0f;
  [HideInInspector]
  public int shadowDivider = 1;
  public MeshRenderer quad, shadow;

  public ImposterTexture imposterTexture = null;
  public ImposterTexture shadowTexture = null;
  private Camera renderingCamera;
  public List<Renderer> renderers;
  private bool castShadow = true;

  void Awake() {
    imposterTexture = shadowTexture = null;
  }

  public void Init(List<Renderer> renderers, bool useShadow) {
    //Debug.Log("Init ImposterProxy with renders (count=" + renderers.Count + ")");
    //for (int i = 0; i < renderers.Count; ++i) {
    //  Debug.Log("33 - renderers[" + i + "].gameObject.name=" + renderers[i].gameObject.name);
    //}

    if (imposterTexture != null) ImposterManager.instance.giveBackRenderTexture(imposterTexture);
    if (shadowTexture != null) ImposterManager.instance.giveBackRenderTexture(shadowTexture);

    this.castShadow = useShadow;
    this.renderers = renderers;
    renderingCamera = ImposterManager.instance.imposterRenderingCamera;
    imposterTexture = shadowTexture = null;
    if (!useShadow) shadow.gameObject.SetActive(false);

    extractBounds();
    adjustSize();
  }

  void OnDestroy() {
    InvalidateTexture();
  }

  public void AdjustTextureSize(int size) {
    if (imposterTexture != null && imposterTexture.size == size) {
      //Debug.Log("55 - ignore adjust texture (imposterTexture already exist, owner =" + gameObject.transform.parent.gameObject.name);
      return;
    }

    if (imposterTexture != null) {
      ImposterManager.instance.giveBackRenderTexture(imposterTexture);
      if (castShadow) ImposterManager.instance.giveBackRenderTexture(shadowTexture);
    }

    //Debug.Log("62 - request render texture for renderers[0].gameObject.name=" + renderers[0].gameObject.name+", size="+size);
    imposterTexture = ImposterManager.instance.getRenderTexture(this, size);
    quad.material.mainTexture = imposterTexture.texture;
    quad.material.SetFloat("_ZOffset", zOffset);

    if (castShadow) {
      shadow.gameObject.SetActive(true);
      updateShadow(size);
    } else {
      shadow.gameObject.SetActive(false);
    }
  }

  private void updateShadow(int size) {
    shadowTexture = ImposterManager.instance.getRenderTexture(this, size / shadowDivider);
    shadow.material.mainTexture = shadowTexture.texture;

    shadow.transform.rotation = Quaternion.LookRotation(ImposterManager.instance.mainLight.transform.forward, Vector3.up);
    shadow.transform.position = transform.position - shadow.transform.forward * (shadowZOffset * maxSize / 2.0f);

    renderShadow();
  }

  private void extractBounds() {
    //if (transform.parent.GetComponent<Renderer>() != null) {
    //  bound = transform.parent.GetComponent<Renderer>().bounds;
    //}

    foreach (Renderer r in transform.parent.gameObject.GetComponentsInChildren<Renderer>()) {
      if (r != GetComponentInChildren<Renderer>()) {
        if (bound.extents == Vector3.zero) bound = r.bounds;
        else bound.Encapsulate(r.bounds);
      }
    }

    shadowBound = bound;
    shadowBound.extents *= (1.0f + maxShadowDistance);

    maxSize = bound.extents.magnitude * 2.0f;
  }

  public void Render() {
    quad.transform.rotation = Quaternion.LookRotation(transform.position - ImposterManager.instance.mainCamera.transform.position, Vector3.up);

    renderImposter();
  }

  public void InvalidateTexture() {
    if (IsTextureInvalid()) {
      return;
    }

    ImposterManager.instance.giveBackRenderTexture(imposterTexture);
    imposterTexture = null;

    if (castShadow) {
      ImposterManager.instance.giveBackRenderTexture(shadowTexture);
      shadowTexture = null;
    }
  }

  public bool IsTextureInvalid() {
    if (imposterTexture == null || imposterTexture.texture == null) {
      return true;
    } else {
      return false;
    }
  }

  private void adjustSize() {
    transform.position = bound.center;
    Transform oldParent = transform.parent;
    transform.SetParent(null);
    transform.localScale = new Vector3(maxSize, maxSize, maxSize);
    transform.SetParent(oldParent);
  }

  private void renderImposter() {
    if (imposterTexture == null) {
      return;
    }

    renderingCamera.transform.position = ImposterManager.instance.mainCamera.transform.position;
    renderingCamera.transform.LookAt(bound.center);
    renderingCamera.cullingMask = 1 << ImposterManager.instance.imposterLayer;
    Vector3 cameraDirection = ImposterManager.instance.mainCamera.transform.position - bound.center;
    renderingCamera.orthographic = false;

    float dist = Vector3.Distance(ImposterManager.instance.mainCamera.transform.position, transform.position);
    float fov = 2.0f * Mathf.Atan(maxSize / (2.0f * dist)) * (180 / Mathf.PI);
    renderingCamera.fieldOfView = fov;
    renderingCamera.nearClipPlane = cameraDirection.magnitude - maxSize/2.0f;
    renderingCamera.farClipPlane = cameraDirection.magnitude + maxSize;

    if (renderingCamera.nearClipPlane < 0) {
      Debug.LogWarning("159 - found negative nearClipPlane : cameraDirection.magnitude=" + cameraDirection.magnitude + ", maxSiz=" + maxSize);
    }

    //Debug.Log("156 - set target texture with imposter texture (owner=" + imposterTexture.owner.gameObject.transform.parent.gameObject.name+")");
    renderingCamera.targetTexture = imposterTexture.texture;
    foreach (Renderer r in renderers) {
      //Debug.Log("158 - render imposter (" + gameObject.transform.parent.gameObject.name + ") with render gameobject =" + r.gameObject.name);
      r.enabled = true;
    }

    //renderingCamera.backgroundColor = UnityEngine.Random.ColorHSV();
    //Debug.Log("166 - bgColor =" + renderingCamera.backgroundColor);

    renderingCamera.forceIntoRenderTexture = true;
    renderingCamera.Render();

    foreach (Renderer r in renderers) r.enabled = false;
    renderingCamera.targetTexture = null;
  }

  private void renderShadow() {
    if (shadowTexture == null) {
      return;
    }

    Vector3 shadowDirection = ImposterManager.instance.mainLight.transform.forward;

    renderingCamera.transform.position = transform.position - shadowDirection * (2.0f * maxSize + 1.0f);
    renderingCamera.transform.LookAt(bound.center);
    renderingCamera.cullingMask = 1 << ImposterManager.instance.imposterLayer;

    renderingCamera.orthographic = true;
    renderingCamera.orthographicSize = maxSize / 2.0f;

    renderingCamera.nearClipPlane = 1.0f;
    renderingCamera.farClipPlane = 4.0f * maxSize + 1.0f;

    renderingCamera.targetTexture = shadowTexture.texture;
    foreach (Renderer r in renderers) r.enabled = true;

    renderingCamera.Render();

    foreach (Renderer r in renderers) r.enabled = false;
    renderingCamera.targetTexture = null;
  }

  public bool isVisible() {
    return castShadow ? ImposterManager.instance.isVisible(shadowBound) : ImposterManager.instance.isVisible(bound);
  }

  public bool isVisibleInCache() {
    return castShadow ? ImposterManager.instance.isVisibleInCache(shadowBound) : ImposterManager.instance.isVisibleInCache(bound);
  }

  public void setVisibility(bool bVisible) {
    if (bVisible && !quad.enabled) {
      quad.enabled = true;
      if (castShadow) shadow.enabled = true;
    } else if (!bVisible && quad.enabled) {
      quad.enabled = false;
      shadow.enabled = false;
    }
  }
}
