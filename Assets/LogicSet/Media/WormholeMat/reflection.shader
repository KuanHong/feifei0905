﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Wormhole/base_reflection" {
    Properties {
        _Cube("Reflection Map", Cube) = "" {}
		_Tint("Tint Color", Color) = (1,1,1,1)
    }
 
SubShader {
    Tags { "RenderType"="Opaque" }
 
    pass
    {    
        //Tags { "LightMode"="ForwardAdd"}
 
        CGPROGRAM
 
        #pragma target 3.0
 
        #pragma vertex vert
        #pragma fragment frag
        #include "UnityCG.cginc"
 
        samplerCUBE _Cube;
		float4 _Tint;
 
        struct v2f{
            float4 pos : SV_POSITION;
            float3 coord: TEXCOORD0;
        };
 
            v2f vert(appdata_base v){
                v2f o;
 
                o.pos = UnityObjectToClipPos(v.vertex);
                o.coord = v.normal;
     
                return o;
            }
 
            float4 frag(v2f i) : COLOR{

                float4 finalColor = 1.0;

                half3 ref =reflect(normalize(i.pos), i.coord);

                float4 val = texCUBE(_Cube, i.coord);
                finalColor.xyz = DecodeHDR(val, unity_SpecCube0_HDR);
				finalColor.xyz *=_Tint.rgb;
                finalColor.w = 1.0;
                return finalColor;

            }
 
            ENDCG
        }
    }
    FallBack Off
}