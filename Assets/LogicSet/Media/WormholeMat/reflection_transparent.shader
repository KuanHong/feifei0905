﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Wormhole/base_transparent_reflection" {
    Properties {
        _Transparent("Transparent", Range(0.0, 1.0)) = 1.0
        _Power("Power", Range(0.0, 5.0)) = 1.0
        _BumpMap("Normal Map", 2D) = "bump" {}

		_Cube("Reflection Map", Cube) = "" {}
		_Tint("Tint Color", Color) = (1,1,1,1)
    }
 
SubShader {
 
    pass
    {    
        Tags { "Queue"= "Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite Off
 
        CGPROGRAM
 
        #pragma target 3.0
 
        #pragma vertex vert
        #pragma fragment frag
        #include "UnityCG.cginc"
 
        samplerCUBE _Cube;
		float4 _Tint;
        fixed _Transparent;
        fixed _Power;
 
            struct v2f {
                float3 worldPos : TEXCOORD0;
                // these three vectors will hold a 3x3 rotation matrix
                // that transforms from tangent to world space
                half3 tspace0 : TEXCOORD1; // tangent.x, bitangent.x, normal.x
                half3 tspace1 : TEXCOORD2; // tangent.y, bitangent.y, normal.y
                half3 tspace2 : TEXCOORD3; // tangent.z, bitangent.z, normal.z
                // texture coordinate for the normal map
                float2 uv : TEXCOORD4;
                float3 norm : TEXCOORD5;
                float4 pos : SV_POSITION;
            };

            // vertex shader now also needs a per-vertex tangent vector.
            // in Unity tangents are 4D vectors, with the .w component used to
            // indicate direction of the bitangent vector.
            // we also need the texture coordinate.
            v2f vert (float4 vertex : POSITION, float3 normal : NORMAL, float4 tangent : TANGENT, float2 uv : TEXCOORD0)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(vertex);
                o.worldPos = mul(unity_ObjectToWorld, vertex).xyz;
                o.norm =normal;
                half3 wNormal = UnityObjectToWorldNormal(normal);
                half3 wTangent = UnityObjectToWorldDir(tangent.xyz);
                // compute bitangent from cross product of normal and tangent
                half tangentSign = tangent.w * unity_WorldTransformParams.w;
                half3 wBitangent = cross(wNormal, wTangent) * tangentSign;
                // output the tangent space matrix
                o.tspace0 = half3(wTangent.x, wBitangent.x, wNormal.x);
                o.tspace1 = half3(wTangent.y, wBitangent.y, wNormal.y);
                o.tspace2 = half3(wTangent.z, wBitangent.z, wNormal.z);
                o.uv = uv;
                return o;
            }

            // normal map texture from shader properties
            sampler2D _BumpMap;
        
            fixed4 frag (v2f i) : SV_Target
            {
                // sample the normal map, and decode from the Unity encoding
                half3 tnormal = UnpackNormal(tex2D(_BumpMap, float2(i.uv.x*5.0f, i.uv.y+_Time.x)));
                // transform normal from tangent to world space
                half3 worldNormal;
                worldNormal.x = dot(i.tspace0, tnormal);
                worldNormal.y = dot(i.tspace1, tnormal);
                worldNormal.z = dot(i.tspace2, tnormal);

                // rest the same as in previous shader
                half3 worldViewDir = normalize(UnityWorldSpaceViewDir(i.worldPos));
                half3 worldRefl = reflect(-worldViewDir, worldNormal);
				float4 skyData = texCUBE(_Cube, worldRefl);
                //half4 skyData = UNITY_SAMPLE_TEXCUBE(unity_SpecCube0, worldRefl);
                half3 skyColor = DecodeHDR (skyData, unity_SpecCube0_HDR);
                fixed4 c = 0;
                c.rgb = skyColor;

                float3 viewDir = i.worldPos-_WorldSpaceCameraPos;
                //float3 viewDir = UNITY_MATRIX_IT_MV[2].xyz;
                float d =_Power-abs(dot(normalize(viewDir), i.norm))*_Power;
                if (d>1.0f){
                    d =1.0f;
                }

                c.rgb = lerp(c.rgb, float3(1.0f, 1.0f, 1.0f)*d*d*d, 0.5f);
				c.rgb*=_Tint.rgb;

                c.a =d;//_Transparent;
                if (c.a <=_Transparent)
                    c.a =_Transparent;
                return c;
            }
            ENDCG
        }
    }
    FallBack Off
}