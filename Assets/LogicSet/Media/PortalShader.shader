﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/PortalShader" {
	Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
		_MaskTex ("Mask (RGB)", 2D) = "White" {}
    }
	SubShader {
	Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
	Blend SrcAlpha OneMinusSrcAlpha
		Pass {
			CGPROGRAM

			#pragma target 3.0
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			uniform sampler2D _MainTex;
			float4 _MainTex_ST;
			uniform sampler2D _MaskTex;
			float4 _MaskTex_ST;

			struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

			struct v2f {
				float4 pos : SV_POSITION;
				float4 pos_frag : TEXCOORD0;
				float2 uv : TEXCOORD1;
			};
			
			v2f vert(appdata v) {
				v2f o;
				float4 clipSpacePosition = UnityObjectToClipPos(v.vertex);
				o.pos = clipSpacePosition;
				// Copy of clip space position for fragment shader
				o.pos_frag = clipSpacePosition;
				o.uv =TRANSFORM_TEX(v.uv, _MaskTex);
				return o;
			}
			
			half4 frag(v2f i) : SV_Target {
				// Perspective divide (Translate to NDC - (-1, 1))
				float2 uv = i.pos_frag.xy / i.pos_frag.w;
				// Map -1, 1 range to 0, 1 tex coord range
				uv = (uv + float2(1.0, 1.0)) * 0.5;
				uv.y =1.0f-uv.y;
				float4 c = tex2D(_MainTex, uv);
				c.a =tex2D(_MaskTex, i.uv).r;
				return c;
			}
			ENDCG
		}
	}
}