﻿Shader "FXXLY/wind_flash"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_FlashTex("Texture", 2D) = "white" {}
		_Mask("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "Queue" = "Transparent" "RenderType" = "Transparent" "IgnoreProjector" = "True" }
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			sampler2D _FlashTex;
			sampler2D _Mask;
			float4 _MainTex_ST;
			float _Alpha;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col1 = tex2D(_FlashTex, i.uv);
				col1 *=float4(1.0f, 1.0f, 1.5f, 0.9f);

				fixed4 col2 = tex2D(_MainTex, i.uv);

				float sv =_Time.y%1.5;
				sv*=1;
				float2 iuv =float2(0.0f, 1.0f)-i.uv;

				fixed4 col3 = tex2D(_Mask, (iuv+float2(sv,0)));

				// float freq =1.0;
				// float repeat =3.0;
				// float highlight =0.5;

				// float alpha = (1.0+sin((_Time.w+(1.0-i.uv.x*repeat))*freq))/2.0;
				// col1.a *=alpha*highlight;

				fixed4 col = col2+(col1*col1.a*col3.a);
				col *= (1.0-_Alpha);

			    
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
