﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleUpdater : MonoBehaviour {

    public GameObject trackedObj = null;
    public GameObject target = null;

	// Use this for initialization
	void Start () {
        
		
	}
	
	// Update is called once per frame
	void Update () {
        target.GetComponent<MeshRenderer>().material.SetVector(Shader.PropertyToID("_HolePos"), new Vector4(trackedObj.transform.position.x, trackedObj.transform.position.y, trackedObj.transform.position.z, 500.0f));
          


    }
}
