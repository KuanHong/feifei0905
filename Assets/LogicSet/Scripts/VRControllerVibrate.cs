﻿using UnityEngine;
using System.Collections;

public class VRControllerVibrate : MonoBehaviour {

	public ushort pulseEnter = 3000;
	public ushort pulseStay = 200;
	SteamVR_TrackedObject trackedObj;

	// Use this for initialization
	void Start () {
		trackedObj = gameObject.transform.parent.gameObject.GetComponent<SteamVR_TrackedObject>();
	
	}
	
	bool moving =false;
	Vector3 prevPosi = Vector3.zero;
	// Update is called once per frame
	void Update () {
		float dist = Vector3.Distance(gameObject.transform.position, prevPosi);
		if (dist >0.001f){
			moving =true;
		}else{
			moving =false;
		}
		prevPosi = gameObject.transform.position;

		//Debug.Log(dist);
	
	}

  void OnCollisionEnter(Collision collision) {
  	OnTriggerEnter(collision.collider);
  }

	void OnTriggerEnter(Collider collider) {
		//Debug.Log(collider.name);
    if (collider.name.Contains("PlayerCollider")) {
      return;
    }
		if (trackedObj !=null){
    		if (trackedObj.index == SteamVR_TrackedObject.EIndex.None) {
      			return;
    		}

			SteamVR_Controller.Input((int)trackedObj.index).TriggerHapticPulse(pulseEnter);
		}
	}

	void OnTriggerStay(Collider collider){
		if (moving ==true && collider.name != "PlayerCollider"){
            //Debug.LogWarning("trigger stay with collider ="+collider.name);
            SteamVR_Controller.Device d = SteamVR_Controller.Input((int)trackedObj.index);
            if (d!=null)
                d.TriggerHapticPulse(pulseStay);
		}
	}
}
