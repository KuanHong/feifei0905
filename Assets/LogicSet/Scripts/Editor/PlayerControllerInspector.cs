﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlayerController))]
public class PlayerControllerInspector : Editor {
  SerializedProperty script = null;

  //status
  SerializedProperty status = null;

  //user camera
  SerializedProperty pc_camera = null;
  SerializedProperty vive_camera = null;
  SerializedProperty pc_eye = null;
  SerializedProperty vive_camerarig = null;
  SerializedProperty vive_hmd = null;
  SerializedProperty dumpling_mgr = null;
  SerializedProperty speedline_mgr = null;

  //ui
  SerializedProperty hud = null;
  SerializedProperty enable_hud = null;
  SerializedProperty show_debug_ui = null;

  //flying ceiling/floor
  SerializedProperty ceilingHeight = null;
  SerializedProperty floorHeight = null;
  SerializedProperty bufferHeight = null;
  SerializedProperty playing_radius = null;

  //
  SerializedProperty launchForce = null;

  //speed limitation
  SerializedProperty maximumSpeed = null;
  SerializedProperty bufferSpeed = null;

  //decending
  SerializedProperty gravityForce = null;
  SerializedProperty antiAscendingForce = null;
  SerializedProperty diveAngle = null;
  SerializedProperty diveCamera = null;
  SerializedProperty diveDuration = null;
  SerializedProperty easeDiveAngle = null;

  //ascending
  SerializedProperty glidingForce = null;
  SerializedProperty glidingForceWhenAscending = null;
  SerializedProperty antiDescendingForce = null;
  SerializedProperty flapForce = null;

  //rotation
  SerializedProperty maxRotForce = null;
  SerializedProperty rotDuration = null;
  SerializedProperty rotDecending = null;
  SerializedProperty easeRotationAngle = null;
  SerializedProperty rollEffect = null;
  SerializedProperty thirdPersonRollEffect = null;

  //sound
  SerializedProperty EnvSoundObj = null;
  SerializedProperty audioFlap1 = null;
  SerializedProperty audioFlap2 = null;
  SerializedProperty dumplingAttracted = null;
  SerializedProperty respawn = null;
  SerializedProperty underattack0 = null;
  SerializedProperty underattack1 = null;
  SerializedProperty shieldSound;
  SerializedProperty rocketSound;

  //adv rotation
  SerializedProperty lowerSpeed = null;
  SerializedProperty rotateHead = null;
  SerializedProperty lowerSpeedRatio = null;
  SerializedProperty rotateHeadRatio = null;
  SerializedProperty disableHeadRotationAtPcMode = null;

  //shield
  SerializedProperty shield_available_duration = null;
  SerializedProperty shield_colddown_time = null;
  SerializedProperty shield_available_times = null;
  SerializedProperty shieldIconObj_vive = null;

  //accelerator
  SerializedProperty accelerator_available_duration = null;
  SerializedProperty accelerator_colddown_time = null;
  SerializedProperty accelerator_available_times = null;
  SerializedProperty acceleratorIconObj_vive = null;

  //effect
  SerializedProperty respawnEffect = null;

  //bound wall
  SerializedProperty boundWall = null;

  //cannons
  SerializedProperty cannons = null;

  //gateway
  SerializedProperty gateway = null;


  public override void OnInspectorGUI() {
    if (script == null) {
      script = serializedObject.FindProperty("m_Script");

      status = serializedObject.FindProperty("current_status");

      pc_camera = serializedObject.FindProperty("pc_camera");
      vive_camera = serializedObject.FindProperty("vive_camera");
      pc_eye = serializedObject.FindProperty("pc_eye");
      vive_camerarig = serializedObject.FindProperty("vive_camerarig");
      vive_hmd = serializedObject.FindProperty("vive_hmd");
      dumpling_mgr = serializedObject.FindProperty("dumplingMgrObj");
      speedline_mgr = serializedObject.FindProperty("speedLineMgr");

      hud = serializedObject.FindProperty("hud");
      enable_hud = serializedObject.FindProperty("enable_hud");
      show_debug_ui = serializedObject.FindProperty("show_debug_ui");

      ceilingHeight = serializedObject.FindProperty("ceilingHeight");
      floorHeight = serializedObject.FindProperty("floorHeight");
      bufferHeight = serializedObject.FindProperty("bufferHeight");
      playing_radius = serializedObject.FindProperty("playing_radius");

      launchForce = serializedObject.FindProperty("launchForce");

      maximumSpeed = serializedObject.FindProperty("maximumSpeed");
      bufferSpeed = serializedObject.FindProperty("bufferSpeed");

      gravityForce = serializedObject.FindProperty("gravityForce");
      antiAscendingForce = serializedObject.FindProperty("antiAscendingForce");
      diveAngle = serializedObject.FindProperty("diveAngle");
      diveCamera = serializedObject.FindProperty("diveCamera");
      diveDuration = serializedObject.FindProperty("diveDuration");
      easeDiveAngle = serializedObject.FindProperty("easeDiveAngle");


      glidingForce = serializedObject.FindProperty("glidingForce");
      glidingForceWhenAscending = serializedObject.FindProperty("glidingForceWhenAscending");
      antiDescendingForce = serializedObject.FindProperty("antiDescendingForce");
      flapForce = serializedObject.FindProperty("flapForce");
      dumplingAttracted = serializedObject.FindProperty("dumplingAttracted");
      respawn = serializedObject.FindProperty("respawn");

      maxRotForce = serializedObject.FindProperty("maxRotForce");
      rotDuration = serializedObject.FindProperty("rotDuration");
      rotDecending = serializedObject.FindProperty("rotDecending");
      easeRotationAngle = serializedObject.FindProperty("easeRotationAngle");
      rollEffect = serializedObject.FindProperty("rollEffect");
      thirdPersonRollEffect = serializedObject.FindProperty("thirdPersonRollEffect");

      shield_available_duration = serializedObject.FindProperty("shield_available_duration");
      shield_colddown_time = serializedObject.FindProperty("shield_colddown_time");
      shield_available_times = serializedObject.FindProperty("shield_available_times");
      shieldIconObj_vive = serializedObject.FindProperty("shieldIconObj_vive");

      accelerator_available_duration = serializedObject.FindProperty("accelerator_available_duration");
      accelerator_colddown_time = serializedObject.FindProperty("accelerator_colddown_time");
      accelerator_available_times = serializedObject.FindProperty("accelerator_available_times");
      acceleratorIconObj_vive = serializedObject.FindProperty("acceleratorIconObj_vive");

      EnvSoundObj = serializedObject.FindProperty("EnvSoundObj");
      audioFlap1 = serializedObject.FindProperty("flapAudio1");
      audioFlap2 = serializedObject.FindProperty("flapAudio2");
      underattack0 = serializedObject.FindProperty("underattack0");
      underattack1 = serializedObject.FindProperty("underattack1");
      shieldSound = serializedObject.FindProperty("shieldSound");
      rocketSound = serializedObject.FindProperty("rocketSound");

      lowerSpeed = serializedObject.FindProperty("lowerSpeed");
      lowerSpeedRatio = serializedObject.FindProperty("lowerSpeedRatio");
      rotateHead = serializedObject.FindProperty("rotateHead");
      rotateHeadRatio = serializedObject.FindProperty("rotateHeadRatio");
      disableHeadRotationAtPcMode = serializedObject.FindProperty("disableHeadRotationAtPcMode");
      //minFlapDist = serializedObject.FindProperty("minFlapDist");

      respawnEffect = serializedObject.FindProperty("respawnEffect");

      boundWall = serializedObject.FindProperty("boundWall");

      cannons = serializedObject.FindProperty("cannons");
      gateway = serializedObject.FindProperty("gateway_function");
    }

    serializedObject.Update();

    GUI.enabled = false;
    EditorGUILayout.PropertyField(script, false, new GUILayoutOption[0]);
    GUI.enabled = true;

    EditorGUILayout.Space();
    GUILayout.Label("User Camera", EditorStyles.boldLabel);
    GUILayout.Label("Status : " + status.enumNames[status.enumValueIndex]);

    EditorGUILayout.Space();
    GUILayout.Label("User Camera", EditorStyles.boldLabel);
    EditorGUILayout.PropertyField(pc_camera, new GUIContent("PC Mode"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(pc_eye, new GUIContent("PC Mode Eye"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(vive_camera, new GUIContent("Vive Mode"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(vive_camerarig, new GUIContent("Vive CameraRig"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(vive_hmd, new GUIContent("Vive Hmd (Eye)"), false, new GUILayoutOption[0]);

    EditorGUILayout.Space();
    GUILayout.Label("Extra Manager", EditorStyles.boldLabel);
    EditorGUILayout.PropertyField(speedline_mgr, new GUIContent("SpeedlineMgr"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(dumpling_mgr, new GUIContent("DumplingMgr"), false, new GUILayoutOption[0]);

    EditorGUILayout.Space();
    GUILayout.Label("HUD", EditorStyles.boldLabel);
    EditorGUILayout.PropertyField(enable_hud, new GUIContent("Enable"), false, new GUILayoutOption[0]);
    if (enable_hud.boolValue == true) {
      EditorGUILayout.PropertyField(show_debug_ui, new GUIContent("Show Debug UI"), false, new GUILayoutOption[0]);
      EditorGUILayout.PropertyField(hud, new GUIContent("GameObject"), false, new GUILayoutOption[0]);
    }
    EditorGUILayout.Space();
    GUILayout.Label("Ceiling/Floor Limitation", EditorStyles.boldLabel);
    EditorGUILayout.PropertyField(ceilingHeight, new GUIContent("Ceiling (m)"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(floorHeight, new GUIContent("Floor (m)"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(bufferHeight, new GUIContent("Buffer Height (m)"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(playing_radius, new GUIContent("Available Playing Radius"), false, new GUILayoutOption[0]);

    EditorGUILayout.Space();
    GUILayout.Label("Launch", EditorStyles.boldLabel);
    EditorGUILayout.PropertyField(launchForce, new GUIContent("Launch Force"), false, new GUILayoutOption[0]);

    EditorGUILayout.Space();
    GUILayout.Label("Speed Limitation", EditorStyles.boldLabel);
    EditorGUILayout.PropertyField(maximumSpeed, new GUIContent("Max speed (km/h)"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(bufferSpeed, new GUIContent("Buffer speed (km/h)"), false, new GUILayoutOption[0]);

    EditorGUILayout.Space();
    GUILayout.Label("Ascending Force", EditorStyles.boldLabel);
    EditorGUILayout.PropertyField(glidingForce, new GUIContent("Gliding Force"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(glidingForceWhenAscending, new GUIContent("Apply When Ascending"), false, new GUILayoutOption[0]);
    EditorGUILayout.Space();
    EditorGUILayout.PropertyField(antiDescendingForce, new GUIContent("Anti-Decending"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(flapForce, new GUIContent("Flap Force"), false, new GUILayoutOption[0]);


    EditorGUILayout.Space();
    GUILayout.Label("Decending Force", EditorStyles.boldLabel);
    EditorGUILayout.PropertyField(gravityForce, new GUIContent("Gravity"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(antiAscendingForce, new GUIContent("Anti-Ascending"), false, new GUILayoutOption[0]);
    EditorGUILayout.Space();
    EditorGUILayout.PropertyField(diveCamera, new GUIContent("Pitch Effect"), false, new GUILayoutOption[0]);
    if (diveCamera.boolValue == true) {
      EditorGUILayout.PropertyField(diveDuration, new GUIContent("Dive Duration"), false, new GUILayoutOption[0]);
      EditorGUILayout.PropertyField(diveAngle, new GUIContent("Dive Angle"), false, new GUILayoutOption[0]);
      EditorGUILayout.PropertyField(easeDiveAngle, new GUIContent("Ease Dive Angle"), false, new GUILayoutOption[0]);
    }

    EditorGUILayout.Space();
    GUILayout.Label("Rotation", EditorStyles.boldLabel);
    EditorGUILayout.PropertyField(maxRotForce, new GUIContent("Max Rotation Force"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(rotDuration, new GUIContent("Rotation Duration"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(rotDecending, new GUIContent("Rotation Decending"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(easeRotationAngle, new GUIContent("Ease Rotation Angle"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(rollEffect, new GUIContent("Roll Effect"), false, new GUILayoutOption[0]);
    if (rollEffect.boolValue == true) {
      EditorGUILayout.PropertyField(thirdPersonRollEffect, new GUIContent("3rd. Person RollEffect"), false, new GUILayoutOption[0]);
    }

    EditorGUILayout.Space();
    GUILayout.Label("Adv. Rotation", EditorStyles.boldLabel);
    EditorGUILayout.PropertyField(lowerSpeed, new GUIContent("Lower Speed"), false, new GUILayoutOption[0]);
    if (lowerSpeed.boolValue == true) {
      EditorGUILayout.PropertyField(lowerSpeedRatio, new GUIContent("Lower Speed Ratio"), false, new GUILayoutOption[0]);
    }
    EditorGUILayout.PropertyField(rotateHead, new GUIContent("Rotate Head"), false, new GUILayoutOption[0]);
    if (rotateHead.boolValue == true) {
      EditorGUILayout.PropertyField(rotateHeadRatio, new GUIContent("Rotate Head Ratio"), false, new GUILayoutOption[0]);
      EditorGUILayout.PropertyField(disableHeadRotationAtPcMode, new GUIContent("Disable Head Rotatation At PCMode"), false, new GUILayoutOption[0]);
    }

    EditorGUILayout.Space();
    GUILayout.Label("Shield", EditorStyles.boldLabel);
    EditorGUILayout.PropertyField(shield_available_times, new GUIContent("Shield Available Times"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(shield_available_duration, new GUIContent("Shield Duration"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(shield_colddown_time, new GUIContent("Shield ColdTime"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(shieldIconObj_vive, new GUIContent("Shield Icon (VIVE)"), false, new GUILayoutOption[0]);

    EditorGUILayout.Space();
    GUILayout.Label("Accelerator", EditorStyles.boldLabel);
    EditorGUILayout.PropertyField(accelerator_available_times, new GUIContent("Accelerator Available times"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(accelerator_available_duration, new GUIContent("Accelerator Duration"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(accelerator_colddown_time, new GUIContent("Accelerator ColdTime"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(acceleratorIconObj_vive, new GUIContent("Accelerator Icon (VIVE)"), false, new GUILayoutOption[0]);

    EditorGUILayout.Space();
    GUILayout.Label("Sound", EditorStyles.boldLabel);
    EditorGUILayout.PropertyField(EnvSoundObj, new GUIContent("Env Sound Obj"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(audioFlap1, new GUIContent("Flap Sound 1"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(audioFlap2, new GUIContent("Flap Sound 2"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(dumplingAttracted, new GUIContent("Dumpling Attracted"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(respawn, new GUIContent("Respawn"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(underattack0, new GUIContent("Underattack0"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(underattack1, new GUIContent("Underattack1"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(shieldSound, new GUIContent("shieldSound"), false, new GUILayoutOption[0]);
    EditorGUILayout.PropertyField(rocketSound, new GUIContent("rocketSound"), false, new GUILayoutOption[0]);

    EditorGUILayout.Space();
    GUILayout.Label("Effects", EditorStyles.boldLabel);
    EditorGUILayout.PropertyField(respawnEffect, new GUIContent("respawn"), false, new GUILayoutOption[0]);

    EditorGUILayout.Space();
    GUILayout.Label("Bound Wall", EditorStyles.boldLabel);
    EditorGUILayout.PropertyField(boundWall, new GUIContent("bound wall"), false, new GUILayoutOption[0]);

    EditorGUILayout.Space();
    GUILayout.Label("Gateway", EditorStyles.boldLabel);
    EditorGUILayout.PropertyField(gateway, new GUIContent("eanble"), false, new GUILayoutOption[0]);

    EditorGUILayout.Space();
    GUILayout.Label("Cannons", EditorStyles.boldLabel);
    EditorGUILayout.PropertyField(cannons, new GUIContent("list"), true, new GUILayoutOption[0]);

    serializedObject.ApplyModifiedProperties();
  }
}
