﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DumplingManager))]
public class DumplingManagerInspector : Editor
{
    SerializedProperty script = null;

    //npc target
    SerializedProperty npcRoot = null;
    SerializedProperty npcTarget1 = null;
    SerializedProperty npcTarget2 = null;

    //target generation
    SerializedProperty targetPrefab = null;
    SerializedProperty shieldPrefab = null;
    SerializedProperty acceleratorPrefab = null;
    SerializedProperty radius = null;
    SerializedProperty min_radius = null;
    SerializedProperty height = null;
    SerializedProperty numTarget = null;
    SerializedProperty numShield = null;
    SerializedProperty numAccelerator = null;
    SerializedProperty scale = null;
    SerializedProperty traceNumber = null;

    //target tracer
    SerializedProperty traceLength = null;
    SerializedProperty minTraceLength = null;
    SerializedProperty h_traceAngle = null;
    SerializedProperty v_traceAngle = null;
    SerializedProperty highlitedMat = null;
    SerializedProperty sightGameObj = null;

    //missile system
    SerializedProperty missileLaunchEffectPrefab = null;
    SerializedProperty missilePrefab = null;
    SerializedProperty remoteMissilePrefabB = null;
    SerializedProperty remoteMissilePrefabR = null;
    SerializedProperty playerHitPrefab = null;
    SerializedProperty bunHitPrefab = null;
    SerializedProperty shieldEffect = null;

    //holo radar
    SerializedProperty enableHoloRadar = null;
    SerializedProperty radarBottomImage = null;
    SerializedProperty radarBottomImageRed = null;

    SerializedProperty alliesArrow = null;
    SerializedProperty hostileArrow = null;
    SerializedProperty arrowImage = null;
    SerializedProperty dumplingImage = null;
    SerializedProperty playerImage = null;
    SerializedProperty npcImage = null;
    SerializedProperty gndImage = null;
    SerializedProperty radarAlliesPrefab = null;
    SerializedProperty linePrefab = null;
    SerializedProperty trailPrefab = null;
    SerializedProperty playerTrailPrefab = null;
    SerializedProperty hostileTrailPrefab = null;
    SerializedProperty alliesTrailPrefab = null;
    SerializedProperty radar_SpawnPrefab = null;


    SerializedProperty radarMissilePrefab = null;
    SerializedProperty healthy_sprite = null;

    SerializedProperty npcColor = null;
    SerializedProperty dumplingColor = null;
    SerializedProperty playerColor = null;
    SerializedProperty missileColor = null;

    SerializedProperty radarRadius = null;
    SerializedProperty radarHeight = null;
    SerializedProperty radar_followViewDir = null;

    SerializedProperty radar_terrain = null;

    SerializedProperty dumpling_counter_digits = null;
    SerializedProperty dumplingCollected = null;
    SerializedProperty dumplingUnloaded = null;

    SerializedProperty dumplingShield = null;
    SerializedProperty missileStatus = null;

    SerializedProperty accelerator = null;

    SerializedProperty rocketBtnGO = null;
    SerializedProperty shieldBtnGO = null;


    public override void OnInspectorGUI()
    {
        if (script == null)
        {
            script = serializedObject.FindProperty("m_Script");

            npcRoot = serializedObject.FindProperty("NPCRoot");
            npcTarget1 = serializedObject.FindProperty("npcPrefab1");
            npcTarget2 = serializedObject.FindProperty("npcPrefab2");

            targetPrefab = serializedObject.FindProperty("targetPrefab");
            shieldPrefab = serializedObject.FindProperty("shieldPrefab");
            acceleratorPrefab = serializedObject.FindProperty("acceleratorPrefab");
            radius = serializedObject.FindProperty("radius");
            min_radius = serializedObject.FindProperty("min_radius");
            height = serializedObject.FindProperty("height");
            numTarget = serializedObject.FindProperty("numTarget");
            numShield = serializedObject.FindProperty("numShield");
            numAccelerator = serializedObject.FindProperty("numAccelerator");
            scale = serializedObject.FindProperty("scale");
            traceNumber = serializedObject.FindProperty("traceNumber");

            traceLength = serializedObject.FindProperty("traceLength");
            minTraceLength = serializedObject.FindProperty("minTraceLength");
            h_traceAngle = serializedObject.FindProperty("h_traceAngle");
            v_traceAngle = serializedObject.FindProperty("v_traceAngle");
            highlitedMat = serializedObject.FindProperty("lineMat");
            sightGameObj = serializedObject.FindProperty("sightGameObj");

            missileLaunchEffectPrefab = serializedObject.FindProperty("missileLaunchEffectPrefab");
            missilePrefab = serializedObject.FindProperty("missilePrefab");
            remoteMissilePrefabB = serializedObject.FindProperty("remoteMissilePrefabB");
            remoteMissilePrefabR = serializedObject.FindProperty("remoteMissilePrefabR");
            playerHitPrefab = serializedObject.FindProperty("playerHit");
            bunHitPrefab = serializedObject.FindProperty("bunHit");
            shieldEffect = serializedObject.FindProperty("shieldEffect");

            enableHoloRadar = serializedObject.FindProperty("enableHoloRadar");
            radarBottomImage = serializedObject.FindProperty("radarBottomImage");
            radarBottomImageRed = serializedObject.FindProperty("radarBottomImageRed");

            alliesArrow = serializedObject.FindProperty("radar_alliesArrowPrefab");
            hostileArrow = serializedObject.FindProperty("radar_hostileArrowPrefab");
            arrowImage = serializedObject.FindProperty("radar_arrowPrefab");
            dumplingImage = serializedObject.FindProperty("radar_dumplingPrefab");
            playerImage = serializedObject.FindProperty("radar_playerPrefab");
            npcImage = serializedObject.FindProperty("radar_npcPrefab");
            radarAlliesPrefab = serializedObject.FindProperty("radar_alliesPrefab");
            trailPrefab = serializedObject.FindProperty("radar_trailPrefab");
            playerTrailPrefab = serializedObject.FindProperty("radar_playerTrailPrefab");
            hostileTrailPrefab = serializedObject.FindProperty("radar_hostileTrailPrefab");
            alliesTrailPrefab = serializedObject.FindProperty("radar_alliesTrailPrefab");
            radar_SpawnPrefab = serializedObject.FindProperty("radar_SpawnPrefab");

            radar_terrain = serializedObject.FindProperty("radar_terrain");

            radarMissilePrefab = serializedObject.FindProperty("radarMissilePrefab");
            healthy_sprite = serializedObject.FindProperty("radar_healthy_sprite");

            radarRadius = serializedObject.FindProperty("radarRadius");
            radarHeight = serializedObject.FindProperty("radarHeight");
            radar_followViewDir = serializedObject.FindProperty("followViewDir");

            dumpling_counter_digits = serializedObject.FindProperty("dumpling_counter_digits");
            dumplingCollected = serializedObject.FindProperty("dumplingCollected");
            dumplingUnloaded = serializedObject.FindProperty("dumplingUnloaded");

            dumplingShield = serializedObject.FindProperty("dumplingShield");
            missileStatus = serializedObject.FindProperty("missileStatus");

            accelerator = serializedObject.FindProperty("accelerator");

            rocketBtnGO = serializedObject.FindProperty("rocketBtnGO");
            shieldBtnGO = serializedObject.FindProperty("shieldBtnGO");
        }

        serializedObject.Update();

        GUI.enabled = false;
        EditorGUILayout.PropertyField(script, false, new GUILayoutOption[0]);
        GUI.enabled = true;

        EditorGUILayout.Space();
        GUILayout.Label("Target Generation", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(npcRoot, new GUIContent("NPCRoot"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(npcTarget1, new GUIContent("NPC Prefab1"), new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(npcTarget2, new GUIContent("NPC Prefab2"), new GUILayoutOption[0]);
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(targetPrefab, new GUIContent("Target Prefab"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(shieldPrefab, new GUIContent("Shield Prefab"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(acceleratorPrefab, new GUIContent("Accelerator Prefab"), false, new GUILayoutOption[0]);
        EditorGUILayout.Space();
        EditorGUILayout.PropertyField(radius, new GUIContent("radius (m)"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(min_radius, new GUIContent("min radius"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(height, new GUIContent("height (m)"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(numTarget, new GUIContent("dumpling amount"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(numShield, new GUIContent("shield amount"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(numAccelerator, new GUIContent("accelerator amount"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(scale, new GUIContent("scale"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(traceNumber, new GUIContent("trace number"), false, new GUILayoutOption[0]);

        EditorGUILayout.Space();
        GUILayout.Label("Target Tracer", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(traceLength, new GUIContent("distance"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(minTraceLength, new GUIContent("min distance"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(h_traceAngle, new GUIContent("horizontal angle"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(v_traceAngle, new GUIContent("vertical angle"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(highlitedMat, new GUIContent("highlited Material"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(sightGameObj, new GUIContent("Sight UI GameObject"), false, new GUILayoutOption[0]);

        EditorGUILayout.Space();
        GUILayout.Label("Missile System", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(missileLaunchEffectPrefab, new GUIContent("missile launch effect"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(missilePrefab, new GUIContent("missile prefab"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(remoteMissilePrefabB, new GUIContent("remote missile prefab (Blue)"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(remoteMissilePrefabR, new GUIContent("remote missile prefab (Red)"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(playerHitPrefab, new GUIContent("Player Hit"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(bunHitPrefab, new GUIContent("Bun Hit"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(shieldEffect, new GUIContent("Shield Effect"), false, new GUILayoutOption[0]);

        EditorGUILayout.Space();
        GUILayout.Label("Holoradar", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(enableHoloRadar, new GUIContent("Enable HoloRadar"), false, new GUILayoutOption[0]);
        if (enableHoloRadar.boolValue == true)
        {
            EditorGUILayout.PropertyField(radarBottomImage, new GUIContent("Bottom Image"), false, new GUILayoutOption[0]);
            EditorGUILayout.PropertyField(radarBottomImageRed, new GUIContent("Bottom Image (Red)"), false, new GUILayoutOption[0]);

            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(alliesArrow, new GUIContent("Allies Arrow Prefab"), false, new GUILayoutOption[0]);
            EditorGUILayout.PropertyField(hostileArrow, new GUIContent("Hostile Arrow Prefab"), false, new GUILayoutOption[0]);
            EditorGUILayout.PropertyField(arrowImage, new GUIContent("Arrow Prefab"), false, new GUILayoutOption[0]);
            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(dumplingImage, new GUIContent("Dumpling Prefab"), false, new GUILayoutOption[0]);
            EditorGUILayout.PropertyField(playerImage, new GUIContent("Player Prefab"), false, new GUILayoutOption[0]);
            EditorGUILayout.PropertyField(npcImage, new GUIContent("NPC Prefab"), false, new GUILayoutOption[0]);
            EditorGUILayout.PropertyField(radarMissilePrefab, new GUIContent("Missile Prefab"), false, new GUILayoutOption[0]);
            EditorGUILayout.PropertyField(radarAlliesPrefab, new GUIContent("Allies Prefab"), false, new GUILayoutOption[0]);
            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(trailPrefab, new GUIContent("Trail Prefab"), false, new GUILayoutOption[0]);
            EditorGUILayout.PropertyField(playerTrailPrefab, new GUIContent("Player Trial Prefab"), false, new GUILayoutOption[0]);
            EditorGUILayout.PropertyField(hostileTrailPrefab, new GUIContent("Hostile Trial Prefab"), false, new GUILayoutOption[0]);
            EditorGUILayout.PropertyField(alliesTrailPrefab, new GUIContent("Allies Trial Prefab"), false, new GUILayoutOption[0]);
            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(radar_SpawnPrefab, new GUIContent("Spawn Prefab"), false, new GUILayoutOption[0]);
            EditorGUILayout.PropertyField(healthy_sprite, new GUIContent("healthySprite"), false, new GUILayoutOption[0]);

            EditorGUILayout.PropertyField(radar_terrain, new GUIContent("terrain objects"), false, new GUILayoutOption[0]);

            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(radarRadius, new GUIContent("Radar radius"), false, new GUILayoutOption[0]);
            EditorGUILayout.PropertyField(radarHeight, new GUIContent("Radar height"), false, new GUILayoutOption[0]);
            EditorGUILayout.PropertyField(radar_followViewDir, new GUIContent("Follow ViewDir"), false, new GUILayoutOption[0]);

            EditorGUILayout.Space();
            bool islist = EditorGUILayout.PropertyField(dumpling_counter_digits, new GUIContent("Dumpling counter digits"), new GUILayoutOption[0]);
            if (islist && dumpling_counter_digits.isExpanded)
            {
                EditorGUI.indentLevel++;
                EditorGUILayout.PropertyField(dumpling_counter_digits.FindPropertyRelative("Array.size"));
                for (int j = 0; j < dumpling_counter_digits.arraySize; ++j)
                {
                    EditorGUILayout.PropertyField(dumpling_counter_digits.GetArrayElementAtIndex(j));
                }
                EditorGUI.indentLevel--;
            }
            EditorGUILayout.PropertyField(dumplingCollected, new GUIContent("Dumpling Collected"), false, new GUILayoutOption[0]);
            EditorGUILayout.PropertyField(dumplingUnloaded, new GUIContent("Dumpling Unloaded"), false, new GUILayoutOption[0]);
            EditorGUILayout.PropertyField(dumplingShield, new GUIContent("Dumpling Shield"), false, new GUILayoutOption[0]);
            EditorGUILayout.PropertyField(missileStatus, new GUIContent("Missile Status"), false, new GUILayoutOption[0]);
            EditorGUILayout.PropertyField(accelerator, new GUIContent("Accelerator Icon"), false, new GUILayoutOption[0]);
        }
        EditorGUILayout.PropertyField(rocketBtnGO, new GUIContent("rocketBtnGO"), false, new GUILayoutOption[0]);
        EditorGUILayout.PropertyField(shieldBtnGO, new GUIContent("shieldBtnGO"), false, new GUILayoutOption[0]);

        serializedObject.ApplyModifiedProperties();
    }
}