﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DizzyViewDisplayer : MonoBehaviour {

  public GameObject eyeObj = null;
  GameObject rootObj = null;

  // Use this for initialization
  void Start() {
    rootObj = gameObject.transform.Find("Root").gameObject;
  }

  // Update is called once per frame
  void Update() {
    if (eyeObj != null) {
      transform.position = eyeObj.transform.position/* + eyeObj.transform.rotation * new Vector3(0.0f, 0.0f, 0.0f)*/;
      transform.rotation = eyeObj.transform.rotation;
    }
  }

  public void showDizzyView() {
    //gameObject.transform.Find("PlayerBall_FX").GetComponent<Animator>().SetTrigger("dizzy");
    rootObj.GetComponent<BloodView>().showView();
  }
}
