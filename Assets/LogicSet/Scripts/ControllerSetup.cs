﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerSetup : MonoBehaviour {

  public GameObject[] hands_array = null;

  public GameObject[] trackerSrcObj = null;
  public GameObject[] trackerDstObj = null;

  // Use this for initialization
  void Start() {
    //setup controller
    if (ViveChecker.instance.currCM == ViveChecker.VIVE_ControllingMode.TRACKER) {
      hands_array[0].transform.localRotation = Quaternion.Euler(-20.0f, -90.0f, -90.0f);
      hands_array[0].transform.localPosition = new Vector3(-0.0356f, 0.0189f, -0.03f);

      hands_array[1].transform.localRotation = Quaternion.Euler(-20.0f, 90.0f, 90.0f);
      hands_array[1].transform.localPosition = new Vector3(0.0356f, 0.0189f, -0.03f);

    }
  }

  // Update is called once per frame
  void Update() {
    if (ViveChecker.instance.getViveMode() == false)
      return;

    //resolve wired error (the controller gameobject needs set active manually)

    if (trackerDstObj[0].active == false) {
      trackerDstObj[0].SetActive(true);
    }
    if (trackerDstObj[1].active == false) {
      trackerDstObj[1].SetActive(true);
    }


    if (Input.GetKeyUp(KeyCode.C) && ViveChecker.instance.currCM == ViveChecker.VIVE_ControllingMode.TRACKER) {
      for (int i = 0; i < 2; ++i) {
        ViveChecker.instance.trackerMapSeq[i] = (ViveChecker.instance.trackerMapSeq[i] + 1) % 2;
      }
    }
  }

  private void FixedUpdate() {
    if (ViveChecker.instance.getViveMode() == false)
      return;

    //sync tracker index
    if (ViveChecker.instance.currCM == ViveChecker.VIVE_ControllingMode.TRACKER) {
      for (int i = 0; i < 2; ++i) {
        if (trackerSrcObj[ViveChecker.instance.trackerMapSeq[i]].transform.Find("Model") != null) {
          trackerDstObj[i].GetComponent<SteamVR_TrackedObject>().SetDeviceIndex((int)trackerSrcObj[ViveChecker.instance.trackerMapSeq[i]].transform.Find("Model").GetComponent<SteamVR_RenderModel>().index);
        }
      }
    }
  }
}
