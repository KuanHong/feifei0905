﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostileDeadAnimation : MonoBehaviour
{

    Vector3 gravity = new Vector3(0.0f, -18.0f, 0.0f);
    float counter = 0.0f;
    float duration = 3.0f;


    float invisible_counter = 0.0f;
    float invisible_rate = 0.0f;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    bool curr_visible_state = true;
    void Update()
    {

        //life time
        counter += Time.deltaTime;
        if (counter >= duration)
        {
            GameObject.Destroy(gameObject);
            return;
        }

        //visibility
        invisible_rate = (float)QuartEaseIn(counter, 0.0f, 0.1f, duration);
        invisible_counter += Time.deltaTime;
        while (invisible_counter >= 0.1f)
        {
            invisible_counter -= 0.1f;
        }
        if (invisible_counter < invisible_rate)
        {
            //invisible
            if (curr_visible_state == true)
            {
                curr_visible_state = false;

                //Debug.Log("scale zero");
            }
        }
        else
        {
            //visible
            if (curr_visible_state == false)
            {
                curr_visible_state = true;

                //Debug.Log("scale one");
            }
        }

        //position
        mVel += gravity * Time.deltaTime;
        transform.position += (mVel * Time.deltaTime);


    }

    Vector3 mVel = Vector3.zero;
    public void setVelocity(Vector3 vel)
    {
        mVel = vel;
    }

    /// <summary>
    /// Easing equation function for a quartic (t^4) easing in: 
    /// accelerating from zero velocity.
    /// </summary>
    /// <param name="t">Current time in seconds.</param>
    /// <param name="b">Starting value.</param>
    /// <param name="c">Change in value.</param>
    /// <param name="d">Duration of animation.</param>
    /// <returns>The correct value.</returns>
    public static double QuartEaseIn(double t, double b, double c, double d)
    {
        return c * (t /= d) * t + b;
    }
}
