﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommitSuicide : MonoBehaviour {
  public float duration = 5.0f;
  float counter = 0.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
    counter += Time.deltaTime;
    if (counter >= duration) {
      GameObject.Destroy(gameObject);
    }
		
	}
}
