﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SightDisplayer : MonoBehaviour {

  public Texture2D sight_tex_on = null;
  public Texture2D sight_tex_off = null;

  public TextMesh missile_lock_text = null;
  public TextMesh radar_lock_text = null;

  public TextMesh my_score_text = null;
  public TextMesh tp_score_text = null;

  //public GameObject dizzyView = null;

  Renderer render = null;
  public GameObject eyeObj = null;

  // Use this for initialization
  void Start() {
    render = GetComponent<MeshRenderer>();
    setMode(Mode.NORMAL);
  }

  // Update is called once per frame
  void Update() {
    if (eyeObj != null) {
      transform.position = eyeObj.transform.position + eyeObj.transform.rotation * new Vector3(0.0f, 0.0f, 10.0f);
      transform.rotation = eyeObj.transform.rotation;
    }

    if (missile_locked) {
      flash_counter += Time.deltaTime;
      if (flash_counter >= flash_duration) {
        missile_locked_light = !missile_locked_light;
        flash_counter = 0.0f;
      }

      if (missile_locked_light) {
        Color c = missile_lock_text.color;
        c.r = 1.0f;
        c.g = 0.0f;
        c.b = 0.0f;
        missile_lock_text.color = c;
      } else {
        Color c = missile_lock_text.color;
        c.r = 1.0f;
        c.g = 1.0f;
        c.b = 1.0f;
        missile_lock_text.color = c;
      }
    }

  }

  public enum Mode {
    RED,
    GREEN,
    NORMAL
  }
  public void setMode(Mode m) {
    if (m == Mode.NORMAL) {
      //white
      render.material.SetTexture("_MainTex", sight_tex_off);
      render.material.SetColor("_TintColor", new Color(1.0f, 1.0f, 1.0f, 32.0f / 255.0f));
    } else
    if (m == Mode.RED) {
      //red
      render.material.SetTexture("_MainTex", sight_tex_on);
      render.material.SetColor("_TintColor", new Color(1.0f, 0.0f, 0.0f, 0.5f));
    } else {
      //green
      render.material.SetTexture("_MainTex", sight_tex_on);
      render.material.SetColor("_TintColor", new Color(0.0f, 1.0f, 0.0f, 0.5f));
    }


  }

  bool missile_locked = false;
  float flash_counter = 0.0f;
  float flash_duration = 0.05f;
  bool missile_locked_light = false;
  public void setMissileLock(bool locked) {
    if (missile_locked == locked)
      return;

    missile_locked = locked;
    if (locked) {
      Color c = missile_lock_text.color;
      c.r = 1.0f;
      c.g = 0.0f;
      c.b = 0.0f;
      missile_lock_text.color = c;
      missile_locked_light = true;
      flash_counter = 0.0f;

    } else {
      Color c = missile_lock_text.color;
      c.r = 1.0f;
      c.g = 1.0f;
      c.b = 1.0f;
      missile_lock_text.color = c;
    }

  }

  public void setRadarLock(bool locked) {
    if (locked) {
      Color c = radar_lock_text.color;
      c.r = 1.0f;
      c.g = 0.0f;
      c.b = 0.0f;
      radar_lock_text.color = c;
    } else {
      Color c = radar_lock_text.color;
      c.r = 1.0f;
      c.g = 1.0f;
      c.b = 1.0f;
      radar_lock_text.color = c;
    }
  }

  public void setMyScore(int loaded, int tmp_loaded) {
    my_score_text.text = "SCORE  " + loaded + "  (" + tmp_loaded + ")";
  }

  public void setHostileScore(int loaded, int tmp_loaded) {
    tp_score_text.text = "2P SCORE " + loaded + "  (" + tmp_loaded + ")";
  }

  //public void showDizzyView() {
  //    dizzyView.transform.Find("PlayerBall_FX").GetComponent<Animator>().SetTrigger("dizzy");
  //}
}
