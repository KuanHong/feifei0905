﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerModelFlash : MonoBehaviour
{
    public GameObject[] models;

    float flash_duration = 0.025f;
    float flash_counter = 0.0f;
    int current_flash = 0;

    float sustaining_duration = 2.0f;
    float sustaining_counter = 0.0f;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (sustaining_counter <= 0.0f)
            return;

        sustaining_counter -= Time.deltaTime;
        if (sustaining_counter > 0.0f)
        {
            flash_counter += Time.deltaTime;
            if (flash_counter >= flash_duration)
            {
                flash_counter = 0.0f;
                invertFlashState();
            }
        }
        else
        {
            //fallback flashing status
            for (int i = 0; i < models.Length; ++i)
            {
                models[i].GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.black);
            }

            current_flash = 0;
        }

    }

    void invertFlashState()
    {
        if (current_flash == 0)
        {
            //lighting up
            //...
            for (int i = 0; i < models.Length; ++i)
            {
                models[i].GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.red);
            }

            current_flash = 1;
        }
        else
        if (current_flash == 1)
        {
            //darken down
            //...
            for (int i = 0; i < models.Length; ++i)
            {
                models[i].GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.blue);
            }

            current_flash = 2;
        }
        else
         if (current_flash == 2)
        {
            //darken down
            //...
            for (int i = 0; i < models.Length; ++i)
            {
                models[i].GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.black);
            }

            current_flash = 0;
        }
    }

    public void flash()
    {
        sustaining_counter = sustaining_duration;
    }
}
