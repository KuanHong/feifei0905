﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabInstantiator : MonoBehaviour {

    public GameObject[] w_prefab = null;
    public GameObject[] l_prefab = null;

    public GameObject w_explosion = null;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void instantiatePrefab() {
        GameObject pp = new GameObject();
        pp.transform.position = Vector3.zero;
        pp.name = "PrefabInstantiatorObj";
        MissileBehaviours.Misc.DestroyAfterTime adt =pp.AddComponent<MissileBehaviours.Misc.DestroyAfterTime>();
        adt.time = 3.0f;

        for (int i = 0; i < w_prefab.Length; ++i) {
            GameObject tmp = GameObject.Instantiate(w_prefab[i]);
            tmp.transform.SetParent(pp.transform, false);
            tmp.transform.position = transform.position;
        }

        GameObject pp2 = new GameObject();
        pp2.name = "LPrefabInstantiatorObj";
        MissileBehaviours.Misc.DestroyAfterTime adt2= pp2.AddComponent<MissileBehaviours.Misc.DestroyAfterTime>();
        adt2.time = 3.0f;
        pp2.transform.SetParent(transform, false);
        pp2.transform.localPosition = Vector3.zero;

        for (int i = 0; i < l_prefab.Length; ++i) {
            GameObject tmp = GameObject.Instantiate(l_prefab[i]);
            tmp.transform.SetParent(pp2.transform, false);
            tmp.transform.localPosition = Vector3.zero;
        }

    }

    public void instantiateExplosion() {
        GameObject pp = new GameObject();
        pp.transform.position = Vector3.zero;
        pp.name = "PrefabInstantiatorObj";
        MissileBehaviours.Misc.DestroyAfterTime adt = pp.AddComponent<MissileBehaviours.Misc.DestroyAfterTime>();
        adt.time = 3.0f;


        GameObject tmp = GameObject.Instantiate(w_explosion);
        tmp.transform.SetParent(pp.transform, false);
        tmp.transform.position = transform.position;

    }
}
