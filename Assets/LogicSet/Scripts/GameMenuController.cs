﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMenuController : MonoBehaviour {

    public enum UIEvent {
        ENTER,
        EXIT
    }

    public enum GameType {
        UNKNOWN,
        SINGLE,
        DOUBLE
    }

    public GameObject single_btn_prefab = null;
    public GameObject double_btn_prefab = null;
    public GameObject wait_user_prefab = null;
    GameObject single_btn = null;
    GameObject double_btn = null;
    GameObject wait_user = null;

    public delegate void GameHandler(object param);

    GameType gt = GameType.UNKNOWN;
    GameType tmp_gt = GameType.UNKNOWN;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    Transform spawn_transform = null;
    bool vive_mode = false;
    public void initGameMenu(Transform t, bool viveMode) {
        spawn_transform = t;
        vive_mode = viveMode;

        //
        // SINGLE BTN
        // 
        if (single_btn == null) {
            GameObject sb = GameObject.Instantiate(single_btn_prefab);
            sb.transform.SetParent(transform, false);
            single_btn = sb;
        }else {
            single_btn.GetComponent<Animator>().Rebind();
        }

        single_btn.transform.localRotation = Quaternion.AngleAxis(180.0f, Vector3.up) * t.localRotation;

        if (viveMode == false) {
            single_btn.transform.localScale = Vector3.one * 5.0f;
            single_btn.transform.localPosition = t.position + single_btn.transform.localRotation * new Vector3(2.0f, 0.0f, -4.0f);
        } else {
            single_btn.transform.localScale = Vector3.one * 2.0f;
            single_btn.transform.localPosition = t.position + single_btn.transform.localRotation * new Vector3(0.7f, 0.7f, -0.8f);
        }

        //
        // DOUBLE BTN
        //
        if (double_btn == null) {
            GameObject db = GameObject.Instantiate(double_btn_prefab);
            db.transform.SetParent(transform, false);
            double_btn = db;
        }else {
            double_btn.GetComponent<Animator>().Rebind();
        }

        double_btn.transform.localRotation = Quaternion.AngleAxis(180.0f, Vector3.up) * t.localRotation;

        if (viveMode == false) {
            double_btn.transform.localScale = Vector3.one * 5.0f;
            double_btn.transform.localPosition = t.position + double_btn.transform.localRotation * new Vector3(-2.0f, 0.0f, -4.0f);
        } else {
            double_btn.transform.localScale = Vector3.one * 2.0f;
            double_btn.transform.localPosition = t.position + double_btn.transform.localRotation * new Vector3(-0.7f, 0.7f, -0.8f);
        }




        gt = GameType.UNKNOWN;
        tmp_gt = GameType.UNKNOWN;
        game_selected = false;
    }

    public void resetMenuStatus() {
        initGameMenu(spawn_transform, vive_mode);

        if (wait_user != null) {
            wait_user.GetComponentInChildren<Animator>().SetTrigger("EXIT");
        }
    }

    GameHandler gameHandlerFunc = null;
    public void registerGameHandler(GameHandler pfunc) {
        gameHandlerFunc = pfunc;
    }

    public void networkLinked() {
        if (wait_user != null) {
            wait_user.GetComponentInChildren<Animator>().SetTrigger("EXIT");
        }
    }

    bool game_selected = false;
    public void menuSelected(string option) {
        if (gt != GameType.UNKNOWN)
            return;

        Debug.Log("120 - menu selected " + option);

        switch (option) {
            case "SingleKeyDown": {
                if (game_selected == false) {
                    double_btn.GetComponent<Animator>().SetTrigger("leaveHover");
                    single_btn.GetComponent<Animator>().SetTrigger("enterHover");

                    tmp_gt = GameType.SINGLE;
                }
            }
            break;

            case "DoubleKeyDown": {
                if (game_selected == false) {
                    single_btn.GetComponent<Animator>().SetTrigger("leaveHover");
                    double_btn.GetComponent<Animator>().SetTrigger("enterHover");

                    tmp_gt = GameType.DOUBLE;
                }
            }
            break;

            case "SingleKeyUp": 
            case "DoubleKeyUp": {
                if (game_selected == false) {
                    single_btn.GetComponent<Animator>().SetTrigger("leaveHover");
                    double_btn.GetComponent<Animator>().SetTrigger("leaveHover");

                    tmp_gt = GameType.UNKNOWN;
                }
            }
            break;

            case "GameSelected": {
                if (tmp_gt != GameType.UNKNOWN && game_selected ==false) {
                    gt = tmp_gt;
                    tmp_gt = GameType.UNKNOWN;
                    game_selected = true;

                    single_btn.GetComponent<Animator>().SetTrigger("exit");
                    double_btn.GetComponent<Animator>().SetTrigger("exit");

                    //continue game
                    gameHandlerFunc(gt.ToString());

                    if (gt == GameType.DOUBLE) {
                        if (wait_user == null) {
                            GameObject w = GameObject.Instantiate(wait_user_prefab);
                            w.transform.SetParent(transform, false);
                            w.transform.localRotation = Quaternion.AngleAxis(180.0f, Vector3.up) * spawn_transform.localRotation;

                            if (vive_mode == false) {
                                w.transform.localScale = Vector3.one * 5.0f;
                                w.transform.localPosition = spawn_transform.position + w.transform.localRotation * new Vector3(0.0f, 0.0f, -4.0f);
                            } else {
                                w.transform.localScale = Vector3.one * 2.0f;
                                w.transform.localPosition = spawn_transform.position + w.transform.localRotation * new Vector3(0.0f, 0.7f, -0.8f);
                            }

                            wait_user = w;

                        }else {
                            //force restart animator
                            wait_user.GetComponentInChildren<Animator>().Rebind();
                        }
                    }

                    gt = GameType.UNKNOWN;
                }
            }
            break;
        }

    }

    public void setUIEvent(UIEvent e, string obj_name, Collider c) {
        if (game_selected == true)
            return;

        Debug.Log("198 - rcv ui evnet: "+obj_name);

        if (e == UIEvent.ENTER) {
            if (obj_name.Contains("Single") && tmp_gt !=GameType.SINGLE) {
                double_btn.GetComponent<Animator>().SetTrigger("leaveHover");
                single_btn.GetComponent<Animator>().SetTrigger("enterHover");

                tmp_gt = GameType.SINGLE;
            } else
            if (obj_name.Contains("Double") && tmp_gt !=GameType.DOUBLE){
                single_btn.GetComponent<Animator>().SetTrigger("leaveHover");
                double_btn.GetComponent<Animator>().SetTrigger("enterHover");

                tmp_gt = GameType.DOUBLE;
            }
        } else 
        if (e== UIEvent.EXIT && tmp_gt !=GameType.UNKNOWN){
            single_btn.GetComponent<Animator>().SetTrigger("leaveHover");
            double_btn.GetComponent<Animator>().SetTrigger("leaveHover");

            tmp_gt = GameType.UNKNOWN;
        }


    }
}
