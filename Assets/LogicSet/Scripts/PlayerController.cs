﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    List<IAnimation> mAnimationList = new List<IAnimation>();

    public CannonBall[] cannons = null;

    public GameObject pc_camera = null;
    public GameObject vive_camera = null;

    public GameObject pc_eye = null;
    public GameObject vive_camerarig = null;
    public GameObject vive_hmd = null;

    public GameObject dumplingMgrObj = null;
    DumplingManager dumplingManager = null;
    public SpeedLinePlayer speedLineMgr = null;

    GameObject vive_l_controller = null;
    GameObject vive_r_controller = null;

    public GameObject hud = null;
    GameObject sightUIObj = null;
    CircleProgressLine cpl = null;
    public bool enable_hud = true;
    public bool show_debug_ui = false;

    GameObject workingCamera = null;
    GameObject workingEye = null;
    GameObject workingGyroscope = null;
    GameObject workingCockpit = null;

    GameObject dizzyView = null;
    GameObject warningView = null;
    GameObject gametimeView = null;
    GameObject shieldtimeView = null;
    GameObject acceleratorTimeView = null;

    bool viveMode = false;
    bool trackerMode = false;

    public float playing_radius = 1000.0f;
    public float ceilingHeight = 200.0f;
    public float floorHeight = -100.0f;
    public float bufferHeight = 10.0f;

    public float launchForce = 200.0f; //
    public float maximumSpeed = 100.0f; // km/hr
    public float bufferSpeed = 10.0f;

    //decending force
    public float gravityForce = -9.81f;
    public float antiAscendingForce = -1.0f;

    public bool diveCamera = false;
    public float diveAngle = -60.0f;
    public bool easeDiveAngle = true;
    public float diveDuration = 2.5f;

    //ascending force
    public float glidingForce = 6.0f;
    public bool glidingForceWhenAscending = false;
    public float antiDescendingForce = 1.0f;
    public float flapForce = 10.0f;

    //shield
    public int shield_available_times = 0;
    int _curr_shield_available_times = 0;
    int curr_shield_available_times
    {
        get
        {
            return _curr_shield_available_times;
        }
        set
        {
            int prev_val = _curr_shield_available_times;
            _curr_shield_available_times = value;

            if (_curr_shield_available_times > 0 && prev_val <= 0)
            {
                //scale badge
                //...
                if (viveMode == true)
                {

                    shieldIconObj_vive.GetComponent<SphereCollider>().enabled = true;

                    ScaleAnimation sa = new ScaleAnimation();
                    sa.initAnimation(CurveUtil.ElasticEaseInOut, 0.0f, 1.0f, new GameObject[] { shieldIconObj_vive }, true);
                    sa.setupScaleVal(Vector3.zero, Vector3.one * 0.25f);
                    mAnimationList.Add(sa);
                }

            }
            else if (_curr_shield_available_times <= 0)
                shieldIconObj_vive.SetActive(false);
        }
    }
    public float shield_available_duration = 0.0f;
    public float shield_colddown_time = 0.0f;
    public GameObject shieldIconObj_vive = null;

    //accelerator
    public int accelerator_available_times = 0;
    int _curr_accelerator_available_times = 0;
    int curr_accelerator_available_times
    {
        get
        {
            return _curr_accelerator_available_times;
        }
        set
        {
            int prev_val = _curr_accelerator_available_times;
            _curr_accelerator_available_times = value;

            if (_curr_accelerator_available_times > 0 && prev_val <= 0)
            {
                if (viveMode == true)
                {

                    acceleratorIconObj_vive.GetComponent<SphereCollider>().enabled = true;

                    ScaleAnimation sa = new ScaleAnimation();
                    sa.initAnimation(CurveUtil.ElasticEaseInOut, 0.0f, 1.0f, new GameObject[] { acceleratorIconObj_vive }, true, delegate (object[] arr)
                    {
                        //show particle effect
                        acceleratorIconObj_vive.transform.Find("Rocket/FlyAircraft_Hi/Buff_W_AirLight_out").gameObject.SetActive(true);
                    });
                    sa.setupScaleVal(Vector3.zero, Vector3.one * 0.2f);
                    mAnimationList.Add(sa);

                    acceleratorIconObj_vive.SetActive(true);
                }
            }
            else if (_curr_accelerator_available_times <= 0)
                acceleratorIconObj_vive.SetActive(false);
        }
    }
    public float accelerator_available_duration = 0.0f;
    public float accelerator_colddown_time = 0.0f;
    public GameObject acceleratorIconObj_vive = null;

    //rotation
    public float maxRotForce = 2.0f;
    public float rotDuration = 2.5f;
    public bool rotDecending = false;
    public bool easeRotationAngle = true;
    public bool rollEffect = true;
    public bool thirdPersonRollEffect = false;

    //advanced rotation
    public bool lowerSpeed = false;
    public bool rotateHead = false;
    public float lowerSpeedRatio = 0.5f;
    public float rotateHeadRatio = 0.5f;
    float default_max_horizontal_speed = 83.0f;
    float curr_max_horizontal_speed = 83.0f; //83 m/s => 300 km/h
    public bool disableHeadRotationAtPcMode = true;

    //sound
    public GameObject EnvSoundObj = null;
    public AudioClip flapAudio1 = null;
    public AudioClip flapAudio2 = null;
    public AudioClip dumplingAttracted = null;
    public AudioClip respawn = null;
    public AudioClip underattack0 = null;
    public AudioClip underattack1 = null;
    public AudioClip shieldSound;
    public AudioClip rocketSound;

    //camera effect
    public GameObject respawnEffect = null;

    //bound wall
    public GameObject boundWall = null;


    public float missileDamage = 25;
    float _healthy_percentage = 100.0f;
    float healthy_percentage
    {
        get
        {
            return _healthy_percentage;
        }

        set
        {
            _healthy_percentage = value;

            //update display
            dumplingManager.updateHealthyPercentage(_healthy_percentage);

            //broadcast to other player
            //...

            if (_healthy_percentage <= 0.0f)
            {
                _healthy_percentage = 100.0f;

                //reset dof post process image effect
                //deactive blur effect
                if (viveMode == false)
                {
                    //workingEye.GetComponent<UnityStandardAssets.ImageEffects.BlurOptimized>().enabled = false;
                    workingEye.GetComponent<UnityEngine.PostProcessing.PostProcessingBehaviour>().profile.depthOfField.enabled = false;
                }
                else
                {
                    //vive_hmd.GetComponent<UnityStandardAssets.ImageEffects.BlurOptimized>().enabled = false;
                    vive_hmd.GetComponent<UnityEngine.PostProcessing.PostProcessingBehaviour>().profile.depthOfField.enabled = false;
                }

                //player respawn effect ?
                GameObject tmp = GameObject.Instantiate(respawnEffect);
                tmp.name = "Feather Effect";
                tmp.transform.SetParent(hud.transform, false);
                tmp.transform.localPosition = new Vector3(0.0f, 0.0f, 6.0f);

                //losing all dumplings
                List<DumplingManager.TargetInfo> ds = dumplingManager.pickAllAttachedTarget();
                Debug.Log("losing all dumplings (" + ds.Count + ")...");

                List<Vector3> dump_loss_posi_arr = new List<Vector3>();
                for (int i = 0; i < ds.Count; ++i)
                {
                    Vector3 rndDir = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
                    Vector3 oriDir = (ds[i].obj.transform.localPosition - rndDir * 150.0f) - ds[i].obj.transform.localPosition;
                    dump_loss_posi_arr.Add(ds[i].obj.transform.localPosition + oriDir);
                }

                if (mCurrMRS.isHost())
                {

                    List<object> req_arr = new List<object>();
                    req_arr.Add(mCurrMRS.myGuid);

                    for (int i = 0; i < ds.Count; ++i)
                    {
                        if (dumplingManager.targetLosing(ds[i], dump_loss_posi_arr[i]))
                        {

                            req_arr.Add(ds[i].id);

                            req_arr.Add(dump_loss_posi_arr[i].x);
                            req_arr.Add(dump_loss_posi_arr[i].y);
                            req_arr.Add(dump_loss_posi_arr[i].z);
                        }
                    }

                    //broadcast to all
                    //send message
                    dumplingEventHandler(MultiplayerHandler.GameEvent.DUMPLING_LOSING, req_arr.ToArray());

                }
                else
                {

                    List<object> req_arr = new List<object>();
                    for (int i = 0; i < ds.Count; ++i)
                    {
                        req_arr.Add(ds[i].id);

                        req_arr.Add(dump_loss_posi_arr[i].x);
                        req_arr.Add(dump_loss_posi_arr[i].y);
                        req_arr.Add(dump_loss_posi_arr[i].z);
                    }

                    //request losing dumpling		
                    sendMessageToNetwork(mCurrMRS.gameLeaderGuid, MultiplayerHandler.GameEvent.DUMPLING_LOSING_REQUEST, req_arr);
                }

                //just respawn
                Debug.Log("104- fade to black after 1.0f");
                SteamVR_Fade.Start(Color.black, 1.0f, true);
                current_status = Status.BACK_TO_SPAWN_FADE_OUT;

            }
        }
    }

    //ui
    TextMesh ui_h_velocity = null;
    TextMesh ui_velocity = null;
    TextMesh ui_acceleration = null;
    TextMesh ui_position = null;
    TextMesh ui_dive_angle = null;
    TextMesh ui_rotinfo = null;
    TextMesh ui_control_info = null;
    TextMesh ui_control_mode = null;
    TextMesh ui_control_flap_l = null;
    TextMesh ui_control_flap_r = null;
    TextMesh ui_dot_val = null;
    TextMesh ui_turn_angle = null;
    TextMesh ui_vvel_l = null;
    TextMesh ui_vvel_r = null;

    LinePloter ui_h_vel_ploter = null;
    LinePloter ui_vel_ploter = null;
    LinePloter ui_acc_ploter = null;
    LinePloter ui_height_ploter = null;
    LinePloter ui_dive_angle_ploter = null;
    LinePloter ui_rot_force = null;
    LinePloter ui_l_controller_ploter = null;
    LinePloter ui_r_controller_ploter = null;
    LinePloter ui_l_controller_flap_ploter = null;
    LinePloter ui_r_controller_flap_ploter = null;
    LinePloter ui_dot_val_ploter = null;
    LinePloter ui_turn_angle_ploter = null;
    LinePloter ui_vvel_l_ploter = null;
    LinePloter ui_vvel_r_ploter = null;

    AudioSource asund = null;
    bool inited = false;

    //used for multi-player
    bool multi_player_mode = false;
    BeaconAPLayer.MatchmakingRoomStatus mCurrMRS = null;
    List<string> sceneReadyRemoteGUID = new List<string>();

    float tmp_processing_counter = 0.0f;
    float tmp_processing_duration = 0.0f;
    Vector3 tmp_processing_collision_from_position = Vector3.zero;
    Vector3 tmp_processing_collision_to_position = Vector3.zero;


    enum ViveStatus
    {
        LANDING,
        IDLE,
        GLIDING_OR_TURNING,
        FLAPING,
        DIVE,
        //SHIELD,
        CONTROLLER_MISSING,
        UNKNOWN,
        SZ
    }

    public enum Status
    {
        PRE_LAUNCH,

        LANDING_IDLE,
        FLYING,
        PROCESSING_ON_COLLISION,
        BACK_TO_SPAWN_FADE_OUT,   //遊戲中死亡
        BACK_TO_SPAWN_FADE_OUT2,
        VIVE_FADE_OUT,

        END_FADE_OUT, //遊戲結束
        END,
        ENDING,

        //MULTIPLAYER FLOW
        WAIT_REMOTE_SCENE, //HOST
        REPORT_SCENE_LOADED, //REMOTE
        SYNC_SCENE_HOST,

        WAIT_SHOW_TIMEUP,
        WAIT_SHOW_GAMERESULT,

        WAIT_GAME_END,

        SZ
    }
    public Status current_status = Status.PRE_LAUNCH;

    class PhysicsStatus
    {
        public Vector3 position = Vector3.zero;
        public Vector3 velocity = Vector3.zero;
        public Vector3 acceleration = Vector3.zero;
        public Quaternion angular_velocity = Quaternion.identity;
        public Quaternion rotation = Quaternion.identity;

        public void duplicate(PhysicsStatus ps)
        {
            position = ps.position;
            velocity = ps.velocity;
            acceleration = ps.acceleration;
            angular_velocity = ps.angular_velocity;
            rotation = ps.rotation;
        }
    }
    class PlayerInfo
    {
        public PhysicsStatus prev_status = null;
        public PhysicsStatus curr_status = null;

        //impulse force
        public Vector3 impulseForce = Vector3.zero;

        //curved force
        public Vector3 curvedForce = Vector3.zero;
        public float curvedForceCounter = 0.0f;
        public float curvedForceDuration = 0.5f;
        public delegate double CurveFunc(double t, double b, double c, double d);
        public CurveFunc curveFunction = null;

        //DIVE
        public float dive_target_forward = -60.0f;
        public float dive_start_forward = 0;
        public float dive_start_tangent = 0;
        public float dive_curr_forward = 0;
        public float dive_counter = 0.0f;
        public bool diveActing = false;

        public float dive_curr_velocity = 0;

        //ROTATION
        public bool rotateActing = false;
        public float rotationForce_Duration = 2.5f;
        public float rotationForce_Counter = 0.0f;
        public float rotationForce_Start = 0.0f;
        public float rotationForce_Start_Tangent = 0.0f;
        public float rotationForce_Current = 0.0f;
        public float rotationForce_Target = 0.0f;

        public float rotationForce_Velocity = 0.0f;
        public Vector3 rotForce = Vector3.zero;

        //vive
        public ViveStatus viveStatus = ViveStatus.IDLE;

        public float[] flapVelocity = new float[2];            //0:left, 1:right
        public float[] accumulatedFlapVelocity = new float[2]; //0:left, 1:right
        public float turningFactor = 0.0f;
        public float[] controller_hmd_dist = new float[2];         //dist between controller and hmd
        public float controller_dist = 0.0f;


        public bool handsBehnadPlane = false;

        public float p_shield_available_time_counter = 0.0f;
        public float p_shield_cold_time_counter = 0.0f;
        public bool shield_enabled = false;

        public float p_accelerator_available_time_counter = 0.0f;
        public float p_accelerator_cold_time_counter = 0.0f;
        public bool accelerator_enabled = false;

        // Update is called once per frame
        public float missile_cold_down_counter = 0.0f;

    }
    PlayerInfo pi = null;

    public delegate void MenuEvent(object extra_param);
    // MenuEvent registeredMenuEventHandler = null;

    public delegate void PlayerEvent1(MultiplayerHandler.GameEvent e, List<object> extra_param, bool reliable);
    PlayerEvent1 registeredPlayerEvenet1Handler = null; //redirect to GameController::player_event_handler()

    public delegate void PlayerEvent2(string dest_guid, MultiplayerHandler.GameEvent e, List<object> extra_param, bool reliable);
    PlayerEvent2 registeredPlayerEvent2Handler = null; //send to specific player

    LayerMask TerrainLM;
    LayerMask GatewayLM;

    Dictionary<int, GameObject> mDicUnloadPointGameObject = new Dictionary<int, GameObject>();
    public void addUnloadPt(int idx, GameObject obj)
    {
        Debug.Log("320 - add unload pt (idx=" + idx + ")");
        mDicUnloadPointGameObject.Add(idx, obj);
    }

    public void genSpawnPt()
    {
        int gid = (int)mCurrMRS.getMyPlayerInfo().groupid;
        dumplingManager.genRadarSpawnPt(
          mDicUnloadPointGameObject[gid].transform.position,
          mDicUnloadPointGameObject[(gid + 1) % 2].transform.position);
    }

    // Use this for initialization
    void Start()
    {
        countDownRespawnTarget = respawnTargetTime;

        TerrainLM = 1 << LayerMask.NameToLayer("Terrain");
        GatewayLM = 1 << LayerMask.NameToLayer("Gateway");

        asund = GetComponent<AudioSource>();
        dumplingManager = dumplingMgrObj.GetComponent<DumplingManager>();
        dumplingManager.registerDumplingEventHandler(dumplingEventHandler);

        //
        //
        //
        dizzyView = hud.transform.Find("DizzyView").gameObject;
        warningView = hud.transform.Find("Warning").gameObject;
        gametimeView = hud.transform.Find("GameTime").gameObject;
        shieldtimeView = hud.transform.Find("ShieldTime").gameObject;
        acceleratorTimeView = hud.transform.Find("AcceleratorTime").gameObject;

        //
        //TextMesh
        //
        ui_h_velocity = hud.transform.Find("Debug/hVelocity").GetComponent<TextMesh>();
        ui_velocity = hud.transform.Find("Debug/Velocity").GetComponent<TextMesh>();
        ui_acceleration = hud.transform.Find("Debug/Acceleration").GetComponent<TextMesh>();
        ui_position = hud.transform.Find("Debug/Position").GetComponent<TextMesh>();
        ui_dive_angle = hud.transform.Find("Debug/DiveAngle").GetComponent<TextMesh>();
        ui_rotinfo = hud.transform.Find("Debug/RotInfo").GetComponent<TextMesh>();
        ui_control_info = hud.transform.Find("Debug/ControllerDist").GetComponent<TextMesh>();
        ui_control_mode = hud.transform.Find("Debug/ControllerMode").GetComponent<TextMesh>();
        ui_control_flap_l = hud.transform.Find("Debug/FlapInfo_L").GetComponent<TextMesh>();
        ui_control_flap_r = hud.transform.Find("Debug/FlapInfo_R").GetComponent<TextMesh>();
        ui_turn_angle = hud.transform.Find("Debug/TurnAngle").GetComponent<TextMesh>();

        ui_vvel_l = hud.transform.Find("Debug/VerticalVelL").GetComponent<TextMesh>();
        ui_vvel_r = hud.transform.Find("Debug/VerticalVelR").GetComponent<TextMesh>();


        //
        //LinePloter
        //
        ui_h_vel_ploter = hud.transform.Find("Debug/hVelocityPlot").GetComponent<LinePloter>();
        ui_h_vel_ploter.initPlot(100, 5.0f, LinePloter.HeightType.ADAPTIVE);

        ui_vel_ploter = hud.transform.Find("Debug/VelocityPlot").GetComponent<LinePloter>();
        ui_vel_ploter.initPlot(100, 5.0f);

        ui_acc_ploter = hud.transform.Find("Debug/AccelerationPlot").GetComponent<LinePloter>();
        ui_acc_ploter.initPlot(100, 5.0f);

        ui_height_ploter = hud.transform.Find("Debug/PositionPlot").GetComponent<LinePloter>();
        ui_height_ploter.initPlot(100, 5.0f, LinePloter.HeightType.ADAPTIVE);

        ui_dive_angle_ploter = hud.transform.Find("Debug/DiveAnglePlot").GetComponent<LinePloter>();
        ui_dive_angle_ploter.initPlot(100, 5.0f);
        ui_dive_angle_ploter.setFixedHeight(diveAngle, 0);

        ui_rot_force = hud.transform.Find("Debug/RotForcePlot").GetComponent<LinePloter>();
        ui_rot_force.initPlot(100, 5.0f);
        ui_rot_force.setFixedHeight(-maxRotForce, maxRotForce);

        ui_l_controller_ploter = hud.transform.Find("Debug/ControllerDist_L").GetComponent<LinePloter>();
        ui_r_controller_ploter = hud.transform.Find("Debug/ControllerDist_R").GetComponent<LinePloter>();
        ui_l_controller_ploter.initPlot(100, 5.0f);
        ui_r_controller_ploter.initPlot(100, 5.0f);

        ui_l_controller_flap_ploter = hud.transform.Find("Debug/ControllerFlap_L").GetComponent<LinePloter>();
        ui_r_controller_flap_ploter = hud.transform.Find("Debug/ControllerFlap_R").GetComponent<LinePloter>();
        ui_l_controller_flap_ploter.initPlot(100, 5.0f);
        ui_r_controller_flap_ploter.initPlot(100, 5.0f);
        ui_l_controller_flap_ploter.setFixedHeight(0, 200);
        ui_r_controller_flap_ploter.setFixedHeight(0, 200);

        ui_turn_angle_ploter = hud.transform.Find("Debug/TurnAnglePlot").GetComponent<LinePloter>();
        ui_turn_angle_ploter.initPlot(100, 5.0f);
        ui_turn_angle_ploter.setFixedHeight(-90.0f, 90.0f);

        ui_vvel_l_ploter = hud.transform.Find("Debug/VerticalVelLPlot").GetComponent<LinePloter>();
        ui_vvel_l_ploter.initPlot(100, 5.0f);
        ui_vvel_r_ploter = hud.transform.Find("Debug/VerticalVelRPlot").GetComponent<LinePloter>();
        ui_vvel_r_ploter.initPlot(100, 5.0f);

        sightUIObj = hud.transform.Find("Sight").gameObject;
        cpl = sightUIObj.transform.Find("CircleProgressLine").GetComponent<CircleProgressLine>();

        if (show_debug_ui == true)
        {
            hud.transform.Find("Debug").gameObject.SetActive(true);
        }
        else
        {
            hud.transform.Find("Debug").gameObject.SetActive(false);
        }

        if (enable_hud == false)
        {
            hud.SetActive(false);
        }

        ////my score statistics
        //ScoreStatistics myss = new ScoreStatistics();
        //myss.group_id = 0;
        //tmpScoreStatistics.Add(myss.group_id, myss);

        ////hostile score statstics
        //ScoreStatistics hoss = new ScoreStatistics();
        //hoss.group_id = 1;
        //tmpScoreStatistics.Add(hoss.group_id, hoss);
    }

#if UNITY_EDITOR
    void OnDrawGizmosSelected()
    {
        UnityEditor.Handles.DrawWireDisc(transform.position, Vector3.up, playing_radius);
    }
#endif

    void sendUserRespawnInfo()
    {
        //
        //CAMERA POSITION (player position info.)
        sendMessageToNetwork(MultiplayerHandler.GameEvent.PLAYER_MOVEMENT_INFO, new List<object>() {
                    (int)MultiplayerHandler.GameSubEvent.LF_POSITION,
                    mMySpawnPosition.x, mMySpawnPosition.y, mMySpawnPosition.z,
                    mMySpawnRotation.x, mMySpawnRotation.y, mMySpawnRotation.z, mMySpawnRotation.w,
                    0.0f, 0.0f, 0.0f,
                });

        //
        //HMD POSITION (VIVE MDOE)
        if (viveMode)
        {
            sendMessageToNetwork(MultiplayerHandler.GameEvent.PLAYER_MOVEMENT_INFO, new List<object>() {
                        (int)MultiplayerHandler.GameSubEvent.HMD_POSITION,
                        mMySpawnPosition.x, mMySpawnPosition.y, mMySpawnPosition.z
                    });
        }
    }

    void sendMissileInfo()
    {
        DumplingManager.TrackedMissileInfo[] marr = dumplingManager.getTrackedMissile();
        for (int i = 0; i < marr.Length; ++i)
        {
            if (marr[i].obj != null)
            {
                marr[i].delay_counter += Time.deltaTime;
                if (marr[i].delay_counter >= marr[i].delay_duration)
                {
                    marr[i].delay_counter = 0.0f;
                    int nidx = marr[i].idx;

                    sendMessageToNetwork(MultiplayerHandler.GameEvent.MISSILE_MOVEMENT_INFO, new List<object>() {
            nidx,
            marr[i].obj.transform.position.x, marr[i].obj.transform.position.y, marr[i].obj.transform.position.z,
            marr[i].obj.transform.rotation.x, marr[i].obj.transform.rotation.y, marr[i].obj.transform.rotation.z, marr[i].obj.transform.rotation.w,
            marr[i].obj.GetComponent<Rigidbody>().velocity.x, marr[i].obj.GetComponent<Rigidbody>().velocity.y, marr[i].obj.GetComponent<Rigidbody>().velocity.z
          }, false);
                }
            }

            if (marr[i].target != null && marr[i].target.name.Contains("Hostile"))
            {
                //send_missile_lock_on = true;
                sendMessageToNetwork(marr[i].target.player_guid, MultiplayerHandler.GameEvent.MISSILE_LOCKED_BY_HOSTILE, null, false);
            }
        }
    }

    void sendUserMovementInfo(bool zero_velocity = false)
    {
        //
        //CAMERA POSITION (player position info.)
        Vector3 velocity_info = Vector3.zero;
        if (zero_velocity == false)
        {
            velocity_info = pi.curr_status.velocity;
        }
        sendMessageToNetwork(MultiplayerHandler.GameEvent.PLAYER_MOVEMENT_INFO, new List<object>() {
                    (int)MultiplayerHandler.GameSubEvent.LF_POSITION,
                    pi.curr_status.position.x, pi.curr_status.position.y, pi.curr_status.position.z,
                    workingCamera.transform.localRotation.x, workingCamera.transform.localRotation.y, workingCamera.transform.localRotation.z, workingCamera.transform.localRotation.w,
                    velocity_info.x, velocity_info.y, velocity_info.z
                }, false);

        //
        //HMD POSITION (VIVE MDOE)
        if (viveMode)
        {
            sendMessageToNetwork(MultiplayerHandler.GameEvent.PLAYER_MOVEMENT_INFO, new List<object>() {
                        (int)MultiplayerHandler.GameSubEvent.HMD_POSITION,
                        vive_hmd.transform.position.x, vive_hmd.transform.position.y, vive_hmd.transform.position.z
                    }, false);
        }
    }

    int fixedUpdateCounter = 0;
    void FixedUpdate()
    {
        if (viveMode == true && pi != null)
        {
            analyseViveController(pi);
        }

        //SEND PLAYER CAMERA INFO.
        if (multi_player_mode == true)
        {
            fixedUpdateCounter++;
            if (fixedUpdateCounter >= 5)
            { //每五次 update 執行一次
                fixedUpdateCounter = 0;

                if (registeredPlayerEvenet1Handler != null)
                {

                    if (current_status == Status.FLYING || current_status == Status.PROCESSING_ON_COLLISION)
                    {
                        sendUserMovementInfo(current_status == Status.PROCESSING_ON_COLLISION);
                    }
                }

                //
                //MISSILE POSITION
                sendMissileInfo();
            }

        }

        if (mCurrMRS != null && mCurrMRS.isHost())
        {
            countDownRespawnTarget -= Time.deltaTime;
            if (countDownRespawnTarget <= 0.0f)
            {
                countDownRespawnTarget = respawnTargetTime;

                if (pendingRespawnTargetList.Count > 0)
                {
                    int dump_idx = pendingRespawnTargetList[0];
                    pendingRespawnTargetList.RemoveAt(0);

                    //respawn dumpling and broadcast message
                    if (dumplingManager.getTargetInfo(dump_idx).curr_status == DumplingManager.TargetInfo.Status.DISPOSED)
                    {
                        Debug.Log("595 - reset dumpling idx : " + dump_idx);
                        Vector3 posi = dumplingManager.resetDumpling(dump_idx);

                        //broadcast message
                        //...
                        sendMessageToNetwork(MultiplayerHandler.GameEvent.DUMPLING_RESET, new List<object>() {
              dump_idx,
              posi.x, posi.y, posi.z
            });
                    }

                }

                if (pendingRespawnShieldTargetList.Count > 0)
                {
                    int dump_idx = pendingRespawnShieldTargetList[0];
                    pendingRespawnShieldTargetList.RemoveAt(0);

                    if (dumplingManager.getTargetInfo(dump_idx).curr_status == DumplingManager.TargetInfo.Status.DISPOSED)
                    {
                        Debug.Log("612 - reset shield dumpling idx : " + dump_idx);
                        Vector3 posi = dumplingManager.resetShieldTarget(dump_idx);

                        sendMessageToNetwork(MultiplayerHandler.GameEvent.SHIELDTARGET_RESET, new List<object>() {
              dump_idx,
              posi.x, posi.y, posi.z
            });
                    }
                }

                if (pendingRespawnAcceleratorTargetList.Count > 0)
                {
                    int dump_idx = pendingRespawnAcceleratorTargetList[0];
                    pendingRespawnAcceleratorTargetList.RemoveAt(0);

                    if (dumplingManager.getTargetInfo(dump_idx).curr_status == DumplingManager.TargetInfo.Status.DISPOSED)
                    {
                        Debug.Log("612 - reset accelerator dumpling idx : " + dump_idx);
                        Vector3 posi = dumplingManager.resetAcceleratorTarget(dump_idx);

                        sendMessageToNetwork(MultiplayerHandler.GameEvent.ACCELERATORTARGET_REST, new List<object>() {
              dump_idx,
              posi.x, posi.y, posi.z
            });
                    }
                }
            }

        }

    }

    void resetFlyingStatus(Vector3 velocity)
    {
        //RESET STATUS AND SET NEW VELOCITY
        pi.prev_status.acceleration = Vector3.zero;
        pi.prev_status.velocity = Vector3.zero;
        pi.prev_status.angular_velocity = Quaternion.identity;

        pi.impulseForce = Vector3.zero;
        pi.rotForce = Vector3.zero;

        //DIVE
        pi.dive_target_forward = -60.0f;
        pi.dive_start_forward = 0;
        pi.dive_start_tangent = 0;
        pi.dive_curr_forward = 0;
        pi.dive_counter = 0.0f;
        pi.diveActing = false;

        pi.dive_curr_velocity = 0;

        //ROTATION
        pi.rotateActing = false;
        pi.rotationForce_Duration = 2.5f;
        pi.rotationForce_Counter = 0.0f;
        pi.rotationForce_Start = 0.0f;
        pi.rotationForce_Start_Tangent = 0.0f;
        pi.rotationForce_Current = 0.0f;
        pi.rotationForce_Target = 0.0f;

        pi.rotationForce_Velocity = 0.0f;
        pi.rotForce = Vector3.zero;

        //clear dive effect
        workingEye.GetComponent<Klak.Motion.BrownianMotion>().rotationAmplitude = 0.0f;

        pi.curr_status.velocity = velocity;
        Vector3 f = calculateForwardDirection(pi); //velocity dir + dive angle 
        if (f != Vector3.zero)
            workingCamera.transform.forward = f;
        workingCamera.transform.localRotation = calculateRotation(pi); //camera orientation depends on its velocity (row angle) 

        //update setup
        workingGyroscope.GetComponent<Gyroscope>().enable = thirdPersonRollEffect;
        workingGyroscope.GetComponent<Gyroscope>().rotateHead = rotateHead;
        if (viveMode == false && disableHeadRotationAtPcMode == true)
        {
            workingGyroscope.GetComponent<Gyroscope>().rotateHead = false;
        }
        workingGyroscope.GetComponent<Gyroscope>().rotateHeadRatio = 1.0f - rotateHeadRatio;

        //update status
        workingGyroscope.GetComponent<Gyroscope>().updateForwardingDir(workingCamera.transform.forward, 1.0f / workingGyroscope.GetComponent<Gyroscope>().rotateHeadRatio);
    }

    public bool gateway_function = false;
    bool checkGateway(GameObject hmd, Vector3 velocity, out Vector3 new_pos, out Vector3 new_dir)
    {
        if (gateway_function == false)
        {
            new_pos = Vector3.zero;
            new_dir = Vector3.zero;
            return false;
        }

        new_pos = Vector3.zero;
        new_dir = Vector3.forward;

        //ray-cast to gateway
        Collider[] carr = Physics.OverlapSphere(hmd.transform.position, 600.0f, GatewayLM, QueryTriggerInteraction.Ignore);
        for (int i = 0; i < carr.Length; ++i)
        {
            //found gateway
            if (carr[i].gameObject.transform.parent.parent.GetComponent<TransGateway>().isEnabled() == false)
            {

                if (mCurrMRS.isHost())
                {
                    if (carr[i].gameObject.transform.parent.parent.GetComponent<TransGateway>().setOpen((int)mCurrMRS.getMyPlayerInfo().groupid))
                    {

                        //broadcast msg for door open
                        sendMessageToNetwork(MultiplayerHandler.GameEvent.GATEWAY_OPENED,
                          new List<object>() {
                  carr[i].gameObject.transform.parent.parent.gameObject.name
                          });
                    }

                }
                else
                {
                    //request door open
                    sendMessageToNetwork(mCurrMRS.gameLeaderGuid, MultiplayerHandler.GameEvent.REQUEST_GATEWAY_OPEN,
                      new List<object>() {
                carr[i].gameObject.transform.parent.parent.gameObject.name
                      });

                }


                return false;
            }
            else
            {
                if (Vector3.Distance(hmd.transform.position, carr[i].gameObject.transform.position) <= 60.0f)
                {
                    new_pos = carr[i].gameObject.transform.parent.parent.GetComponent<TransGateway>().exitance.transform.position;// + rp;
                    new_dir = carr[i].gameObject.transform.parent.parent.GetComponent<TransGateway>().exitance.transform.forward;

                    if (mCurrMRS.isHost())
                    {
                        //準備下次生成位置
                        Vector3 np = carr[i].gameObject.transform.parent.parent.GetComponent<GatewayPlacement>().prepareNextGateway();
                        if (carr[i].gameObject.transform.parent.parent.GetComponent<TransGateway>().setClose(np))
                        {

                            //broadcast msg for door closing
                            sendMessageToNetwork(MultiplayerHandler.GameEvent.GATEWAY_CLOSED,
                              new List<object>() {
                np.x, np.y, np.z,
                carr[i].gameObject.transform.parent.parent.gameObject.name
                              });
                        }

                    }
                    else
                    {
                        //request for door closing
                        sendMessageToNetwork(mCurrMRS.gameLeaderGuid, MultiplayerHandler.GameEvent.REQUEST_GATEWAY_CLOSE,
                          new List<object>() {
                carr[i].gameObject.transform.parent.parent.gameObject.name
                          });

                    }

                    return true;
                }
            }
        }

        return false;
    }

    class AutoTrigger
    {
        public float duration = 0.0f;
        public DumplingManager.TargetInfo focusing_ti = null;
        public float count_down_time = 0.0f; //trigger when amount value below zero

        public AutoTrigger(DumplingManager.TargetInfo focus_ti)
        {
            duration = 0.5f;

            focusing_ti = focus_ti;
            count_down_time = duration;
        }
    }
    AutoTrigger curr_auto_trigger = null;

    float timeup_counter = 0.0f;
    float fade_out_counter = 0.0f;
    float report_counter = 0.0f;
    Vector3 tmp_acc = Vector3.zero;
    void Update()
    {
        if (inited == false)
            return;

        //resolve wired error (the controller gameobject needs set active manually)
        if (viveMode == true)
        {
            if (vive_l_controller.active == false)
            {
                vive_l_controller.SetActive(true);
            }
            if (vive_r_controller.active == false)
            {
                vive_r_controller.SetActive(true);
            }
        }

        //animation
        for (int i = 0; i < mAnimationList.Count;/*++i*/)
        {
            mAnimationList[i].updateAnimation(Time.deltaTime);

            if (mAnimationList[i].isAnimationDone() && mAnimationList[i].isAutoDismiss())
            {
                mAnimationList.RemoveAt(i);
            }
            else
            {
                ++i;
            }
        }

        //counters
        pi.p_accelerator_available_time_counter -= Time.deltaTime;
        pi.p_accelerator_cold_time_counter -= Time.deltaTime;
        pi.p_shield_available_time_counter -= Time.deltaTime;
        pi.p_shield_cold_time_counter -= Time.deltaTime;
        pi.missile_cold_down_counter -= Time.deltaTime;
        audioCounter += Time.deltaTime;

        //shield counter
        if (pi.shield_enabled == true && pi.p_shield_available_time_counter <= 0.0f)
        {
            disableShield();
            sendMessageToNetwork(MultiplayerHandler.GameEvent.PLAYER_SHIELD_DISABLED, null);
        }

        //accelerator counter
        if (pi.accelerator_enabled == true && pi.p_accelerator_available_time_counter <= 0.0f)
        {
            disable_accelerator();
            sendMessageToNetwork(MultiplayerHandler.GameEvent.PLAYER_ACCELERATING_DISABLED, null);
        }

        if (current_status == Status.PRE_LAUNCH)
        {

        }
        else
        if (current_status == Status.WAIT_REMOTE_SCENE)
        {
            //HOST (WAIT REMOTE SCENE)

            if (sceneReadyRemoteGUID.Count >= mCurrMRS.groupChoosenPlayerList.Count - 1)
            {
                //notify game start
                //publish game start
                Debug.Log("554 - all clients (" + sceneReadyRemoteGUID.Count + ") report ready");
                current_status = Status.SYNC_SCENE_HOST;

            }

        }
        else
        if (current_status == Status.REPORT_SCENE_LOADED)
        {
            //NOT-HOST (REPORT SCENE LOADED DONE TO HOST)

            report_counter += Time.deltaTime;
            if (report_counter >= 3.0f)
            {
                report_counter = 0.0f;

                //send report
                Debug.Log("777 - send scene ready signal...");
                sendMessageToNetwork(mCurrMRS.gameLeaderGuid, MultiplayerHandler.GameEvent.SCENE_READY, null);
            }

        }
        else
        if (current_status == Status.SYNC_SCENE_HOST)
        {
            //wait the handle been registered
            if (registeredPlayerEvenet1Handler == null)
                return;

            //START GAME (NETWORK)
            gametimeView.GetComponent<GameTimeViewDisplayer>().showGameTime(true);
            labelUIController.instance.StartShowGoObj();
            gametimeView.GetComponent<GameTimeViewDisplayer>().resetTimer();
            labelUIController.instance.UnActiveUIObjs();
            shieldtimeView.GetComponent<ShieldDurationDisplayer>().resetTimer();
            acceleratorTimeView.GetComponent<ShieldDurationDisplayer>().resetTimer();

            //broadcast scene status
            //dumpling
            Debug.Log("582 - sync dumpling position...");
            Vector3[] dump_posi_arr = dumplingManager.getSceneDumplingPosition();
            int startVal = 0;

            while (true)
            {
                List<object> dump_posi_list = new List<object>();
                for (int i = startVal; i < startVal + 25; ++i)
                { //一次全送超過 UNITY PACKAGE 大小限制
                    if (i >= dump_posi_arr.Length)
                    {
                        break;
                    }

                    dump_posi_list.Add(dump_posi_arr[i].x);
                    dump_posi_list.Add(dump_posi_arr[i].y);
                    dump_posi_list.Add(dump_posi_arr[i].z);


                }
                Debug.Log("596 - dump_posi_list.Count=" + dump_posi_list.Count + ", startVal=" + startVal);
                sendMessageToNetwork(MultiplayerHandler.GameEvent.START_GAME_DUMPLING_SPAWN, new List<object>() { dump_posi_list, startVal });
                startVal += 25;
                if (startVal >= dump_posi_arr.Length)
                {
                    break;
                }
            }

            //shield
            Debug.Log("874 - sync shield position...");
            Vector3[] shield_posi_arr = dumplingManager.getSceneShieldPosition();
            startVal = 0;

            while (true)
            {
                List<object> shield_posi_list = new List<object>();
                for (int i = startVal; i < startVal + 25; ++i)
                {
                    if (i >= shield_posi_arr.Length)
                    {
                        break;
                    }

                    shield_posi_list.Add(shield_posi_arr[i].x);
                    shield_posi_list.Add(shield_posi_arr[i].y);
                    shield_posi_list.Add(shield_posi_arr[i].z);


                }
                Debug.Log("596 - shield_posi_list.Count=" + shield_posi_list.Count + ", startVal=" + startVal);
                sendMessageToNetwork(MultiplayerHandler.GameEvent.START_GAME_SHIELD_SPAWN, new List<object>() { shield_posi_list, dump_posi_arr.Length });
                startVal += 25;
                if (startVal >= shield_posi_arr.Length)
                {
                    break;
                }
            }


            //accelerator
            //...
            Debug.Log("874 - sync accelerator position...");
            Vector3[] accelerator_posi_arr = dumplingManager.getSceneAcceleratorPosition();
            startVal = 0;

            while (true)
            {
                List<object> acc_posi_list = new List<object>();
                for (int i = startVal; i < startVal + 25; ++i)
                {
                    if (i >= accelerator_posi_arr.Length)
                    {
                        break;
                    }

                    acc_posi_list.Add(accelerator_posi_arr[i].x);
                    acc_posi_list.Add(accelerator_posi_arr[i].y);
                    acc_posi_list.Add(accelerator_posi_arr[i].z);


                }
                Debug.Log("596 - acc_posi_list.Count=" + acc_posi_list.Count + ", startVal=" + startVal);
                sendMessageToNetwork(MultiplayerHandler.GameEvent.START_GAME_ACCELERATOR_SPAWN, new List<object>() { acc_posi_list, dump_posi_arr.Length + shield_posi_arr.Length });
                startVal += 25;
                if (startVal >= accelerator_posi_arr.Length)
                {
                    break;
                }
            }

            //gateway
            Debug.Log("1014 - sync gateway position...");
            Vector3 pos0 = transform.parent.Find("TransGateway/Wormhole1/Sphere").transform.localPosition;
            Vector3 pos1 = transform.parent.Find("TransGateway/Wormhole2/Sphere").transform.localPosition;

            sendMessageToNetwork(MultiplayerHandler.GameEvent.START_GAME_GATEWAY_SPAWN, new List<object>() { pos0.x, pos0.y, pos0.z, pos1.x, pos1.y, pos1.z });

            //start multiplayer game
            sendMessageToNetwork(MultiplayerHandler.GameEvent.START_GAME, null);
            sendUserRespawnInfo();

            Debug.Log("589 - sync done. start game (host)");
            EnvSoundObj.GetComponent<EnvironmentSound>().setGamePlaying(true);

            curr_shield_available_times = shield_available_times;
            curr_accelerator_available_times = accelerator_available_times;
            updateDisplayedScore();

            current_status = Status.LANDING_IDLE;

        }
        else
        if (current_status == Status.LANDING_IDLE)
        {
            //press trigger to launch
            if (input_launchTriggered())
            {
                //give an impulse force on the payload
                pi.impulseForce = pi.curr_status.rotation * ((Vector3.up + Vector3.forward).normalized * launchForce);
                current_status = Status.FLYING;
            }

        }
        else
        if (current_status == Status.VIVE_FADE_OUT)
        {
            fade_out_counter += Time.deltaTime;
            if (fade_out_counter >= 1.0f)
            {
                fade_out_counter = 0.0f;
                Debug.Log("518- fade to clear after 1.0f");
                SteamVR_Fade.Start(Color.clear, 1.0f, true);
                EnvSoundObj.GetComponent<EnvironmentSound>().setGamePlaying(true);

                current_status = Status.LANDING_IDLE;
            }

        }
        else
        if (current_status == Status.BACK_TO_SPAWN_FADE_OUT)
        {
            if (multi_player_mode == true)
            {
                if (registeredPlayerEvenet1Handler != null)
                {
                    Debug.Log("109 - send player dead animation...");
                    sendMessageToNetwork(MultiplayerHandler.GameEvent.PLAYER_DEAD, null);
                    sendUserRespawnInfo();
                }
            }
            current_status = Status.BACK_TO_SPAWN_FADE_OUT2;
        }
        else
        if (current_status == Status.BACK_TO_SPAWN_FADE_OUT2)
        {
            fade_out_counter += Time.deltaTime;
            if (fade_out_counter >= 1.0f)
            {
                fade_out_counter = 0.0f;
                backToSpawnStatus();
            }
        }
        else
        if (current_status == Status.PROCESSING_ON_COLLISION)
        {
            tmp_processing_counter += Time.deltaTime;
            if (tmp_processing_counter > tmp_processing_duration)
            {
                //proceeding flying status
                //...
                tmp_reactForce = Vector3.zero;
                current_status = Status.FLYING;

                //deactive blur effect
                if (viveMode == false)
                {
                    //workingEye.GetComponent<UnityStandardAssets.ImageEffects.BlurOptimized>().enabled = false;
                    workingEye.GetComponent<UnityEngine.PostProcessing.PostProcessingBehaviour>().profile.depthOfField.enabled = false;
                }
                else
                {
                    //vive_hmd.GetComponent<UnityStandardAssets.ImageEffects.BlurOptimized>().enabled = false;
                    vive_hmd.GetComponent<UnityEngine.PostProcessing.PostProcessingBehaviour>().profile.depthOfField.enabled = false;
                }

                return;
            }

            float scale = (float)CurveUtil.SineEaseInOut(tmp_processing_counter / tmp_processing_duration, 0.0f, 1.0f, 1.0f);
            Vector3 curr_posi = Vector3.Slerp(tmp_processing_collision_from_position, tmp_processing_collision_to_position, scale);

            resetFlyingStatus(curr_posi);

            float corrected_scale = 0.0f;
            if (tmp_processing_counter < (tmp_processing_duration * 0.5f))
            {
                corrected_scale = (float)CurveUtil.ExpoEaseOut(tmp_processing_counter / (tmp_processing_duration * 0.5f), 0.0f, 1.0f, 1.0f);
            }
            else
            {
                corrected_scale = (float)CurveUtil.ExpoEaseIn((tmp_processing_counter - (tmp_processing_duration * 0.5f)) / (tmp_processing_duration * 0.5f), 0.0f, 1.0f, 1.0f);
                corrected_scale = 1.0f - corrected_scale;
            }

            //setup blur value
            if (viveMode == false)
            {
                //workingEye.GetComponent<UnityStandardAssets.ImageEffects.BlurOptimized>().blurSize = corrected_scale * 10.0f;

                UnityEngine.PostProcessing.DepthOfFieldModel.Settings s = workingEye.GetComponent<UnityEngine.PostProcessing.PostProcessingBehaviour>().profile.depthOfField.settings;
                s.focusDistance = 10.0f - (corrected_scale * 10.0f);
                workingEye.GetComponent<UnityEngine.PostProcessing.PostProcessingBehaviour>().profile.depthOfField.settings = s;

                //UnityEngine.PostProcessing.VignetteModel.Settings s2 = workingEye.GetComponent<UnityEngine.PostProcessing.PostProcessingBehaviour>().profile.vignette.settings;
                //s2.roundness = 0.759f + (1.0f - 0.759f) * corrected_scale;
                //workingEye.GetComponent<UnityEngine.PostProcessing.PostProcessingBehaviour>().profile.vignette.settings = s2;


            }
            else
            {
                //vive_hmd.GetComponent<UnityStandardAssets.ImageEffects.BlurOptimized>().blurSize = corrected_scale * 10.0f;

                UnityEngine.PostProcessing.DepthOfFieldModel.Settings s = vive_hmd.GetComponent<UnityEngine.PostProcessing.PostProcessingBehaviour>().profile.depthOfField.settings;
                s.focusDistance = 10.0f - (corrected_scale * 10.0f);
                vive_hmd.GetComponent<UnityEngine.PostProcessing.PostProcessingBehaviour>().profile.depthOfField.settings = s;

                //UnityEngine.PostProcessing.VignetteModel.Settings s2 = vive_hmd.GetComponent<UnityEngine.PostProcessing.PostProcessingBehaviour>().profile.vignette.settings;
                //s2.roundness = 0.759f + (1.0f - 0.759f) * corrected_scale;
                //vive_hmd.GetComponent<UnityEngine.PostProcessing.PostProcessingBehaviour>().profile.vignette.settings = s2;

            }

            speedLineMgr.updateCameraInfo(workingCamera.transform.position, workingCamera.transform.forward, 0, 83);

        }
        else
        if (current_status == Status.FLYING)
        {

            //FORCE RESTART
            if (Input.GetKeyUp(KeyCode.R))
            {
                healthy_percentage = 0.0f;
            }

            //update speedline
            Vector3 v = pi.curr_status.velocity;
            if (v.y > 0.0f)
            {
                v.y = 0.0f;
            }
            speedLineMgr.updateCameraInfo(workingCamera.transform.position, v, pi.curr_status.velocity.magnitude, default_max_horizontal_speed);

            //check gateway
            {
                GameObject tmpCam = workingCamera;
                if (viveMode)
                {
                    tmpCam.transform.position = vive_hmd.transform.position;
                }

                Vector3 new_pos = Vector3.zero;
                Vector3 new_dir = Vector3.forward;
                if (checkGateway(tmpCam, pi.curr_status.velocity, out new_pos, out new_dir))
                {
                    //jump to new_pos
                    Debug.Log("908 - Camera throughout from position " + workingCamera.transform.position + " to " + new_pos);
                    pi.curr_status.position = new_pos;

                    //translat velocity to new direction
                    pi.curr_status.velocity = new_dir.normalized * pi.curr_status.velocity.magnitude;
                    pi.curr_status.acceleration = new_dir.normalized * pi.curr_status.acceleration.magnitude;

                }

            }

            //
            //FIRING MISSILE / STEALING DUMPLING / ATTRACT DUMPLING
            //dumpling
            List<DumplingManager.TargetInfo> ti = null;
            if (viveMode == false)
            {
                ti = dumplingManager.findAvailableTarget((int)mCurrMRS.getMyPlayerInfo().groupid, workingCamera.transform.position, workingEye.transform.forward, true);
            }
            else
            {
                ti = dumplingManager.findAvailableTarget((int)mCurrMRS.getMyPlayerInfo().groupid, workingCamera.transform.position, vive_hmd.transform.forward, true);
            }

            bool autoTrigger = false;
            if (ti == null || ti.Count == 0)
            {
                if (curr_auto_trigger != null)
                {
                    curr_auto_trigger = null;

                    //hide count down progress bar
                    cpl.setProgress(0.0f);

                }

            }
            else
            {
                if (curr_auto_trigger == null)
                {
                    curr_auto_trigger = new AutoTrigger(ti[0]);

                    //show count down progress bar
                    cpl.setProgress(0.0f);

                }
                else
                {
                    if (curr_auto_trigger.focusing_ti != ti[0])
                    {
                        curr_auto_trigger.count_down_time = curr_auto_trigger.duration;
                        curr_auto_trigger.focusing_ti = ti[0];
                    }
                }

                curr_auto_trigger.count_down_time -= Time.deltaTime;

                //update progress bar
                bool red_progress_bar = (ti[0].t == DumplingManager.TargetInfo.Type.ENERMY ||
                                         ti[0].curr_status == DumplingManager.TargetInfo.Status.COLLECTED ||
                                         ti[0].curr_status == DumplingManager.TargetInfo.Status.COLLECTING);
                cpl.setProgress((curr_auto_trigger.duration - curr_auto_trigger.count_down_time) / curr_auto_trigger.duration, red_progress_bar);

                if (curr_auto_trigger.count_down_time <= 0.0f)
                {

                    autoTrigger = true;

                    //hide count down progress bar
                    cpl.setProgress(0.0f);
                    curr_auto_trigger = null;
                }

            }


            if (Input.GetKeyDown(KeyCode.F))
            { //FIRE MISSILE ANYWAY
                if (ti != null && ti.Count > 0)
                {
                    if (pi.missile_cold_down_counter <= 0.0f)
                    {
                        if (viveMode == false)
                        {
                            dumplingManager.destroyTarget(ti, workingCamera.transform.position, workingEye.transform.forward, pi.curr_status.velocity);
                        }
                        else
                        {
                            dumplingManager.destroyTarget(ti, workingCamera.transform.position, vive_hmd.transform.forward, pi.curr_status.velocity);
                        }
                        pi.missile_cold_down_counter = 1.0f;
                    }
                }
            }
            else
            if (input_firemissileTriggered() || input_targetAttractionTriggered() || autoTrigger)
            {
                if (ti != null && ti.Count > 0)
                {
                    if (pi.missile_cold_down_counter <= 0.0f &&
                        (
                         ti[0].t == DumplingManager.TargetInfo.Type.ENERMY || //目標是歒人 
                         (ti[0].t == DumplingManager.TargetInfo.Type.NORMAL &&
                          ti[0].attracted_player_guid != null &&
                          ti[0].attracted_player_guid != mCurrMRS.myGuid &&
                          mCurrMRS.getPlayerInfoByGUID(ti[0].attracted_player_guid).groupid != mCurrMRS.getMyPlayerInfo().groupid) //目標是敵人的包子
                        )
                       )
                    {
                        //FIRE MISSILE
                        if (viveMode == false)
                        {
                            dumplingManager.destroyTarget(ti, workingCamera.transform.position, workingEye.transform.forward, pi.curr_status.velocity);
                        }
                        else
                        {
                            dumplingManager.destroyTarget(ti, workingCamera.transform.position, vive_hmd.transform.forward, pi.curr_status.velocity);
                        }
                        pi.missile_cold_down_counter = 1.0f;
                    }
                    else
                    {
                        //ATTRACT DUMPLING
                        if (viveMode == false)
                        {
                            dumplingManager.attractTarget(ti, workingCamera);
                        }
                        else
                        {
                            dumplingManager.attractTarget(ti, workingCamera);
                        }
                    }

                }
                else
                {
                    //FIRE MISSILE
                    //blind shot
                    if (pi.missile_cold_down_counter <= 0.0f)
                    {
                        if (viveMode == false)
                        {
                            dumplingManager.destroyTarget(null, workingCamera.transform.position, workingEye.transform.forward, pi.curr_status.velocity);
                        }
                        else
                        {
                            dumplingManager.destroyTarget(null, workingCamera.transform.position, vive_hmd.transform.forward, pi.curr_status.velocity);
                        }
                        pi.missile_cold_down_counter = 1.0f;
                    }
                }
            }

            //POSITION ITERATION (ITERATE TO NEXT POSITION)
            //found collision force
            if (tmp_reactForce != Vector3.zero)
            {

                List<DumplingManager.TargetInfo> dump_arr = dumplingManager.randomPickAttachedTarget(Random.Range(1, 3));
                Debug.Log(dump_arr.Count + " dumpling loss...");

                List<Vector3> dump_loss_posi_arr = new List<Vector3>();
                for (int i = 0; i < dump_arr.Count; ++i)
                {
                    Vector3 oriDir = (dump_arr[i].obj.transform.localPosition - pi.curr_status.velocity * 1.0f) - dump_arr[i].obj.transform.localPosition;
                    Vector3 hDir = Vector3.Cross(oriDir.normalized, Vector3.up) * Random.Range(-5.0f, 5.0f);
                    Vector3 vDir = Vector3.Cross(hDir, oriDir.normalized) * Random.Range(-5.0f, 5.0f);
                    oriDir = oriDir + hDir + vDir;

                    dump_loss_posi_arr.Add(dump_arr[i].obj.transform.localPosition + oriDir);
                }

                if (mCurrMRS.isHost())
                {

                    List<object> req_arr = new List<object>();
                    req_arr.Add(mCurrMRS.myGuid);

                    for (int i = 0; i < dump_arr.Count; ++i)
                    {
                        if (dumplingManager.targetLosing(dump_arr[i], dump_loss_posi_arr[i]))
                        {

                            req_arr.Add(dump_arr[i].id);

                            req_arr.Add(dump_loss_posi_arr[i].x);
                            req_arr.Add(dump_loss_posi_arr[i].y);
                            req_arr.Add(dump_loss_posi_arr[i].z);
                        }
                    }

                    //broadcast to all
                    //send message
                    dumplingEventHandler(MultiplayerHandler.GameEvent.DUMPLING_LOSING, req_arr.ToArray());

                }
                else
                {

                    List<object> req_arr = new List<object>();
                    for (int i = 0; i < dump_arr.Count; ++i)
                    {
                        req_arr.Add(dump_arr[i].id);

                        req_arr.Add(dump_loss_posi_arr[i].x);
                        req_arr.Add(dump_loss_posi_arr[i].y);
                        req_arr.Add(dump_loss_posi_arr[i].z);
                    }

                    //request losing dumpling		
                    sendMessageToNetwork(mCurrMRS.gameLeaderGuid, MultiplayerHandler.GameEvent.DUMPLING_LOSING_REQUEST, req_arr);
                }

                //是否轉向
                float ang = Vector3.Angle(pi.curr_status.velocity, tmp_reactForce);
                Debug.Log("1033 - Reflect angle : " + ang);
                float h_ang = Vector3.Angle(new Vector3(pi.curr_status.velocity.x, 0.0f, pi.curr_status.velocity.z), new Vector3(tmp_reactForce.x, 0.0f, tmp_reactForce.z));
                Debug.Log("1035 - Horizontal Reflect angle : " + h_ang);
                if (h_ang <= 30.0f)
                {
                    resetFlyingStatus(tmp_reactForce);
                    tmp_reactForce = Vector3.zero;
                    current_status = Status.FLYING;

                }
                else
                {

                    tmp_processing_duration = h_ang * 0.03f;
                    tmp_processing_counter = 0.0f;
                    tmp_processing_collision_from_position = pi.curr_status.velocity;
                    tmp_processing_collision_to_position = tmp_reactForce;

                    Debug.Log("process position from " + tmp_processing_collision_from_position + ", to position " + tmp_processing_collision_to_position);

                    //active camera blur effect
                    if (viveMode == false)
                    {
                        //workingEye.GetComponent<UnityStandardAssets.ImageEffects.BlurOptimized>().enabled = true;
                        workingEye.GetComponent<UnityEngine.PostProcessing.PostProcessingBehaviour>().profile.depthOfField.enabled = true;
                    }
                    else
                    {
                        //vive_hmd.GetComponent<UnityStandardAssets.ImageEffects.BlurOptimized>().enabled = true;
                        vive_hmd.GetComponent<UnityEngine.PostProcessing.PostProcessingBehaviour>().profile.depthOfField.enabled = true;
                    }
                    current_status = Status.PROCESSING_ON_COLLISION;
                }

            }
            else
            {

                //time integration for physics simulation
                tmp_acc = processAcceleration(pi);
                if (float.IsNaN(tmp_acc.x) || float.IsNaN(tmp_acc.y) || float.IsNaN(tmp_acc.z))
                {
                    Debug.LogWarning("1044 - found tmp_acc is NaN");
                }
                else
                {
                    pi.curr_status.velocity += tmp_acc * Time.deltaTime;
                }

                pi.curr_status.position += pi.curr_status.velocity * Time.deltaTime;
                pi.prev_status.duplicate(pi.curr_status);

                //apply to camera
                workingCamera.transform.position = pi.curr_status.position;
                Vector3 f = calculateForwardDirection(pi); //velocity dir + dive angle 
                if (f != Vector3.zero)
                    workingCamera.transform.forward = f;
                workingCamera.transform.localRotation = calculateRotation(pi); //camera orientation depends on its velocity (row angle) 

                workingGyroscope.GetComponent<Gyroscope>().enable = thirdPersonRollEffect;
                workingGyroscope.GetComponent<Gyroscope>().rotateHead = rotateHead;
                if (viveMode == false && disableHeadRotationAtPcMode == true)
                {
                    workingGyroscope.GetComponent<Gyroscope>().rotateHead = false;
                }
                workingGyroscope.GetComponent<Gyroscope>().rotateHeadRatio = 1.0f - rotateHeadRatio;
                workingGyroscope.GetComponent<Gyroscope>().forwardingDir = workingCamera.transform.forward;

                //check game end condition
                if (Vector3.Distance(
                  new Vector3(workingCamera.transform.position.x, 0.0f, workingCamera.transform.position.z),
                  new Vector3(transform.position.x, 0.0f, transform.position.z)) > playing_radius)
                {
                    /*
                    EnvSoundObj.GetComponent<EnvironmentSound>().setPlayWarn(true);
                    warningView.GetComponent<WarningViewDisplayer>().showWarningView(true);
                    if (warningView.GetComponent<WarningViewDisplayer>().isTimesUp()) {
                      //just respawn

                      Debug.Log("644- fade to black after 1.0f");
                      SteamVR_Fade.Start(Color.black, 1.0f, true);
                      current_status = Status.BACK_TO_SPAWN_FADE_OUT;

                    }
                    */
                    //Debug.Log("817 - cross border...");

                }
                else
                {
                    EnvSoundObj.GetComponent<EnvironmentSound>().setPlayWarn(false);
                    warningView.GetComponent<WarningViewDisplayer>().showWarningView(false);
                }

            }

            //setup sound
            EnvSoundObj.GetComponent<EnvironmentSound>().setSpeed(pi.curr_status.velocity.magnitude);

        }
        else
        if (current_status == Status.END_FADE_OUT)
        {

            fade_out_counter += Time.deltaTime;
            if (fade_out_counter >= 1.0f)
            {
                fade_out_counter = 0.0f;
                Debug.Log("587- fade to clear after 1.0f");
                SteamVR_Fade.Start(Color.clear, 1.0f, true);

                current_status = Status.END;
                return; //dont sim the frame
            }
        }
        else
        if (current_status == Status.END)
        {
            inited = false;
            if (registeredPlayerEvenet1Handler != null)
            {
                Debug.Log("703 - send end game message...");
                sendMessageToNetwork(MultiplayerHandler.GameEvent.END_GAME, null);
            }
            else
            {
                Debug.LogWarning("599 - registeredPlayerEventHandler is null");
            }

            EnvSoundObj.GetComponent<EnvironmentSound>().setGamePlaying(false);

            current_status = Status.ENDING;
            return;
        }
        else
        if (current_status == Status.WAIT_SHOW_TIMEUP)
        {
            timeup_counter += Time.deltaTime;
            if (timeup_counter >= 3.0f)
            {

                gametimeView.GetComponent<GameTimeViewDisplayer>().showGameTime(false);
                //gametimeView.GetComponent<GameTimeViewDisplayer>().resetTimer();
                shieldtimeView.GetComponent<ShieldDurationDisplayer>().resetTimer();
                acceleratorTimeView.GetComponent<ShieldDurationDisplayer>().resetTimer();


                if (MatchmakingController.instance.gameMode == MatchmakingController.GameMode.TeamFight)
                {
                    //show game result
                    //...
                    int gid = (int)mCurrMRS.getMyPlayerInfo().groupid;
                    int inv_gid = (gid + 1) % 2; //invert group position
                    if (tmpTeamScoreStatistics[gid] >= tmpTeamScoreStatistics[inv_gid])
                        labelUIController.instance.ShowGameResult(true);
                    else
                        labelUIController.instance.ShowGameResult(false);

                }
                else
                {
                    //乱斗模式分数最高的赢
                    int highestScore = 0;
                    string highestScoreGUID = null;
                    Dictionary<string, int>.Enumerator itr = tmpScoreStatistics.GetEnumerator();
                    while (itr.MoveNext())
                    {
                        if (itr.Current.Value > highestScore)
                        {
                            highestScore = itr.Current.Value;
                            highestScoreGUID = itr.Current.Key;
                        }
                    }
                    if (highestScoreGUID == mCurrMRS.getMyPlayerInfo().guid)
                        labelUIController.instance.ShowGameResult(true);
                    else
                        labelUIController.instance.ShowGameResult(false);
                }


                current_status = Status.WAIT_SHOW_GAMERESULT;
                timeup_counter = 0.0f;
            }
        }
        else
        if (current_status == Status.WAIT_SHOW_GAMERESULT)
        {
            timeup_counter += Time.deltaTime;
            if (timeup_counter >= 3.0f)
            {
                gametimeView.GetComponent<GameTimeViewDisplayer>().resetTimer();
                labelUIController.instance.UnActiveUIObjs();
                if (multi_player_mode == false ||
                    (multi_player_mode == true && mCurrMRS.isHost() == true))
                {
                    current_status = Status.END_FADE_OUT;
                    fade_out_counter = 0.0f;

                    Debug.Log("663- fade to black after 1.0f");
                    SteamVR_Fade.Start(Color.black, 1.0f, true);
                }
                else
                {

                    current_status = Status.WAIT_GAME_END;
                }
            }
        }

        //ui
        if (enable_hud == true)
        {
            if (pi != null && current_status == Status.FLYING)
            {
                hud.transform.position = pi.curr_status.position;
                if (viveMode == false)
                {
                    hud.transform.localRotation = workingCamera.transform.localRotation;
                }
                else
                {
                    hud.transform.localRotation = workingEye.transform.rotation; //camera_rig
                }
            }

            //radar
            if (viveMode == false)
            {
                dumplingManager.updateRadarProjection(viveMode, workingCamera.transform.position, workingCamera.transform.forward, workingCamera.transform.localRotation, workingEye.transform.position);
            }
            else
            {
                dumplingManager.updateRadarProjection(viveMode, workingCamera.transform.position, workingCamera.transform.forward, workingEye.transform.rotation, vive_hmd.transform.position);
            }

            //debug ui
            if (show_debug_ui == true)
            {
                if (pi != null)
                {
                    Vector3 hv = new Vector3(pi.curr_status.velocity.x, 0.0f, pi.curr_status.velocity.z);
                    ui_h_velocity.text = "h_vel: " + hv.ToString() + ", " + hv.magnitude * 3600.0f / 1000.0f + " km/hr (max: " + h_max_speed_limit * 3600.0f / 1000.0f + ")";
                    ui_velocity.text = "v_vel: " + pi.curr_status.velocity.y.ToString() + ", " + pi.curr_status.velocity.y * 3600.0f / 1000.0f + " km/hr";
                    ui_acceleration.text = "acc: " + tmp_acc.ToString();
                    ui_position.text = "attitude: " + pi.curr_status.position.y;

                    ui_h_vel_ploter.setupValue((new Vector2(pi.curr_status.velocity.x, pi.curr_status.velocity.z)).magnitude);
                    ui_vel_ploter.setupValue(pi.curr_status.velocity.y);
                    ui_acc_ploter.setupValue(tmp_acc.y);
                    ui_height_ploter.setupValue(pi.curr_status.position.y);

                    Vector3 f = workingCamera.transform.forward;
                    float fv = Vector3.Angle(f, new Vector3(f.x, 0.0f, f.z));
                    if (fv >= 0)
                        fv = -fv;
                    ui_dive_angle_ploter.setupValue(fv);
                    ui_dive_angle.text = "dive: " + fv;

                    ui_rotinfo.text = "rot: " + pi.rotationForce_Current;
                    ui_rot_force.setupValue(pi.rotationForce_Current);

                    //vive hud
                    ui_l_controller_ploter.setupValue(pi.controller_hmd_dist[0]);
                    ui_r_controller_ploter.setupValue(pi.controller_hmd_dist[1]);
                    ui_l_controller_flap_ploter.setupValue(pi.accumulatedFlapVelocity[0]);
                    ui_r_controller_flap_ploter.setupValue(pi.accumulatedFlapVelocity[1]);
                    ui_control_mode.text = "mode: " + pi.viveStatus.ToString() + ", bh: " + pi.handsBehnadPlane + ", VIVE: " + viveMode;
                    ui_control_info.text = "l_ctrl: " + pi.controller_hmd_dist[0] + ", r_ctrl: " + pi.controller_hmd_dist[1] + ", lr_dist: " + pi.controller_dist;
                    ui_control_flap_l.text = "l_flap: " + pi.accumulatedFlapVelocity[0];
                    ui_control_flap_r.text = "r_flap: " + pi.accumulatedFlapVelocity[1];

                    ui_turn_angle_ploter.setupValue(pi.turningFactor);
                    ui_turn_angle.text = "turn angle: " + pi.turningFactor;

                    ui_vvel_l_ploter.setupValue(pi.flapVelocity[0]);
                    ui_vvel_r_ploter.setupValue(pi.flapVelocity[1]);
                    ui_vvel_l.text = "l_vvel: " + pi.flapVelocity[0];
                    ui_vvel_r.text = "r_vvel: " + pi.flapVelocity[1];

                }
                else
                {
                    ui_velocity.text = "vel: " + Vector3.zero.ToString();
                    ui_acceleration.text = "acc: " + Vector3.zero.ToString();
                    ui_position.text = "attitude: " + Vector3.zero.y;
                }
            }

            if (pi != null && (current_status == Status.FLYING || current_status == Status.LANDING_IDLE))
            {
                if (gametimeView.GetComponent<GameTimeViewDisplayer>().isTimesUp())
                {

                    timeup_counter = 0.0f;
                    current_status = Status.WAIT_SHOW_TIMEUP;
                }
            }
        }

    }

    Quaternion calculateRotation(PlayerInfo pi)
    {
        if (rollEffect == false)
        {
            return workingCamera.transform.localRotation;
        }
        Vector3 d = -new Vector3(pi.rotForce.x, 0.0f, pi.rotForce.z) + Vector3.up * gravityForce * maxRotForce * 0.1f;
        Vector3 rdir = Vector3.Cross(d, new Vector3(pi.curr_status.velocity.x, 0.0f, pi.curr_status.velocity.z)).normalized;
        return Quaternion.FromToRotation(-workingCamera.transform.right, rdir) * workingCamera.transform.localRotation;
    }

    float shift_angle = 45.0f;
    Vector3 calculateForwardDirection(PlayerInfo pi)
    {
        if (pi.diveActing == false)
        {
            pi.dive_target_forward = 0;
        }

        Vector3 fd = new Vector3(pi.curr_status.velocity.x, 0.0f, pi.curr_status.velocity.z);
        Vector3 ret_dir = fd.normalized;

        //--------------------------------
        //DIVE PART
        if (diveCamera == true)
        {
            pi.dive_counter += Time.deltaTime / diveDuration;
            if (pi.dive_counter <= 1.0f)
            {
                float newForward = CurveUtil.HermiteInterpolation(pi.dive_counter, pi.dive_start_forward, pi.dive_target_forward, pi.dive_start_tangent, 0.0f);
                pi.dive_curr_velocity = (newForward - pi.dive_curr_forward) / Time.deltaTime * diveDuration;
                pi.dive_curr_forward = newForward;
            }

            if (easeDiveAngle == true)
            {
                ret_dir = Quaternion.AngleAxis((float)CurveUtil.SineEaseInOut(pi.dive_curr_forward, 0, diveAngle, diveAngle), Vector3.Cross(fd, Vector3.up)) * fd;
            }
            else
            {
                ret_dir = Quaternion.AngleAxis(pi.dive_curr_forward, Vector3.Cross(fd, Vector3.up)) * fd;
            }

            if (pi.accelerator_enabled == false)
            { //加速功能也使用 camera 振動
                float shake_amp = Mathf.Abs(pi.dive_curr_forward / diveAngle) * 0.2f;
                workingEye.GetComponent<Klak.Motion.BrownianMotion>().rotationAmplitude = shake_amp;
            }
        }

        return ret_dir;
    }

    bool isControllerOnline(GameObject controller)
    {
        var tobj = controller.GetComponent<SteamVR_TrackedObject>();
        if (tobj.index == SteamVR_TrackedObject.EIndex.None)
        {
            return false;
        }
        return true;
    }

    bool analyseViveController(PlayerInfo pi)
    {
        if (viveMode == false)
            return false;

        if (isControllerOnline(vive_l_controller) == false || isControllerOnline(vive_r_controller) == false)
        {
            pi.viveStatus = ViveStatus.CONTROLLER_MISSING;
            return false;
        }

        //local parameters
        float maxShieldControllerDist = 0.25f;
        //float minShieldControllerDirDot = 0.1f; //1.0 is perfact val
        //float maxFlapControllerDirDot = -0.1f; //-1.0 is perface val
        float minFlapStateThreashold = 2.0f;
        float accFlapVelDecay = 0.95f;
        float minWorkingDist = 0.2f;
        float minFlapDist = 0.7f;

        //
        //calculate basic info.
        //
        float dist_ctrl_l = Vector3.Distance(vive_l_controller.transform.position, vive_hmd.transform.position);
        float dist_ctrl_r = Vector3.Distance(vive_r_controller.transform.position, vive_hmd.transform.position);

        float ctrl_dist = Vector3.Distance(vive_l_controller.transform.position, vive_r_controller.transform.position);
        //float ctrl_dot = Vector3.Dot(vive_l_controller.transform.forward.normalized, vive_r_controller.transform.forward.normalized);
        Vector3 vcenter = (vive_r_controller.transform.position - vive_l_controller.transform.position).normalized * (ctrl_dist * 0.5f);

        pi.controller_dist = ctrl_dist;
        pi.controller_hmd_dist[0] = dist_ctrl_l;
        pi.controller_hmd_dist[1] = dist_ctrl_r;

        //behind or above center vertical plane
        bool behindCenterPlane = false;
        Plane p;
        //if (current_status == Status.FLYING) {
        //    p = new Plane(pi.curr_status.velocity.normalized, vive_hmd.transform.position);
        //    if (p.GetDistanceToPoint(vive_l_controller.transform.position) > 0 &&
        //        p.GetDistanceToPoint(vive_r_controller.transform.position) > 0) {
        //        behindCenterPlane = true;
        //    }
        //} else {
        p = new Plane(new Vector3(pi.curr_status.velocity.x, 0.0f, pi.curr_status.velocity.z).normalized, vive_hmd.transform.position);
        if (p.GetDistanceToPoint(vive_l_controller.transform.position) > 0 &&
            p.GetDistanceToPoint(vive_r_controller.transform.position) > 0)
        {
            behindCenterPlane = true;
        }
        //}
        pi.handsBehnadPlane = behindCenterPlane;

        //flap or gliding or turning
        var tracked_l_controller = vive_l_controller.GetComponent<SteamVR_TrackedObject>();
        var tracked_r_controller = vive_r_controller.GetComponent<SteamVR_TrackedObject>();
        var device_l = SteamVR_Controller.Input((int)tracked_l_controller.index);
        var device_r = SteamVR_Controller.Input((int)tracked_r_controller.index);

        if (device_l.velocity.y < -2.5f || device_r.velocity.y < -2.5f)
        {
            playOneShot(flapAudio2);
        }
        else
          if (device_l.velocity.y < -1.5f || device_r.velocity.y < -1.5f)
        {
            playOneShot(flapAudio1);
        }

        //get vertical velocity
        pi.flapVelocity[0] = device_l.velocity.y;
        pi.flapVelocity[1] = device_r.velocity.y;
        if (device_l.velocity.y < 0.0f)
            pi.accumulatedFlapVelocity[0] += Mathf.Abs(device_l.velocity.y);
        if (device_r.velocity.y < 0.0f)
            pi.accumulatedFlapVelocity[1] += Mathf.Abs(device_r.velocity.y);

        //velocity decay
        for (int i = 0; i < pi.accumulatedFlapVelocity.Length; ++i)
        {
            pi.accumulatedFlapVelocity[i] *= accFlapVelDecay;
        }

        //calculate turning factor
        Vector3 r = vive_r_controller.transform.position - vive_l_controller.transform.position;
        Vector3 c = new Vector3(vive_r_controller.transform.position.x, vive_l_controller.transform.position.y, vive_r_controller.transform.position.z) - vive_l_controller.transform.position;
        float a = Vector3.Angle(r, c);
        if (vive_r_controller.transform.position.y > vive_l_controller.transform.position.y)
        {
            a = -a;
        }
        pi.turningFactor = a;


        if (dist_ctrl_l > minWorkingDist && dist_ctrl_r > minWorkingDist)
        {

            ////-------------------
            ////shield mode
            ////
            ////1.手合拼
            ////2.位於前方
            //if (ctrl_dist <= maxShieldControllerDist &&
            //    behindCenterPlane == true) {

            //  pi.viveStatus = ViveStatus.SHIELD;

            //  //...

            //} else

            //------------------
            //flap or gliding
            //
            //1.手展得夠開
            if (ctrl_dist > minFlapDist)
            {
                //flaping
                if (Mathf.Abs(device_l.velocity.y) + Mathf.Abs(device_r.velocity.y) >= minFlapStateThreashold)
                {

                    //flaping
                    pi.viveStatus = ViveStatus.FLAPING;

                    //...


                }
                else
                {

                    //gliding + turning
                    pi.viveStatus = ViveStatus.GLIDING_OR_TURNING;

                    //...

                }


            }
            else

            //-------------------
            //dive
            //
            //1.手分開但不至於展開
            //2. 位於後方
            //3. 低於一定高度
            //4. 高於一定高度
            if (//ctrl_dist > maxShieldControllerDist &&
                //vive_l_controller.transform.position.y < vive_camerarig.transform.position.y + ((vive_hmd.transform.position.y - vive_camerarig.transform.position.y) * 0.5f) &&
                //vive_r_controller.transform.position.y < vive_camerarig.transform.position.y + ((vive_hmd.transform.position.y - vive_camerarig.transform.position.y) * 0.5f) &&

                p.GetDistanceToPoint(vive_l_controller.transform.position) < -0.25f &&
                p.GetDistanceToPoint(vive_r_controller.transform.position) < -0.25f &&

                behindCenterPlane == false)
            {

                pi.viveStatus = ViveStatus.DIVE;

                //...

            }
            else
            {
                pi.viveStatus = ViveStatus.UNKNOWN;
            }

        }
        else
        {
            //idle
            pi.viveStatus = ViveStatus.IDLE;
        }

        return true;

    }

    float audioCounter = 0.0f;
    void playOneShot(AudioClip ac, bool immediatelly = false)
    {
        if (immediatelly == true)
        {
            asund.PlayOneShot(ac, 0.1f);
            return;
        }

        if (audioCounter <= 0.5f)
            return;

        audioCounter = 0.0f;
        asund.PlayOneShot(ac);
    }

    Vector3 processAcceleration(PlayerInfo pi)
    {
        Vector3 curr_acc = Vector3.zero;

        //-------------------
        //decay previous acceleration
        curr_acc += pi.prev_status.acceleration * 0.75f;

        //-------------------
        //impulse force
        curr_acc += pi.impulseForce;
        pi.impulseForce = Vector3.zero;

        //-------------------
        //Finalize
        pi.curr_status.acceleration = curr_acc;

        //========================
        // force down
        //========================
        //-------------------
        //gravity
        curr_acc += new Vector3(0.0f, gravityForce, 0.0f);

        //-------------------
        //decending force (anti-ascending) (不振趐時能減短上昇時間)
        if (pi.impulseForce.magnitude <= 0.05f && pi.curr_status.velocity.y > 0.0f)
        {
            curr_acc += new Vector3(0.0f, antiAscendingForce, 0.0f);
        }

        //-------------------
        //dive force
        curr_acc = diveForceInput(pi, curr_acc);

        //========================
        // force up
        //========================
        //-------------------
        //gliding force (anti-decending force)
        if (pi.diveActing == false && glidingForceWhenAscending == false && pi.curr_status.velocity.y <= 0.0f)
        {
            curr_acc += new Vector3(0.0f, glidingForce, 0.0f);
        }

        //-------------------
        //ascending force
        curr_acc = ascendingForceInput(pi, curr_acc);

        //-------------------
        //anti-decending (振趐時能減短下降時間)
        if (pi.impulseForce.magnitude > 0.0f && pi.curr_status.velocity.y < 0.0f)
        {
            curr_acc += new Vector3(0.0f, antiDescendingForce, 0.0f);
        }

        //========================
        // SHIELD
        //========================
        curr_acc = shieldForce(pi, curr_acc);

        //========================
        // ACCELERATOR
        //========================
        curr_acc = accelerateForce(pi, curr_acc);

        //========================
        // rotation
        //========================
        curr_acc = rotationForce(pi, curr_acc);

        //========================
        // forward speed maintainer
        //========================
        curr_acc = forwardSpeedMaintainer(pi, curr_acc);

        //========================
        // limitation
        //========================
        //-------------------
        //ceiling limitation
        if (pi.curr_status.position.y >= (ceilingHeight - bufferHeight) && pi.curr_status.velocity.y > 0.0f)
        {
            //Proportional Control
            float acc = -pi.curr_status.velocity.y * (pi.curr_status.position.y - (ceilingHeight - bufferHeight)) / bufferHeight;
            if (curr_acc.y > acc)
            {
                curr_acc = new Vector3(curr_acc.x, acc, curr_acc.z);
            }

        }

        //-------------------
        //floor limitation
        float th = 0.0f;

        RaycastHit hit;
        Ray r = new Ray(new Vector3(workingCamera.transform.position.x, 500.0f, workingCamera.transform.position.z), -Vector3.up);
        if (Physics.Raycast(r, out hit, 500.0f, TerrainLM, QueryTriggerInteraction.Ignore))
        {
            //Debug.Log("raycast return true ("+hit.collider.name+", layer="+hit.collider.gameObject.layer+")");
            th = 500.0f - hit.distance;
        }

        float tt = Mathf.Max(floorHeight, th) + bufferHeight;
        //Debug.Log("th ="+th+", target_height ="+tt+", c.y="+pi.curr_status.position.y);
        acc_err_floor_height += (tt - pi.curr_status.position.y);

        if (pi.curr_status.position.y <= tt /*&& pi.curr_status.velocity.y < 0.0f*/)
        {
            //Proportional control
            float acc = -pi.curr_status.velocity.y * (1.0f - (pi.curr_status.position.y - tt) / bufferHeight);
            acc += (tt - pi.curr_status.position.y) * 10.0f;
            if (curr_acc.y < acc)
            {
                curr_acc = new Vector3(curr_acc.x, acc, curr_acc.z);
            }
        }

        //========================
        // speed limitation
        //========================
        float speed_limit_ms = maximumSpeed / 3600.0f * 1000.0f;
        float buffer_speed_limit_ms = bufferSpeed / 3600.0f * 1000.0f;
        if (buffer_speed_limit_ms >= speed_limit_ms)
        {
            buffer_speed_limit_ms = speed_limit_ms;
        }
        float curr_vel = pi.curr_status.velocity.magnitude;
        if (curr_vel >= (speed_limit_ms - buffer_speed_limit_ms))
        {
            float acc = curr_vel * 0.5f * (curr_vel - (speed_limit_ms - buffer_speed_limit_ms)) / buffer_speed_limit_ms;
            curr_acc += (new Vector3(0.0f, -pi.curr_status.velocity.y, 0.0f)).normalized * acc;
        }

        return curr_acc;
    }

    Vector3 tmp_reactForce = Vector3.zero;
    public void setReactForce(Vector3 f)
    {
        tmp_reactForce = f;
    }

    public Vector3 getCurrVelocity()
    {
        if (pi == null || pi.curr_status == null)
            return Vector3.zero;
        return pi.curr_status.velocity;
    }

    Vector3 shieldForce(PlayerInfo pi, Vector3 tmp_acc)
    {

        //if (viveMode == true) {
        //if (pi.viveStatus == ViveStatus.SHIELD) {

        //  if (pi.p_shield_cold_time_counter <= 0.0f &&
        //      pi.shield_enabled == false &&
        //      curr_shield_available_times > 0) {
        //    curr_shield_available_times--;

        //    shieldtimeView.GetComponent<ShieldDurationDisplayer>().showShieldTime(shield_available_duration);
        //    updateDisplayedScore();

        //    pi.p_shield_available_time_counter = shield_available_duration;
        //    pi.p_shield_cold_time_counter = shield_colddown_time;

        //    //shield enabled
        //    pi.shield_enabled = true;
        //    if (multi_player_mode) {
        //      //broadcast message
        //      sendMessageToNetwork(MultiplayerHandler.GameEvent.PLAYER_SHIELD_ENABLED, new List<object>() { shield_available_duration });

        //    }
        //    hud.transform.Find("Shield").gameObject.SetActive(true);

        //  }
        //} else {
        //  if (pi.shield_enabled == true) {
        //    //不保持 shield 姿勢仍保有 shield 功能
        //    /*
        //    //force leave shield mode
        //    pi.p_shield_available_time_counter = 0.0f;
        //    pi.shield_enabled = false;
        //    sendMessageToNetwork(MultiplayerHandler.GameEvent.PLAYER_SHIELD_DISABLED, null);
        //    hud.transform.Find("Shield").gameObject.SetActive(false);
        //    */
        //  }
        //}

        //} else {
        if (Input.GetKey(KeyCode.S) || Input.GetKeyDown("joystick button 3"))
        {

            if (pi.p_shield_cold_time_counter <= 0.0f && pi.shield_enabled == false && curr_shield_available_times > 0)
            {
                enableShield();

            }
        }
        else
        {
            //不保持 shield 姿勢仍保有 shield 功能
            /*
            if (pi.shield_enabled == true) {
              //force leave shield mode
              pi.p_shield_available_time_counter = 0.0f;
              pi.shield_enabled = false;
              sendMessageToNetwork(MultiplayerHandler.GameEvent.PLAYER_SHIELD_DISABLED, null);
              hud.transform.Find("Shield").gameObject.SetActive(false);
            }*/
        }
        //}

        //if (pi.shield_enabled) {
        //  //h_max_speed_limit = 1.0f;
        //  h_max_speed_limit = max_horizontal_speed;
        //  if (pi.curr_status.velocity.y <= 0.0) {
        //    tmp_acc += new Vector3(0.0f, -tmp_acc.y + (0.0f - pi.curr_status.velocity.y / Time.deltaTime) * 0.0065f, 0.0f);
        //  }
        //} else {
        //h_max_speed_limit = max_horizontal_speed;
        //}


        return tmp_acc;
    }

    void disable_accelerator()
    {

        if (viveMode && curr_accelerator_available_times > 0)
        {
            acceleratorIconObj_vive.GetComponent<SphereCollider>().enabled = true;

            ScaleAnimation sa = new ScaleAnimation();
            sa.initAnimation(CurveUtil.ElasticEaseInOut, 0.0f, 1.0f, new GameObject[] { acceleratorIconObj_vive }, true, delegate (object[] arr)
            {
                //show particle effect
                acceleratorIconObj_vive.transform.Find("Rocket/FlyAircraft_Hi/Buff_W_AirLight_out").gameObject.SetActive(true);
            });
            sa.setupScaleVal(Vector3.zero, Vector3.one * 0.2f);
            mAnimationList.Add(sa);

            acceleratorIconObj_vive.SetActive(true);
        }

        Debug.Log("840 - disable accelerating");
        //leave accelerating mode
        //...


        pi.accelerator_enabled = false;

        //turn off accelerating effect
        //...
        curr_max_horizontal_speed = default_max_horizontal_speed;
        h_max_speed_limit = curr_max_horizontal_speed;

        workingEye.GetComponent<Klak.Motion.BrownianMotion>().rotationAmplitude = 0.0f;
        acceleratorTimeView.GetComponent<ShieldDurationDisplayer>().resetTimer();
    }

    void enable_accelerator()
    {

        if (viveMode)
        {
            acceleratorIconObj_vive.GetComponent<SphereCollider>().enabled = false;

            ScaleAnimation sa = new ScaleAnimation();
            sa.initAnimation(CurveUtil.ElasticEaseInOut, 0.0f, 1.0f, new GameObject[] { acceleratorIconObj_vive }, true, delegate (object[] arr)
            {
                acceleratorIconObj_vive.SetActive(false);
            });
            sa.setupScaleVal(Vector3.one * 0.2f, Vector3.zero);
            mAnimationList.Add(sa);

            //hide particle effect
            acceleratorIconObj_vive.transform.Find("Rocket/FlyAircraft_Hi/Buff_W_AirLight_out").gameObject.SetActive(false);

        }

        curr_accelerator_available_times--;
        updateDisplayedScore();
        acceleratorTimeView.GetComponent<ShieldDurationDisplayer>().showShieldTime(accelerator_available_duration);

        pi.p_accelerator_available_time_counter = accelerator_available_duration;
        pi.p_accelerator_cold_time_counter = accelerator_colddown_time;

        pi.accelerator_enabled = true;
        if (multi_player_mode)
        {
            sendMessageToNetwork(MultiplayerHandler.GameEvent.PLAYER_ACCELERATING_ENABLED, new List<object>() { accelerator_available_duration });
        }

        curr_max_horizontal_speed = default_max_horizontal_speed * 3.0f;
        h_max_speed_limit = curr_max_horizontal_speed;
    }

    void disableShield()
    {
        if (viveMode && curr_shield_available_times > 0)
        {

            shieldIconObj_vive.GetComponent<SphereCollider>().enabled = true;

            ScaleAnimation sa = new ScaleAnimation();
            sa.initAnimation(CurveUtil.ElasticEaseInOut, 0.0f, 1.0f, new GameObject[] { shieldIconObj_vive }, true);
            sa.setupScaleVal(Vector3.zero, Vector3.one * 0.25f);
            mAnimationList.Add(sa);
        }

        //force leave shield mode
        pi.shield_enabled = false;
        hud.transform.Find("Shield").gameObject.SetActive(false);

        shieldtimeView.GetComponent<ShieldDurationDisplayer>().resetTimer();

        pi.p_shield_available_time_counter = 0.0f;
        pi.p_shield_cold_time_counter = 0.0f;
    }

    void enableShield()
    {

        if (viveMode)
        {

            shieldIconObj_vive.GetComponent<SphereCollider>().enabled = false;

            ScaleAnimation sa = new ScaleAnimation();
            sa.initAnimation(CurveUtil.ElasticEaseInOut, 0.0f, 1.0f, new GameObject[] { shieldIconObj_vive }, true);
            sa.setupScaleVal(Vector3.one * 0.25f, Vector3.zero);
            mAnimationList.Add(sa);
        }

        curr_shield_available_times--;
        updateDisplayedScore();
        shieldtimeView.GetComponent<ShieldDurationDisplayer>().showShieldTime(shield_available_duration);

        pi.p_shield_available_time_counter = shield_available_duration;
        pi.p_shield_cold_time_counter = shield_colddown_time;

        //shield enabled
        pi.shield_enabled = true;
        if (multi_player_mode)
        {
            //broadcast message
            sendMessageToNetwork(MultiplayerHandler.GameEvent.PLAYER_SHIELD_ENABLED, new List<object>() { shield_available_duration });

        }
        hud.transform.Find("Shield").gameObject.SetActive(true);

    }

    Vector3 accelerateForce(PlayerInfo pi, Vector3 tmp_acc)
    {
        //if (viveMode == true) {

        //} else {
        if (Input.GetKey(KeyCode.A) || Input.GetKeyDown("joystick button 4"))
        {
            if (pi.p_accelerator_cold_time_counter <= 0.0f && pi.accelerator_enabled == false && curr_accelerator_available_times > 0)
            {
                enable_accelerator();
            }
        }
        //}

        if (pi.accelerator_enabled)
        {
            float shake_amp = (new Vector3(pi.curr_status.velocity.x, 0.0f, pi.curr_status.velocity.z).magnitude - default_max_horizontal_speed) / (h_max_speed_limit - default_max_horizontal_speed) * 0.2f;
            if (shake_amp < 0.0f)
                shake_amp = 0.0f;

            //if (viveMode == false) {
            workingEye.GetComponent<Klak.Motion.BrownianMotion>().rotationAmplitude = shake_amp;
            //} else {
            //  vive_hmd.GetComponent<Klak.Motion.BrownianMotion>().rotationAmplitude = shake_amp;
            //}

        }

        return tmp_acc;
    }

    Vector3 forwardSpeedMaintainer(PlayerInfo pi, Vector3 tmp_acc)
    {

        if (float.IsInfinity(pi.curr_status.velocity.x) || float.IsNaN(pi.curr_status.velocity.x))
        {
            Debug.Log("found invalid velocity value (component x)");
            Vector3 tmp_v = pi.curr_status.velocity;
            tmp_v.x = 0.0f;
            pi.curr_status.velocity = tmp_v;
        }

        if (float.IsInfinity(pi.curr_status.velocity.y) || float.IsNaN(pi.curr_status.velocity.y))
        {
            Debug.Log("found invalid velocity value (component y)");
            Vector3 tmp_v = pi.curr_status.velocity;
            tmp_v.y = 0.0f;
            pi.curr_status.velocity = tmp_v;
        }

        if (float.IsInfinity(pi.curr_status.velocity.z) || float.IsNaN(pi.curr_status.velocity.z))
        {
            Debug.Log("found invalid velocity value (component z)");
            Vector3 tmp_v = pi.curr_status.velocity;
            tmp_v.z = 0.0f;
            pi.curr_status.velocity = tmp_v;
        }

        Vector2 hv = new Vector2(pi.curr_status.velocity.x, pi.curr_status.velocity.z);
        Vector3 pvel = pi.curr_status.velocity + tmp_acc * Time.deltaTime;
        if (h_max_speed_limit != hv.magnitude)
        {
            Vector3 pvel_n = new Vector3(pvel.x, 0.0f, pvel.z).normalized;
            Vector3 dev = pvel_n * (h_max_speed_limit - hv.magnitude) * 0.65f;
            tmp_acc += dev;
            //Debug.Log(dev);
        }
        return tmp_acc;
    }

    int prevRotAction = -1; //0:left, 1:right
    float acc_err_floor_height = 0.0f;
    Vector3 rotationForce(PlayerInfo pi, Vector3 tmp_acc)
    {
        Vector3 r = (Vector3.Cross(Vector3.up, pi.curr_status.velocity)).normalized;
        //Vive input
        if (viveMode == true)
        {
            if (pi.viveStatus == ViveStatus.GLIDING_OR_TURNING)
            {
                if (pi.rotateActing == false)
                {
                    acc_err_target = 0;
                }

                pi.rotateActing = true;

                pi.rotationForce_Start_Tangent = pi.rotationForce_Velocity;
                pi.rotationForce_Start = pi.rotationForce_Current;
                pi.rotationForce_Target = pi.turningFactor / (90.0f / maxRotForce);
                pi.rotationForce_Counter = 0.0f;
                pi.rotationForce_Duration = rotDuration;

            }
            else
            {
                pi.rotateActing = false;

                //roll back
                pi.rotationForce_Start_Tangent = pi.rotationForce_Velocity;
                pi.rotationForce_Start = pi.rotationForce_Current;
                pi.rotationForce_Target = 0.0f;
                pi.rotationForce_Counter = 0.0f;
                pi.rotationForce_Duration = rotDuration;
            }
        }
        else
        {

            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetAxis("Horizontal") < -0.5f)
            {
                if (pi.rotateActing == true && prevRotAction == 1)
                {
                    //Debug.Log("rotate acting set as false");
                    pi.rotateActing = false;
                    prevRotAction = -1;

                    //roll back
                    pi.rotationForce_Start_Tangent = pi.rotationForce_Velocity;
                    pi.rotationForce_Start = pi.rotationForce_Current;
                    pi.rotationForce_Target = 0.0f;
                    pi.rotationForce_Counter = 0.0f;
                    pi.rotationForce_Duration = rotDuration;
                }
                if (pi.rotateActing == false)
                {
                    //Debug.Log("rotate acting ==false, turn left");
                    pi.rotateActing = true;
                    prevRotAction = 0;
                    acc_err_target = 0;

                    //turn left
                    pi.rotationForce_Start_Tangent = pi.rotationForce_Velocity;
                    pi.rotationForce_Start = pi.rotationForce_Current;
                    pi.rotationForce_Target = -maxRotForce;
                    pi.rotationForce_Counter = 0.0f;
                    pi.rotationForce_Duration = rotDuration;
                }

            }
            else
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetAxis("Horizontal") > 0.5f)
            {
                if (pi.rotateActing == true && prevRotAction == 0)
                {
                    //Debug.Log("rotate acting set as false");
                    pi.rotateActing = false;
                    prevRotAction = -1;

                    //roll back
                    pi.rotationForce_Start_Tangent = pi.rotationForce_Velocity;
                    pi.rotationForce_Start = pi.rotationForce_Current;
                    pi.rotationForce_Target = 0.0f;
                    pi.rotationForce_Counter = 0.0f;
                    pi.rotationForce_Duration = rotDuration;
                }
                if (pi.rotateActing == false)
                {
                    //Debug.Log("rotate acting ==false, turn right");
                    pi.rotateActing = true;
                    prevRotAction = 1;
                    acc_err_target = 0;

                    //turn right
                    pi.rotationForce_Start_Tangent = pi.rotationForce_Velocity;
                    pi.rotationForce_Start = pi.rotationForce_Current;
                    pi.rotationForce_Target = maxRotForce;
                    pi.rotationForce_Counter = 0.0f;
                    pi.rotationForce_Duration = rotDuration;
                }

            }
            else
            {

                if (pi.rotateActing == true)
                {
                    //Debug.Log("rotate acting set as false");
                    pi.rotateActing = false;
                    prevRotAction = -1;

                    //roll back
                    pi.rotationForce_Start_Tangent = pi.rotationForce_Velocity;
                    pi.rotationForce_Start = pi.rotationForce_Current;
                    pi.rotationForce_Target = 0.0f;
                    pi.rotationForce_Counter = 0.0f;
                    pi.rotationForce_Duration = rotDuration;
                }
            }
        }


        pi.rotationForce_Counter += Time.deltaTime / pi.rotationForce_Duration;
        if (pi.rotationForce_Counter < 1.0f)
        {
            float force = CurveUtil.HermiteInterpolation(pi.rotationForce_Counter, pi.rotationForce_Start, pi.rotationForce_Target, pi.rotationForce_Start_Tangent, 0.0f);
            pi.rotationForce_Velocity = (force - pi.rotationForce_Current) / Time.deltaTime * pi.rotationForce_Duration;
            pi.rotationForce_Current = force;
        }
        else
        {
            float force = pi.rotationForce_Target;
            pi.rotationForce_Velocity = (force - pi.rotationForce_Current) / Time.deltaTime * pi.rotationForce_Duration;
            pi.rotationForce_Current = force;
        }

        //calculate force
        float f = 0.0f;
        if (pi.rotationForce_Current != 0.0f)
        {
            if (easeRotationAngle == true)
            {

                if (pi.rotationForce_Current > 0)
                {
                    f = (float)CurveUtil.SineEaseInOut(pi.rotationForce_Current, 0, maxRotForce, maxRotForce);
                }
                else
                if (pi.rotationForce_Current < 0)
                {
                    f = (float)CurveUtil.SineEaseInOut(pi.rotationForce_Current, 0, -maxRotForce, maxRotForce);
                }
                else
                {
                    f = pi.rotationForce_Current;
                }
            }
            else
            {
                f = pi.rotationForce_Current;
            }

        }

        Vector3 vf = f * r;
        tmp_acc += vf;

        //
        //slowing down
        if (lowerSpeed /*&& pi.shield_enabled == false*/)
        {
            if (pi.rotationForce_Current != 0.0f)
            {
                float fratio_o = pi.rotationForce_Current / maxRotForce;
                float fratio = (float)CurveUtil.QuartEaseIn(Mathf.Abs(fratio_o), 0, 1, 1);

                //fade cockpit
                workingCockpit.GetComponent<MeshRenderer>().material.SetFloat("_Alpha", fratio);

                h_max_speed_limit = curr_max_horizontal_speed - (fratio * lowerSpeedRatio * curr_max_horizontal_speed);
            }
        }


        if (rotDecending == false && pi.rotateActing == true)
        {
            acc_err_target += -pi.curr_status.velocity.y;
            if (pi.curr_status.velocity.y < 0.0f)
            {
                tmp_acc += new Vector3(0.0f, -pi.curr_status.velocity.y / Time.deltaTime * 0.04f + 0.02f * acc_err_target, 0.0f);
            }
        }

        pi.rotForce = vf;


        return tmp_acc;
    }
    float h_max_speed_limit = 0.0f;
    float acc_err_target = 0.0f;

    Vector3 diveForceInput(PlayerInfo pi, Vector3 tmp_acc)
    {
        Vector3 acc = tmp_acc;

        //key input
        if (Input.GetKey(KeyCode.D) ||
          Input.GetAxis("Vertical") < -0.5f)
        {
            if (pi.diveActing == false)
            {
                pi.diveActing = true;

                //start dive
                pi.dive_counter = 0.0f;
                pi.dive_start_forward = pi.dive_curr_forward;
                pi.dive_start_tangent = pi.dive_curr_velocity;
                pi.dive_target_forward = diveAngle;

            }
        }
        else
        {
            if (pi.diveActing == true)
            {
                pi.diveActing = false;

                //end dive
                pi.dive_counter = 0.0f;
                pi.dive_start_forward = pi.dive_curr_forward;
                pi.dive_start_tangent = pi.dive_curr_velocity;
                pi.dive_target_forward = 0.0f;
            }
        }

        //
        //vive input
        //
        if (viveMode == true)
        {
            if (pi.viveStatus == ViveStatus.DIVE)
            {
                pi.diveActing = true;

                //start dive
                pi.dive_counter = 0.0f;
                pi.dive_start_forward = pi.dive_curr_forward;
                pi.dive_start_tangent = pi.dive_curr_velocity;
                pi.dive_target_forward = diveAngle;
            }
            else
            {
                pi.diveActing = false;

                //end dive
                pi.dive_counter = 0.0f;
                pi.dive_start_forward = pi.dive_curr_forward;
                pi.dive_start_tangent = pi.dive_curr_velocity;
                pi.dive_target_forward = 0.0f;
            }
        }

        return acc;
    }

    Vector3 ascendingForceInput(PlayerInfo pi, Vector3 tmp_acc)
    {
        Vector3 acc = tmp_acc;

        //key input
        if (Input.GetKeyDown(KeyCode.Space) ||
          Input.GetKeyDown("joystick button 1"))
        {
            pi.curvedForceCounter = 0.0f;
            pi.curveFunction = PlayerController.flyingForce;
            playOneShot(flapAudio1);
        }

        if (pi.curveFunction != null && pi.curvedForceCounter < pi.curvedForceDuration)
        {
            pi.curvedForceCounter += Time.deltaTime;
            acc = new Vector3(acc.x, (float)pi.curveFunction(pi.curvedForceCounter, 0.0, (double)flapForce * 5.0f, (double)pi.curvedForceDuration), acc.z);
        }

        //
        //vive input (flap, gliding)
        //
        if (viveMode == true)
        {
            acc += new Vector3(0.0f, ((pi.accumulatedFlapVelocity[0] + pi.accumulatedFlapVelocity[1]) * 1.25f * (flapForce / 10.0f)), 0.0f);
        }

        return acc;
    }

    bool input_launchTriggered()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            return true;
        }

        if (Input.GetKeyDown("joystick button 1"))
        {
            return true;
        }

        //vive method
        if (viveMode == true && current_status != Status.FLYING)
        {
            //if (trackerMode == false) {
            //  //controller
            //  var tracked_r_controller = vive_r_controller.GetComponent<SteamVR_TrackedObject>();
            //  if (tracked_r_controller.index != SteamVR_TrackedObject.EIndex.None) {
            //    var device_r = SteamVR_Controller.Input((int)tracked_r_controller.index);

            //    if (device_r.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger)) {
            //      //start game
            //      return true;
            //    }
            //  }
            //} else {
            //tracker
            //...
            Vector3 cc = new Vector3(0.0f, ((pi.accumulatedFlapVelocity[0] + pi.accumulatedFlapVelocity[1]) * 1.25f * (flapForce / 10.0f)), 0.0f);
            if (cc.y >= 75.0f)
            {
                return true;
            }

            //}
        }

        return false;
    }

    bool input_firemissileTriggered()
    {

        if (Input.GetKeyDown("joystick button 2"))
        {
            return true;
        }

        ////vive method
        //if (viveMode == true && current_status == Status.FLYING && trackerMode == false) {
        //  var tracked_l_controller = vive_l_controller.GetComponent<SteamVR_TrackedObject>();
        //  if (tracked_l_controller.index != SteamVR_TrackedObject.EIndex.None) {
        //    var device_l = SteamVR_Controller.Input((int)tracked_l_controller.index);

        //    if (device_l.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger)) {
        //      //start game
        //      return true;
        //    }
        //  }
        //}


        return false;
    }

    bool input_targetAttractionTriggered()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            return true;
        }

        if (Input.GetKeyDown("joystick button 2"))
        {
            return true;
        }

        ////vive method
        //if (viveMode == true && current_status == Status.FLYING && trackerMode == false) {
        //  var tracked_r_controller = vive_r_controller.GetComponent<SteamVR_TrackedObject>();
        //  if (tracked_r_controller.index != SteamVR_TrackedObject.EIndex.None) {
        //    var device_r = SteamVR_Controller.Input((int)tracked_r_controller.index);

        //    if (device_r.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger)) {
        //      //start game
        //      return true;
        //    }
        //  }
        //}

        return false;
    }

    public void startGame(bool multiplayer, BeaconAPLayer.MatchmakingRoomStatus mrs = null)
    {
        if (current_status != Status.PRE_LAUNCH)
            return;

        multi_player_mode = multiplayer;
        mCurrMRS = mrs;

        //记录所有人的分数
        for (int i = 0; i < mCurrMRS.groupChoosenPlayerList.Count; i++)
        {
            string guid = mCurrMRS.groupChoosenPlayerList[i].guid;
            tmpScoreStatistics.Add(guid, 0);
        }
        //记录队伍总分
        if (MatchmakingController.instance.gameMode == MatchmakingController.GameMode.TeamFight)
        {
            genSpawnPt();
            tmpTeamScoreStatistics.Add(0, 0);
            tmpTeamScoreStatistics.Add(1, 0);
        }



        //init cannons
        for (int i = 0; i < cannons.Length; ++i)
        {
            cannons[i].init(i, mCurrMRS.isHost(), cannonEventHandler);
        }

        if (multi_player_mode == false)
        {
            current_status = Status.LANDING_IDLE;

            gametimeView.GetComponent<GameTimeViewDisplayer>().showGameTime(true);
            labelUIController.instance.StartShowGoObj();
            gametimeView.GetComponent<GameTimeViewDisplayer>().resetTimer();
            labelUIController.instance.UnActiveUIObjs();
            shieldtimeView.GetComponent<ShieldDurationDisplayer>().resetTimer();
            acceleratorTimeView.GetComponent<ShieldDurationDisplayer>().resetTimer();

            EnvSoundObj.GetComponent<EnvironmentSound>().setGamePlaying(true);

            curr_shield_available_times = shield_available_times;
            curr_accelerator_available_times = accelerator_available_times;
            updateDisplayedScore();
        }
        else
        {

            //spawn hostile ?
            for (int i = 0; i < mrs.groupChoosenPlayerList.Count; ++i)
            {
                if (mrs.groupChoosenPlayerList[i].guid != mrs.myGuid)
                {
                    DumplingManager.TargetInfo info = dumplingManager.spawnHostile(mrs.groupChoosenPlayerList[i].guid, (int)mrs.groupChoosenPlayerList[i].groupid, (int)mrs.getMyPlayerInfo().groupid);
                    DumplingManager.instance.CreateHostileScoreUI(info);
                    DumplingManager.instance.CreatePlayerInGameIdUI(info, mrs.groupChoosenPlayerList[i].inGameId);
                }
            }

            if (MatchmakingController.instance.gameMode == MatchmakingController.GameMode.TeamFight)
            {
                //spawn big_bun
                for (int i = 0; i < mrs.groupChoosenPlayerList.Count; ++i)
                {
                    if (mrs.groupChoosenPlayerList[i].guid != mrs.myGuid)
                    {
                        Debug.Log("1868 - spawn big-bun (my unload pt idx =" + mrs.groupChoosenPlayerList[i].groupid + ")");
                        dumplingManager.spawnBigBun(mDicUnloadPointGameObject[(int)mrs.groupChoosenPlayerList[i].groupid]);
                        break;
                    }
                }
            }

            if (mCurrMRS.isHost() == true)
            {
                //current_status = Status.SYNC_SCENE_HOST;

                current_status = Status.WAIT_REMOTE_SCENE;
                Debug.Log("1594 - start sync scene (host)...");

                sceneReadyRemoteGUID.Clear();

                //gametimeView.GetComponent<GameTimeViewDisplayer>().showGameTime(true);
                //gametimeView.GetComponent<GameTimeViewDisplayer>().resetTimer();
            }
            else
            {
                //current_status = Status.SYNC_SCENE_REMOTE;

                current_status = Status.REPORT_SCENE_LOADED;
                Debug.Log("1604 - start sync scene (remote)...");

                //clear current dumpling setup
                dumplingManager.purgeSceneDumpling();
                dumplingManager.purgeSceneHostile();
                dumplingManager.purgeTrackedObj();

                report_counter = 0.0f;
            }

        }

    }

    Vector3 mMySpawnPosition = Vector3.zero;
    Quaternion mMySpawnRotation = Quaternion.identity;
    public void spawn(Vector3 pos, Quaternion q, bool p_viveMode, bool p_tracker)
    {
        inited = true;

        healthy_percentage = 100.0f;
        EnvSoundObj.GetComponent<EnvironmentSound>().setPlayWarn(false);
        warningView.GetComponent<WarningViewDisplayer>().showWarningView(false);
        warningView.GetComponent<WarningViewDisplayer>().resetTimer();

        Debug.LogWarning("spawn player with vive : " + p_viveMode + ", tracker = " + p_tracker);
        viveMode = p_viveMode;
        trackerMode = p_tracker;

        //dispose current setup
        disposeUser();

        mMySpawnPosition = pos;
        mMySpawnRotation = q;
        if (viveMode == false)
        {
            workingCamera = pc_camera;
            workingEye = pc_eye;
            workingGyroscope = pc_eye.transform.parent.gameObject;
            workingGyroscope.GetComponent<Gyroscope>().reset();
            workingCockpit = pc_eye.transform.parent.parent.Find("cockpit").gameObject;

            pc_camera.SetActive(true);
            vive_camera.SetActive(false);

            //GetComponent<TenkokuCameraSetup>().setupWorkingCamera(workingEye.GetComponent<Camera>());
        }
        else
        {
            workingCamera = vive_camera;
            workingEye = vive_camerarig;
            workingGyroscope = vive_hmd.transform.parent.gameObject;
            workingGyroscope.GetComponent<Gyroscope>().reset();
            workingCockpit = vive_hmd.transform.parent.parent.Find("cockpit").gameObject;

            vive_camera.SetActive(true);
            pc_camera.SetActive(false);

            vive_l_controller = vive_hmd.transform.parent.Find("Controller (left)").gameObject;
            vive_r_controller = vive_hmd.transform.parent.Find("Controller (right)").gameObject;

            //GetComponent<TenkokuCameraSetup>().setupWorkingCamera(vive_hmd.GetComponent<Camera>());

            //GFW Setup
            //boundWall.GetComponent<HoleUpdater>().trackedObj = vive_camera;
        }

        if (viveMode == true)
        {
            sightUIObj.GetComponent<SightDisplayer>().eyeObj = vive_hmd;
            dizzyView.GetComponent<DizzyViewDisplayer>().eyeObj = vive_hmd;
            warningView.GetComponent<WarningViewDisplayer>().eyeObj = vive_hmd;
            labelUIController.instance.eyeObj = vive_hmd;
            shieldtimeView.GetComponent<ShieldDurationDisplayer>().eyeObj = vive_hmd;
            acceleratorTimeView.GetComponent<ShieldDurationDisplayer>().eyeObj = vive_hmd;

            shieldIconObj_vive.transform.parent = vive_r_controller.transform;
            shieldIconObj_vive.transform.localPosition = new Vector3(0, 0.15f, 0);
            shieldIconObj_vive.transform.localRotation = Quaternion.identity;
            acceleratorIconObj_vive.transform.parent = vive_l_controller.transform;
            acceleratorIconObj_vive.transform.localPosition = new Vector3(0, 0.15f, 0);
            acceleratorIconObj_vive.transform.localRotation = Quaternion.identity;
        }
        else
        {
            sightUIObj.GetComponent<SightDisplayer>().eyeObj = workingEye;
            dizzyView.GetComponent<DizzyViewDisplayer>().eyeObj = workingEye;
            warningView.GetComponent<WarningViewDisplayer>().eyeObj = workingEye;
            labelUIController.instance.eyeObj = workingEye;
            shieldtimeView.GetComponent<ShieldDurationDisplayer>().eyeObj = workingEye;
            acceleratorTimeView.GetComponent<ShieldDurationDisplayer>().eyeObj = workingEye;
        }


        workingCamera.transform.localPosition = pos;
        workingCamera.transform.localRotation = q;
        Debug.Log("1327 - " + q + ", " + workingCamera.transform.localRotation.eulerAngles);

        resetPlayerInfo(workingCamera.transform);

        hud.transform.position = pi.curr_status.position;
        hud.transform.rotation = pi.curr_status.rotation;

        //reset amplitude
        workingEye.GetComponent<Klak.Motion.BrownianMotion>().rotationAmplitude = 0.0f;

        //reset radar lock status
        //dumplingManager.setRadarBottomRedImageFlash(false);

        current_status = Status.PRE_LAUNCH;
    }

    ////multiplayer func
    //public void spawn_hostile(string spawn_id, int team_id, Vector3 pos, Quaternion q) {
    //  int hid = dumplingManager.spawnHostile(spawn_id, team_id, (int)mCurrMRS.getMyPlayerInfo().groupid);
    //  dumplingManager.setHostilePosition(hid, pos, q, Vector3.zero, true);

    //}

    void sendMessageToNetwork(MultiplayerHandler.GameEvent e, List<object> extra_param, bool reliable = true)
    {
        //conversion
        //...
        if (registeredPlayerEvenet1Handler != null)
            registeredPlayerEvenet1Handler(e, extra_param, reliable);
    }

    void sendMessageToNetwork(string guid, MultiplayerHandler.GameEvent e, List<object> extra_param, bool reliable = true)
    {
        if (registeredPlayerEvent2Handler != null)
            registeredPlayerEvent2Handler(guid, e, extra_param, reliable);
    }

    float toFloat(object val)
    {
        float ret = 0;
        if (val.GetType() == typeof(double))
        {
            ret = (float)(double)val;
        }
        else
        if (val.GetType() == typeof(float))
        {
            ret = (float)val;
        }
        else
        if (val.GetType() == typeof(int))
        {
            ret = (float)(int)val;
        }
        else
        if (val.GetType() == typeof(uint))
        {
            ret = (float)(uint)val;
        }
        else
        if (val.GetType() == typeof(System.Int64))
        {
            ret = (float)(System.Int64)val;
        }
        else
        if (val.GetType() == typeof(System.UInt64))
        {
            ret = (float)(System.UInt64)val;
        }

        return ret;
    }

    int toInt(object val)
    {
        int ret = 0;
        if (val.GetType() == typeof(double))
        {
            ret = (int)(double)val;
        }
        else
        if (val.GetType() == typeof(float))
        {
            ret = (int)val;
        }
        else
        if (val.GetType() == typeof(int))
        {
            ret = (int)val;
        }
        else
        if (val.GetType() == typeof(uint))
        {
            ret = (int)(uint)val;
        }
        else
        if (val.GetType() == typeof(System.Int64))
        {
            ret = (int)(System.Int64)val;
        }
        else
        if (val.GetType() == typeof(System.UInt64))
        {
            ret = (int)(System.UInt64)val;
        }

        return ret;
    }

    //message from network
    public void processNetworkMsg(string src_guid, MultiplayerHandler.GameEvent e, List<object> extra_param)
    {
        switch (e)
        {
            case MultiplayerHandler.GameEvent.PLAYER_ACCELERATING_ENABLED_FROM_COLLIDER:
                {
                    if (current_status == Status.FLYING)
                    {
                        if (pi.p_accelerator_cold_time_counter <= 0.0f && pi.accelerator_enabled == false && curr_accelerator_available_times > 0)
                        {
                            enable_accelerator();
                            playOneShot(rocketSound, true);
                        }
                    }

                }
                break;

            case MultiplayerHandler.GameEvent.PLAYER_SHIELD_ENABLED_FROM_COLLIDER:
                {
                    if (current_status == Status.FLYING)
                    {
                        if (pi.p_shield_cold_time_counter <= 0.0f && pi.shield_enabled == false && curr_shield_available_times > 0)
                        {
                            enableShield();
                            playOneShot(shieldSound, true);
                        }
                    }
                }
                break;

            case MultiplayerHandler.GameEvent.SCENE_READY:
                {
                    if (current_status == Status.WAIT_REMOTE_SCENE)
                    {
                        //register ready scene
                        if (sceneReadyRemoteGUID.Contains(src_guid) == false)
                        {
                            Debug.Log("1736 - register scene ready client (" + src_guid + ")");
                            sceneReadyRemoteGUID.Add(src_guid);
                        }
                    }
                    else
                    {
                        Debug.Log("1741 - rcv SCENE_REDAY within wrong status : " + current_status);
                    }

                }
                break;

            case MultiplayerHandler.GameEvent.START_GAME_DUMPLING_SPAWN:
                {
                    List<object> dump_arr = (List<object>)extra_param[0];
                    int startVal = (int)(System.Int64)extra_param[1];
                    Debug.Log("1769 - receive dumpling spawn info. (count=" + (dump_arr.Count / 3) + ")");
                    for (int i = 0; i < dump_arr.Count / 3; ++i)
                    {
                        Vector3 dump_posi = new Vector3(toFloat(dump_arr[i * 3 + 0]), toFloat(dump_arr[i * 3 + 1]), toFloat(dump_arr[i * 3 + 2]));
                        dumplingManager.setDumplingPosition(startVal + i, dump_posi, Quaternion.identity, true);
                    }
                }
                break;

            case MultiplayerHandler.GameEvent.START_GAME_SHIELD_SPAWN:
                {
                    List<object> shield_arr = (List<object>)extra_param[0];
                    int startVal = (int)(System.Int64)extra_param[1];
                    Debug.Log("1769 - receive shield spawn info. (count=" + (shield_arr.Count / 3) + ")");
                    for (int i = 0; i < shield_arr.Count / 3; ++i)
                    {
                        Vector3 shield_posi = new Vector3(toFloat(shield_arr[i * 3 + 0]), toFloat(shield_arr[i * 3 + 1]), toFloat(shield_arr[i * 3 + 2]));
                        dumplingManager.setShieldPosition(startVal + i, shield_posi, Quaternion.identity, true);
                    }
                }
                break;

            case MultiplayerHandler.GameEvent.START_GAME_ACCELERATOR_SPAWN:
                {
                    List<object> acc_arr = (List<object>)extra_param[0];
                    int startVal = (int)(System.Int64)extra_param[1];
                    Debug.Log("1769 - receive accelerator spawn info. (count=" + (acc_arr.Count / 3) + ")");
                    for (int i = 0; i < acc_arr.Count / 3; ++i)
                    {
                        Vector3 shield_posi = new Vector3(toFloat(acc_arr[i * 3 + 0]), toFloat(acc_arr[i * 3 + 1]), toFloat(acc_arr[i * 3 + 2]));
                        dumplingManager.setAcceleratorPosition(startVal + i, shield_posi, Quaternion.identity, true);
                    }
                }
                break;

            case MultiplayerHandler.GameEvent.START_GAME_GATEWAY_SPAWN:
                {
                    Vector3 p0 = new Vector3(toFloat(extra_param[0]), toFloat(extra_param[1]), toFloat(extra_param[2]));
                    Vector3 p1 = new Vector3(toFloat(extra_param[3]), toFloat(extra_param[4]), toFloat(extra_param[5]));

                    Debug.Log("2802 - sync gateway position...");
                    transform.parent.Find("TransGateway/Wormhole1/Sphere").transform.localPosition = p0;
                    transform.parent.Find("TransGateway/Wormhole2/Sphere").transform.localPosition = p1;
                }
                break;

            case MultiplayerHandler.GameEvent.START_GAME:
                {
                    if (mCurrMRS.isHost() == false && current_status == Status.REPORT_SCENE_LOADED)
                    {

                        //START GAME (NETWORK)

                        Debug.Log("sync done. start game (remote)");
                        current_status = Status.LANDING_IDLE;

                        gametimeView.GetComponent<GameTimeViewDisplayer>().showGameTime(true);
                        labelUIController.instance.StartShowGoObj();
                        gametimeView.GetComponent<GameTimeViewDisplayer>().resetTimer();
                        labelUIController.instance.UnActiveUIObjs();
                        shieldtimeView.GetComponent<ShieldDurationDisplayer>().resetTimer();
                        acceleratorTimeView.GetComponent<ShieldDurationDisplayer>().resetTimer();

                        EnvSoundObj.GetComponent<EnvironmentSound>().setGamePlaying(true);

                        sendUserRespawnInfo();

                        curr_shield_available_times = shield_available_times;
                        curr_accelerator_available_times = accelerator_available_times;
                        updateDisplayedScore();

                    }

                }
                break;

            case MultiplayerHandler.GameEvent.PLAYER_MOVEMENT_INFO:
                {

                    MultiplayerHandler.GameSubEvent gse = (MultiplayerHandler.GameSubEvent)(System.Int64)extra_param[0];
                    switch (gse)
                    {
                        case MultiplayerHandler.GameSubEvent.LF_POSITION:
                            {
                                dumplingManager.setHostilePosition(
                                  dumplingManager.getHostileIdxByGUID(src_guid),
                                  new Vector3(toFloat(extra_param[1]), toFloat(extra_param[2]), toFloat(extra_param[3])),
                                  new Quaternion(toFloat(extra_param[4]), toFloat(extra_param[5]), toFloat(extra_param[6]), toFloat(extra_param[7])),
                                  new Vector3(toFloat(extra_param[8]), toFloat(extra_param[9]), toFloat(extra_param[10])),
                                  true
                                );

                            }
                            break;

                        case MultiplayerHandler.GameSubEvent.HMD_POSITION:
                            {
                                dumplingManager.setHostileHMDPosition(
                                  dumplingManager.getHostileIdxByGUID(src_guid),
                                  new Vector3(toFloat(extra_param[1]), toFloat(extra_param[2]), toFloat(extra_param[3]))
                                );

                            }
                            break;
                    }


                }
                break;

            case MultiplayerHandler.GameEvent.MISSILE_MOVEMENT_INFO:
                {
                    dumplingManager.setRemoteMissilePosition(src_guid,
                      (int)(System.Int64)extra_param[0],
                      new Vector3(toFloat(extra_param[1]), toFloat(extra_param[2]), toFloat(extra_param[3])),
                      new Quaternion(toFloat(extra_param[4]), toFloat(extra_param[5]), toFloat(extra_param[6]), toFloat(extra_param[7])),
                      new Vector3(toFloat(extra_param[8]), toFloat(extra_param[9]), toFloat(extra_param[10]))
                    );

                }
                break;

            case MultiplayerHandler.GameEvent.MISSILE_SELF_DESTROYED:
                {
                    int missile_idx = (int)(System.Int64)extra_param[0];
                    dumplingManager.setRemoteMissileDestroyed(src_guid, missile_idx);
                }
                break;

            case MultiplayerHandler.GameEvent.DUMPLING_RESET:
                {
                    int dump_id = (int)(System.Int64)extra_param[0];
                    Vector3 posi = new Vector3(toFloat(extra_param[1]), toFloat(extra_param[2]), toFloat(extra_param[3]));

                    if (dumplingManager.getTargetInfo(dump_id).curr_status != DumplingManager.TargetInfo.Status.DISPOSED)
                    {
                        Debug.LogWarning("2540 - host try to reset dumpling status with " + dumplingManager.getTargetInfo(dump_id).curr_status);
                    }

                    Debug.Log("2543 - reset dumpling : " + dump_id);
                    dumplingManager.resetDumpling(dump_id, posi);
                }
                break;

            case MultiplayerHandler.GameEvent.ACCELERATORTARGET_REST:
                {
                    int dump_id = (int)(System.Int64)extra_param[0];
                    Vector3 posi = new Vector3(toFloat(extra_param[1]), toFloat(extra_param[2]), toFloat(extra_param[3]));

                    if (dumplingManager.getTargetInfo(dump_id).curr_status != DumplingManager.TargetInfo.Status.DISPOSED)
                    {
                        Debug.LogWarning("2882 - host try to reset shield status with " + dumplingManager.getTargetInfo(dump_id).curr_status);
                    }

                    Debug.Log("2543 - reset accelerator : " + dump_id);
                    dumplingManager.resetAcceleratorTarget(dump_id, posi);
                }
                break;

            case MultiplayerHandler.GameEvent.SHIELDTARGET_RESET:
                {
                    int dump_id = (int)(System.Int64)extra_param[0];
                    Vector3 posi = new Vector3(toFloat(extra_param[1]), toFloat(extra_param[2]), toFloat(extra_param[3]));

                    if (dumplingManager.getTargetInfo(dump_id).curr_status != DumplingManager.TargetInfo.Status.DISPOSED)
                    {
                        Debug.LogWarning("2555 - host try to reset shield status with " + dumplingManager.getTargetInfo(dump_id).curr_status);
                    }

                    Debug.Log("2543 - reset shield : " + dump_id);
                    dumplingManager.resetShieldTarget(dump_id, posi);
                }
                break;

            case MultiplayerHandler.GameEvent.MISSILE_LAUNCH:
                {

                    int missile_idx = (int)(System.Int64)extra_param[0];
                    Vector3 posi = new Vector3(toFloat(extra_param[1]), toFloat(extra_param[2]), toFloat(extra_param[3]));
                    Vector3 vel = new Vector3(toFloat(extra_param[4]), toFloat(extra_param[5]), toFloat(extra_param[6]));
                    float missile_life = toFloat(extra_param[7]);

                    dumplingManager.setRemoteMissileLaunch((int)mCurrMRS.getMyPlayerInfo().groupid, src_guid, missile_idx, posi, Quaternion.identity, vel, missile_life);
                }
                break;

            case MultiplayerHandler.GameEvent.ACCELERATORTARGET_DESTROY_REQUEST:
                {
                    if (mCurrMRS.isHost())
                    {
                        int dump_idx = (int)(System.Int64)extra_param[0];
                        if (dumplingManager.acceleratortargetDestroyed(dump_idx))
                        {

                            //broadcast to all
                            sendMessageToNetwork(MultiplayerHandler.GameEvent.ACCELERATORTARGET_DESTROYED, new List<object>() {
              dump_idx
            });

                            //respawn
                            pendingRespawnAcceleratorTargetList.Add(dump_idx);
                        }
                    }
                }
                break;

            case MultiplayerHandler.GameEvent.DUMPLING_DESTROY_REQUEST:
                {
                    if (mCurrMRS.isHost())
                    {
                        int dump_idx = (int)(System.Int64)extra_param[0];
                        if (dumplingManager.targetDestroyed(dump_idx))
                        {

                            //broadcast to all
                            sendMessageToNetwork(MultiplayerHandler.GameEvent.DUMPLING_DESTROYED, new List<object>() {
              dump_idx
            });

                            //respawn dumpling
                            pendingRespawnTargetList.Add(dump_idx);
                        }
                    }
                }
                break;

            case MultiplayerHandler.GameEvent.SHIELDTARGET_DESTROY_REQUEST:
                {
                    if (mCurrMRS.isHost())
                    {
                        int dump_idx = (int)(System.Int64)extra_param[0];
                        if (dumplingManager.shieldtargetDestroyed(dump_idx))
                        {

                            //broadcast to all
                            sendMessageToNetwork(MultiplayerHandler.GameEvent.SHIELDTARGET_DESTROYED, new List<object>() {
              dump_idx
            });

                            //respawn
                            pendingRespawnShieldTargetList.Add(dump_idx);
                        }
                    }
                }
                break;

            //各別 client 向 leader 要求 collect dumpling ( 只有 leader 能處理此 request )
            case MultiplayerHandler.GameEvent.DUMPLING_COLLECTING_REQUEST:
                {
                    if (mCurrMRS.isHost())
                    {

                        //檢查完成之後散佈
                        string attracter_guid = (string)extra_param[0];
                        int dump_idx = (int)(System.Int64)extra_param[1];
                        Vector3 shift = new Vector3(toFloat(extra_param[2]), toFloat(extra_param[3]), toFloat(extra_param[4]));

                        Debug.Log("2196 - trial dumpling attraction, and broadcast result... (dumpling id=" + dump_idx + ")");
                        if (dumplingManager.remoteAttractTarget(attracter_guid, dump_idx, shift))
                        {

                            //broadcast to all
                            sendMessageToNetwork(MultiplayerHandler.GameEvent.DUMPLING_COLLECTING, new List<object>() {
              attracter_guid,
              dump_idx,
              shift.x, shift.y, shift.z }
                            );
                        }

                    }

                }
                break;

            case MultiplayerHandler.GameEvent.DUMPLING_LOSING_REQUEST:
                {
                    if (mCurrMRS.isHost())
                    {

                        //check and then broadcast to all
                        int total_dumplings = (extra_param.Count) / 4;
                        int[] dump_id_arr = new int[total_dumplings];
                        Vector3[] dump_losing_posi_arr = new Vector3[total_dumplings];

                        for (int i = 0; i < total_dumplings; ++i)
                        {
                            dump_id_arr[i] = (int)(System.Int64)extra_param[i * 4 + 0];

                            Vector3 tmp_v = new Vector3(
                                              toFloat(extra_param[i * 4 + 1]),
                                              toFloat(extra_param[i * 4 + 2]),
                                              toFloat(extra_param[i * 4 + 3])
                                            );
                            dump_losing_posi_arr[i] = tmp_v;
                        }

                        List<object> req_arr = new List<object>();
                        req_arr.Add(src_guid);

                        for (int i = 0; i < dump_id_arr.Length; ++i)
                        {
                            DumplingManager.TargetInfo tgid = dumplingManager.getTargetInfo(dump_id_arr[i]);
                            if (tgid != null)
                            {

                                if (dumplingManager.hostileTargetLosing(src_guid, tgid.id, dump_losing_posi_arr[i]))
                                {

                                    req_arr.Add(tgid.id);

                                    req_arr.Add(dump_losing_posi_arr[i].x);
                                    req_arr.Add(dump_losing_posi_arr[i].y);
                                    req_arr.Add(dump_losing_posi_arr[i].z);
                                }
                            }
                        }

                        //broadcast to all
                        //send message
                        dumplingEventHandler(MultiplayerHandler.GameEvent.DUMPLING_LOSING, req_arr.ToArray());

                    }
                }
                break;

            case MultiplayerHandler.GameEvent.DUMPLING_STEALING_REQUEST:
                {
                    if (mCurrMRS.isHost())
                    {

                        //檢查完成之後散佈
                        string attracter_guid = (string)extra_param[0];
                        Vector3 shift = new Vector3(toFloat(extra_param[1]), toFloat(extra_param[2]), toFloat(extra_param[3]));
                        int dump_idx = -1;
                        Debug.Log("2225 - trial stealing dumpling, and broadcast result...");
                        int gid = (int)mCurrMRS.getPlayerInfoByGUID(attracter_guid).groupid;
                        Debug.Log("2228 - attracter group id =" + gid);
                        gid = (gid + 1) % 2; //invert group position

                        //check remains dumpling
                        if (tmpTeamScoreStatistics[gid] <= 0)
                        {
                            Debug.LogWarning("2204 - insufficient dumpling to steal");
                            return;
                        }

                        Vector3 tgt_pt = mDicUnloadPointGameObject[gid].transform.localPosition;
                        Debug.Log("2230 - enemy position =" + tgt_pt + "(group id =" + gid + ")");
                        if (dumplingManager.remoteStealTarget(attracter_guid, tgt_pt - dumplingManager.gameObject.transform.localPosition, shift, out dump_idx))
                        {

                            //update score
                            tmpTeamScoreStatistics[gid]--;
                            if (tmpTeamScoreStatistics[gid] < 0)
                            {
                                tmpTeamScoreStatistics[gid] = 0;
                                Debug.LogError("2214 - dumpling score below zero");
                            }
                            updateDisplayedScore();

                            if (tmpTeamScoreStatistics[gid] <= 0)
                            {
                                Debug.LogWarning("2601 - animate bun scale to zero");
                                mDicUnloadPointGameObject[gid].GetComponent<UnloadBunAnim>().hideBigBun();

                            }
                            else
                            {
                                Debug.LogWarning("2611 - pumping bun scale");
                                mDicUnloadPointGameObject[gid].GetComponent<UnloadBunAnim>().pumpBun();

                            }

                            //broadcast to all
                            sendMessageToNetwork(MultiplayerHandler.GameEvent.DUMPLING_STEAL, new List<object>() {
              attracter_guid,
              dump_idx,
              shift.x, shift.y, shift.z,
              (tmpTeamScoreStatistics[gid] == 0)
            });

                        }

                    }
                }
                break;

            case MultiplayerHandler.GameEvent.ACCELERATORTARGET_COLLECTING_REQUEST:
                {
                    if (mCurrMRS.isHost())
                    {

                        //檢查完成之後散佈
                        string attracter_guid = (string)extra_param[0];
                        int dump_idx = (int)(System.Int64)extra_param[1];
                        Vector3 shift = new Vector3(toFloat(extra_param[2]), toFloat(extra_param[3]), toFloat(extra_param[4]));

                        Debug.Log("2695 - trial accelerator attraction, and broadcast result... (dumpling id=" + dump_idx + ")");
                        if (dumplingManager.remoteAttractAcceleratorTarget(attracter_guid, dump_idx, shift))
                        {

                            //broadcast to all
                            sendMessageToNetwork(MultiplayerHandler.GameEvent.ACCELERATORTARGET_COLLECTING, new List<object>() {
              attracter_guid,
              dump_idx,
              shift.x, shift.y, shift.z }
                            );
                        }

                    }
                }
                break;

            case MultiplayerHandler.GameEvent.SHIELDTARGET_COLLECTING_REQUEST:
                {
                    if (mCurrMRS.isHost())
                    {

                        //檢查完成之後散佈
                        string attracter_guid = (string)extra_param[0];
                        int dump_idx = (int)(System.Int64)extra_param[1];
                        Vector3 shift = new Vector3(toFloat(extra_param[2]), toFloat(extra_param[3]), toFloat(extra_param[4]));

                        Debug.Log("2695 - trial dumpling attraction, and broadcast result... (dumpling id=" + dump_idx + ")");
                        if (dumplingManager.remoteAttractShieldTarget(attracter_guid, dump_idx, shift))
                        {

                            //broadcast to all
                            sendMessageToNetwork(MultiplayerHandler.GameEvent.SHIELDTARGET_COLLECTING, new List<object>() {
              attracter_guid,
              dump_idx,
              shift.x, shift.y, shift.z }
                            );
                        }

                    }
                }
                break;

            case MultiplayerHandler.GameEvent.ACCELERATORTARGET_COLLECTING:
                {
                    string attracter = (string)extra_param[0];
                    int dump_id = (int)(System.Int64)extra_param[1];
                    Vector3 shift = new Vector3(toFloat(extra_param[2]), toFloat(extra_param[3]), toFloat(extra_param[4]));
                    if (attracter == mCurrMRS.myGuid)
                    {
                        Debug.Log("2715 - perform client accelerator colleciton (dump_id=" + dump_id + ")");
                        dumplingManager.clientAttractAcceleratorTarget(workingCamera, attracter, dump_id, shift);
                    }
                    else
                    {
                        Debug.Log("2718 - perform remote accelerator colleciton (dump_id=" + dump_id + ", remote id=" + attracter + ")");
                        dumplingManager.remoteAttractAcceleratorTarget(attracter, dump_id, shift);
                    }
                }
                break;

            case MultiplayerHandler.GameEvent.SHIELDTARGET_COLLECTING:
                {
                    string attracter = (string)extra_param[0];
                    int dump_id = (int)(System.Int64)extra_param[1];
                    Vector3 shift = new Vector3(toFloat(extra_param[2]), toFloat(extra_param[3]), toFloat(extra_param[4]));
                    if (attracter == mCurrMRS.myGuid)
                    {
                        Debug.Log("2715 - perform client shield colleciton (dump_id=" + dump_id + ")");
                        dumplingManager.clientAttractShieldTarget(workingCamera, attracter, dump_id, shift);
                    }
                    else
                    {
                        Debug.Log("2718 - perform remote shield colleciton (dump_id=" + dump_id + ", remote id=" + attracter + ")");
                        dumplingManager.remoteAttractShieldTarget(attracter, dump_id, shift);
                    }
                }
                break;

            case MultiplayerHandler.GameEvent.DUMPLING_COLLECTING:
                {
                    string attracter = (string)extra_param[0];
                    int dump_id = (int)(System.Int64)extra_param[1];
                    Vector3 shift = new Vector3(toFloat(extra_param[2]), toFloat(extra_param[3]), toFloat(extra_param[4]));
                    if (attracter == mCurrMRS.myGuid)
                    {
                        Debug.Log("2227 - perform client dumpling colleciton (dump_id=" + dump_id + ")");
                        dumplingManager.clientAttractTarget(workingCamera, attracter, dump_id, shift);
                    }
                    else
                    {
                        Debug.Log("2230 - perform remote dumpling colleciton (dump_id=" + dump_id + ", remote id=" + attracter + ")");
                        dumplingManager.remoteAttractTarget(attracter, dump_id, shift);
                    }
                }
                break;

            case MultiplayerHandler.GameEvent.DUMPLING_DESTROYED:
                {
                    int dump_id = (int)(System.Int64)extra_param[0];
                    dumplingManager.targetDestroyed(dump_id);
                }
                break;

            case MultiplayerHandler.GameEvent.SHIELDTARGET_DESTROYED:
                {
                    int dump_id = (int)(System.Int64)extra_param[0];
                    dumplingManager.shieldtargetDestroyed(dump_id);
                }
                break;

            case MultiplayerHandler.GameEvent.ACCELERATORTARGET_DESTROYED:
                {
                    int dump_id = (int)(System.Int64)extra_param[0];
                    dumplingManager.acceleratortargetDestroyed(dump_id);
                }
                break;

            case MultiplayerHandler.GameEvent.DUMPLING_LOSING:
                {
                    //owner
                    string dumpling_owner_guid = (string)extra_param[0];

                    int total_dumplings = (extra_param.Count - 1) / 4;

                    int[] dump_id_arr = new int[total_dumplings];
                    Vector3[] dump_losing_posi_arr = new Vector3[total_dumplings];

                    for (int i = 0; i < total_dumplings; ++i)
                    {
                        dump_id_arr[i] = (int)(System.Int64)extra_param[1 + (i * 4 + 0)];

                        Vector3 tmp_v = new Vector3(
                          toFloat(extra_param[1 + (i * 4 + 1)]),
                          toFloat(extra_param[1 + (i * 4 + 2)]),
                          toFloat(extra_param[1 + (i * 4 + 3)])
                        );
                        dump_losing_posi_arr[i] = tmp_v;
                    }


                    //ID
                    Debug.Log("2172 - hostile (" + dumpling_owner_guid + ") losing " + dump_id_arr.Length + " dumplings");
                    if (dumpling_owner_guid == mCurrMRS.myGuid)
                    {
                        for (int i = 0; i < total_dumplings; ++i)
                        {
                            DumplingManager.TargetInfo tgid = dumplingManager.getTargetInfo(dump_id_arr[i]);
                            if (tgid != null)
                            {
                                dumplingManager.targetLosing(tgid, dump_losing_posi_arr[i]);
                            }

                        }
                    }
                    else
                    {
                        for (int i = 0; i < total_dumplings; ++i)
                        {
                            dumplingManager.hostileTargetLosing(dumpling_owner_guid, dump_id_arr[i], dump_losing_posi_arr[i]);
                        }
                    }
                }
                break;

            case MultiplayerHandler.GameEvent.DUMPLING_STEAL:
                {

                    string thief = (string)extra_param[0];
                    int dump_id = (int)(System.Int64)extra_param[1];
                    Vector3 shift = new Vector3(toFloat(extra_param[2]), toFloat(extra_param[3]), toFloat(extra_param[4]));
                    bool hide_big_bun = (bool)extra_param[5];

                    int gid = -1;
                    gid = (int)mCurrMRS.getPlayerInfoByGUID(thief).groupid;
                    Debug.Log("2270 - attracter group id (self)=" + gid);
                    gid = (gid + 1) % 2; //invert group position
                    Vector3 tgt_pt = mDicUnloadPointGameObject[gid].transform.localPosition;
                    Debug.Log("2273 - enemy position =" + tgt_pt + "(group id =" + gid + ")");

                    if (thief == mCurrMRS.myGuid)
                    {
                        Debug.Log("2264 - perform client dumpling stealing (dump_id=" + dump_id + ")");
                        dumplingManager.clientStealTarget(workingCamera, thief, tgt_pt - dumplingManager.gameObject.transform.localPosition, shift, dump_id);
                    }
                    else
                    {
                        Debug.Log("2267 - perform remote dumpling stealing (dump_id=" + dump_id + ", remote id=" + thief + ")");
                        dumplingManager.remoteStealTarget(thief, tgt_pt - dumplingManager.gameObject.transform.localPosition, shift, dump_id);
                    }

                    //dumpling effect
                    if (hide_big_bun)
                    {
                        Debug.LogWarning("2601 - animate bun scale to zero");
                        mDicUnloadPointGameObject[gid].GetComponent<UnloadBunAnim>().hideBigBun();

                    }
                    else
                    {
                        Debug.LogWarning("2611 - pumping bun scale");
                        mDicUnloadPointGameObject[gid].GetComponent<UnloadBunAnim>().pumpBun();

                    }

                    //update score
                    tmpTeamScoreStatistics[gid]--;
                    if (tmpTeamScoreStatistics[gid] < 0)
                    {
                        tmpTeamScoreStatistics[gid] = 0;
                        Debug.LogError("2702 - dumpling score below zero");
                    }
                    updateDisplayedScore();
                }
                break;

            case MultiplayerHandler.GameEvent.DUMPLING_UNLOADING_FROM_COLLIDER:
                {
                    int pt_idx = (int)extra_param[0];
                    GameObject unload_go = mDicUnloadPointGameObject[pt_idx];
                    //dumplingManager.unload_my_dumpling(unload_go, pt_idx);
                    //processAndBroadcastDumplingScore();

                    //check score and collected dumpling
                    List<DumplingManager.TargetInfo> collected_dumpling = dumplingManager.getCollectedTarget(mCurrMRS.myGuid);
                    int score = tmpScoreStatistics[src_guid];
                    if (score != collected_dumpling.Count)
                    {
                        Debug.LogError("2734 - inconsistant statistics score and collection dumpling amount value");
                        Debug.LogError("2735 - dumpling_score =" + score + ", collected dumpling amount =" + collected_dumpling.Count);
                    }

                    List<int> tgt_arr = new List<int>();
                    List<object> req_arr = new List<object>();
                    req_arr.Add(mCurrMRS.myGuid);
                    req_arr.Add(pt_idx);
                    req_arr.Add(score); //score sum
                    for (int i = 0; i < collected_dumpling.Count; ++i)
                    {
                        req_arr.Add(collected_dumpling[i].id);
                        tgt_arr.Add(collected_dumpling[i].id);
                    }

                    int groupID = (int)mCurrMRS.getMyPlayerInfo().groupid;

                    Debug.Log("2751 - add score " + score + ", total=" + score + tmpTeamScoreStatistics[groupID]);
                    Debug.Log("2750 - found collected dumpling amount : " + collected_dumpling.Count);

                    if (mCurrMRS.isHost())
                    {
                        Debug.Log("2751 - purge dumplings (" + tgt_arr.Count + ")");
                        //purge all dumplings
                        //play unloading animation
                        dumplingManager.purgeDumpling(unload_go, pt_idx, tgt_arr);

                        //update score
                        tmpTeamScoreStatistics[groupID] += score;
                        tmpScoreStatistics[src_guid] = 0;
                        updateDisplayedScore();

                        //broadcast event (for player animation and update score)
                        sendMessageToNetwork(MultiplayerHandler.GameEvent.DUMPLING_UNLOADING, req_arr);


                    }
                    else
                    {
                        Debug.Log("2751 - request unloading dumpling...");

                        //request dumpling unloading
                        sendMessageToNetwork(mCurrMRS.gameLeaderGuid, MultiplayerHandler.GameEvent.DUMPLING_UNLOADING_REQUEST, req_arr);

                    }

                }
                break;

            case MultiplayerHandler.GameEvent.DUMPLING_UNLOADING_REQUEST:
                {
                    if (mCurrMRS.isHost())
                    {

                        string unloader_guid = (string)extra_param[0];
                        int pt_idx = (int)(System.Int64)extra_param[1];
                        int add_score = (int)(System.Int64)extra_param[2];

                        //dumpling idx
                        int[] dumpling_idx = new int[extra_param.Count - 3];
                        for (int i = 3; i < extra_param.Count; ++i)
                        {
                            dumpling_idx[i - 3] = (int)(System.Int64)extra_param[i];
                        }

                        //check dumpling
                        List<DumplingManager.TargetInfo> collected_dumpling = dumplingManager.getCollectedTarget(unloader_guid);
                        if (collected_dumpling.Count != dumpling_idx.Length)
                        {
                            Debug.LogError("2787 - inconsistant dumpling amount val, collected_dumpling.Count=" + collected_dumpling + ", dumpling_idx.Length=" + dumpling_idx.Length);
                        }

                        //overwrite dumping idx
                        List<int> dmp_arr = new List<int>();
                        List<object> req_arr = new List<object>();
                        req_arr.Add(unloader_guid);
                        req_arr.Add(pt_idx);
                        req_arr.Add(add_score);
                        for (int i = 0; i < collected_dumpling.Count; ++i)
                        {
                            req_arr.Add(collected_dumpling[i].id);
                            dmp_arr.Add(collected_dumpling[i].id);
                        }

                        GameObject unload_go = mDicUnloadPointGameObject[pt_idx];

                        //purge all dumplings
                        //play unloading animation
                        dumplingManager.purgeDumpling(unload_go, pt_idx, dmp_arr);

                        //update score

                        Debug.Log("2819 - add score " + add_score + ", total=" + tmpTeamScoreStatistics[(int)mCurrMRS.getPlayerInfoByGUID(unloader_guid).groupid] + add_score);
                        tmpTeamScoreStatistics[(int)mCurrMRS.getPlayerInfoByGUID(unloader_guid).groupid] += add_score;
                        //ss.dumpling_score = 0; //被 request 所以一定不是自己
                        updateDisplayedScore();
                        Debug.Log("2820 - found collected dumpling amount : " + collected_dumpling.Count);

                        //broadcast event (for player animation and update score)
                        sendMessageToNetwork(MultiplayerHandler.GameEvent.DUMPLING_UNLOADING, req_arr);
                    }
                }
                break;

            case MultiplayerHandler.GameEvent.DUMPLING_UNLOADING:
                {
                    string unloader_guid = (string)extra_param[0];
                    int pt_idx = (int)(System.Int64)extra_param[1];
                    int add_score = (int)(System.Int64)extra_param[2];

                    //dumpling idx
                    int[] dumpling_idx = new int[extra_param.Count - 3];
                    for (int i = 3; i < extra_param.Count; ++i)
                    {
                        dumpling_idx[i - 3] = (int)(System.Int64)extra_param[i];
                    }

                    GameObject unload_go = mDicUnloadPointGameObject[pt_idx];

                    //purge all dumplings
                    //play unloading animation
                    Debug.Log("2844 - purge dumplings (" + dumpling_idx.Length + "), unloader guid : " + unloader_guid);
                    dumplingManager.purgeDumpling(unload_go, pt_idx, new List<int>(dumpling_idx));

                    //check and re-sync dumpling
                    //check dumpling
                    List<DumplingManager.TargetInfo> collected_dumpling = dumplingManager.getCollectedTarget(unloader_guid);
                    if (collected_dumpling.Count > 0)
                    {
                        //re-sync dumpling
                        Debug.LogError("2852 - collected dumpling should be zero, bug found " + collected_dumpling.Count);

                        //...

                    }

                    //update score
                    int groupID = (int)mCurrMRS.getPlayerInfoByGUID(unloader_guid).groupid;
                    Debug.Log("2860 - add score " + add_score + ", total=" + add_score + tmpTeamScoreStatistics[groupID]);

                    tmpTeamScoreStatistics[groupID] += add_score;
                    if (unloader_guid == mCurrMRS.myGuid)
                    {
                        tmpScoreStatistics[mCurrMRS.myGuid] = 0;
                    }
                    updateDisplayedScore();

                }
                break;

            case MultiplayerHandler.GameEvent.MISSILE_LOCKED_BY_HOSTILE:
                {

                    dumplingManager.setRadarBottomRedImageFlash(true);
                }
                break;

            case MultiplayerHandler.GameEvent.MISSILE_HIT_TARGET:
                {
                    healthy_percentage -= missileDamage;
                    playDizzyView();

                    if (mCurrMRS.getMyPlayerInfo().groupid == 0)
                        playOneShot(underattack0);
                    else
                        playOneShot(underattack1);
                }
                break;

            case MultiplayerHandler.GameEvent.PLAYER_DEAD:
                {
                    dumplingManager.playHostileDeadAnimation(dumplingManager.getHostileIdxByGUID(src_guid), (int)mCurrMRS.getPlayerInfoByGUID(src_guid).groupid, (int)mCurrMRS.getMyPlayerInfo().groupid);

                    //清除所有鎖定該玩家的 missile
                    DumplingManager.TrackedMissileInfo[] tmiarr = dumplingManager.getTrackedMissile();
                    for (int i = 0; i < tmiarr.Length; ++i)
                    {
                        if (tmiarr[i].target != null && tmiarr[i].target.player_guid == src_guid)
                        {
                            //...
                            tmiarr[i].obj.GetComponent<MissileBehaviours.Actions.MissileRepoter>().missile_life = 0.0f;


                        }

                    }

                }
                break;

            case MultiplayerHandler.GameEvent.PLAYER_SHIELD_ENABLED:
                {
                    //active shield
                    Debug.Log("1861 - set hostile shield : true");
                    float duration = toFloat(extra_param[0]);
                    dumplingManager.setHostileShield(dumplingManager.getHostileIdxByGUID(src_guid), true, duration);

                }
                break;

            case MultiplayerHandler.GameEvent.PLAYER_SHIELD_DISABLED:
                {
                    //deactive shield
                    Debug.Log("1869 - set hostile shield : false");
                    dumplingManager.setHostileShield(dumplingManager.getHostileIdxByGUID(src_guid), false);

                }
                break;

            case MultiplayerHandler.GameEvent.REQUEST_GATEWAY_OPEN:
                {
                    if (mCurrMRS.isHost())
                    {
                        string gn = (string)extra_param[0];

                        Debug.Log("3465 - request to close gateway (path:TransGateway/" + gn + ", current gameobject name:" + gameObject.name + ")");

                        TransGateway tg = transform.parent.Find("TransGateway/" + gn).GetComponent<TransGateway>();
                        if (tg.isEnabled() == false)
                        {
                            if (tg.setOpen((int)mCurrMRS.getMyPlayerInfo().groupid))
                            {

                                //broadcast msg for door open
                                sendMessageToNetwork(MultiplayerHandler.GameEvent.GATEWAY_OPENED,
                                  new List<object>() {
                  gn
                                  });
                            }
                        }

                    }
                }
                break;

            case MultiplayerHandler.GameEvent.GATEWAY_OPENED:
                {
                    string gn = (string)extra_param[0];

                    Debug.Log("3486 - request to open gateway (path:TransGateway/" + gn + ", current gameobject name:" + gameObject.name + ")");

                    TransGateway tg = transform.parent.Find("TransGateway/" + gn).GetComponent<TransGateway>();
                    tg.setOpen((int)mCurrMRS.getMyPlayerInfo().groupid, true);
                }
                break;

            case MultiplayerHandler.GameEvent.REQUEST_GATEWAY_CLOSE:
                {
                    if (mCurrMRS.isHost())
                    {
                        string gn = (string)extra_param[0];

                        Debug.Log("3497 - request to close gateway (path:TransGateway/" + gn + ", current gameobject name:" + gameObject.name + ")");

                        TransGateway tg = transform.parent.Find("TransGateway/" + gn).GetComponent<TransGateway>();
                        Vector3 np = tg.gameObject.GetComponent<GatewayPlacement>().prepareNextGateway();

                        if (tg.isEnabled())
                        {
                            if (tg.setClose(np))
                            {

                                //broadcast msg for door close
                                sendMessageToNetwork(MultiplayerHandler.GameEvent.GATEWAY_CLOSED,
                                  new List<object>() {
                  np.x, np.y, np.z, gn
                                  });
                            }
                        }
                    }
                }
                break;

            case MultiplayerHandler.GameEvent.GATEWAY_CLOSED:
                {
                    Vector3 new_posi = new Vector3(toFloat(extra_param[0]), toFloat(extra_param[1]), toFloat(extra_param[2]));
                    string gn = (string)extra_param[3];

                    Debug.Log("3520 - request to close gateway (path:TransGateway/" + gn + ", current gameobject name:" + gameObject.name + ")");

                    TransGateway tg = transform.parent.Find("TransGateway/" + gn).GetComponent<TransGateway>();
                    tg.setClose(new_posi, true);
                }
                break;

            case MultiplayerHandler.GameEvent.FIRE_CANNON:
                {
                    if (mCurrMRS.isHost() == true)
                        return;

                    //manually fire cannon
                    int cannon_id = (int)(System.Int64)extra_param[0];
                    int ball_id = (int)(System.Int64)extra_param[1];
                    //Debug.Log("2666 - manuallyFireCannon (cid=" + cannon_id + ", bid=" + ball_id + ")");
                    cannons[cannon_id].manuallyFireCannon(ball_id);

                }
                break;

            case MultiplayerHandler.GameEvent.CANNON_EXPLODE:
                {
                    if (mCurrMRS.isHost() == true)
                        return;

                    //manually explode cannon
                    int cannon_id = (int)(System.Int64)extra_param[0];
                    int ball_id = (int)(System.Int64)extra_param[1];
                    //Debug.Log("2666 - manuallyExplodeCannonBall (cid=" + cannon_id + ", bid=" + ball_id + ")");
                    cannons[cannon_id].manuallyExplodeCannonBall(ball_id);
                }
                break;

            case MultiplayerHandler.GameEvent.CANNON_COLLIDE_WITH_PLAYER:
                {
                    string target_guid = (string)extra_param[0];

                    if (mCurrMRS.myGuid == target_guid && pi.shield_enabled == false)
                    {

                        healthy_percentage -= 15.0f;
                        playDizzyView();

                        if (mCurrMRS.getMyPlayerInfo().groupid == 0)
                            playOneShot(underattack0);
                        else
                            playOneShot(underattack1);

                    }
                }
                break;
            case MultiplayerHandler.GameEvent.SCORE_UPDATE_REQUEST:
                {
                    if (mCurrMRS.isHost())
                    {
                        MultiplayerHandler.GameSubEvent gse = (MultiplayerHandler.GameSubEvent)(System.Int64)extra_param[0];
                        UpdateTmpScoreStatistics(gse, extra_param[1].ToString());
                        sendMessageToNetwork(MultiplayerHandler.GameEvent.SCORE_UPDATE, extra_param);
                    }
                }
                break;
            case MultiplayerHandler.GameEvent.SCORE_UPDATE:
                {
                    MultiplayerHandler.GameSubEvent gse = (MultiplayerHandler.GameSubEvent)(System.Int64)extra_param[0];
                    UpdateTmpScoreStatistics(gse, extra_param[1].ToString());
                }
                break;
                /*
                case MultiplayerHandler.GameEvent.START_GAME_PLAYER_SPAWN: {
                  if (current_status != Status.SYNC_SCENE_REMOTE)
                    return;

                  bool host = (bool)extra_param[0];
                  int s_id = (int)extra_param[3];
                  Vector3 lp = (Vector3)extra_param[1];
                  Quaternion lr = (Quaternion)extra_param[2];

                  if (mCurrMRS.isHost() == host) {
                    //my position
                    playOneShot(respawn, true);
                    spawn(lp, lr, viveMode);
                    current_status = Status.SYNC_SCENE_REMOTE;
                  } else {
                    //hostile position
                    spawn_hostile(lp, lr, s_id, host);

                  }
                }
                break;
                */

                //case MultiplayerHandler.GameEvent.DUMPLING_MOVEMENT_INFO: {

                //  if (current_status == Status.LANDING_IDLE || current_status == Status.FLYING) {
                //    dumplingManager.setDumplingPosition(
                //      (int)extra_param[0],
                //      new Vector3(toFloat(extra_param[1]), toFloat(extra_param[2]), toFloat(extra_param[3])),
                //      new Quaternion(toFloat(extra_param[4]), toFloat(extra_param[5]), toFloat(extra_param[6]), toFloat(extra_param[7])), 
                //      false);
                //  }
                //}
                //break;

                /*
                case MultiplayerHandler.GameEvent.RADAR_LOCKED_BY_HOSTILE: {
                  sightUIObj.GetComponent<SightDisplayer>().setRadarLock(true);
                }
                break;

                case MultiplayerHandler.GameEvent.RADAR_UNLOCK: {
                  sightUIObj.GetComponent<SightDisplayer>().setRadarLock(false);
                }
                break;
                */

                /*
                case MultiplayerHandler.GameEvent.MISSILE_UNLOCK: {
                  //sightUIObj.GetComponent<SightDisplayer>().setMissileLock(false);
                  if (current_status == Status.LANDING_IDLE || current_status == Status.FLYING)
                    dumplingManager.setRadarBottomRedImageFlash(false);
                }
                break;
                */

                //case MultiplayerHandler.GameEvent.PLAYER_ACTION: {
                //  if (current_status == Status.LANDING_IDLE || current_status == Status.FLYING) {
                //    MultiplayerHandler.GameSubEvent gse = (MultiplayerHandler.GameSubEvent)(System.Int64)extra_param[0];
                //    switch (gse) {
                //      case MultiplayerHandler.GameSubEvent.DUMPLING_UNLOADING: { //由網路乎叫, 直接顯示敵人分數
                //        int hostile_score = (int)(System.Int64)extra_param[1];
                //        int hostile_unload_score = (int)(System.Int64)extra_param[2];

                //        dumplingManager.purgeHostileDumpling();
                //        dumplingManager.setupHostileScore(hostile_unload_score);
                //      }
                //      break;
                //    }
                //  }
                //}
                //break;

        }


    }

    public void playDizzyView()
    {
        if (viveMode)
        {
            dizzyView.transform.Find("Root").transform.localScale = Vector3.one;
            dizzyView.transform.Find("Root").transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
        }
        else
        {
            dizzyView.transform.Find("Root").transform.localScale = Vector3.one * 50.0f;
            dizzyView.transform.Find("Root").transform.localPosition = new Vector3(0.0f, 0.0f, 1.0f);
        }
        dizzyView.GetComponent<DizzyViewDisplayer>().showDizzyView();
    }

    /// <summary>
    /// 基地包子数，key是groupID
    /// </summary>
    Dictionary<int, int> tmpTeamScoreStatistics = new Dictionary<int, int>();
    /// <summary>
    /// 玩家身上包子数，key是guid
    /// </summary>
    Dictionary<string, int> tmpScoreStatistics = new Dictionary<string, int>();

    void updateDisplayedScore()
    {
        Debug.Log("2771 - update score value...");

        List<int> otherPlayerDumplingScore = new List<int>();
        int myDumplingScore = 0;
        foreach (string ss in tmpScoreStatistics.Keys)
        {
            if (ss == mCurrMRS.getMyPlayerInfo().guid)
                myDumplingScore = tmpScoreStatistics[ss];
            else
                otherPlayerDumplingScore.Add(tmpScoreStatistics[ss]);
        }

        dumplingManager.setupScore(curr_accelerator_available_times, curr_shield_available_times, myDumplingScore, otherPlayerDumplingScore);

        if (MatchmakingController.instance.gameMode == MatchmakingController.GameMode.FreeFight)
            return;

        int myteamScore = 0;
        int hostileTeamScore = 0;
        {
            if (mCurrMRS.getMyPlayerInfo().groupid == BeaconAPLayer.Group.TEAM1)
            {
                myteamScore = tmpTeamScoreStatistics[0];
                hostileTeamScore = tmpTeamScoreStatistics[1];
            }
            else
            {
                myteamScore = tmpTeamScoreStatistics[1];
                hostileTeamScore = tmpTeamScoreStatistics[0];
            }
        }
        dumplingManager.setupTeamScore(myteamScore, hostileTeamScore);
    }

    //message from cannon
    public void cannonEventHandler(int cannon_id, CannonBall.CannonEvent event_type, object param)
    {
        if (mCurrMRS.isHost() == false)
            return;

        //broadcast event
        if (event_type == CannonBall.CannonEvent.FIRE)
        {
            object[] parr = (object[])param;
            sendMessageToNetwork(MultiplayerHandler.GameEvent.FIRE_CANNON, new List<object>() { cannon_id, (int)parr[0] }); //cannon_id, ball_id
        }
        else
        if (event_type == CannonBall.CannonEvent.COLLIDE_WITH_PLAYER)
        {
            if (mCurrMRS.isHost() == false)
                return;

            object[] parr = (object[])param;
            sendMessageToNetwork(MultiplayerHandler.GameEvent.CANNON_EXPLODE, new List<object>() { cannon_id, (int)parr[0] }); //cannon_id, ball_id

            string target = (string)parr[1];
            if (target == "PlayerCollider")
            {
                //shiled enabled
                if (pi.shield_enabled == false)
                {
                    //自己扣血
                    healthy_percentage -= 15.0f;
                    playDizzyView();

                    if (mCurrMRS.getMyPlayerInfo().groupid == 0)
                        playOneShot(underattack0);
                    else
                        playOneShot(underattack1);
                }

            }
            else
            {
                //通知其它 player 扣血
                //哪個 player ?
                string target_guid = dumplingManager.getHostileNetworkUID(target);
                if (target_guid != null)
                {
                    sendMessageToNetwork(target_guid, MultiplayerHandler.GameEvent.CANNON_COLLIDE_WITH_PLAYER, new List<object>() { target_guid });
                }
            }
        }


    }

    public float respawnTargetTime = 10;
    float countDownRespawnTarget;
    List<int> pendingRespawnTargetList = new List<int>();
    List<int> pendingRespawnShieldTargetList = new List<int>();
    List<int> pendingRespawnAcceleratorTargetList = new List<int>();

    void UpdateTmpScoreStatistics(MultiplayerHandler.GameSubEvent gse, string guid)
    {


        //处理不同的分数变化请求
        if (gse == MultiplayerHandler.GameSubEvent.DUMPLING_SCORE_DECREASE_ONE)
        {
            tmpScoreStatistics[guid]--;
            if (tmpScoreStatistics[guid] < 0)
            {
                Debug.LogError("3088 - ss.dumpling_score below zero !");
                tmpScoreStatistics[guid] = 0;
            }
            Debug.Log("2329 - updated dumpling_score (minus) = " + tmpScoreStatistics[guid]);
            updateDisplayedScore();
        }
        else
        if (gse == MultiplayerHandler.GameSubEvent.DUMPLING_SCORE_INCREASE_ONE)
        {
            tmpScoreStatistics[guid]++;

            Debug.Log("2335 - updated dumpling_score (add) = " + tmpScoreStatistics[guid]);
            updateDisplayedScore();
        }
        else
        if (gse == MultiplayerHandler.GameSubEvent.SHIELDTARGET_SCORE_INCREASE_ONE)
        {
            curr_shield_available_times++;

            updateDisplayedScore();
        }
        else
        if (gse == MultiplayerHandler.GameSubEvent.ACCELERATORTARGET_SCORE_INCREASE_ONE)
        {
            curr_accelerator_available_times++;
            updateDisplayedScore();
        }
    }

    //message from dumpling to other player
    public void dumplingEventHandler(MultiplayerHandler.GameEvent e, object extra_param)
    {
        if (e == MultiplayerHandler.GameEvent.SCORE_UPDATE_REQUEST)
        {
            MultiplayerHandler.GameSubEvent gse = (MultiplayerHandler.GameSubEvent)((object[])extra_param)[0];

            if (mCurrMRS.isHost())
            {
                //PROCESS SCORE THEN BROADCAST TO ALL
                UpdateTmpScoreStatistics(gse, mCurrMRS.myGuid);
                sendMessageToNetwork(MultiplayerHandler.GameEvent.SCORE_UPDATE, new List<object>() { (int)gse, mCurrMRS.myGuid });
            }
            else
            {
                //SEND EVENT TO HOST
                sendMessageToNetwork(mCurrMRS.gameLeaderGuid, MultiplayerHandler.GameEvent.SCORE_UPDATE_REQUEST, new List<object>() { (int)gse, mCurrMRS.myGuid });
            }
        }
        else
        if (e == MultiplayerHandler.GameEvent.SHIELDTARGET_DESTROY_REQUEST)
        {
            int dump_idx = (int)((object[])extra_param)[0];

            if (mCurrMRS.isHost())
            {
                if (dumplingManager.shieldtargetDestroyed(dump_idx))
                {

                    //broadcast to all
                    sendMessageToNetwork(MultiplayerHandler.GameEvent.SHIELDTARGET_DESTROYED, new List<object>(){
            dump_idx
          });

                    //respawn dumpling
                    pendingRespawnShieldTargetList.Add(dump_idx);

                }
            }
            else
            {
                sendMessageToNetwork(mCurrMRS.gameLeaderGuid, MultiplayerHandler.GameEvent.SHIELDTARGET_DESTROY_REQUEST, new List<object>() {
          dump_idx
        });

            }


        }
        else
        if (e == MultiplayerHandler.GameEvent.ACCELERATORTARGET_DESTROY_REQUEST)
        {
            int dump_idx = (int)((object[])extra_param)[0];

            if (mCurrMRS.isHost())
            {
                if (dumplingManager.acceleratortargetDestroyed(dump_idx))
                {

                    //broadcast to all
                    sendMessageToNetwork(MultiplayerHandler.GameEvent.ACCELERATORTARGET_DESTROYED, new List<object>(){
            dump_idx
          });

                    //respawn dumpling
                    pendingRespawnAcceleratorTargetList.Add(dump_idx);

                }
            }
            else
            {
                sendMessageToNetwork(mCurrMRS.gameLeaderGuid, MultiplayerHandler.GameEvent.ACCELERATORTARGET_DESTROY_REQUEST, new List<object>() {
          dump_idx
        });

            }

        }
        else
        if (e == MultiplayerHandler.GameEvent.DUMPLING_DESTROY_REQUEST)
        {
            int dump_idx = (int)((object[])extra_param)[0];

            if (mCurrMRS.isHost())
            {
                if (dumplingManager.targetDestroyed(dump_idx))
                {

                    //broadcast to all
                    sendMessageToNetwork(MultiplayerHandler.GameEvent.DUMPLING_DESTROYED, new List<object>(){
            dump_idx
          });

                    //respawn dumpling
                    //...
                    pendingRespawnTargetList.Add(dump_idx);

                }
            }
            else
            {
                sendMessageToNetwork(mCurrMRS.gameLeaderGuid, MultiplayerHandler.GameEvent.DUMPLING_DESTROY_REQUEST, new List<object>() {
          dump_idx
        });

            }

        }
        else
        if (e == MultiplayerHandler.GameEvent.DUMPLING_STEALING_REQUEST)
        {
            float shift_x = UnityEngine.Random.Range(-15.0f, 15.0f);
            float shift_y = UnityEngine.Random.Range(-15.0f, 15.0f);
            float shift_z = UnityEngine.Random.Range(-5.0f, 30.0f);

            //SEND TO HOST, (OR PROCESS SCORE THEN BROADCAST TO ALL)
            if (mCurrMRS.isHost())
            {
                Debug.Log("2486 - dumpling stealing reqeust from host, achive command and broadcast result...");
                int dump_idx = -1;

                int gid = (int)mCurrMRS.getMyPlayerInfo().groupid;
                Debug.Log("2541 - attracter group id (self)=" + gid);
                gid = (gid + 1) % 2; //invert group position
                Vector3 tgt_pt = mDicUnloadPointGameObject[gid].transform.localPosition;
                Debug.Log("2544 - enemy position =" + tgt_pt + "(group id =" + gid + ")");

                //CHECK remains DUMPLINGS
                if (tmpTeamScoreStatistics[gid] <= 0)
                {
                    Debug.LogWarning("2580 - insufficient dumpling to steal");
                    return;
                }

                if (dumplingManager.clientStealTarget(workingCamera, mCurrMRS.myGuid, tgt_pt - dumplingManager.gameObject.transform.localPosition, new Vector3(shift_x, shift_y, shift_z), out dump_idx))
                {

                    //update score
                    tmpTeamScoreStatistics[gid]--;
                    if (tmpTeamScoreStatistics[gid] < 0)
                    {
                        tmpTeamScoreStatistics[gid] = 0;
                        Debug.LogError("2589 - dumpling score below zero");
                    }
                    updateDisplayedScore();

                    if (tmpTeamScoreStatistics[gid] <= 0)
                    {
                        Debug.LogWarning("2601 - animate bun scale to zero");
                        mDicUnloadPointGameObject[gid].GetComponent<UnloadBunAnim>().hideBigBun();

                    }
                    else
                    {
                        Debug.LogWarning("2611 - pumping bun scale");
                        mDicUnloadPointGameObject[gid].GetComponent<UnloadBunAnim>().pumpBun();

                    }

                    //broadcast to all
                    sendMessageToNetwork(MultiplayerHandler.GameEvent.DUMPLING_STEAL, new List<object>() {
            mCurrMRS.myGuid,
            dump_idx,
            shift_x, shift_y, shift_z,
            (tmpTeamScoreStatistics[gid] == 0)
          });
                }
            }
            else
            {
                Debug.Log("2502 - relay stealing command to host...");
                sendMessageToNetwork(mCurrMRS.gameLeaderGuid, MultiplayerHandler.GameEvent.DUMPLING_STEALING_REQUEST, new List<object>() {
          mCurrMRS.myGuid,
          shift_x, shift_y, shift_z
        });
            }

        }
        else
        if (e == MultiplayerHandler.GameEvent.SHIELDTARGET_COLLECTING_REQUEST)
        {
            int dump_idx = (int)((object[])extra_param)[0];

            float shift_x = UnityEngine.Random.Range(-15.0f, 15.0f);
            float shift_y = UnityEngine.Random.Range(-15.0f, 15.0f);
            float shift_z = UnityEngine.Random.Range(-5.0f, 30.0f);

            //SEND TO HOST, (OR PROCESS SCORE THEN BROADCAST TO ALL)
            if (mCurrMRS.isHost())
            {
                Debug.Log("3355 - shield_obj attraction request from host, achive command and broadcast result...");

                //process and then distribute outcome message
                if (dumplingManager.clientAttractShieldTarget(workingCamera, mCurrMRS.myGuid, dump_idx, new Vector3(shift_x, shift_y, shift_z)))
                {

                    //broadcast to all
                    sendMessageToNetwork(MultiplayerHandler.GameEvent.SHIELDTARGET_COLLECTING, new List<object>() {
            mCurrMRS.myGuid,
            dump_idx,
            shift_x, shift_y, shift_z }
                    );
                }

            }
            else
            {
                Debug.Log("3369 - relay attraction command to host...");
                //send message to host
                sendMessageToNetwork(mCurrMRS.gameLeaderGuid, MultiplayerHandler.GameEvent.SHIELDTARGET_COLLECTING_REQUEST, new List<object>() {
          mCurrMRS.myGuid,
          dump_idx,
          shift_x, shift_y, shift_z
        });
            }


        }
        else
        if (e == MultiplayerHandler.GameEvent.ACCELERATORTARGET_COLLECTING_REQUEST)
        {
            int dump_idx = (int)((object[])extra_param)[0];

            float shift_x = UnityEngine.Random.Range(-15.0f, 15.0f);
            float shift_y = UnityEngine.Random.Range(-15.0f, 15.0f);
            float shift_z = UnityEngine.Random.Range(-5.0f, 30.0f);

            //SEND TO HOST, (OR PROCESS SCORE THEN BROADCAST TO ALL)
            if (mCurrMRS.isHost())
            {
                Debug.Log("3558 - accelerator attraction request from host, achive command and broadcast result...");

                //process and then distribute outcome message
                if (dumplingManager.clientAttractAcceleratorTarget(workingCamera, mCurrMRS.myGuid, dump_idx, new Vector3(shift_x, shift_y, shift_z)))
                {

                    //broadcast to all
                    sendMessageToNetwork(MultiplayerHandler.GameEvent.ACCELERATORTARGET_COLLECTING, new List<object>() {
            mCurrMRS.myGuid,
            dump_idx,
            shift_x, shift_y, shift_z }
                    );
                }

            }
            else
            {
                Debug.Log("3572 - relay attraction command to host...");
                //send message to host
                sendMessageToNetwork(mCurrMRS.gameLeaderGuid, MultiplayerHandler.GameEvent.ACCELERATORTARGET_COLLECTING_REQUEST, new List<object>() {
          mCurrMRS.myGuid,
          dump_idx,
          shift_x, shift_y, shift_z
        });
            }
        }
        else
        if (e == MultiplayerHandler.GameEvent.DUMPLING_COLLECTING_REQUEST)
        {
            int dump_idx = (int)((object[])extra_param)[0];

            float shift_x = UnityEngine.Random.Range(-15.0f, 15.0f);
            float shift_y = UnityEngine.Random.Range(-15.0f, 15.0f);
            float shift_z = UnityEngine.Random.Range(-5.0f, 30.0f);

            //SEND TO HOST, (OR PROCESS SCORE THEN BROADCAST TO ALL)
            if (mCurrMRS.isHost())
            {
                Debug.Log("2488 - dumpling attraction request from host, achive command and broadcast result...");

                //process and then distribute outcome message
                if (dumplingManager.clientAttractTarget(workingCamera, mCurrMRS.myGuid, dump_idx, new Vector3(shift_x, shift_y, shift_z)))
                {

                    //broadcast to all
                    sendMessageToNetwork(MultiplayerHandler.GameEvent.DUMPLING_COLLECTING, new List<object>() {
            mCurrMRS.myGuid,
            dump_idx,
            shift_x, shift_y, shift_z }
                    );
                }

            }
            else
            {
                Debug.Log("2502 - relay attraction command to host...");
                //send message to host
                sendMessageToNetwork(mCurrMRS.gameLeaderGuid, MultiplayerHandler.GameEvent.DUMPLING_COLLECTING_REQUEST, new List<object>() {
          mCurrMRS.myGuid,
          dump_idx,
          shift_x, shift_y, shift_z
        });
            }


        }
        else
        if (e == MultiplayerHandler.GameEvent.DUMPLING_COLLECTED)
        {
            playOneShot(dumplingAttracted, true);

        }
        else
        if (e == MultiplayerHandler.GameEvent.DUMPLING_LOOSING_ANIM)
        {
            DumplingManager.TargetInfo ti = (DumplingManager.TargetInfo)((object[])extra_param)[0];
            Vector3 target_losing_posi = (Vector3)((object[])extra_param)[1];

            //setup animation
            //hostile 撞到東西時包子掉出去
            PositionAnimation sa = new PositionAnimation();
            sa.initAnimation(CurveUtil.ElasticEaseOut, 0.0f + Random.Range(0.0f, 0.25f), 0.8f, new GameObject[] { ti.obj }, true);
            sa.setupPositionVal(ti.obj.transform.localPosition, target_losing_posi);
            mAnimationList.Add(sa);

        }
        else
        if (e == MultiplayerHandler.GameEvent.DUMPLING_PLAY_UNLOADING_ANIM)
        {
            DumplingManager.TargetInfo ti = (DumplingManager.TargetInfo)((object[])extra_param)[0];
            Vector3 tgt_pt = (Vector3)((object[])extra_param)[1];
            int unload_pt_idx = (int)((object[])extra_param)[2];

            //setup animation
            //hostile 放包子
            {
                PositionAnimation sa = new PositionAnimation();
                sa.initAnimation(CurveUtil.QuartEaseIn, 0.0f + Random.Range(0.0f, 0.25f), 1.0f, new GameObject[] { ti.obj }, true, delegate (object[] p)
                {
                    dumplingManager.purgeSpecificDumpling(ti);

                    //基地包子動畫
                    //show pumping effect
                    //show bun model
                    GameObject tgt_go = mDicUnloadPointGameObject[unload_pt_idx];
                    if (tgt_go.GetComponent<UnloadBunAnim>().isBunShowing())
                    {
                        tgt_go.GetComponent<UnloadBunAnim>().pumpBun();
                    }
                    else
                    {
                        tgt_go.GetComponent<UnloadBunAnim>().showBigBun();
                    }

                });
                sa.setupPositionVal(ti.obj.transform.localPosition, tgt_pt - dumplingManager.gameObject.transform.localPosition);
                mAnimationList.Add(sa);
            }
            {
                ScaleAnimation sa = new ScaleAnimation();
                sa.initAnimation(CurveUtil.ExpoEaseIn, 0.0f, 1.0f, new GameObject[] { ti.obj }, true);
                sa.setupScaleVal(ti.obj.transform.localScale, Vector3.zero);
                mAnimationList.Add(sa);
            }
        }
        else
        if (e == MultiplayerHandler.GameEvent.MISSILE_HIT_TARGET)
        {
            //SEND HIT EVENT TO SPECIFIC CLIENT
            object[] oarr = (object[])extra_param;
            string tgt_guid = (string)oarr[0];

            sendMessageToNetwork(tgt_guid, MultiplayerHandler.GameEvent.MISSILE_HIT_TARGET, null);


        }
        else
        {
            //UNHANDLED DUMPLING EVENT
            if (extra_param != null)
            {
                List<object> extra_param_list = new List<object>();
                object[] oarr = (object[])extra_param;
                for (int i = 0; i < oarr.Length; ++i)
                {
                    extra_param_list.Add(oarr[i]);
                }
                sendMessageToNetwork(e, extra_param_list);
            }
            else
            {
                sendMessageToNetwork(e, null);
            }

        }

    }


    void resetPlayerInfo(Transform t)
    {
        pi = new PlayerInfo();
        pi.prev_status = new PhysicsStatus();
        pi.curr_status = new PhysicsStatus();
        //...

        pi.curr_status.position = t.position;
        pi.curr_status.rotation = t.rotation;
        pi.curr_status.velocity = Vector3.zero;
        pi.curr_status.acceleration = Vector3.zero;

        pi.dive_curr_forward = 0;
        pi.dive_target_forward = 0;
        pi.dive_start_forward = 0;

        pi.accumulatedFlapVelocity = new float[2];
        pi.accumulatedFlapVelocity[0] = 0.0f;
        pi.accumulatedFlapVelocity[1] = 0.0f;

        pi.flapVelocity = new float[2];
        pi.flapVelocity[0] = 0.0f;
        pi.flapVelocity[1] = 0.0f;

        pi.controller_hmd_dist = new float[2];
        pi.controller_hmd_dist[0] = 0.0f;
        pi.controller_hmd_dist[1] = 0.0f;

        pi.rotationForce_Current = 0.0F;

        //max_horizontal_speed = 300.0f;
        h_max_speed_limit = curr_max_horizontal_speed;

        //force disable shield
        sendMessageToNetwork(MultiplayerHandler.GameEvent.PLAYER_SHIELD_DISABLED, null, true);
        disableShield();

        pi.p_accelerator_available_time_counter = 0.0f;
        pi.p_accelerator_cold_time_counter = 0.0f;

    }

    void disposeUser()
    {
        if (workingCamera != null)
        {
            workingCamera.transform.localPosition = Vector3.zero;
            workingCamera.transform.localRotation = Quaternion.identity;

            //workingEye.transform.localPosition = Vector3.zero;
            //workingEye.transform.localRotation = Quaternion.identity;
        }

        workingCamera = null;
        workingEye = null;

        pi = null;

    }

    //public void registerMenuHandler(MenuEvent pfunc) {
    //  registeredMenuEventHandler = pfunc;
    //}
    public void registerGameHandler1(PlayerEvent1 pfunc)
    {
        registeredPlayerEvenet1Handler = pfunc;

    }

    public void registerGameHandler2(PlayerEvent2 pfunc)
    {
        registeredPlayerEvent2Handler = pfunc;
    }

    public void resetHostile()
    {
        dumplingManager.resetHostile();
    }

    public void resetDumpling()
    {
        dumplingManager.resetDumpling();
    }

    public void backToSpawnStatus()
    {

        playOneShot(respawn, true);

        EnvSoundObj.GetComponent<EnvironmentSound>().setPlayWarn(false);
        warningView.GetComponent<WarningViewDisplayer>().showWarningView(false);
        warningView.GetComponent<WarningViewDisplayer>().resetTimer();

        gametimeView.GetComponent<GameTimeViewDisplayer>().showGameTime(true);
        labelUIController.instance.StartShowGoObj();
        //gametimeView.GetComponent<GameTimeViewDisplayer>().resetTimer();
        shieldtimeView.GetComponent<ShieldDurationDisplayer>().resetTimer();
        acceleratorTimeView.GetComponent<ShieldDurationDisplayer>().resetTimer();

        curr_shield_available_times = shield_available_times;
        curr_accelerator_available_times = accelerator_available_times;
        updateDisplayedScore();

        //dispose current setup
        disposeUser();

        if (viveMode == false)
        {
            workingCamera = pc_camera;
            workingEye = pc_eye;
            workingGyroscope = pc_eye.transform.parent.gameObject;
            workingCockpit = pc_eye.transform.parent.parent.Find("cockpit").gameObject;

            pc_camera.SetActive(true);
            vive_camera.SetActive(false);

        }
        else
        {
            workingCamera = vive_camera;
            workingEye = vive_camerarig;
            workingGyroscope = vive_hmd.transform.parent.gameObject;
            workingCockpit = vive_hmd.transform.parent.parent.Find("cockpit").gameObject;

            vive_camera.SetActive(true);
            pc_camera.SetActive(false);

            vive_l_controller = vive_hmd.transform.parent.Find("Controller (left)").gameObject;
            vive_r_controller = vive_hmd.transform.parent.Find("Controller (right)").gameObject;

        }

        if (viveMode == true)
        {
            sightUIObj.GetComponent<SightDisplayer>().eyeObj = vive_hmd;
            dizzyView.GetComponent<DizzyViewDisplayer>().eyeObj = vive_hmd;
        }
        else
        {
            sightUIObj.GetComponent<SightDisplayer>().eyeObj = workingEye;
            dizzyView.GetComponent<DizzyViewDisplayer>().eyeObj = workingEye;
        }


        workingCamera.transform.localPosition = mMySpawnPosition;
        workingCamera.transform.localRotation = mMySpawnRotation;
        Debug.Log("1594 - " + mMySpawnRotation + ", " + workingCamera.transform.localRotation.eulerAngles);

        resetPlayerInfo(workingCamera.transform);

        hud.transform.position = pi.curr_status.position;
        hud.transform.rotation = pi.curr_status.rotation;

        healthy_percentage = 100.0f;

        dumplingManager.purgeTrackedObj();
        dumplingManager.resetSightDisplayerStatus();
        dumplingManager.setRadarBottomRedImageFlash(false);

        //clear dive effect
        workingEye.GetComponent<Klak.Motion.BrownianMotion>().rotationAmplitude = 0.0f;
        workingGyroscope.GetComponent<Gyroscope>().updateForwardingDir(workingCamera.transform.forward, 1.0f / workingGyroscope.GetComponent<Gyroscope>().rotateHeadRatio);

        current_status = Status.VIVE_FADE_OUT;

    }

    public void rcv_gameEnd()
    {

        gametimeView.GetComponent<GameTimeViewDisplayer>().showGameTime(false);
        gametimeView.GetComponent<GameTimeViewDisplayer>().resetTimer();
        labelUIController.instance.UnActiveUIObjs();
        shieldtimeView.GetComponent<ShieldDurationDisplayer>().resetTimer();
        acceleratorTimeView.GetComponent<ShieldDurationDisplayer>().resetTimer();

        if (current_status == Status.LANDING_IDLE || current_status == Status.FLYING || current_status == Status.WAIT_GAME_END)
        {

            current_status = Status.END_FADE_OUT;
            fade_out_counter = 0.0f;

            Debug.Log("1808- fade to black after 1.0f");
            SteamVR_Fade.Start(Color.black, 1.0f, true);
        }
        else
        {
            Debug.Log(current_status);
        }

    }

    public static double flyingForce(double a, double b, double c, double d)
    {
        double ret = 0.0;
        if (a <= d * 0.5f)
        {
            ret = (float)CurveUtil.CubicEaseInOut(a, b, c, d * 0.5f);
        }
        else
        {
            ret = (float)CurveUtil.CubicEaseInOut(a - (d * 0.5f), b + c, -c, d * 0.5f);
        }

        return ret;
    }

}
