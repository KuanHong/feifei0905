﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class labelUIController : MonoBehaviour
{
    public GameObject eyeObj = null;
    public GameObject timeUpObj = null;
    public GameObject goObj = null;
    public GameObject winObj = null;
    public GameObject loseObj = null;

    List<IAnimation> mAnimationList = new List<IAnimation>();
    enum TextAnimation
    {
        NULL,
        SHOW_GO,
        WAIT_SHOW_GO,
        IDLE,
        HIDE_GO,
        WAIT_HIDE_GO
    }
    TextAnimation ta = TextAnimation.NULL;

    float anim_counter = 0.0f;

    public static labelUIController instance { get; private set; }
    void Awake()
    {
        if (instance != null)
        {
            enabled = false;
            DestroyImmediate(this);
            return;
        }

        instance = this;
    }

    // Use this for initialization
    void Start()
    {
        goObj = transform.Find("go_label").gameObject;
        goObj.SetActive(false);

        winObj = transform.Find("win_label").gameObject;
        winObj.SetActive(false);

        loseObj = transform.Find("lose_label").gameObject;
        loseObj.SetActive(false);

        timeUpObj = transform.Find("timeUP_label").gameObject;
        timeUpObj.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (eyeObj != null)
        {
            transform.position = eyeObj.transform.position;
            transform.rotation = eyeObj.transform.rotation;
        }

        UpdateGoObjAnimationTime();
        UpdateGoObjAnimation();
    }

    void UpdateGoObjAnimationTime()
    {
        for (int i = 0; i < mAnimationList.Count;/*++i*/)
        {
            mAnimationList[i].updateAnimation(Time.deltaTime);

            if (mAnimationList[i].isAnimationDone() && mAnimationList[i].isAutoDismiss())
            {
                mAnimationList.RemoveAt(i);
            }
            else
            {
                ++i;
            }
        }
    }

    void UpdateGoObjAnimation()
    {
        //TEXT ANIMATION
        if (ta == TextAnimation.SHOW_GO)
        {
            ScaleAnimation sa = new ScaleAnimation();
            sa.initAnimation(CurveUtil.ElasticEaseOut, 0.0f, 0.5f, new GameObject[] { goObj }, true);
            sa.setupScaleVal(Vector3.zero, Vector3.one * 0.8f);
            mAnimationList.Add(sa);

            goObj.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);

            goObj.SetActive(true);

            anim_counter = 0.0f;

            ta = TextAnimation.WAIT_SHOW_GO;

        }
        else if (ta == TextAnimation.WAIT_SHOW_GO)
        {
            anim_counter += Time.deltaTime;
            if (anim_counter >= 0.5f)
            {
                ta = TextAnimation.IDLE;
                anim_counter = 0.0f;
            }
        }
        else if (ta == TextAnimation.IDLE)
        {
            anim_counter += Time.deltaTime;
            if (anim_counter >= 0.5f)
            {
                ta = TextAnimation.HIDE_GO;
            }
        }
        else if (ta == TextAnimation.HIDE_GO)
        {
            ScaleAnimation sa = new ScaleAnimation();
            sa.initAnimation(CurveUtil.QuintEaseOut, 0.0f, 1.0f, new GameObject[] { goObj }, true);
            sa.setupScaleVal(Vector3.one * 0.8f, Vector3.one * 10.0f);
            mAnimationList.Add(sa);

            ColorAnimation ca = new ColorAnimation();
            ca.initAnimation(CurveUtil.ExpoEaseOut, 0.0f, 0.9f, new SpriteRenderer[] { goObj.GetComponent<SpriteRenderer>() }, true);
            ca.setupColorVal(new Color(1.0f, 1.0f, 1.0f, 1.0f), new Color(1.0f, 1.0f, 1.0f, 0.0f));
            mAnimationList.Add(ca);

            ta = TextAnimation.WAIT_HIDE_GO;
            anim_counter = 0.0f;
        }
        else if (ta == TextAnimation.WAIT_HIDE_GO)
        {
            anim_counter += Time.deltaTime;
            if (anim_counter >= 1.0f)
            {
                ta = TextAnimation.NULL;
                goObj.SetActive(false);
            }
        }
    }

    public void ShowGameResult(bool win)
    {
        timeUpObj.SetActive(false);
        if (win)
            winObj.SetActive(true);
        else
            loseObj.SetActive(true);
    }

    public void StartShowGoObj()
    {
        ta = TextAnimation.SHOW_GO;
    }

    public void UnActiveUIObjs()
    {
        timeUpObj.SetActive(false);
        winObj.SetActive(false);
        loseObj.SetActive(false);
    }
}
