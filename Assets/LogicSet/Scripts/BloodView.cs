﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodView : MonoBehaviour {

  enum Phase {
    NULL,
    FADE_IN,
    FADE_OUT
  }
  Phase curr_phase = Phase.NULL;
  float currAlpha = 0.0f;

  GameObject[] view_obj = new GameObject[2];

	// Use this for initialization
	void Start () {
    view_obj[0] = transform.Find("WarningArea_01").gameObject;
    view_obj[1] = transform.Find("WarningArea_02").gameObject;
	
    //setup current alpha
    for (int i = 0; i < view_obj.Length; ++i) {
      view_obj[i].GetComponent<MeshRenderer>().material.SetColor("_Color", new Color(1.0f, 1.0f, 1.0f, 0.0f));

    }
	}
	
	// Update is called once per frame
	void Update () {
    if (curr_phase == Phase.FADE_IN) {
      counter += Time.deltaTime;
      if (counter >= 0.7f) {
        curr_phase = Phase.FADE_OUT;

        counter = 0.0f;
        startAlpha = 1.0f;
        endAlpha = 0.0f;
        return;
      }
      currAlpha = (float)CurveUtil.ExpoEaseOut(counter / 0.7f, startAlpha, endAlpha - startAlpha, 1.0f);
      //Debug.Log(currAlpha);
      //setup current alpha
      for (int i = 0; i < view_obj.Length; ++i) {
        view_obj[i].GetComponent<MeshRenderer>().material.SetColor("_Color", new Color(1.0f, 1.0f, 1.0f, currAlpha));

      }

    } else
    if (curr_phase == Phase.FADE_OUT) {
      counter += Time.deltaTime;
      if (counter >= 2.0f){
        curr_phase = Phase.NULL;

        counter = 0.0f;
        return;
      }

      currAlpha = (float)CurveUtil.ExpoEaseIn(counter / 2.0f, startAlpha, endAlpha - startAlpha, 1.0f);
      //Debug.Log(currAlpha);

      //setup current alpha
      for (int i = 0; i < view_obj.Length; ++i) {
        view_obj[i].GetComponent<MeshRenderer>().material.SetColor("_Color", new Color(1.0f, 1.0f, 1.0f, currAlpha));

      }

    }

		
	}

  float counter = 0.0f;
  float startAlpha = 0.0f;
  float endAlpha = 0.0f;
  public void showView() {
    Debug.Log("75 - show Blooded View !");
    //setup view
    curr_phase = Phase.FADE_IN;
    counter = 0.0f;
    startAlpha = currAlpha;
    endAlpha = 1.0f;

  }
}
