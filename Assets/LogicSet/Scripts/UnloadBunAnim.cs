﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnloadBunAnim : MonoBehaviour {
  float angle = 0.0f;
  GameObject bunRoot = null;
  float baseHeight = 0.0f;
  float floating_val = 0.0f;

  float animTimer = 0.0f;
  List<IAnimation> mAnimationList = new List<IAnimation>();

  enum BunStatus {
    IDLE_HIDE,
    SHOW,
    WAIT_SHOW,
    IDLE_SHOW,
    HIDE,
    WAIT_HIDE,
    PUMPING,
    WAIT_PUMPING
  }
  BunStatus currStatue = BunStatus.IDLE_HIDE;
  float stateCounter = 0.0f;

	bool showing =false;

	// Use this for initialization
	void Start () {
    GameObject bunObj =gameObject.transform.Find("bun/bun_root/bun").gameObject;
    bunObj.GetComponent<Animator>().speed = 0.0f;

    bunRoot = gameObject.transform.Find("bun").gameObject;
		bunRoot.transform.localScale = Vector3.zero;

    baseHeight = gameObject.transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
    for (int i = 0; i < mAnimationList.Count;/*++i*/) {
      mAnimationList[i].updateAnimation(Time.deltaTime);

      if (mAnimationList[i].isAnimationDone() && mAnimationList[i].isAutoDismiss()) {
        mAnimationList.RemoveAt(i);
      } else {
        ++i;
      }
    }

    if (currStatue == BunStatus.IDLE_HIDE) {

    }else
    if (currStatue == BunStatus.SHOW) {
      {
        ScaleAnimation sa = new ScaleAnimation();
        sa.initAnimation(CurveUtil.ElasticEaseOut, 0.0f, 1.0f, new GameObject[] { bunRoot }, true);
				sa.setupScaleVal(bunRoot.transform.localScale, Vector3.one*0.35f);
        mAnimationList.Add(sa);
      }
      {
        PositionAnimation sa = new PositionAnimation();
        sa.initAnimation(CurveUtil.QuartEaseOut, 0.0f, 1.0f, new GameObject[] { bunRoot }, true);
        sa.setupPositionVal(bunRoot.transform.localPosition, Vector3.zero);
        mAnimationList.Add(sa);
      }
      bunRoot.SetActive(true);

      animTimer = 0.0f;
      currStatue = BunStatus.WAIT_SHOW;

    } else
    if (currStatue == BunStatus.WAIT_SHOW) {
      animTimer += Time.deltaTime;
      if (animTimer >= 1.0f) {
        currStatue = BunStatus.IDLE_SHOW;
      }

    }else
    if (currStatue == BunStatus.IDLE_SHOW) {
      angle += Time.deltaTime * 100.0f;
      if (angle >= 360.0f) {
        angle = angle % 360.0f;
      }
      bunRoot.transform.localRotation = Quaternion.Euler(30.0f, angle, 0.0f);

      floating_val = Mathf.Sin(angle / 360.0f * Mathf.PI * 2.0F);

      Vector3 p = bunRoot.transform.position;
      p.y = baseHeight + floating_val * 5.0F;

      bunRoot.transform.position = p;
      

    } else
    if (currStatue == BunStatus.HIDE) {
      {
        ScaleAnimation sa = new ScaleAnimation();
        sa.initAnimation(CurveUtil.BackEaseIn, 0.0f, 1.0f, new GameObject[] { bunRoot }, true);
				sa.setupScaleVal(bunRoot.transform.localScale, Vector3.zero);
        mAnimationList.Add(sa);
      }
      {
        PositionAnimation sa = new PositionAnimation();
        sa.initAnimation(CurveUtil.QuartEaseOut, 0.0f, 1.0f, new GameObject[] { bunRoot }, true);
        sa.setupPositionVal(bunRoot.transform.localPosition, Vector3.zero);
        mAnimationList.Add(sa);
      }

      animTimer = 0.0f;
      currStatue = BunStatus.WAIT_HIDE;
    } else
    if (currStatue == BunStatus.WAIT_HIDE) {
      animTimer += Time.deltaTime;
      if (animTimer >= 1.0f) {
        currStatue = BunStatus.IDLE_HIDE;
      }

    } else
    if (currStatue == BunStatus.PUMPING) {
      {
        ScaleAnimation sa = new ScaleAnimation();
        sa.initAnimation(CurveUtil.ElasticEaseOut, 0.0f, 1.0f, new GameObject[] { bunRoot }, true);
        sa.setupScaleVal(Vector3.one*0.85f, Vector3.one*0.35f);
        mAnimationList.Add(sa);
      }
			bunRoot.SetActive(true);

      animTimer = 0.0f;
      currStatue = BunStatus.WAIT_PUMPING;

    } else
    if (currStatue == BunStatus.WAIT_PUMPING) {
      animTimer += Time.deltaTime;
      if (animTimer >= 1.0f) {
        currStatue = BunStatus.IDLE_SHOW;
      }
    }

	}

  public void showBigBun() {
		showing = true;
		currStatue =BunStatus.SHOW;
  }

  public void hideBigBun() {
		showing = false;
		currStatue = BunStatus.HIDE;
  }

  public void pumpBun() {
		currStatue = BunStatus.PUMPING;
  }

	public bool isBunShowing(){
		return showing;
	}

}
