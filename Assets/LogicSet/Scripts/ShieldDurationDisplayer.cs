﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldDurationDisplayer : MonoBehaviour {

  public GameObject eyeObj = null;

  GameObject icon = null;
  GameObject[] sec = null;
  GameObject[] msec = null;

  public Sprite[] sprite_arr = null;

  // Use this for initialization
  void Start() {

    icon = transform.Find("ShieldIcon").gameObject;
    icon.SetActive(false);

    msec = new GameObject[5];
    msec[0] = transform.Find("Time/MIN0").gameObject;
    msec[1] = transform.Find("Time/MIN1").gameObject;
    msec[2] = transform.Find("Time/SEP").gameObject;
    msec[3] = transform.Find("Time/SEC0").gameObject;
    msec[4] = transform.Find("Time/SEC1").gameObject;
    for (int i = 0; i < msec.Length; ++i)
      msec[i].SetActive(false);



  }

  // Update is called once per frame
  void Update() {

    if (showing) {
      if (eyeObj != null) {
        transform.position = eyeObj.transform.position/* + eyeObj.transform.rotation * new Vector3(0.0f, 0.0f, 0.0f)*/;
        transform.rotation = eyeObj.transform.rotation;
      }

      counter += Time.deltaTime;
      if (counter < duration) {
        //remain sec
        float rs = duration - counter;

        //rmain min
        int rm = (int)(rs / 60.0f);
        msec[0].GetComponent<SpriteRenderer>().sprite = sprite_arr[rm / 10];
        msec[1].GetComponent<SpriteRenderer>().sprite = sprite_arr[rm % 10];

        //sec
        int rs2 = (int)rs % 60;
        msec[3].GetComponent<SpriteRenderer>().sprite = sprite_arr[rs2 / 10];
        msec[4].GetComponent<SpriteRenderer>().sprite = sprite_arr[rs2 % 10];

      } else {
        //rmain min
        int rm = 0;
        msec[0].GetComponent<SpriteRenderer>().sprite = sprite_arr[rm / 10];
        msec[1].GetComponent<SpriteRenderer>().sprite = sprite_arr[rm % 10];

        //sec
        int rs2 = 0;
        msec[3].GetComponent<SpriteRenderer>().sprite = sprite_arr[rs2 / 10];
        msec[4].GetComponent<SpriteRenderer>().sprite = sprite_arr[rs2 % 10];

      }
    } else {
      resetTimer();
    }


  }

  float counter = 0.0f;
  float duration = 1 * 60.0f;

  bool showing = false;
  public void showShieldTime(float d) {
    if (showing)
      return;

    duration = d;

    showing = true;
    icon.SetActive(true);

    for (int i = 0; i < msec.Length; ++i) {
      msec[i].SetActive(true);
    }

    counter = 0.0f;

  }

  public void resetTimer() {
    showing = false;

    icon.SetActive(false);
    for (int i = 0; i < msec.Length; ++i) {
      msec[i].SetActive(false);
    }

  }

}
