﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTimeViewDisplayer : MonoBehaviour
{
    GameObject[] sec = null;
    GameObject[] msec = null;

    public Sprite[] sprite_arr = null;

    float counter = 0.0f;
    public float duration = 1 * 60.0f;
    bool showing = false;

    // Use this for initialization
    void Start()
    {
        msec = new GameObject[5];
        msec[0] = transform.Find("Time/MIN0").gameObject;
        msec[1] = transform.Find("Time/MIN1").gameObject;
        msec[2] = transform.Find("Time/SEP").gameObject;
        msec[3] = transform.Find("Time/SEC0").gameObject;
        msec[4] = transform.Find("Time/SEC1").gameObject;
        for (int i = 0; i < msec.Length; ++i)
            msec[i].SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (showing)
        {
            counter += Time.deltaTime;
            if (counter < duration)
            {
                //remain sec
                float rs = duration - counter;

                //rmain min
                int rm = (int)(rs / 60.0f);
                msec[0].GetComponent<SpriteRenderer>().sprite = sprite_arr[rm / 10];
                msec[1].GetComponent<SpriteRenderer>().sprite = sprite_arr[rm % 10];

                //sec
                int rs2 = (int)rs % 60;
                msec[3].GetComponent<SpriteRenderer>().sprite = sprite_arr[rs2 / 10];
                msec[4].GetComponent<SpriteRenderer>().sprite = sprite_arr[rs2 % 10];

            }
            else
            {
                //rmain min
                int rm = 0;
                msec[0].GetComponent<SpriteRenderer>().sprite = sprite_arr[rm / 10];
                msec[1].GetComponent<SpriteRenderer>().sprite = sprite_arr[rm % 10];

                //sec
                int rs2 = 0;
                msec[3].GetComponent<SpriteRenderer>().sprite = sprite_arr[rs2 / 10];
                msec[4].GetComponent<SpriteRenderer>().sprite = sprite_arr[rs2 % 10];

                labelUIController.instance.timeUpObj.SetActive(true);
            }
        }
    }

    public void showGameTime(bool show)
    {
        if (show != showing)
        {
            showing = show;
            //textObj.SetActive(true);

            for (int i = 0; i < msec.Length; ++i)
            {
                msec[i].SetActive(show);
            }

            if (show == true)
                counter = 0.0f;
        }
    }

    public bool isTimesUp()
    {
        if (counter >= duration)
        {
            return true;
        }
        return false;
    }

    public void resetTimer()
    {
        counter = 0.0f;
    }


}
