﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DumplingManager : MonoBehaviour
{
    public static DumplingManager instance { get; private set; }

    class RemoteShieldStatus
    {
        public bool activiated = false;
        public float counter = 0.0f;

        public int owner_id = -1;

        public RemoteShieldStatus(int id)
        {
            owner_id = id;
        }
    }
    Dictionary<int, RemoteShieldStatus> remote_shield_status = new Dictionary<int, RemoteShieldStatus>();

    class RemoteMissileStatus
    {
        public bool explosed = false;
        public float counter = 0.0f;

        public string owner_guid = "";
        public int missile_id = -1;

        public RemoteMissileStatus(string o_id, int m_id)
        {
            owner_guid = o_id;
            missile_id = m_id;
        }
    }
    Dictionary<int, RemoteMissileStatus> remote_missile_status = new Dictionary<int, RemoteMissileStatus>();

    LayerMask GatewayLM;

    //Multiplayer
    public delegate void DumplingEventFunc(MultiplayerHandler.GameEvent e, object param);
    DumplingEventFunc dumplingEventHandler = null;

    //npc target generator
    public GameObject NPCRoot = null;
    public GameObject npcPrefab1 = null;
    public GameObject npcPrefab2 = null;

    //target generator
    public GameObject targetPrefab = null; //包子
    public GameObject shieldPrefab = null; //防護罩道具
    public GameObject acceleratorPrefab = null; //加速道具

    public float radius = 1000.0f;
    public float min_radius = 3000.0f;

    public float height = 200.0f;
    public float scale = 1.0f;

    public int numTarget = 100;
    public int numShield = 10;
    public int numAccelerator = 10;

    //target tracer
    public float traceLength = 1000.0f; //1000 m
    public float h_traceAngle = 40.0f;
    public float v_traceAngle = 30.0f;
    public float minTraceLength = 100.0f;
    public int traceNumber = 1;
    public GameObject sightGameObj = null;
    public Material lineMat = null;

    //missile system
    public GameObject missileLaunchEffectPrefab = null;
    public GameObject missilePrefab = null;
    public GameObject remoteMissilePrefabB = null;
    public GameObject remoteMissilePrefabR = null;
    public GameObject playerHit = null;
    public GameObject bunHit = null;
    public GameObject shieldEffect = null;

    //holoradar
    public bool enableHoloRadar = true;
    public Material radarBottomImage = null;
    public Material radarBottomImageRed = null;

    public GameObject radar_alliesArrowPrefab = null;
    public GameObject radar_hostileArrowPrefab = null;
    public GameObject radar_arrowPrefab = null;

    public GameObject radar_dumplingPrefab = null;
    public GameObject radar_npcPrefab = null;
    public GameObject radar_playerPrefab = null;
    public GameObject radar_alliesPrefab = null;

    public GameObject radarMissilePrefab = null;

    public GameObject radar_trailPrefab = null;
    public GameObject radar_playerTrailPrefab = null;
    public GameObject radar_hostileTrailPrefab = null;
    public GameObject radar_alliesTrailPrefab = null;

    public GameObject radar_terrain = null;

    public GameObject radar_SpawnPrefab = null;
    public Sprite radar_healthy_sprite = null;

    public float radarRadius = 3.0f;
    public Vector3 radarShift = new Vector3(0.0f, -2.0f, 6.0f);
    public float radarHeight = 1.0f;
    public bool followViewDir = false;

    public Sprite[] dumpling_counter_digits = null;
    public Sprite dumplingCollected = null;
    public Sprite dumplingUnloaded = null;
    public Sprite dumplingShield = null;
    public Sprite missileStatus = null;
    public Sprite accelerator = null;

    GameObject radarRootForDumplingCounter = null;
    GameObject radarRoot = null;
    GameObject radarOrientation = null;
    GameObject radarBottomObj = null;
    GameObject radarBottomObjRed = null;
    GameObject healthyIndRoot = null;
    GameObject[] healthyIndicator = null;

    GameObject mPlayerSpawnPt = null;
    GameObject mHostileSpwanPt = null;

    public GameObject rocketBtnGO;
    public GameObject shieldBtnGO;

    //dumpling counter
    GameObject dumplingCounterRoot = null;
    SpriteRenderer[] sr_available_shield = null;
    SpriteRenderer[] sr_available_accelerator = null;
    SpriteRenderer[] sr_localPlayerDumplingCollected = null;
    List<SpriteRenderer[]> sr_otherPlayerDumplingCollected = new List<SpriteRenderer[]>();
    SpriteRenderer[] sr_dumplingUnloaded = null;
    SpriteRenderer[] sr_dumplingHostileUnloaded = null;

    Color[] teamColor = new Color[] {
        new Color (135f/255f, 210f/ 255f, 1),//蓝
        new Color (233f/255f, 42f/255f, 29f/255f),//红
        new Color (120f/255f, 69f/255f, 299f/255f),//紫
        new Color (227f/255f, 211f/255f, 79f/255f),//黄
        new Color (0, 82/255f, 24f/255f),//深绿
        new Color (0, 1, 73f/255f), //绿
        new Color (1, 69f/255f, 0), //橙
        new Color (21f/255f, 21f/255, 21f/255f),//黑
    };

    GameObject tracingFrustum = null;
    TracerFrustum tf = null;

    GameObject[] highlightRect = null;

    List<TargetInfo> numAvailableTarget = new List<TargetInfo>();
    int target_id_counter = 0;
    int hostile_count = 0;

    public class TargetInfo
    {
        public enum Status
        {
            NULL,
            AVAILABLE,
            DISPOSED,
            COLLECTING,
            COLLECTED //ATTACH TO PLAYER
        }
        public enum Type
        {
            ENERMY,
            NORMAL, //DUMPLINGS
            UNLOADPT_TGT,
            SHIELD_TGT,
            SPEEDING_TGT
        }
        public Type t = Type.NORMAL;
        public string name = null;
        public int id = -1;
        public GameObject obj = null;
        public Status curr_status = Status.NULL;

        //npc target
        //...

        //radar light pt
        public GameObject radar_pt = null;

        //attraction
        public float delay_attraction_counter = 0.0f;
        public GameObject attached_player = null;
        public bool is_attached_by_self = false;
        public bool attraction_done = false;
        public Vector3 shift_attraction_posi = Vector3.zero;

        public List<Vector3> prev_attraction_posi = new List<Vector3>();
        public List<float> prev_attraction_posi_time = new List<float>();

        //multi-player
        public string attracted_player_guid = null;
        public GameObject hmd = null;
        public string player_guid = null;
        public int team_id = -1;
        public GameObject radar_hostileArrow = null;
        public GameObject radar_hostile_trail;
        public Transform positionTag;
        public bool showOnRadar;

        public Quaternion fromInterpolatedRotation = Quaternion.identity;
        public Quaternion targetInterpolatedRoation = Quaternion.identity;
        public float targetInterpolatedRotationTime = 0.0f;

    }
    Dictionary<string, TargetInfo> mTargetInfo_NameMapper = new Dictionary<string, TargetInfo>();
    Dictionary<string, int> mHostileGUID_idxMapper = new Dictionary<string, int>();

    //player data on radar
    GameObject radar_player = null;
    GameObject radar_arrow = null;
    List<GameObject> radar_missile = new List<GameObject>(); //local missile

    public class TrackedMissileInfo
    {
        public GameObject obj = null;
        public string launcher_guid = "";
        public int idx = -1;
        public string name = "";

        public TargetInfo target = null;

        //remote
        public GameObject remote_radar_missile = null;

        //network sync.
        public float delay_counter = 0.0f;
        public float delay_duration = 0.1f;
    }
    List<TrackedMissileInfo> tracked_missile = new List<TrackedMissileInfo>();
    List<TrackedMissileInfo> tracked_remote_missile = new List<TrackedMissileInfo>();

    SpriteRenderer[] constructDigits(GameObject parent_go, Color c)
    {
        SpriteRenderer[] srarr = new SpriteRenderer[3];
        for (int i = 0; i < 3; ++i)
        {
            GameObject digit = new GameObject();
            digit.name = "DIGIT" + i;
            digit.transform.SetParent(parent_go.transform, false);
            digit.transform.localPosition = new Vector3(2.75f + i * 1.75f, 0.0f, 0.0f);
            digit.transform.localRotation = Quaternion.identity;
            digit.transform.localScale = Vector3.one * 2.0f;
            srarr[i] = digit.AddComponent<SpriteRenderer>();
            srarr[i].sprite = dumpling_counter_digits[0];
            srarr[i].color = c;

        }

        return srarr;
    }

    void Awake()
    {
        // Only allow one instance at runtime.
        if (instance != null)
        {
            enabled = false;
            DestroyImmediate(this);
            return;
        }

        instance = this;
    }

    // Use this for initialization
    void Start()
    {
        BeaconAPLayer.Group myGroupId = MatchmakingController.instance.getMatchmakingInfo().getMyPlayerInfo().groupid;
        GatewayLM = 1 << LayerMask.NameToLayer("Gateway");
        GameObject staticTarget = null;
        if (radarRoot == null)
        {
            radarRoot = new GameObject();
            radarRoot.name = "HoloRadar";
            radarRoot.transform.SetParent(transform, false);
            radarRoot.transform.localScale = Vector3.one;

            radarOrientation = new GameObject();
            radarOrientation.name = "Orientation";
            radarOrientation.transform.SetParent(radarRoot.transform, false);
            radarOrientation.transform.localScale = Vector3.one;
            radarOrientation.transform.localPosition = radarShift;

            GameObject terrain_obj = GameObject.Instantiate(radar_terrain);
            terrain_obj.name = "TerrainObjects";
            terrain_obj.transform.SetParent(radarOrientation.transform, false);

            staticTarget = new GameObject();
            staticTarget.name = "StaticTarget";
            staticTarget.transform.SetParent(radarOrientation.transform, false);
            staticTarget.transform.localPosition = Vector3.zero;
            staticTarget.transform.localRotation = Quaternion.identity;
            staticTarget.transform.localScale = Vector3.one;

            healthyIndRoot = new GameObject();
            healthyIndRoot.name = "Healthy";
            healthyIndRoot.transform.SetParent(radarRoot.transform, false);
            healthyIndRoot.transform.localPosition = new Vector3(0.0f, -2.0f, 6.0f);
            healthyIndRoot.transform.localRotation = Quaternion.identity;
            healthyIndRoot.transform.localScale = Vector3.one;

            int num_healthy_ind = (int)(180.0f / 5.0f);
            healthyIndicator = new GameObject[num_healthy_ind];
            for (int i = 0; i < num_healthy_ind; ++i)
            {
                healthyIndicator[i] = new GameObject();
                healthyIndicator[i].name = "Ind" + i;
                healthyIndicator[i].transform.SetParent(healthyIndRoot.transform, false);
                healthyIndicator[i].transform.localPosition = Quaternion.AngleAxis(i * -5.0f + 180.0f, Vector3.up) * (new Vector3(-3.5f, 0.0f, 0.0f));

                healthyIndicator[i].transform.localRotation *= Quaternion.AngleAxis(i * -5.0f + 180.0f, Vector3.up);
                healthyIndicator[i].transform.localRotation *= Quaternion.AngleAxis(-90.0f, Vector3.right);

                healthyIndicator[i].transform.localScale = Vector3.one;

                SpriteRenderer sr = healthyIndicator[i].AddComponent<SpriteRenderer>();
                sr.sprite = radar_healthy_sprite;

                healthyIndicator[i].AddComponent<HealthIndicatorEffect>();
            }
            if (MatchmakingController.instance.gameMode == MatchmakingController.GameMode.TeamFight)
            {
                GameObject tmp_dump_unloaded = new GameObject();
                tmp_dump_unloaded.name = "DumplingCollected_symbol";
                tmp_dump_unloaded.transform.SetParent(radarRoot.transform, false);
                tmp_dump_unloaded.transform.localPosition = new Vector3(-3.1f, -1.68f, 8.64f);
                tmp_dump_unloaded.transform.localRotation = Quaternion.Euler(0, -35, 0);
                tmp_dump_unloaded.transform.localScale = Vector3.one * 0.3f;
                SpriteRenderer sr2 = tmp_dump_unloaded.AddComponent<SpriteRenderer>();
                sr2.sprite = dumplingUnloaded;
                sr2.color = new Color(135.0f / 255.0f, 210.0f / 255.0f, 1.0f);
                sr_dumplingUnloaded = constructDigits(tmp_dump_unloaded, new Color(135.0f / 255.0f, 210.0f / 255.0f, 1.0f));

                GameObject tmp_hostile_dump_unloaded = new GameObject();
                tmp_hostile_dump_unloaded.name = "HostileDumplingCollected_symbol";
                tmp_hostile_dump_unloaded.transform.SetParent(radarRoot.transform, false);
                tmp_hostile_dump_unloaded.transform.localPosition = new Vector3(1.48f, -1.68f, 9.62f);
                tmp_hostile_dump_unloaded.transform.localRotation = Quaternion.Euler(0, 35, 0);
                tmp_hostile_dump_unloaded.transform.localScale = Vector3.one * 0.3f;
                SpriteRenderer sr3 = tmp_hostile_dump_unloaded.AddComponent<SpriteRenderer>();
                sr3.sprite = dumplingUnloaded;
                sr3.color = new Color(1.0f, 126.0f / 255.0f, 126.0f / 255.0f);
                sr_dumplingHostileUnloaded = constructDigits(tmp_hostile_dump_unloaded, new Color(1.0f, 126.0f / 255.0f, 126.0f / 255.0f));
            }
        }

        if (radarRootForDumplingCounter == null)
        {
            radarRootForDumplingCounter = new GameObject();
            radarRootForDumplingCounter.name = "HoloRadar (statistics)";
            radarRootForDumplingCounter.transform.SetParent(transform, false);
            radarRootForDumplingCounter.transform.localScale = Vector3.one;

            dumplingCounterRoot = new GameObject();
            dumplingCounterRoot.name = "DumplingCounter";
            dumplingCounterRoot.transform.SetParent(radarRootForDumplingCounter.transform, false);
            dumplingCounterRoot.transform.localPosition = Vector3.zero;
            dumplingCounterRoot.transform.localRotation = Quaternion.identity;
            dumplingCounterRoot.transform.localScale = Vector3.one;

            GameObject tmp_available_shield = new GameObject();
            tmp_available_shield.name = "available_shield_symbol";
            tmp_available_shield.transform.SetParent(shieldBtnGO.transform, false);
            tmp_available_shield.transform.localPosition = new Vector3(-0.16f, 0.66f, 0);
            tmp_available_shield.transform.localRotation = Quaternion.identity;
            tmp_available_shield.transform.localScale = Vector3.one * 0.05f;
            SpriteRenderer sr0 = tmp_available_shield.AddComponent<SpriteRenderer>();
            sr0.sprite = dumplingShield;
            sr0.color = new Color(253.0f / 255.0f, 1.0f, 135.0f / 255.0f);
            sr_available_shield = constructDigits(tmp_available_shield, new Color(253.0f / 255.0f, 1.0f, 135.0f / 255.0f));
            foreach (SpriteRenderer sr in sr_available_shield)
            {
                sr.gameObject.layer = 18;
            }
            tmp_available_shield.layer = 18;

            GameObject tmp_remain_accelerator = new GameObject();
            tmp_remain_accelerator.name = "RemainingAcceleraotr_symbol";
            tmp_remain_accelerator.transform.SetParent(rocketBtnGO.transform, false);
            tmp_remain_accelerator.transform.localPosition = new Vector3(-0.16f, 0.66f, 0);
            tmp_remain_accelerator.transform.localRotation = Quaternion.identity;
            tmp_remain_accelerator.transform.localScale = Vector3.one * 0.05f;
            SpriteRenderer sr_tmp_remain_accelerator = tmp_remain_accelerator.AddComponent<SpriteRenderer>();
            sr_tmp_remain_accelerator.sprite = accelerator;
            Color redColor = new Color(253.0f / 255.0f, 94.0f / 255.0f, 94.0f / 255.0f);
            sr_tmp_remain_accelerator.color = redColor;
            sr_available_accelerator = constructDigits(tmp_remain_accelerator, redColor);
            foreach (SpriteRenderer sr in sr_available_accelerator)
            {
                sr.gameObject.layer = 18;
            }
            tmp_remain_accelerator.layer = 18;

        }

        //自己的三角
        radar_player = GameObject.Instantiate(radar_playerPrefab);
        radar_player.transform.SetParent(radarOrientation.transform, false);
        radar_player.transform.localScale = new Vector3(0.075f, 0.075f, 0.075f);
        radar_player.transform.localRotation = Quaternion.identity;

        radar_arrow = GameObject.Instantiate(radar_arrowPrefab);
        MeshRenderer[] renders = radar_arrow.GetComponentsInChildren<MeshRenderer>();
        if (MatchmakingController.instance.gameMode == MatchmakingController.GameMode.TeamFight)
        {
            foreach (MeshRenderer render in renders)
                render.material.color = teamColor[0];
        }
        else
        {
            foreach (MeshRenderer render in renders)
                render.material.color = teamColor[(int)myGroupId];
        }
        radar_arrow.transform.SetParent(radarOrientation.transform, false);
        radar_arrow.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        radar_arrow.transform.localRotation = Quaternion.identity;

        //自己的分数

        GameObject tmp_dump_collected_root = new GameObject();
        tmp_dump_collected_root.name = "root";
        tmp_dump_collected_root.transform.SetParent(radar_arrow.transform, false);
        tmp_dump_collected_root.transform.localPosition = new Vector3(0, 0, -1);

        GameObject tmp_dump_collected = new GameObject();
        tmp_dump_collected.name = "DumplingCollected_symbol";
        tmp_dump_collected.transform.SetParent(tmp_dump_collected_root.transform, false);
        tmp_dump_collected.transform.localRotation = Quaternion.identity;
        tmp_dump_collected.transform.localScale = Vector3.one * 0.3f;
        faceCam face = tmp_dump_collected.AddComponent<faceCam>();
        face.adj_angle = 180;
        SpriteRenderer sr_localplayer = tmp_dump_collected.AddComponent<SpriteRenderer>();
        sr_localplayer.sprite = dumplingCollected;
        Debug.Log("ScoreUI GroupID=" + (int)myGroupId);
        SpriteRenderer[] srs;
        if (MatchmakingController.instance.gameMode == MatchmakingController.GameMode.TeamFight)
        {
            sr_localplayer.color = teamColor[0];
            srs = constructDigits(tmp_dump_collected, teamColor[0]);
        }
        else
        {
            sr_localplayer.color = teamColor[(int)myGroupId];
            srs = constructDigits(tmp_dump_collected, teamColor[(int)myGroupId]);
        }
        sr_localPlayerDumplingCollected = srs;



        //radar_hostileArrow = GameObject.Instantiate(radar_hostileArrowPrefab);
        //radar_hostileArrow.transform.SetParent(radarOrientation.transform, false);
        //radar_hostileArrow.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        //radar_hostileArrow.transform.localRotation = Quaternion.identity;

        //自己的拖尾
        GameObject trail = GameObject.Instantiate(radar_playerTrailPrefab);
        trail.GetComponent<TrailPrinter>().trailObj = radar_player;
        trail.GetComponentInChildren<LineRenderer>().startColor = new Color(0, 0, 0, 0);
        if (MatchmakingController.instance.gameMode == MatchmakingController.GameMode.TeamFight)
            trail.GetComponentInChildren<LineRenderer>().endColor = teamColor[0];
        else
            trail.GetComponentInChildren<LineRenderer>().endColor = teamColor[(int)myGroupId];
        trail.transform.SetParent(radarOrientation.transform, false);
        trail.transform.localPosition = Vector3.zero;

        //create tracing frustum
        createFrustumModel(h_traceAngle * 0.5f, h_traceAngle * 0.5f, v_traceAngle * 0.5f, v_traceAngle * 0.5f, traceLength, minTraceLength);

        //create highlight rect
        highlightRect = new GameObject[traceNumber];

        for (int i = 0; i < traceNumber; ++i)
        {
            highlightRect[i] = new GameObject();
            highlightRect[i].name = "Highlight Rect";
            highlightRect[i].transform.SetParent(transform, false);
            highlightRect[i].transform.localPosition = Vector3.zero;
            LineRenderer lr = highlightRect[i].AddComponent<LineRenderer>();
            lr.material = lineMat;
            lr.startWidth = 20.0f;
            lr.endWidth = 20.0f;
            lr.startColor = Color.green;
            lr.endColor = Color.green;
        }

    }

    void Update()
    {

        //
        //update dumpling position (follow attracter){
        //
        for (int i = 0; i < numAvailableTarget.Count; ++i)
        {
            //更新包子狀態 (包子被吸引)
            if (numAvailableTarget[i].attached_player != null)
            {
                if (numAvailableTarget[i].curr_status == TargetInfo.Status.DISPOSED)
                {
                    if (numAvailableTarget[i].attraction_done == true)
                    {
                        //score update ?
                        //屬於目己的包子但已損毀 （//只處理吸收包子的情況）
                        if (numAvailableTarget[i].is_attached_by_self && numAvailableTarget[i].t == TargetInfo.Type.NORMAL)
                        {
                            dumplingEventHandler(MultiplayerHandler.GameEvent.SCORE_UPDATE_REQUEST, new object[] {
                (int)MultiplayerHandler.GameSubEvent.DUMPLING_SCORE_DECREASE_ONE //SEND TO HOST, (OR PROCESS SCORE THEN BROADCAST TO ALL)
              });
                        }

                    }
                    numAvailableTarget[i].attached_player = null;
                    numAvailableTarget[i].attraction_done = false;
                    numAvailableTarget[i].is_attached_by_self = false;

                    //
                    if (numAvailableTarget[i].t == TargetInfo.Type.NORMAL)
                    {
                        numAvailableTarget[i].obj.transform.Find("bun").GetComponent<Klak.Motion.BrownianMotion>().positionAmplitude = 0.0f;
                        numAvailableTarget[i].obj.transform.Find("bun").localPosition = new Vector3(0.0f, -0.6952f, 0.0f);
                    }

                }
                else
                {
                    //吸引中
                    numAvailableTarget[i].delay_attraction_counter += Time.deltaTime;
                    if (numAvailableTarget[i].delay_attraction_counter >= (numAvailableTarget[i].t == TargetInfo.Type.NORMAL ? 0.25f : 0.4f))
                    {
                        numAvailableTarget[i].prev_attraction_posi.Add(numAvailableTarget[i].attached_player.transform.position);
                        numAvailableTarget[i].prev_attraction_posi_time.Add(Time.realtimeSinceStartup);
                        while (numAvailableTarget[i].prev_attraction_posi.Count > 30)
                        { //buffer postion for 30 frames
                            numAvailableTarget[i].prev_attraction_posi.RemoveAt(0);
                            numAvailableTarget[i].prev_attraction_posi_time.RemoveAt(0);
                        }
                        numAvailableTarget[i].obj.transform.position += ((numAvailableTarget[i].prev_attraction_posi[0] + numAvailableTarget[i].obj.transform.rotation * numAvailableTarget[i].shift_attraction_posi) - numAvailableTarget[i].obj.transform.position) * 5.0f * Time.deltaTime;

                        if (numAvailableTarget[i].curr_status == TargetInfo.Status.COLLECTING)
                        {
                            if (Vector3.Distance(numAvailableTarget[i].obj.transform.position, numAvailableTarget[i].attached_player.transform.position + numAvailableTarget[i].obj.transform.rotation * numAvailableTarget[i].shift_attraction_posi) < 50.0f)
                            {
                                Debug.Log(numAvailableTarget[i].obj.name + " attraction successful.");
                                numAvailableTarget[i].curr_status = TargetInfo.Status.COLLECTED;
                                numAvailableTarget[i].attraction_done = true;

                                if (numAvailableTarget[i].t == TargetInfo.Type.NORMAL)
                                {
                                    numAvailableTarget[i].obj.transform.Find("bun").GetComponent<Klak.Motion.BrownianMotion>().positionAmplitude = 1.0f;
                                    numAvailableTarget[i].obj.GetComponent<TrailRenderer>().enabled = false;
                                }

                                if (numAvailableTarget[i].is_attached_by_self)
                                {
                                    dumplingEventHandler(MultiplayerHandler.GameEvent.DUMPLING_COLLECTED, null); // PLAY DUMPLING ATTRACTED SOUND EFFECT

                                    //只處理吸收包子的情況
                                    if (numAvailableTarget[i].t == TargetInfo.Type.NORMAL)
                                    {
                                        dumplingEventHandler(MultiplayerHandler.GameEvent.SCORE_UPDATE_REQUEST, new object[] {
                      (int)MultiplayerHandler.GameSubEvent.DUMPLING_SCORE_INCREASE_ONE //SEND TO HOST, (OR PROCESS SCORE THEN BROADCAST TO ALL)
                    });

                                    }

                                    //處理吸收防護罩的情況
                                    if (numAvailableTarget[i].t == TargetInfo.Type.SHIELD_TGT)
                                    {
                                        //更新可用數
                                        dumplingEventHandler(MultiplayerHandler.GameEvent.SCORE_UPDATE_REQUEST, new object[] {
                      (int)MultiplayerHandler.GameSubEvent.SHIELDTARGET_SCORE_INCREASE_ONE
                    });

                                        //刪除並重置防護罩
                                        dumplingEventHandler(MultiplayerHandler.GameEvent.SHIELDTARGET_DESTROY_REQUEST, new object[] { numAvailableTarget[i].id });

                                    }

                                    //吸收加速道具
                                    if (numAvailableTarget[i].t == TargetInfo.Type.SPEEDING_TGT)
                                    {
                                        dumplingEventHandler(MultiplayerHandler.GameEvent.SCORE_UPDATE_REQUEST, new object[] {
                      (int)MultiplayerHandler.GameSubEvent.ACCELERATORTARGET_SCORE_INCREASE_ONE
                    });

                                        //刪除並重置
                                        dumplingEventHandler(MultiplayerHandler.GameEvent.ACCELERATORTARGET_DESTROY_REQUEST, new object[] { numAvailableTarget[i].id });
                                    }


                                }

                            }
                        }
                    }
                }

            }

            //更新網路同步資料 (rotation)
            if (numAvailableTarget[i].t == TargetInfo.Type.ENERMY)
            {
                numAvailableTarget[i].targetInterpolatedRotationTime += Time.deltaTime;
                numAvailableTarget[i].obj.transform.localRotation = Quaternion.Slerp(
                  numAvailableTarget[i].fromInterpolatedRotation,
                  numAvailableTarget[i].targetInterpolatedRoation,
                  numAvailableTarget[i].targetInterpolatedRotationTime);
            }
        }

        /*
        //
        //check missile and gateway
        //
        for (int i = 0; i < tracked_missile.Count; ++i) {
          if (tracked_missile[i].obj == null)
            continue;

          //ray-cast to gateway
          RaycastHit hit;
          Ray r = new Ray(tracked_missile[i].obj.transform.position, tracked_missile[i].obj.GetComponent<Rigidbody>().velocity);
          if (Physics.Raycast(r, out hit, 100.0f, GatewayLM, QueryTriggerInteraction.Ignore)) {
            if ((hit.collider.gameObject.transform.parent.GetComponent<TransGateway>().isEnabled()) &&
                (hit.distance < 1.0f)) {

              Vector3 rp = hit.point - hit.collider.gameObject.transform.position;
              Quaternion q1 = Quaternion.FromToRotation(hit.collider.gameObject.transform.parent.GetComponent<TransGateway>().enterence.transform.forward,
                hit.collider.gameObject.transform.parent.GetComponent<TransGateway>().exitance.transform.forward);
              rp = q1 * rp;
              Vector3 new_pos = hit.collider.gameObject.transform.parent.GetComponent<TransGateway>().exitance.transform.position + rp;

              Quaternion q2 = Quaternion.FromToRotation(hit.collider.gameObject.transform.parent.GetComponent<TransGateway>().enterence.transform.forward,
                tracked_missile[i].obj.GetComponent<Rigidbody>().velocity.normalized);
              Vector3 new_dir = q2 * hit.collider.gameObject.transform.parent.GetComponent<TransGateway>().exitance.transform.forward;

              Debug.Log("480 - set tracked mission position from " + tracked_missile[i].obj.transform.position + " to " + new_pos);
              tracked_missile[i].obj.transform.position = new_pos;
              tracked_missile[i].obj.transform.forward = new_dir;
              tracked_missile[i].obj.GetComponent<Rigidbody>().velocity = tracked_missile[i].obj.GetComponent<Rigidbody>().velocity.magnitude * new_dir;

            }

          }
        }
        */

        //
        //flash radar
        //
        if (flash_radar_red_bottom)
        {
            flash_countdown -= Time.deltaTime;
            if (flash_countdown <= 0.0f)
            {
                flash_radar_red_bottom = false;

                Color c = radarBottomObjRed.GetComponent<MeshRenderer>().material.GetColor("_TintColor");
                c.a = 0.0f;
                radarBottomObjRed.GetComponent<MeshRenderer>().material.SetColor("_TintColor", c);

            }
            else
            {

                flash_counter += Time.deltaTime;
                if (flash_counter > 0.05f)
                {
                    flash_counter = 0.0f;
                    flash_radar_lights_on = !flash_radar_lights_on;

                    Color c = radarBottomObjRed.GetComponent<MeshRenderer>().material.GetColor("_TintColor");
                    c.a = flash_radar_lights_on ? 1.0f : 0.0f;
                    radarBottomObjRed.GetComponent<MeshRenderer>().material.SetColor("_TintColor", c);
                }


            }
        }

        //
        //counting remote shield
        //
        foreach (KeyValuePair<int, RemoteShieldStatus> entry in remote_shield_status)
        {
            RemoteShieldStatus rss = entry.Value;
            if (rss.activiated == true)
            {
                rss.counter -= Time.deltaTime;
                if (rss.counter <= 0.0f)
                {
                    rss.activiated = false;

                    setHostileShield(rss.owner_id, false);
                }
            }
        }

        //
        //counting remote missile
        //
        foreach (KeyValuePair<int, RemoteMissileStatus> entry in remote_missile_status)
        {
            RemoteMissileStatus rms = entry.Value;
            rms.counter -= Time.deltaTime;
            if (rms.counter <= 0.0f)
            {
                setRemoteMissileDestroyed(entry.Value.owner_guid, entry.Value.missile_id);
                remote_missile_status.Remove(entry.Key);
                break;
            }
        }



    }

    public void SpawnTarget()
    {
        SpawnDumpling(numTarget);
        SpawnShield(numShield);
        SpawnAccelerator(numAccelerator);
    }

    void SpawnDumpling(int numTarget)
    {
        //
        //dumpling, hostile
        //
        numAvailableTarget = new List<TargetInfo>();
        for (int i = 0; i < numTarget; ++i)
        {
            GameObject gg = null;
            gg = GameObject.Instantiate(targetPrefab);

            gg.transform.SetParent(transform, false);
            gg.name = "Target_" + target_id_counter;
            gg.layer = LayerMask.NameToLayer("Target");

            RandomSetTargetTransform(gg);

            SetTargetInfo(gg, TargetInfo.Type.NORMAL);

            /*
            ti.radar_pt.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
            ti.radar_pt.transform.localRotation = Quaternion.identity;
            ti.radar_pt.transform.localPosition = translateToRadarSpace(gg.transform.localPosition);
            */
        }
    }

    void SpawnShield(int numShield)
    {
        //shield 道具
        for (int i = 0; i < numShield; ++i)
        {
            GameObject gg = null;
            gg = GameObject.Instantiate(shieldPrefab);

            TargetInfo ti = new TargetInfo();
            ti.radar_pt = null;//ti.radar_pt = GameObject.Instantiate(radar_dumplingPrefab);
                               //ti.radar_pt.transform.SetParent(staticTarget.transform, false);

            gg.transform.SetParent(transform, false);
            gg.name = "Shield_" + target_id_counter;
            gg.layer = LayerMask.NameToLayer("Target");

            RandomSetTargetTransform(gg);

            SetTargetInfo(gg, TargetInfo.Type.SHIELD_TGT);

            //rnd orientation
            gg.transform.Find("FlyShield(Lod)").transform.localRotation = Quaternion.Euler(0.0f, Random.Range(0.0f, 360.0f), 0.0f);
        }
    }

    void SpawnAccelerator(int numAccelerator)
    {
        //加速道具
        for (int i = 0; i < numAccelerator; ++i)
        {
            GameObject gg = null;
            gg = GameObject.Instantiate(acceleratorPrefab);

            TargetInfo ti = new TargetInfo();
            ti.radar_pt = null;//ti.radar_pt = GameObject.Instantiate(radar_dumplingPrefab);
                               //ti.radar_pt.transform.SetParent(staticTarget.transform, false);

            gg.transform.SetParent(transform, false);
            gg.name = "Accelerator_" + target_id_counter;
            gg.layer = LayerMask.NameToLayer("Target");

            RandomSetTargetTransform(gg);

            SetTargetInfo(gg, TargetInfo.Type.SPEEDING_TGT);
        }
    }

    void RandomSetTargetTransform(GameObject gg)
    {
        float rx = 0;
        float rz = 0;

        float rndr = UnityEngine.Random.Range(min_radius, radius);
        float rndang = UnityEngine.Random.Range(0, 360);
        Vector3 p = Vector3.forward * rndr;
        p = Quaternion.AngleAxis(rndang, Vector3.up) * p;
        rx = p.x;
        rz = p.z;

        gg.transform.localPosition = new Vector3(rx, UnityEngine.Random.Range(-height * 0.5f, height * 0.5f), rz);
        gg.transform.localScale = new Vector3(scale, scale, scale);
        gg.transform.localRotation = Quaternion.identity;
    }

    void SetTargetInfo(GameObject gg, TargetInfo.Type type)
    {
        TargetInfo ti = new TargetInfo();
        ti.radar_pt = null;//ti.radar_pt = GameObject.Instantiate(radar_dumplingPrefab);
                           //ti.radar_pt.transform.SetParent(staticTarget.transform, false);
        ti.name = gg.name;
        ti.id = target_id_counter;
        target_id_counter++;

        ti.obj = gg;
        ti.attached_player = null;
        ti.attracted_player_guid = null;
        ti.t = type;

        ti.attraction_done = false;
        ti.curr_status = TargetInfo.Status.AVAILABLE;
        numAvailableTarget.Add(ti);
        mTargetInfo_NameMapper.Add(ti.name, ti);
    }

    /// <summary>
    /// 把敌人身上的包子数显示在小地图箭头上
    /// </summary>
    public void CreateHostileScoreUI(TargetInfo info)
    {
        GameObject hostile = info.radar_hostileArrow;
        int groupId = info.team_id;

        GameObject tmp_dump_collected_root = new GameObject();
        tmp_dump_collected_root.name = "root";
        tmp_dump_collected_root.transform.SetParent(hostile.transform, false);
        tmp_dump_collected_root.transform.localPosition = new Vector3(0, 0, -1);

        GameObject tmp_dump_collected = new GameObject();
        tmp_dump_collected.name = "DumplingCollected_symbol";
        tmp_dump_collected.transform.SetParent(tmp_dump_collected_root.transform, false);
        tmp_dump_collected.transform.localRotation = Quaternion.identity;
        tmp_dump_collected.transform.localScale = Vector3.one * 0.3f;
        faceCam face = tmp_dump_collected.AddComponent<faceCam>();
        face.adj_angle = 180;

        SpriteRenderer sr = tmp_dump_collected.AddComponent<SpriteRenderer>();
        sr.sprite = dumplingCollected;
        Debug.Log("ScoreUI GroupID=" + groupId);

        SpriteRenderer[] srs;
        Color color = ChooseTeamColor(groupId);
        sr.color = color;
        srs = constructDigits(tmp_dump_collected, color);
        sr_otherPlayerDumplingCollected.Add(srs);
    }

    /// <summary>
    /// 在雷达箭头上显示id
    /// </summary>
    public void CreatePlayerInGameIdUI(TargetInfo info, int inGameId)
    {
        GameObject hostile = info.radar_hostileArrow;
        int groupId = info.team_id;

        GameObject go = new GameObject();
        go.name = "InGameId";
        go.transform.SetParent(hostile.transform, false);
        go.transform.localPosition = new Vector3(0, 3, 0);

        SpriteRenderer sr = go.AddComponent<SpriteRenderer>();
        sr.sprite = dumpling_counter_digits[inGameId];

        Color color = ChooseTeamColor(groupId);
        sr.color = color;
    }

    // Update is called once per frame
    //int my_target_score = 0;
    //int my_unloaded_target_score = 0;

    //int hostile_target_score = 0;
    //int hostile_unloaded_target_score = 0;


    Vector3 translateToRadarSpace(Vector3 world_position, bool attach_gnd = false)
    {
        Vector3 ret = Vector3.zero;

        float ratio = radarRadius * 0.8f / radius;
        ret = world_position * ratio;
        float ori_height = height * ratio;
        float height_scale = 1.0f;// radarHeight / ori_height;
        ret = new Vector3(ret.x, ret.y * height_scale, ret.z);
        //ret += new Vector3(0.0f, radarHeight * 0.5f, 0.0f);

        return ret;
    }

    public void updateRadarProjection(bool vivemode, Vector3 cam_posi, Vector3 cam_dir, Quaternion cam_rot, Vector3 eyePosi)
    {
        if (enableHoloRadar == false)
            return;

        if (radarBottomObj == null)
        {
            //create gameobject
            radarBottomObj = GameObject.CreatePrimitive(PrimitiveType.Quad);
            radarBottomObj.GetComponent<MeshCollider>().enabled = false;
            radarBottomObj.name = "Bottom";
            radarBottomObj.transform.SetParent(radarOrientation.transform, false);
            radarBottomObj.transform.localPosition = new Vector3(0.0f, -0.45f, 0.0f);
            radarBottomObj.transform.localScale = Vector3.one * (radarRadius * 2);
            radarBottomObj.transform.localRotation = Quaternion.AngleAxis(90.0f, Vector3.right);

            radarBottomObj.GetComponent<MeshRenderer>().material = radarBottomImage;

            radarBottomObjRed = GameObject.CreatePrimitive(PrimitiveType.Quad);
            radarBottomObjRed.GetComponent<MeshCollider>().enabled = false;
            radarBottomObjRed.name = "BottomRed";
            radarBottomObjRed.transform.SetParent(radarOrientation.transform, false);
            radarBottomObjRed.transform.localPosition = new Vector3(0.0f, -0.45f, 0.0f);
            radarBottomObjRed.transform.localScale = Vector3.one * (radarRadius * 2.19f);
            radarBottomObjRed.transform.localRotation = Quaternion.AngleAxis(90.0f, Vector3.right);

            radarBottomObjRed.GetComponent<MeshRenderer>().material = radarBottomImageRed;
            Color c = radarBottomObjRed.GetComponent<MeshRenderer>().material.GetColor("_TintColor");
            c.a = 0.0f;
            radarBottomObjRed.GetComponent<MeshRenderer>().material.SetColor("_TintColor", c);
        }

        //update root obj
        radarRoot.transform.position = cam_posi;
        radarRoot.transform.forward = cam_dir;
        radarRoot.transform.rotation = cam_rot;

        //update dumpling statistics ui root
        radarRootForDumplingCounter.transform.position = cam_posi;
        radarRootForDumplingCounter.transform.forward = cam_dir;
        if (vivemode)
            radarRootForDumplingCounter.transform.rotation = cam_rot;


        //update target pt
        for (int i = 0; i < numAvailableTarget.Count; ++i)
        {
            if (numAvailableTarget[i].radar_pt == null)
                continue;

            if (numAvailableTarget[i].t == TargetInfo.Type.ENERMY)
            {
                numAvailableTarget[i].radar_pt.transform.localPosition = translateToRadarSpace(numAvailableTarget[i].obj.transform.localPosition);

                if (numAvailableTarget[i].showOnRadar)
                {
                    numAvailableTarget[i].radar_hostileArrow.transform.localPosition = numAvailableTarget[i].radar_pt.transform.localPosition;
                    numAvailableTarget[i].radar_hostileArrow.transform.localRotation = numAvailableTarget[i].obj.transform.localRotation;
                    numAvailableTarget[i].radar_hostileArrow.transform.localRotation *= Quaternion.AngleAxis(90, Vector3.right);
                }
            }
            else
            {
                //Debug.Log(numAvailableTarget[i].obj.name);
                //numAvailableTarget[i].obj.transform.Find("labelSz").gameObject.GetComponent<LabelHandler>().setupCameraPosition(cam_posi);
            }
            numAvailableTarget[i].radar_pt.transform.forward = eyePosi - numAvailableTarget[i].radar_pt.transform.position;

        }

        radar_player.transform.localPosition = translateToRadarSpace(cam_posi - transform.localPosition);
        radar_player.transform.forward = eyePosi - radar_player.transform.position;

        radar_arrow.transform.localPosition = translateToRadarSpace(cam_posi - transform.localPosition);
        radar_arrow.transform.localRotation = Quaternion.LookRotation(new Vector3(cam_dir.x, 0.0f, cam_dir.z), Vector3.up);
        radar_arrow.transform.localRotation *= Quaternion.AngleAxis(90, Vector3.right);

        //update missile pt
        for (int i = 0; i < tracked_missile.Count;/* ++i*/)
        {
            if (tracked_missile[i].obj == null)
            {
                //Debug.Log("407 - missile destroyed (idx:"+tracked_missile[i].idx+")");

                dumplingEventHandler(MultiplayerHandler.GameEvent.MISSILE_SELF_DESTROYED, new object[] {
          tracked_missile[i].idx
        });

                GameObject.Destroy(radar_missile[i]);
                radar_missile.RemoveAt(i);
                tracked_missile.RemoveAt(i);

            }
            else
            {

                radar_missile[i].transform.localPosition = translateToRadarSpace(tracked_missile[i].obj.transform.position - transform.localPosition);
                radar_missile[i].transform.forward = eyePosi - radar_missile[i].transform.position;

                ++i;
            }
        }

        for (int i = 0; i < tracked_remote_missile.Count; ++i)
        {
            if (tracked_remote_missile[i].obj != null && tracked_remote_missile[i].remote_radar_missile != null)
            {
                tracked_remote_missile[i].remote_radar_missile.transform.localPosition = translateToRadarSpace(tracked_remote_missile[i].obj.transform.position - transform.localPosition);
                tracked_remote_missile[i].remote_radar_missile.transform.forward = eyePosi - tracked_remote_missile[i].remote_radar_missile.transform.position;
            }
        }

        //update spawn_pt
        //mPlayerSpawnPt.transform.forward = eyePosi - mPlayerSpawnPt.transform.position;
        //mHostileSpwanPt.transform.forward = eyePosi - mHostileSpwanPt.transform.position;


        //RADAR FOLLOW VIEWDIR
        if (followViewDir)
        {
            Vector3 w_arrow_dir = radar_arrow.transform.TransformDirection(Vector3.up);
            radarOrientation.transform.localRotation = Quaternion.FromToRotation(radarOrientation.transform.InverseTransformDirection(w_arrow_dir), Vector3.forward);
        }

    }

    Vector3 hv(Vector3 i)
    {
        return new Vector3(i.x, 0.0f, i.z);
    }

#if UNITY_EDITOR
    void OnDrawGizmosSelected()
    {
        UnityEditor.Handles.DrawWireDisc(transform.position - new Vector3(0.0f, height * 0.5f, 0.0f), Vector3.up, radius);
        UnityEditor.Handles.DrawWireDisc(transform.position + new Vector3(0.0f, height * 0.5f, 0.0f), Vector3.up, radius);
    }
#endif

    public List<TargetInfo> pickAllAttachedTarget()
    {
        List<TargetInfo> ti_arr = new List<TargetInfo>();
        for (int j = 0; j < numAvailableTarget.Count; ++j)
        {
            if (numAvailableTarget[j].is_attached_by_self == true && numAvailableTarget[j].obj.name.Contains("Target"))
            {
                ti_arr.Add(numAvailableTarget[j]);
            }
        }

        return ti_arr;
    }

    public List<TargetInfo> randomPickAttachedTarget(int amounts)
    {
        List<TargetInfo> ti_arr = new List<TargetInfo>();
        for (int j = 0; j < numAvailableTarget.Count; ++j)
        {
            if (numAvailableTarget[j].is_attached_by_self == true &&
                numAvailableTarget[j].attraction_done == true &&
                numAvailableTarget[j].curr_status == TargetInfo.Status.COLLECTED)
            {
                ti_arr.Add(numAvailableTarget[j]);

                if (ti_arr.Count == amounts)
                {
                    break;
                }

            }
        }
        return ti_arr;
    }

    public void resetSightDisplayerStatus()
    {
        if (sightGameObj != null)
        {
            sightGameObj.GetComponent<SightDisplayer>().setMode(SightDisplayer.Mode.NORMAL);
        }

        for (int i = 0; i < traceNumber; ++i)
        {
            LineRenderer lr = highlightRect[i].GetComponent<LineRenderer>();
            lr.enabled = false;
        }
    }

    bool isValidTarget(GameObject obj, int myTeamId)
    {

        //disposed gameobject
        if (obj.activeSelf == false)
            return false;

        //hidden base dumpling
        if (obj.name == "bun_root" &&
            obj.transform.parent.parent.GetComponent<UnloadBunAnim>().isBunShowing() == false)
            return false;

        if (mTargetInfo_NameMapper.ContainsKey(obj.name))
        {
            //disposed gameobject
            if (mTargetInfo_NameMapper[obj.name].curr_status == TargetInfo.Status.DISPOSED)
                return false;

            //collected gameobject
            if (mTargetInfo_NameMapper[obj.name].is_attached_by_self)
                return false;

            //teammate
            if (obj.name.Contains("Hostile") && mTargetInfo_NameMapper[obj.name].team_id == myTeamId)
                return false;

            //collected by teammate
            if (mTargetInfo_NameMapper[obj.name].attracted_player_guid != null &&
                mTargetInfo_NameMapper["Hostile_" + getHostileIdxByGUID(mTargetInfo_NameMapper[obj.name].attracted_player_guid)].team_id == myTeamId)
                return false;
        }

        return true;
    }

    public List<TargetInfo> findAvailableTarget(int myTeamId, Vector3 posi, Vector3 dir, bool highlight_target)
    {

        tracingFrustum.transform.position = posi;
        tracingFrustum.transform.forward = dir;

        // step 0
        // choose candidate collide with the view frustum
        List<GameObject> c = tf.getTrackedObject();

        //remove attracted target
        for (int i = 0; i < c.Count;)
        {
            if (isValidTarget(c[i], myTeamId) == false)
            {
                c.RemoveAt(i);
            }
            else
            {
                ++i;
            }
        }

        if (c.Count == 0)
        {
            for (int i = 0; i < traceNumber; ++i)
            {
                LineRenderer lr = highlightRect[i].GetComponent<LineRenderer>();
                lr.enabled = false;
            }

            if (sightGameObj != null)
            {
                sightGameObj.GetComponent<SightDisplayer>().setMode(SightDisplayer.Mode.NORMAL);
            }

            return null;
        }


        List<GameObject> t = new List<GameObject>();

        // step 1
        //choose top 5 targets
        c.Sort(delegate (GameObject g0, GameObject g1)
        {
            Vector3 g0_ray_pt = ProjectPointLine(g0.transform.position, posi, posi + dir * traceLength);
            float g0_dist = Vector3.Distance(g0.transform.position, g0_ray_pt);
            Vector3 g1_ray_pt = ProjectPointLine(g1.transform.position, posi, posi + dir * traceLength);
            float g1_dist = Vector3.Distance(g1.transform.position, g1_ray_pt);
            return g0_dist.CompareTo(g1_dist);
        });

        t.AddRange(c.GetRange(0, c.Count > traceNumber ? traceNumber : c.Count));

        //step 2
        t.Sort(delegate (GameObject g0, GameObject g1)
        {
            float g0_dist = Vector3.Distance(g0.transform.position, posi);
            float g1_dist = Vector3.Distance(g1.transform.position, posi);
            return g0_dist.CompareTo(g1_dist);
        });

        if (highlight_target == true)
        {

            for (int i = 0; i < traceNumber; ++i)
            {
                LineRenderer lr = highlightRect[i].GetComponent<LineRenderer>();
                if (i >= t.Count)
                {
                    lr.enabled = false;
                    continue;
                }
                lr.enabled = true;
                lr.positionCount = 5;
                float rect_half_sz = 20.0f * 0.5f;
                if (mTargetInfo_NameMapper[t[i].name].t == TargetInfo.Type.UNLOADPT_TGT)
                {
                    rect_half_sz = 100.0f * 0.5f;
                }
                Vector3[] v = new Vector3[5];
                Vector3 vdir = Vector3.up;
                Vector3 rdir = Vector3.Cross(posi - t[i].transform.position, vdir).normalized;
                vdir = Vector3.Cross(posi - t[i].transform.position, rdir).normalized;
                Vector3 shift = new Vector3(0.0f, 0.0f, 0.0f);
                v[0] = t[i].transform.position + vdir * rect_half_sz + rdir * rect_half_sz + shift;
                v[1] = t[i].transform.position + vdir * rect_half_sz + rdir * -rect_half_sz + shift;
                v[2] = t[i].transform.position + vdir * -rect_half_sz + rdir * -rect_half_sz + shift;
                v[3] = t[i].transform.position + vdir * -rect_half_sz + rdir * rect_half_sz + shift;
                v[4] = v[0];
                lr.SetPositions(v);

                float lw = 20.0f - 10.0f;
                float d = traceLength - minTraceLength;
                float w = (Vector3.Distance(t[0].transform.position, posi) - minTraceLength) / d;
                if (mTargetInfo_NameMapper[t[i].name].t == TargetInfo.Type.UNLOADPT_TGT)
                {
                    lr.startWidth = 30.0f + (w * lw);
                    lr.endWidth = 30.0f + (w * lw);
                }
                else
                {
                    lr.startWidth = 10.0f + (w * lw);
                    lr.endWidth = 10.0f + (w * lw);
                }
                if (mTargetInfo_NameMapper[t[i].name].t == TargetInfo.Type.ENERMY ||
                     mTargetInfo_NameMapper[t[i].name].curr_status == TargetInfo.Status.COLLECTED ||
                     mTargetInfo_NameMapper[t[i].name].curr_status == TargetInfo.Status.COLLECTING)
                {
                    lr.startColor = Color.red;
                    lr.endColor = Color.red;

                    if (sightGameObj != null)
                    {
                        sightGameObj.GetComponent<SightDisplayer>().setMode(SightDisplayer.Mode.RED);
                    }

                    //dumplingEventHandler(MultiplayerHandler.GameEvent.RADAR_LOCKED_BY_HOSTILE, null);

                }
                else
                {
                    lr.startColor = Color.green;
                    lr.endColor = Color.green;

                    if (sightGameObj != null)
                    {
                        sightGameObj.GetComponent<SightDisplayer>().setMode(SightDisplayer.Mode.GREEN);
                    }

                    //dumplingEventHandler(MultiplayerHandler.GameEvent.RADAR_UNLOCK, null);
                }
            }
        }

        List<TargetInfo> ret = new List<TargetInfo>();
        int n = Mathf.Min(traceNumber, t.Count);
        for (int i = 0; i < n; ++i)
        {
            ret.Add(mTargetInfo_NameMapper[t[i].name]);
        }

        return ret;
    }

    Vector3 ProjectPointLine(Vector3 point, Vector3 lineStart, Vector3 lineEnd)
    {
        Vector3 rhs = point - lineStart;
        Vector3 vector2 = lineEnd - lineStart;
        float magnitude = vector2.magnitude;
        Vector3 lhs = vector2;
        if (magnitude > 1E-06f)
        {
            lhs = (Vector3)(lhs / magnitude);
        }
        float num2 = Mathf.Clamp(Vector3.Dot(lhs, rhs), 0f, magnitude);
        return (lineStart + ((Vector3)(lhs * num2)));
    }

    //steal target with (unknown dump_id)
    public bool clientStealTarget(GameObject attracter, string attracter_guid, Vector3 uld_posi, Vector3 shift, out int dump_id)
    {
        dump_id = -1;

        //dequeue disposed dumpling
        for (int i = 0; i < numAvailableTarget.Count; ++i)
        {
            if (numAvailableTarget[i].t == TargetInfo.Type.NORMAL && numAvailableTarget[i].curr_status == TargetInfo.Status.DISPOSED)
            {
                dump_id = numAvailableTarget[i].id;
                Debug.Log("824 - found disposed dump id = " + dump_id);
                break;
            }
        }

        if (dump_id == -1)
        {
            Debug.LogWarning("830 - could not found any dumpling usable");
            return false;
        }

        return clientStealTarget(attracter, attracter_guid, uld_posi, shift, dump_id);
    }

    //steal target with known id
    public bool clientStealTarget(GameObject attracter, string attracter_guid, Vector3 uld_posi, Vector3 shift, int dump_id)
    {
        //reborn target
        Debug.Log("840 - reset dumpling (id=" + dump_id + "), with position " + uld_posi);
        resetDumpling(dump_id, uld_posi);

        //attract dumpling
        return clientAttractTarget(attracter, attracter_guid, dump_id, shift);
    }

    //steal target with (unknown dump_id)
    public bool remoteStealTarget(string thief_guid, Vector3 uld_posi, Vector3 shift, out int dump_id)
    {
        dump_id = -1;

        //dequeue disposed dumpling
        for (int i = 0; i < numAvailableTarget.Count; ++i)
        {
            if (numAvailableTarget[i].t == TargetInfo.Type.NORMAL && numAvailableTarget[i].curr_status == TargetInfo.Status.DISPOSED)
            {
                dump_id = numAvailableTarget[i].id;
                Debug.Log("850 - found disposed dump id = " + dump_id);
                break;
            }
        }

        if (dump_id == -1)
        {
            Debug.LogWarning("856 - could not found any dumpling usable");
            return false;
        }

        return remoteStealTarget(thief_guid, uld_posi, shift, dump_id);
    }

    public bool remoteStealTarget(string thief_guid, Vector3 uld_posi, Vector3 shift, int dump_id)
    {
        //reborn target
        Debug.Log("870 - reset dumpling (id=" + dump_id + "), with position " + uld_posi);
        resetDumpling(dump_id, uld_posi);

        return remoteAttractTarget(thief_guid, dump_id, shift);
    }

    public bool clientAttractAcceleratorTarget(GameObject attracter, string attracter_guid, int dump_id, Vector3 shift)
    {
        Debug.Log("817 - client attract accelerator target, id=" + dump_id + ")");

        TargetInfo id = mTargetInfo_NameMapper["Accelerator_" + dump_id];
        if (id.curr_status != TargetInfo.Status.AVAILABLE)
        {
            Debug.LogWarning("820 - accelerator target is not available (id=" + dump_id + ")");
            return false;
        }

        id.delay_attraction_counter = 0.0f;
        id.curr_status = TargetInfo.Status.COLLECTING;
        id.attached_player = attracter;
        id.attracted_player_guid = attracter_guid;
        id.is_attached_by_self = true;

        if (id.radar_pt != null)
        {
            id.radar_pt.SetActive(false);
        }

        //播放吸收特效
        //id.obj.transform.Find("bun").GetComponent<Animator>().SetTrigger("attracted");
        //id.obj.transform.Find("bun").GetComponent<PrefabInstantiator>().instantiatePrefab();
        id.obj.transform.Find("Eat_FX").gameObject.SetActive(true);

        id.shift_attraction_posi = shift;

        return true;
    }

    public bool clientAttractShieldTarget(GameObject attracter, string attracter_guid, int dump_id, Vector3 shift)
    {
        Debug.Log("817 - client attract shield target, id=" + dump_id + ")");

        TargetInfo id = mTargetInfo_NameMapper["Shield_" + dump_id];
        if (id.curr_status != TargetInfo.Status.AVAILABLE)
        {
            Debug.LogWarning("820 - shield target is not available (id=" + dump_id + ")");
            return false;
        }

        id.delay_attraction_counter = 0.0f;
        id.curr_status = TargetInfo.Status.COLLECTING;
        id.attached_player = attracter;
        id.attracted_player_guid = attracter_guid;
        id.is_attached_by_self = true;

        if (id.radar_pt != null)
        {
            id.radar_pt.SetActive(false);
        }

        //播放吸收特效
        //id.obj.transform.Find("bun").GetComponent<Animator>().SetTrigger("attracted");
        //id.obj.transform.Find("bun").GetComponent<PrefabInstantiator>().instantiatePrefab();
        id.obj.transform.Find("Eat_FX").gameObject.SetActive(true);

        id.shift_attraction_posi = shift;

        return true;
    }

    public bool clientAttractTarget(GameObject attracter, string attracter_guid, int dump_id, Vector3 shift)
    {
        Debug.Log("817 - client attract target, id=" + dump_id + ")");

        TargetInfo id = mTargetInfo_NameMapper["Target_" + dump_id];
        if (id.curr_status != TargetInfo.Status.AVAILABLE)
        {
            Debug.LogWarning("820 - target is not available (id=" + dump_id + ")");
            return false;
        }

        id.delay_attraction_counter = 0.0f;
        id.curr_status = TargetInfo.Status.COLLECTING;
        id.attached_player = attracter;
        id.attracted_player_guid = attracter_guid;
        id.is_attached_by_self = true;

        if (id.radar_pt != null)
        {
            id.radar_pt.SetActive(false);
        }

        id.obj.transform.Find("bun").GetComponent<Animator>().SetTrigger("attracted");
        id.obj.transform.Find("bun").GetComponent<PrefabInstantiator>().instantiatePrefab();

        id.shift_attraction_posi = shift;

        return true;
    }

    public bool remoteAttractAcceleratorTarget(string hostile_guid, int dump_id, Vector3 shift)
    {
        Debug.Log("1124 - remote attract accelerator, id=" + dump_id + ", guid=" + hostile_guid + ")");

        TargetInfo tgid = mTargetInfo_NameMapper["Accelerator_" + dump_id];
        if (tgid.curr_status != TargetInfo.Status.AVAILABLE)
        {
            Debug.LogWarning("1259 - accelerator is not available (id=" + dump_id + ")");
            return false;
        }

        TargetInfo hostile_tgid = mTargetInfo_NameMapper["Hostile_" + getHostileIdxByGUID(hostile_guid)];

        tgid.delay_attraction_counter = 0.0f;
        tgid.curr_status = TargetInfo.Status.COLLECTING;
        tgid.attached_player = hostile_tgid.obj;
        tgid.attracted_player_guid = hostile_guid;

        tgid.is_attached_by_self = false;
        if (tgid.radar_pt != null)
        {
            tgid.radar_pt.SetActive(false);
        }
        tgid.shift_attraction_posi = shift;

        //播放吸收特效
        //...
        //tgid.obj.transform.Find("bun").GetComponent<Animator>().SetTrigger("attracted");
        //tgid.obj.transform.Find("bun").GetComponent<PrefabInstantiator>().instantiatePrefab();
        //tgid.obj.GetComponent<TrailRenderer>().enabled = true;
        tgid.obj.transform.Find("Eat_FX").gameObject.SetActive(true);

        tgid.attached_player.GetComponentInChildren<PlayerModelController>().SetBearState(PlayerModelController.BearState.draw);

        return true;

    }

    public bool remoteAttractShieldTarget(string hostile_guid, int dump_id, Vector3 shift)
    {
        Debug.Log("1124 - remote attract shield, id=" + dump_id + ", guid=" + hostile_guid + ")");

        TargetInfo tgid = mTargetInfo_NameMapper["Shield_" + dump_id];
        if (tgid.curr_status != TargetInfo.Status.AVAILABLE)
        {
            Debug.LogWarning("860 - shield is not available (id=" + dump_id + ")");
            return false;
        }

        TargetInfo hostile_tgid = mTargetInfo_NameMapper["Hostile_" + getHostileIdxByGUID(hostile_guid)];

        tgid.delay_attraction_counter = 0.0f;
        tgid.curr_status = TargetInfo.Status.COLLECTING;
        tgid.attached_player = hostile_tgid.obj;
        tgid.attracted_player_guid = hostile_guid;

        tgid.is_attached_by_self = false;
        if (tgid.radar_pt != null)
        {
            tgid.radar_pt.SetActive(false);
        }
        tgid.shift_attraction_posi = shift;

        //播放吸收特效
        //...
        //tgid.obj.transform.Find("bun").GetComponent<Animator>().SetTrigger("attracted");
        //tgid.obj.transform.Find("bun").GetComponent<PrefabInstantiator>().instantiatePrefab();
        //tgid.obj.GetComponent<TrailRenderer>().enabled = true;
        tgid.obj.transform.Find("Eat_FX").gameObject.SetActive(true);

        tgid.attached_player.GetComponentInChildren<PlayerModelController>().SetBearState(PlayerModelController.BearState.draw);

        return true;

    }

    public bool remoteAttractTarget(string hostile_guid, int dump_id, Vector3 shift)
    {
        Debug.Log("817 - remote attract target, id=" + dump_id + ", guid=" + hostile_guid + ")");

        TargetInfo tgid = mTargetInfo_NameMapper["Target_" + dump_id];
        if (tgid.curr_status != TargetInfo.Status.AVAILABLE)
        {
            Debug.LogWarning("860 - target is not available (id=" + dump_id + ")");
            return false;
        }

        TargetInfo hostile_tgid = mTargetInfo_NameMapper["Hostile_" + getHostileIdxByGUID(hostile_guid)];

        tgid.delay_attraction_counter = 0.0f;
        tgid.curr_status = TargetInfo.Status.COLLECTING;
        tgid.attached_player = hostile_tgid.obj;
        tgid.attracted_player_guid = hostile_guid;

        tgid.is_attached_by_self = false;
        if (tgid.radar_pt != null)
        {
            tgid.radar_pt.SetActive(false);
        }
        tgid.shift_attraction_posi = shift;

        tgid.obj.transform.Find("bun").GetComponent<Animator>().SetTrigger("attracted");
        tgid.obj.transform.Find("bun").GetComponent<PrefabInstantiator>().instantiatePrefab();

        tgid.obj.GetComponent<TrailRenderer>().enabled = true;

        tgid.attached_player.GetComponentInChildren<PlayerModelController>().SetBearState(PlayerModelController.BearState.draw);

        return true;

    }

    //player interect (input from keyboard/vive ...)
    public bool attractTarget(List<TargetInfo> id, GameObject attracter)
    {
        if (id == null)
            return false;

        for (int i = 0; i < id.Count; ++i)
        {
            if (id[i].curr_status != TargetInfo.Status.AVAILABLE)
                continue;

            if (id[i].t == TargetInfo.Type.ENERMY)
            {
                continue;
            }
            else
          if (id[i].t == TargetInfo.Type.NORMAL)
            {

                //跟 LEADER 要求收包子, 收包子動作交由 LEADER 來發佈
                Debug.Log("884 - request collect dumpling id=" + id[i].id);
                if (dumplingEventHandler != null)
                {
                    //SEND TO HOST, (OR PROCESS SCORE THEN BROADCAST TO ALL)
                    //deliver to PlayerController.dumplingEventHandler()
                    dumplingEventHandler(MultiplayerHandler.GameEvent.DUMPLING_COLLECTING_REQUEST, new object[] { id[i].id });
                }

            }
            else
          if (id[i].t == TargetInfo.Type.SHIELD_TGT)
            {
                Debug.Log("1164 - request collect shield id=" + id[i].id);
                if (dumplingEventHandler != null)
                {
                    dumplingEventHandler(MultiplayerHandler.GameEvent.SHIELDTARGET_COLLECTING_REQUEST, new object[] { id[i].id });
                }
            }
            else
          if (id[i].t == TargetInfo.Type.UNLOADPT_TGT)
            {
                //ATTRACT UNLOADED DUMPLING (FETCH DUMPLING FROM DISPOSED DUMPLING POOL)
                if (dumplingEventHandler != null)
                {
                    dumplingEventHandler(MultiplayerHandler.GameEvent.DUMPLING_STEALING_REQUEST, new object[] { });
                }

            }
            else
          if (id[i].t == TargetInfo.Type.SPEEDING_TGT)
            {
                Debug.Log("1332 - request collect accelerator id=" + id[i].id);
                if (dumplingEventHandler != null)
                {
                    dumplingEventHandler(MultiplayerHandler.GameEvent.ACCELERATORTARGET_COLLECTING_REQUEST, new object[] { id[i].id });
                }
            }
        }
        return true;
    }

    void launchNullTargetMissile(Vector3 startPt, Vector3 viewDir, Vector3 viewVelocity)
    {

        GameObject m = GameObject.Instantiate(missilePrefab);

        missile_id++;
        m.name = "LocalMissile_" + missile_id;
        //Debug.Log("645 - create null target missile (idx:"+missile_id+")");

        m.transform.position = startPt + viewDir * 10.0f;// + Vector3.up * -5.0f;
        m.transform.forward = viewDir;
        m.GetComponent<Rigidbody>().velocity = viewVelocity;
        m.GetComponent<MissileBehaviours.Actions.Explode>().triggers.Add(m.GetComponent<MissileBehaviours.Triggers.OnCollision>());

        m.GetComponent<MissileBehaviours.Controller.MissileController>().Target = null;

        m.GetComponent<MissileBehaviours.Actions.MissileRepoter>().dm = this;
        m.GetComponent<MissileBehaviours.Actions.MissileRepoter>().ti = null;
        m.GetComponent<MissileBehaviours.Actions.MissileRepoter>().missile_life *= 0.25f;

        TrackedMissileInfo tmi = new TrackedMissileInfo();
        tmi.obj = m;
        tmi.name = m.name;
        tmi.idx = missile_id;
        tracked_missile.Add(tmi);

        GameObject tmp_rm = GameObject.Instantiate(radarMissilePrefab);
        tmp_rm.transform.SetParent(radarOrientation.transform, false);
        tmp_rm.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
        tmp_rm.transform.localRotation = Quaternion.identity;
        radar_missile.Add(tmp_rm);

        tmp_rm.transform.Find("MissileID").GetComponent<TextMesh>().text = "L" + missile_id + "(NIL)";

        GameObject trail = GameObject.Instantiate(radar_trailPrefab);
        trail.GetComponent<TrailPrinter>().trailObj = tmp_rm;
        trail.transform.SetParent(radarOrientation.transform, false);
        trail.transform.localPosition = Vector3.zero;

        dumplingEventHandler(MultiplayerHandler.GameEvent.MISSILE_LAUNCH, new object[] {
      missile_id,
      startPt.x, startPt.y, startPt.z,
      viewVelocity.x, viewVelocity.y, viewVelocity.z,
      m.GetComponent<MissileBehaviours.Actions.MissileRepoter>().missile_life
    });
    }

    int missile_id = 0;
    public bool destroyTarget(List<TargetInfo> id, Vector3 startPt, Vector3 viewDir, Vector3 viewVelocity)
    {
        if (id == null)
        {
            launchNullTargetMissile(startPt, viewDir, viewVelocity);
            return true;
        }

        for (int i = 0; i < id.Count; ++i)
        {

            bool could_destroy = false;
            if ((id[i].curr_status == TargetInfo.Status.AVAILABLE ||
                id[i].curr_status == TargetInfo.Status.COLLECTING ||
                id[i].curr_status == TargetInfo.Status.COLLECTED) &&
                id[i].obj.activeSelf == true)
            {
                could_destroy = true;
            }

            if (could_destroy == false || id[i].obj == null)
            {
                //Debug.LogWarning("546 - invalid target : " + id[i].obj.name+", launch nulltarget missile");
                launchNullTargetMissile(startPt, viewDir, viewVelocity);
                return true;
            }

            GameObject m = GameObject.Instantiate(missilePrefab);
            missile_id++;

            //Debug.Log("690 - create missile (idx:"+missile_id+", target:" + id[i].obj.name + ")");
            m.name = "LocalMissile_" + missile_id;

            m.transform.position = startPt + viewDir * 10.0f;// + Vector3.up * -5.0f;
            m.transform.forward = viewDir;
            m.GetComponent<Rigidbody>().velocity = viewVelocity;
            m.GetComponent<MissileBehaviours.Actions.Explode>().triggers.Add(m.GetComponent<MissileBehaviours.Triggers.OnCollision>());

            if (id[i].hmd != null)
            {
                m.GetComponent<MissileBehaviours.Controller.MissileController>().Target = id[i].hmd.transform; //鎖定敵人的頭顯位置
            }
            else
            {
                m.GetComponent<MissileBehaviours.Controller.MissileController>().Target = id[i].obj.transform;
            }

            m.GetComponent<MissileBehaviours.Actions.MissileRepoter>().dm = this;
            m.GetComponent<MissileBehaviours.Actions.MissileRepoter>().ti = id[i];

            TrackedMissileInfo tmi = new TrackedMissileInfo();
            tmi.obj = m;
            tmi.name = m.name;
            tmi.idx = missile_id;
            tmi.target = id[i];
            tracked_missile.Add(tmi);

            GameObject tmp_rm = GameObject.Instantiate(radarMissilePrefab);
            tmp_rm.transform.SetParent(radarOrientation.transform, false);
            tmp_rm.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
            tmp_rm.transform.localRotation = Quaternion.identity;
            radar_missile.Add(tmp_rm);

            tmp_rm.transform.Find("MissileID").GetComponent<TextMesh>().text = "L" + missile_id + "(T" + id[i].id + ")";

            GameObject trail = GameObject.Instantiate(radar_trailPrefab);
            trail.GetComponent<TrailPrinter>().trailObj = tmp_rm;
            trail.transform.SetParent(radarOrientation.transform, false);
            trail.transform.localPosition = Vector3.zero;

            dumplingEventHandler(MultiplayerHandler.GameEvent.MISSILE_LAUNCH, new object[] {
        missile_id,
        startPt.x, startPt.y, startPt.z,
        viewVelocity.x, viewVelocity.y, viewVelocity.z,
        m.GetComponent<MissileBehaviours.Actions.MissileRepoter>().missile_life
      });

        }

        return true;
    }

    //the func. will be called when missile reach the target
    public bool targetDestroyed(GameObject missileObj, string target_name)
    {

        if (mTargetInfo_NameMapper.ContainsKey(target_name) == false)
        {
            Debug.Log("748 - try destroy object : " + target_name);
            GameObject.Destroy(missileObj);

            Debug.LogWarning("583 - invalid target : " + target_name);
            return false;
        }

        TargetInfo tgid = mTargetInfo_NameMapper[target_name];
        return targetDestroyed(missileObj, tgid);
    }

    public bool targetDestroyed(GameObject missileObj, TargetInfo tgid)
    {

        missileObj.transform.Find("Fire_Rock/Meteo").gameObject.SetActive(false);
        missileObj.transform.Find("Fire_Rock/Fire_flak").gameObject.SetActive(false);
        missileObj.transform.Find("Fire_Rock").gameObject.AddComponent<CommitSuicide>();
        missileObj.transform.Find("Fire_Rock").SetParent(missileObj.transform.parent);
        GameObject.Destroy(missileObj);

        if (tgid != null)
        {
            Debug.Log("792 - try destroy object : " + tgid.obj.name);
            bool could_destroy = false;
            if ((tgid.curr_status == TargetInfo.Status.AVAILABLE ||
                tgid.curr_status == TargetInfo.Status.COLLECTING ||
                tgid.curr_status == TargetInfo.Status.COLLECTED) &&
                tgid.obj.activeSelf == true)
            {
                could_destroy = true;
            }

            if (could_destroy == false)
            {
                Debug.LogWarning("622 - invalid target : " + missileObj.name);
                return false;
            }

            if (tgid.name.Contains("Target"))
            {
                Debug.Log("813 - send dumpling destroyed with id=" + tgid.id);
                dumplingEventHandler(MultiplayerHandler.GameEvent.DUMPLING_DESTROY_REQUEST, new object[] { tgid.id });
                //tgid.obj.transform.Find("bun").GetComponent<PrefabInstantiator>().instantiateExplosion();

            }
            else
            if (tgid.name.Contains("Hostile"))
            {
                Debug.Log("988 - player (" + tgid.player_guid + ") hit");
                dumplingEventHandler(MultiplayerHandler.GameEvent.MISSILE_HIT_TARGET, new object[] { tgid.player_guid });

                tgid.radar_hostileArrow.SetActive(false);

                tgid.obj.GetComponentInChildren<PlayerModelController>().SetBearState(PlayerModelController.BearState.hurt);

                //effect
                GameObject effect = GameObject.Instantiate(playerHit);
                effect.transform.position = tgid.obj.transform.position;

                //playHostileDeadAnimation();

                tgid.curr_status = TargetInfo.Status.DISPOSED;
                tgid.obj.SetActive(false);
                if (tgid.radar_pt != null)
                    tgid.radar_pt.SetActive(false);
                tf.objectDestroyed(tgid.obj.name);

            }
            else
            {
                Debug.LogWarning("977 - unknown target : " + tgid.name);
            }

        }

        return true;
    }

    public bool hostileTargetLosing(string hostile_guid, int dump_id, Vector3 losing_posi)
    {

        TargetInfo tgid = mTargetInfo_NameMapper["Target_" + dump_id];
        TargetInfo hostile_tgid = mTargetInfo_NameMapper["Hostile_" + getHostileIdxByGUID(hostile_guid)];

        //reset dumpling
        bool could_destroy = false;
        if ((tgid.curr_status == TargetInfo.Status.AVAILABLE ||
          tgid.curr_status == TargetInfo.Status.COLLECTING ||
          tgid.curr_status == TargetInfo.Status.COLLECTED) &&
          tgid.obj.activeSelf == true)
        {
            could_destroy = true;
        }

        if (could_destroy == false)
        {
            Debug.LogWarning("769 - invalid target : " + tgid.name);
        }

        //send message
        Debug.Log("1059 - hostile target :" + tgid.obj.name + " loss...");

        tgid.curr_status = TargetInfo.Status.AVAILABLE;
        tgid.obj.SetActive(true);
        if (tgid.radar_pt != null)
        {
            tgid.radar_pt.SetActive(true);
        }
        tgid.attached_player = null;
        tgid.attracted_player_guid = null;
        tgid.attraction_done = false;
        tgid.delay_attraction_counter = 0.0f;
        tgid.is_attached_by_self = false;
        tgid.player_guid = null;
        tgid.team_id = -1;
        tgid.obj.transform.Find("bun").GetComponent<Klak.Motion.BrownianMotion>().positionAmplitude = 0.0f;
        tgid.obj.transform.Find("bun").localPosition = new Vector3(0.0f, -0.6952f, 0.0f);

        //dumpling losing animation (handled by PlayerController)
        dumplingEventHandler(MultiplayerHandler.GameEvent.DUMPLING_LOOSING_ANIM, new object[] {
      tgid,
      losing_posi}
        );

        return true;
    }

    public bool targetLosing(TargetInfo tgid, Vector3 posi)
    {
        if (tgid == null)
        {
            return false;
        }

        bool could_destroy = false;
        if ((tgid.curr_status == TargetInfo.Status.AVAILABLE ||
            tgid.curr_status == TargetInfo.Status.COLLECTING ||
            tgid.curr_status == TargetInfo.Status.COLLECTED) &&
          tgid.obj.activeSelf == true)
        {
            could_destroy = true;
        }

        if (could_destroy == false)
        {
            Debug.LogWarning("1335 - invalid target : " + tgid.name);
            return false;
        }

        //      //send message
        Debug.Log("1059 - target :" + tgid.obj.name + " loss...");
        //      dumplingEventHandler(MultiplayerHandler.GameEvent.DUMPLING_ACTION, new object[] {
        //          (int)MultiplayerHandler.GameSubEvent.DUMPLING_LOSING,
        //          tgid.id,
        //          posi_arr[i].x, posi_arr[i].y, posi_arr[i].z
        //      });


        tgid.curr_status = TargetInfo.Status.AVAILABLE;
        tgid.obj.SetActive(true);
        if (tgid.radar_pt != null)
        {
            tgid.radar_pt.SetActive(true);
        }
        tgid.attached_player = null;
        tgid.attracted_player_guid = null;
        tgid.attraction_done = false;
        tgid.delay_attraction_counter = 0.0f;
        tgid.is_attached_by_self = false;
        tgid.player_guid = null;
        tgid.team_id = -1;
        tgid.obj.transform.Find("bun").GetComponent<Klak.Motion.BrownianMotion>().positionAmplitude = 0.0f;
        tgid.obj.transform.Find("bun").localPosition = new Vector3(0.0f, -0.6952f, 0.0f);

        //更新分數顯示
        dumplingEventHandler(MultiplayerHandler.GameEvent.SCORE_UPDATE_REQUEST, new object[] {
      (int)MultiplayerHandler.GameSubEvent.DUMPLING_SCORE_DECREASE_ONE //SEND TO HOST, (OR PROCESS SCORE THEN BROADCAST TO ALL)
    });

        //dumpling losing animation (handled by PlayerController)
        dumplingEventHandler(MultiplayerHandler.GameEvent.DUMPLING_LOOSING_ANIM, new object[] {
      tgid,
      posi}
        );

        return true;
    }

    void createFrustumModel(float fovLeft, float fovRight, float fovTop, float fovBottom, float farZ, float nearZ)
    {
        fovLeft = Mathf.Clamp(fovLeft, 1, 89);
        fovRight = Mathf.Clamp(fovRight, 1, 89);
        fovTop = Mathf.Clamp(fovTop, 1, 89);
        fovBottom = Mathf.Clamp(fovBottom, 1, 89);
        farZ = Mathf.Max(farZ, nearZ + 0.01f);
        nearZ = Mathf.Clamp(nearZ, 0.01f, farZ - 0.01f);

        var lsin = Mathf.Sin(-fovLeft * Mathf.Deg2Rad);
        var rsin = Mathf.Sin(fovRight * Mathf.Deg2Rad);
        var tsin = Mathf.Sin(fovTop * Mathf.Deg2Rad);
        var bsin = Mathf.Sin(-fovBottom * Mathf.Deg2Rad);

        var lcos = Mathf.Cos(-fovLeft * Mathf.Deg2Rad);
        var rcos = Mathf.Cos(fovRight * Mathf.Deg2Rad);
        var tcos = Mathf.Cos(fovTop * Mathf.Deg2Rad);
        var bcos = Mathf.Cos(-fovBottom * Mathf.Deg2Rad);

        var corners = new Vector3[] {
            new Vector3(lsin * nearZ / lcos, tsin * nearZ / tcos, nearZ), //tln
			new Vector3(rsin * nearZ / rcos, tsin * nearZ / tcos, nearZ), //trn
			new Vector3(rsin * nearZ / rcos, bsin * nearZ / bcos, nearZ), //brn
			new Vector3(lsin * nearZ / lcos, bsin * nearZ / bcos, nearZ), //bln
			new Vector3(lsin * farZ  / lcos, tsin * farZ  / tcos, farZ ), //tlf
			new Vector3(rsin * farZ  / rcos, tsin * farZ  / tcos, farZ ), //trf
			new Vector3(rsin * farZ  / rcos, bsin * farZ  / bcos, farZ ), //brf
			new Vector3(lsin * farZ  / lcos, bsin * farZ  / bcos, farZ ), //blf
		};

        var triangles = new int[] {
		//	0, 1, 2, 0, 2, 3, // near
		//	0, 2, 1, 0, 3, 2, // near
		//	4, 5, 6, 4, 6, 7, // far
		//	4, 6, 5, 4, 7, 6, // far
			0, 4, 7, 0, 7, 3, // left
			0, 7, 4, 0, 3, 7, // left
			1, 5, 6, 1, 6, 2, // right
			1, 6, 5, 1, 2, 6, // right
			0, 4, 5, 0, 5, 1, // top
			0, 5, 4, 0, 1, 5, // top
			2, 3, 7, 2, 7, 6, // bottom
			2, 7, 3, 2, 6, 7, // bottom
		};

        int j = 0;
        var vertices = new Vector3[triangles.Length];
        var normals = new Vector3[triangles.Length];
        for (int i = 0; i < triangles.Length / 3; i++)
        {
            var a = corners[triangles[i * 3 + 0]];
            var b = corners[triangles[i * 3 + 1]];
            var c = corners[triangles[i * 3 + 2]];
            var n = Vector3.Cross(b - a, c - a).normalized;
            normals[i * 3 + 0] = n;
            normals[i * 3 + 1] = n;
            normals[i * 3 + 2] = n;
            vertices[i * 3 + 0] = a;
            vertices[i * 3 + 1] = b;
            vertices[i * 3 + 2] = c;
            triangles[i * 3 + 0] = j++;
            triangles[i * 3 + 1] = j++;
            triangles[i * 3 + 2] = j++;
        }

        var mesh = new Mesh();
        mesh.vertices = vertices;
        mesh.normals = normals;
        mesh.triangles = triangles;

        tracingFrustum = new GameObject();
        tracingFrustum.layer = LayerMask.NameToLayer("Frustum");
        tracingFrustum.name = "Frustum";
        tracingFrustum.transform.SetParent(transform, false);
        tracingFrustum.transform.localPosition = Vector3.zero;
        MeshFilter mf = tracingFrustum.AddComponent<MeshFilter>();
        MeshRenderer mr = tracingFrustum.AddComponent<MeshRenderer>();
        MeshCollider mc = tracingFrustum.AddComponent<MeshCollider>();
        mf.mesh = mesh;
        mc.sharedMesh = mesh;
        mc.convex = true;
        mc.isTrigger = true;

        mr.enabled = false;

        tf = tracingFrustum.AddComponent<TracerFrustum>();
    }


    //
    //Multiplayer func.
    //
    public void registerDumplingEventHandler(DumplingEventFunc pfunc)
    {
        dumplingEventHandler = pfunc;
    }

    public bool acceleratortargetDestroyed(int dump_id)
    {
        Debug.Log("1651 - accelerator target destroyed from remote (id=" + dump_id + ")");
        string target_name = "Accelerator_" + dump_id;
        if (mTargetInfo_NameMapper.ContainsKey(target_name) == false)
        {
            Debug.LogWarning("874 - invalid target : " + target_name);
            return false;
        }

        TargetInfo tgid = mTargetInfo_NameMapper[target_name];

        //tgid.obj.transform.Find("bun").GetComponent<PrefabInstantiator>().instantiateExplosion();
        tgid.curr_status = TargetInfo.Status.DISPOSED;
        tgid.obj.transform.Find("Eat_FX").gameObject.SetActive(false);
        tgid.obj.SetActive(false);
        if (tgid.radar_pt != null)
        {
            tgid.radar_pt.SetActive(false);
        }

        tf.objectDestroyed(target_name);

        return true;
    }

    public bool shieldtargetDestroyed(int dump_id)
    {
        Debug.Log("1651 - shield target destroyed from remote (id=" + dump_id + ")");
        string target_name = "Shield_" + dump_id;
        if (mTargetInfo_NameMapper.ContainsKey(target_name) == false)
        {
            Debug.LogWarning("874 - invalid target : " + target_name);
            return false;
        }

        TargetInfo tgid = mTargetInfo_NameMapper[target_name];

        //tgid.obj.transform.Find("bun").GetComponent<PrefabInstantiator>().instantiateExplosion();
        tgid.curr_status = TargetInfo.Status.DISPOSED;
        tgid.obj.transform.Find("Eat_FX").gameObject.SetActive(false);
        tgid.obj.SetActive(false);
        if (tgid.radar_pt != null)
        {
            tgid.radar_pt.SetActive(false);
        }

        tf.objectDestroyed(target_name);

        return true;
    }

    public bool targetDestroyed(int dump_id)
    {
        Debug.Log("927 - target destroyed from remote (id=" + dump_id + ")");
        string target_name = "Target_" + dump_id;
        if (mTargetInfo_NameMapper.ContainsKey(target_name) == false)
        {
            Debug.LogWarning("874 - invalid target : " + target_name);
            return false;
        }

        /*
        bool could_destroy = false;
        if ((mTargetInfo_NameMapper[target_name].curr_status == TargetInfo.Status.AVAILABLE ||
             mTargetInfo_NameMapper[target_name].curr_status == TargetInfo.Status.COLLECTING ||
                     mTargetInfo_NameMapper[target_name].curr_status == TargetInfo.Status.COLLECTED) &&
            mTargetInfo_NameMapper[target_name].obj.activeSelf ==true) {
          could_destroy = true;
        }

        if (could_destroy == false) {
          Debug.LogWarning("594 - invalid target : " + target_name + ", status=" + mTargetInfo_NameMapper[target_name].curr_status);
          return false;
        }
            */

        TargetInfo tgid = mTargetInfo_NameMapper[target_name];

        //disableDumpling(tgid);
        tgid.obj.transform.Find("bun").GetComponent<PrefabInstantiator>().instantiateExplosion();
        tgid.curr_status = TargetInfo.Status.DISPOSED;
        tgid.obj.SetActive(false);
        if (tgid.radar_pt != null)
        {
            tgid.radar_pt.SetActive(false);
        }

        tf.objectDestroyed(target_name);

        return true;
    }

    public void purgeSceneHostile()
    {
        for (int i = 0; i < hostile_count; ++i)
        {
            TargetInfo tgid = mTargetInfo_NameMapper["Hostile_" + i];
            tgid.curr_status = TargetInfo.Status.DISPOSED;
            tgid.obj.SetActive(false);
            if (tgid.radar_pt != null)
            {
                tgid.radar_pt.SetActive(false);
            }
            tgid.attached_player = null;
            tgid.attracted_player_guid = null;
            tgid.attraction_done = false;
        }

        //hostile_target_score = 0;
    }

    public int spawnBigBun(GameObject unload_pt)
    {
        Debug.Log("1309 - spawn big-bun (" + unload_pt.name + ") as target");

        //add big-bun as target
        GameObject tgt_go = unload_pt.transform.Find("bun/bun_root").gameObject;

        TargetInfo ti = new TargetInfo();
        ti.t = TargetInfo.Type.UNLOADPT_TGT;
        ti.radar_pt = null;

        ti.team_id = -1;
        ti.name = tgt_go.name;
        ti.player_guid = null;
        ti.id = 0;
        ti.obj = tgt_go;
        ti.attached_player = null;
        ti.attracted_player_guid = null;
        ti.attraction_done = false;
        ti.curr_status = TargetInfo.Status.AVAILABLE;
        numAvailableTarget.Add(ti);
        mTargetInfo_NameMapper.Add(ti.name, ti);

        return ti.id;
    }

    public int getHostileIdxByGUID(string guid)
    {
        return mHostileGUID_idxMapper[guid];
    }
    public TargetInfo spawnHostile(string hostile_guid, int team_id, int my_team_id)
    {
        Debug.Log("1135 - spawn hostile : " + hostile_guid + " (group:" + team_id + ")");

        mHostileGUID_idxMapper.Add(hostile_guid, target_id_counter);

        GameObject gg = null;
        gg = GameObject.Instantiate(NPCRoot);

        GameObject model = null;
        model = GameObject.Instantiate(ChooseNPCPrefab(team_id));
        model.name = "p";
        model.transform.SetParent(gg.transform.Find("Model"), false);
        model.transform.localPosition = Vector3.zero;
        model.transform.localScale = Vector3.one;
        model.transform.localRotation = Quaternion.identity;
        Color color;
        if (MatchmakingController.instance.gameMode == MatchmakingController.GameMode.TeamFight)
        {
            if (team_id == my_team_id)
                color = teamColor[0];
            else
                color = teamColor[1];
        }
        else
            color = teamColor[team_id];
        //Color blueColor = new Color(0.0f, 139.0f / 255.0f, 1.0f);

        PlayerModelController modelController = model.GetComponent<PlayerModelController>();
        modelController.InitPlayerModel(team_id, color);

        TargetInfo ti = new TargetInfo();
        ti.t = TargetInfo.Type.ENERMY;

        ti.radar_pt = null;
        if (team_id == my_team_id)
        {
            ti.radar_pt = GameObject.Instantiate(radar_alliesPrefab);
            ti.showOnRadar = true;
        }
        else
        {
            ti.radar_pt = GameObject.Instantiate(radar_npcPrefab);
        }
        ti.radar_pt.transform.SetParent(radarOrientation.transform, false);

        ti.positionTag = gg.transform.Find("PositionTag");
        ti.positionTag.GetComponent<SpriteRenderer>().color = color;

        gg.transform.SetParent(transform, false);
        gg.name = "Hostile_" + target_id_counter;
        gg.layer = LayerMask.NameToLayer("Target");

        float rx = 0;
        float rz = 0;

        float rndr = UnityEngine.Random.Range(min_radius, radius);
        float rndang = UnityEngine.Random.Range(0, 360);
        Vector3 p = Vector3.forward * rndr;
        p = Quaternion.AngleAxis(rndang, Vector3.up) * p;
        rx = p.x;
        rz = p.z;

        gg.transform.localPosition = new Vector3(rx, UnityEngine.Random.Range(-height * 0.5f, height * 0.5f), rz);
        gg.transform.localScale = Vector3.one * scale;

        ti.radar_pt.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
        ti.radar_pt.transform.localRotation = Quaternion.identity;
        ti.radar_pt.transform.localPosition = translateToRadarSpace(gg.transform.localPosition);

        ti.radar_hostileArrow = GameObject.Instantiate(radar_alliesArrowPrefab);
        ti.radar_hostileArrow.GetComponentInChildren<MeshRenderer>().material.color = color;
        ti.radar_hostileArrow.transform.SetParent(radarOrientation.transform, false);
        ti.radar_hostileArrow.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        ti.radar_hostileArrow.transform.localRotation = Quaternion.identity;

        ti.team_id = team_id;
        ti.name = gg.name;
        ti.player_guid = hostile_guid;
        ti.id = target_id_counter;
        target_id_counter++;

        ti.obj = gg;
        ti.attached_player = null;
        ti.attracted_player_guid = null;
        ti.attraction_done = false;
        ti.curr_status = TargetInfo.Status.AVAILABLE;
        numAvailableTarget.Add(ti);
        mTargetInfo_NameMapper.Add(ti.name, ti);

        ti.radar_hostile_trail = GameObject.Instantiate(radar_alliesTrailPrefab);
        ti.radar_hostile_trail.GetComponentInChildren<LineRenderer>().startColor = new Color(0, 0, 0, 0);
        ti.radar_hostile_trail.GetComponentInChildren<LineRenderer>().endColor = color;
        ti.radar_hostile_trail.GetComponent<TrailPrinter>().trailObj = ti.radar_pt;
        ti.radar_hostile_trail.transform.SetParent(radarOrientation.transform, false);
        ti.radar_hostile_trail.transform.localPosition = Vector3.zero;

        return ti;
    }

    public void setHostilePosition(int hostile_id, Vector3 world_position, Quaternion q, Vector3 vel, bool forceRespawn = false)
    {
        TargetInfo tgid = mTargetInfo_NameMapper["Hostile_" + hostile_id];
        tgid.obj.transform.position = world_position;

        //roll angle
        //tgid.obj.transform.forward = vel.normalized;
        //tgid.obj.transform.localRotation = q;
        tgid.fromInterpolatedRotation = tgid.obj.transform.localRotation;
        tgid.targetInterpolatedRoation = q;
        tgid.targetInterpolatedRotationTime = 0.0f;

        tgid.obj.GetComponent<Rigidbody>().velocity = vel;
        if (forceRespawn)
        {
            tgid.curr_status = TargetInfo.Status.AVAILABLE;
            tgid.obj.SetActive(true);
            tgid.radar_pt.SetActive(true);
            tgid.radar_pt.transform.localPosition = translateToRadarSpace(tgid.obj.transform.localPosition);
        }

        if (tgid.showOnRadar)
        {
            tgid.radar_pt.SetActive(true);
            tgid.radar_hostileArrow.SetActive(true);
            tgid.radar_hostile_trail.SetActive(true);
            tgid.positionTag.gameObject.SetActive(true);
        }
        else
        {
            tgid.radar_pt.SetActive(false);
            tgid.radar_hostileArrow.SetActive(false);
            tgid.radar_hostile_trail.SetActive(false);
            tgid.positionTag.gameObject.SetActive(false);
        }
    }

    public void setHostileHMDPosition(int hostile_id, Vector3 world_position)
    {
        TargetInfo tgid = mTargetInfo_NameMapper["Hostile_" + hostile_id];
        if (tgid.hmd == null)
        {
            tgid.hmd = new GameObject();
            tgid.hmd.name = "Hostile_" + hostile_id;
            tgid.hmd.transform.SetParent(tgid.obj.transform, false);
            tgid.hmd.transform.localPosition = Vector3.zero;
            tgid.hmd.transform.localScale = Vector3.one;
            tgid.hmd.layer = LayerMask.NameToLayer("Target");

            //disable origin box collider
            tgid.obj.GetComponent<BoxCollider>().enabled = false;

            SphereCollider sc = tgid.hmd.AddComponent<SphereCollider>();
            sc.radius = 1.0f;
            sc.center = Vector3.zero;
            sc.isTrigger = true;

        }
        tgid.hmd.transform.position = world_position;
    }

    void disableDumpling(TargetInfo tgid)
    {
        tgid.curr_status = TargetInfo.Status.DISPOSED;
        tgid.obj.GetComponent<TrailRenderer>().enabled = false;
        tgid.obj.SetActive(false);
        if (tgid.radar_pt != null)
        {
            tgid.radar_pt.SetActive(false);
        }
        tgid.attached_player = null;
        tgid.attracted_player_guid = null;
        tgid.attraction_done = false;

        tgid.is_attached_by_self = false;
        tgid.shift_attraction_posi = Vector3.zero;

        tgid.prev_attraction_posi = new List<Vector3>();
        tgid.prev_attraction_posi_time = new List<float>();

        tgid.obj.transform.Find("bun").GetComponent<Klak.Motion.BrownianMotion>().positionAmplitude = 0.0f;
        tgid.obj.transform.Find("bun").localPosition = new Vector3(0.0f, -0.6952f, 0.0f);
    }

    public void purgeSceneDumpling()
    {
        for (int i = 0; i < numTarget; ++i)
        {
            TargetInfo tgid = mTargetInfo_NameMapper["Target_" + i];
            disableDumpling(tgid);
        }

        //hostile_target_score = 0;
        //my_target_score = 0;
        //dumplingEventHandler(MultiplayerHandler.GameEvent.SCORE_UPDATE, new object[3] { my_target_score, my_unloaded_target_score, hostile_unloaded_target_score });
    }

    public void resetHostile()
    {
        for (int i = 0; i < hostile_count; ++i)
        {

            TargetInfo ti = mTargetInfo_NameMapper["Hostile_" + i];
            GameObject gg = ti.obj;

            float rx = 0;
            float rz = 0;

            float rndr = UnityEngine.Random.Range(min_radius, radius);
            float rndang = UnityEngine.Random.Range(0, 360);
            Vector3 p = Vector3.forward * rndr;
            p = Quaternion.AngleAxis(rndang, Vector3.up) * p;
            rx = p.x;
            rz = p.z;

            ti.obj.SetActive(true);
            ti.radar_pt.SetActive(true);

            gg.transform.localPosition = new Vector3(rx, UnityEngine.Random.Range(-height * 0.5f, height * 0.5f), rz);
            gg.transform.localScale = Vector3.one * scale;

            ti.radar_pt.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
            ti.radar_pt.transform.localRotation = Quaternion.identity;
            ti.radar_pt.transform.localPosition = translateToRadarSpace(gg.transform.localPosition);

            ti.attached_player = null;
            ti.attracted_player_guid = null;

            ti.attraction_done = false;
            ti.curr_status = TargetInfo.Status.AVAILABLE;

        }
    }

    public void resetDumpling()
    {
        Debug.Log("1870 - reset all dumplings...");
        for (int i = 0; i < numTarget; ++i)
        {
            resetDumpling(i);
        }
    }

    public Vector3 resetDumpling(int idx)
    {
        float rx = 0;
        float rz = 0;

        float rndr = UnityEngine.Random.Range(min_radius, radius);
        float rndang = UnityEngine.Random.Range(0, 360);
        Vector3 p = Vector3.forward * rndr;
        p = Quaternion.AngleAxis(rndang, Vector3.up) * p;
        rx = p.x;
        rz = p.z;

        Vector3 posi = new Vector3(rx, UnityEngine.Random.Range(-height * 0.5f, height * 0.5f), rz);
        resetDumpling(idx, posi);
        return posi;
    }

    public void resetDumpling(int id, Vector3 posi)
    {
        TargetInfo ti = mTargetInfo_NameMapper["Target_" + id];
        GameObject gg = ti.obj;

        ti.obj.GetComponent<TrailRenderer>().enabled = false;
        ti.obj.SetActive(true);


        ti.obj.transform.Find("bun").GetComponent<Klak.Motion.BrownianMotion>().positionAmplitude = 0.0f;
        ti.obj.transform.Find("bun").localPosition = new Vector3(0.0f, -0.6952f, 0.0f);

        gg.transform.localPosition = posi;
        gg.transform.localScale = new Vector3(scale, scale, scale);
        gg.transform.localRotation = Quaternion.identity;

        if (ti.radar_pt != null)
        {
            ti.radar_pt.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
            ti.radar_pt.transform.localRotation = Quaternion.identity;
            ti.radar_pt.transform.localPosition = translateToRadarSpace(gg.transform.localPosition);
            ti.radar_pt.SetActive(true);
        }

        ti.attached_player = null;
        ti.attracted_player_guid = null;

        ti.attraction_done = false;
        ti.is_attached_by_self = false;
        ti.curr_status = TargetInfo.Status.AVAILABLE;
        ti.shift_attraction_posi = Vector3.zero;
        ti.t = TargetInfo.Type.NORMAL;

        ti.prev_attraction_posi = new List<Vector3>();
        ti.prev_attraction_posi_time = new List<float>();
    }

    public Vector3 resetAcceleratorTarget(int idx)
    {
        float rx = 0;
        float rz = 0;

        float rndr = UnityEngine.Random.Range(min_radius, radius);
        float rndang = UnityEngine.Random.Range(0, 360);
        Vector3 p = Vector3.forward * rndr;
        p = Quaternion.AngleAxis(rndang, Vector3.up) * p;
        rx = p.x;
        rz = p.z;

        Vector3 posi = new Vector3(rx, UnityEngine.Random.Range(-height * 0.5f, height * 0.5f), rz);
        resetAcceleratorTarget(idx, posi);
        return posi;
    }

    public void resetAcceleratorTarget(int id, Vector3 posi)
    {
        TargetInfo ti = mTargetInfo_NameMapper["Accelerator_" + id];
        GameObject gg = ti.obj;

        //ti.obj.GetComponent<TrailRenderer>().enabled = false;
        ti.obj.SetActive(true);

        ti.obj.transform.Find("Eat_FX").gameObject.SetActive(false);
        //ti.obj.transform.Find("bun").GetComponent<Klak.Motion.BrownianMotion>().positionAmplitude = 0.0f;
        //ti.obj.transform.Find("bun").localPosition = new Vector3(0.0f, -0.6952f, 0.0f);

        gg.transform.localPosition = posi;
        gg.transform.localScale = new Vector3(scale, scale, scale);
        gg.transform.localRotation = Quaternion.identity;

        if (ti.radar_pt != null)
        {
            ti.radar_pt.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
            ti.radar_pt.transform.localRotation = Quaternion.identity;
            ti.radar_pt.transform.localPosition = translateToRadarSpace(gg.transform.localPosition);
            ti.radar_pt.SetActive(true);
        }

        ti.attached_player = null;
        ti.attracted_player_guid = null;

        ti.attraction_done = false;
        ti.is_attached_by_self = false;
        ti.curr_status = TargetInfo.Status.AVAILABLE;
        ti.shift_attraction_posi = Vector3.zero;
        ti.t = TargetInfo.Type.SPEEDING_TGT;

        ti.prev_attraction_posi = new List<Vector3>();
        ti.prev_attraction_posi_time = new List<float>();
    }

    public Vector3 resetShieldTarget(int idx)
    {
        float rx = 0;
        float rz = 0;

        float rndr = UnityEngine.Random.Range(min_radius, radius);
        float rndang = UnityEngine.Random.Range(0, 360);
        Vector3 p = Vector3.forward * rndr;
        p = Quaternion.AngleAxis(rndang, Vector3.up) * p;
        rx = p.x;
        rz = p.z;

        Vector3 posi = new Vector3(rx, UnityEngine.Random.Range(-height * 0.5f, height * 0.5f), rz);
        resetShieldTarget(idx, posi);
        return posi;
    }

    public void resetShieldTarget(int id, Vector3 posi)
    {
        TargetInfo ti = mTargetInfo_NameMapper["Shield_" + id];
        GameObject gg = ti.obj;

        //ti.obj.GetComponent<TrailRenderer>().enabled = false;
        ti.obj.SetActive(true);

        ti.obj.transform.Find("Eat_FX").gameObject.SetActive(false);
        //ti.obj.transform.Find("bun").GetComponent<Klak.Motion.BrownianMotion>().positionAmplitude = 0.0f;
        //ti.obj.transform.Find("bun").localPosition = new Vector3(0.0f, -0.6952f, 0.0f);

        gg.transform.localPosition = posi;
        gg.transform.localScale = new Vector3(scale, scale, scale);
        gg.transform.localRotation = Quaternion.identity;

        if (ti.radar_pt != null)
        {
            ti.radar_pt.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
            ti.radar_pt.transform.localRotation = Quaternion.identity;
            ti.radar_pt.transform.localPosition = translateToRadarSpace(gg.transform.localPosition);
            ti.radar_pt.SetActive(true);
        }

        ti.attached_player = null;
        ti.attracted_player_guid = null;

        ti.attraction_done = false;
        ti.is_attached_by_self = false;
        ti.curr_status = TargetInfo.Status.AVAILABLE;
        ti.shift_attraction_posi = Vector3.zero;
        ti.t = TargetInfo.Type.SHIELD_TGT;

        ti.prev_attraction_posi = new List<Vector3>();
        ti.prev_attraction_posi_time = new List<float>();
    }

    public void setShieldPosition(int shield_id, Vector3 local_position, Quaternion q, bool forceRespawn = false)
    {
        Debug.Log("2337 - set shield (Shield_" + shield_id + ")");
        TargetInfo tgid = mTargetInfo_NameMapper["Shield_" + shield_id];

        tgid.obj.transform.localPosition = local_position;
        tgid.obj.transform.localRotation = q;

        if (forceRespawn)
        {

            tgid.obj.SetActive(true);
            if (tgid.radar_pt != null)
            {
                tgid.radar_pt.SetActive(true);
                tgid.radar_pt.transform.localPosition = translateToRadarSpace(tgid.obj.transform.localPosition);
            }
            //tgid.obj.transform.Find("bun").GetComponent<Klak.Motion.BrownianMotion>().positionAmplitude = 0.0f;
            //tgid.obj.transform.Find("bun").localPosition = new Vector3(0.0f, -0.6952f, 0.0f);
            tgid.obj.transform.Find("Eat_FX").gameObject.SetActive(false);

            tgid.attached_player = null;
            tgid.attracted_player_guid = null;

            tgid.attraction_done = false;
            tgid.is_attached_by_self = false;
            tgid.curr_status = TargetInfo.Status.AVAILABLE;
            tgid.shift_attraction_posi = Vector3.zero;

            tgid.prev_attraction_posi = new List<Vector3>();
            tgid.prev_attraction_posi_time = new List<float>();


        }
    }

    public void setAcceleratorPosition(int acc_id, Vector3 local_position, Quaternion q, bool forceRespawn = false)
    {
        Debug.Log("2337 - set accelerator (Accelerator_" + acc_id + ")");
        TargetInfo tgid = mTargetInfo_NameMapper["Accelerator_" + acc_id];

        tgid.obj.transform.localPosition = local_position;
        tgid.obj.transform.localRotation = q;

        if (forceRespawn)
        {

            tgid.obj.SetActive(true);
            if (tgid.radar_pt != null)
            {
                tgid.radar_pt.SetActive(true);
                tgid.radar_pt.transform.localPosition = translateToRadarSpace(tgid.obj.transform.localPosition);
            }
            //tgid.obj.transform.Find("bun").GetComponent<Klak.Motion.BrownianMotion>().positionAmplitude = 0.0f;
            //tgid.obj.transform.Find("bun").localPosition = new Vector3(0.0f, -0.6952f, 0.0f);
            tgid.obj.transform.Find("Eat_FX").gameObject.SetActive(false);

            tgid.attached_player = null;
            tgid.attracted_player_guid = null;

            tgid.attraction_done = false;
            tgid.is_attached_by_self = false;
            tgid.curr_status = TargetInfo.Status.AVAILABLE;
            tgid.shift_attraction_posi = Vector3.zero;

            tgid.prev_attraction_posi = new List<Vector3>();
            tgid.prev_attraction_posi_time = new List<float>();


        }
    }

    public void setDumplingPosition(int dump_id, Vector3 local_position, Quaternion q, bool forceRespawn = false)
    {
        Debug.Log("2403 - set dumpling (Target_" + dump_id + ")");
        TargetInfo tgid = mTargetInfo_NameMapper["Target_" + dump_id];

        tgid.obj.transform.localPosition = local_position;
        tgid.obj.transform.localRotation = q;

        if (forceRespawn)
        {

            tgid.obj.SetActive(true);
            if (tgid.radar_pt != null)
            {
                tgid.radar_pt.SetActive(true);
                tgid.radar_pt.transform.localPosition = translateToRadarSpace(tgid.obj.transform.localPosition);
            }
            tgid.obj.transform.Find("bun").GetComponent<Klak.Motion.BrownianMotion>().positionAmplitude = 0.0f;
            tgid.obj.transform.Find("bun").localPosition = new Vector3(0.0f, -0.6952f, 0.0f);

            tgid.attached_player = null;
            tgid.attracted_player_guid = null;

            tgid.attraction_done = false;
            tgid.is_attached_by_self = false;
            tgid.curr_status = TargetInfo.Status.AVAILABLE;
            tgid.shift_attraction_posi = Vector3.zero;

            tgid.prev_attraction_posi = new List<Vector3>();
            tgid.prev_attraction_posi_time = new List<float>();


        }

    }

    public Vector3[] getSceneDumplingPosition()
    {
        List<Vector3> posi = new List<Vector3>();
        for (int i = 0; i < numAvailableTarget.Count; ++i)
        {
            if (numAvailableTarget[i].t == TargetInfo.Type.NORMAL)
            {
                posi.Add(numAvailableTarget[i].obj.transform.localPosition);
            }
        }

        return posi.ToArray();
    }

    public Vector3[] getSceneShieldPosition()
    {
        List<Vector3> posi = new List<Vector3>();
        for (int i = 0; i < numAvailableTarget.Count; ++i)
        {
            if (numAvailableTarget[i].t == TargetInfo.Type.SHIELD_TGT)
            {
                posi.Add(numAvailableTarget[i].obj.transform.localPosition);
            }
        }

        return posi.ToArray();
    }

    public Vector3[] getSceneAcceleratorPosition()
    {
        List<Vector3> posi = new List<Vector3>();
        for (int i = 0; i < numAvailableTarget.Count; ++i)
        {
            if (numAvailableTarget[i].t == TargetInfo.Type.SPEEDING_TGT)
            {
                posi.Add(numAvailableTarget[i].obj.transform.localPosition);
            }
        }

        return posi.ToArray();
    }


    public void purgeTrackedObj()
    {
        tf.purgeTrackedObj();
    }

    public TrackedMissileInfo[] getTrackedMissile()
    {
        return tracked_missile.ToArray();
    }

    //network usage
    public TargetInfo[] getTargetInfoList()
    {
        List<TargetInfo> tilist = new List<TargetInfo>();
        for (int i = 0; i < numAvailableTarget.Count; ++i)
        {
            if (numAvailableTarget[i].t == TargetInfo.Type.NORMAL &&
                (numAvailableTarget[i].curr_status == TargetInfo.Status.COLLECTING || numAvailableTarget[i].curr_status == TargetInfo.Status.COLLECTED)
               )
            { //DONT SYNC HOSTILE POSITION HERE
                tilist.Add(numAvailableTarget[i]);
            }
        }
        return tilist.ToArray();
    }

    public TargetInfo getTargetInfo(int dump_idx)
    {
        for (int i = 0; i < numAvailableTarget.Count; ++i)
        {
            if (numAvailableTarget[i].id == dump_idx)
            {
                return numAvailableTarget[i];
            }
        }

        return null;
    }

    public void setRemoteMissilePosition(string hostile_guid, int missile_idx, Vector3 posi, Quaternion q, Vector3 vel)
    {
        bool missile_set = false;
        for (int i = 0; i < tracked_remote_missile.Count; ++i)
        {
            if (tracked_remote_missile[i].idx == missile_idx && tracked_remote_missile[i].launcher_guid == hostile_guid)
            {

                //update missile
                tracked_remote_missile[i].obj.transform.position = posi;
                tracked_remote_missile[i].obj.transform.forward = q * Vector3.forward;
                tracked_remote_missile[i].obj.GetComponent<Rigidbody>().velocity = vel;

                missile_set = true;
                break;
            }
        }

        if (missile_set == false)
        {
            Debug.LogWarning("1946 - missile not found (missile id:" + missile_idx + ")");
        }
    }

    public void setRemoteMissileLaunch(int myTeamId, string hostile_guid, int missile_idx, Vector3 posi, Quaternion q, Vector3 vel, float life_time)
    {

        bool missile_found = false;
        for (int i = 0; i < tracked_remote_missile.Count; ++i)
        {
            if (tracked_remote_missile[i].idx == missile_idx && tracked_remote_missile[i].launcher_guid == hostile_guid)
            {
                missile_found = true;
                break;
            }
        }

        if (missile_found == true)
        {
            Debug.Log("1087 - duplicated remote missile id (idx:" + missile_idx + ")");
            return;
        }

        //create a new missile
        //Debug.Log("1073 - create remote missile (idx:" + idx + ")");
        GameObject m = null;

        TargetInfo hostile_tgid = mTargetInfo_NameMapper["Hostile_" + mHostileGUID_idxMapper[hostile_guid]];
        if (hostile_tgid.team_id == myTeamId)
        {
            //alliance fire
            m = GameObject.Instantiate(remoteMissilePrefabB);
        }
        else
        {
            //enemy fire
            m = GameObject.Instantiate(remoteMissilePrefabR);
        }

        m.name = "RemoteMissile_" + missile_idx;

        m.transform.position = posi;
        m.transform.forward = q * Vector3.forward;
        m.GetComponent<Rigidbody>().velocity = vel;
        //m.GetComponent<MissileBehaviours.Actions.Explode>().triggers.Add(m.GetComponent<MissileBehaviours.Triggers.OnCollision>());

        TrackedMissileInfo tmi = new TrackedMissileInfo();
        tmi.obj = m;
        tmi.name = m.name;
        tmi.idx = missile_idx;
        tmi.launcher_guid = hostile_guid;
        tracked_remote_missile.Add(tmi);

        //local countdown
        RemoteMissileStatus rms = new RemoteMissileStatus(hostile_guid, missile_idx);
        rms.counter = life_time;
        remote_missile_status.Add(missile_idx, rms);

        GameObject tmp_rm = GameObject.Instantiate(radarMissilePrefab);
        tmp_rm.transform.SetParent(radarOrientation.transform, false);
        tmp_rm.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
        tmp_rm.transform.localRotation = Quaternion.identity;
        tmp_rm.transform.localPosition = translateToRadarSpace(tmi.obj.transform.position - transform.localPosition);
        tmi.remote_radar_missile = tmp_rm;

        tmp_rm.transform.Find("MissileID").GetComponent<TextMesh>().text = "R" + missile_idx + "(-)";

        GameObject trail = GameObject.Instantiate(radar_trailPrefab);
        trail.GetComponent<TrailPrinter>().trailObj = tmp_rm;
        trail.transform.SetParent(radarOrientation.transform, false);
        trail.transform.localPosition = Vector3.zero;


        hostile_tgid.obj.GetComponentInChildren<PlayerModelController>().SetBearState(PlayerModelController.BearState.attack);
        GameObject me = GameObject.Instantiate(missileLaunchEffectPrefab);
        me.name = "RemoteMissileLaunchEffect_" + missile_idx;
        //me.transform.position = posi;
        //me.transform.localRotation = hostile_tgid.obj.transform.localRotation;
        me.transform.SetParent(hostile_tgid.obj.transform, false);
    }

    public void setRemoteMissileDestroyed(string hostile_guid, int idx)
    {
        for (int i = 0; i < tracked_remote_missile.Count; ++i)
        {
            if (tracked_remote_missile[i].idx == idx && tracked_remote_missile[i].launcher_guid == hostile_guid)
            {
                Debug.Log("1105 - destroy remote missile (idx:" + idx + ")");

                //play explosion effect
                if (tracked_remote_missile[i].obj != null && tracked_remote_missile[i].obj.name.Contains("Hostile"))
                {
                    //effect
                    GameObject effect = GameObject.Instantiate(playerHit);
                    effect.transform.position = tracked_remote_missile[i].obj.transform.position;
                }
                else
                if (tracked_remote_missile[i].obj != null && tracked_remote_missile[i].obj.name.Contains("Target"))
                {
                    GameObject effect = GameObject.Instantiate(bunHit);
                    effect.transform.position = tracked_remote_missile[i].obj.transform.position;
                }
                else
                {
                    tracked_remote_missile[i].obj.GetComponent<MissileBehaviours.Actions.Explode>().TriggerWithoutReport();
                }

                tracked_remote_missile[i].obj.transform.Find("Fire_Rock/Meteo").gameObject.SetActive(false);
                tracked_remote_missile[i].obj.transform.Find("Fire_Rock/Fire_flak").gameObject.SetActive(false);
                tracked_remote_missile[i].obj.transform.Find("Fire_Rock").gameObject.AddComponent<CommitSuicide>();
                tracked_remote_missile[i].obj.transform.Find("Fire_Rock").SetParent(tracked_remote_missile[i].obj.transform.parent);
                GameObject.Destroy(tracked_remote_missile[i].obj);

                GameObject.Destroy(tracked_remote_missile[i].remote_radar_missile);
                tracked_remote_missile.RemoveAt(i);
                return;
            }
        }

        Debug.Log("1119 - remote missile (idx:" + idx + ") not found");

    }

    public void updateHealthyPercentage(float percentage)
    {
        int light_healthy = (int)(healthyIndicator.Length * (percentage / 100.0f));

        for (int i = 0; i < healthyIndicator.Length; ++i)
        {
            if (i < light_healthy)
            {
                //healthyIndicator[i].SetActive(true);
                healthyIndicator[i].GetComponent<HealthIndicatorEffect>().setActive(true);
            }
            else
            {
                //healthyIndicator[i].SetActive(false);
                healthyIndicator[i].GetComponent<HealthIndicatorEffect>().setActive(false);
            }
        }
    }

    bool flash_radar_red_bottom = false;
    float flash_counter = 0.0f;
    bool flash_radar_lights_on = false;

    float flash_countdown = 2.5f;

    public void setRadarBottomRedImageFlash(bool flash)
    {

        if (flash == true)
        {
            if (flash_radar_red_bottom == false)
            {
                flash_radar_red_bottom = true;

                flash_counter = 0.0f;
                flash_countdown = 5.0f; //auto disable flash

                flash_radar_lights_on = false;
                Color c = radarBottomObjRed.GetComponent<MeshRenderer>().material.GetColor("_TintColor");
                c.a = 0.0f;
                radarBottomObjRed.GetComponent<MeshRenderer>().material.SetColor("_TintColor", c);
            }
        }
        else
        {
            if (flash_radar_red_bottom == true)
            {
                Debug.Log("1983 - stop missile lock indicator counting...");
                flash_countdown = -1.0f;
            }
        }

    }

    //public void unload_my_dumpling() {
    //  my_unloaded_target_score += my_target_score;
    //  my_target_score = 0;

    //  dumplingEventHandler(MultiplayerHandler.GameEvent.SCORE_UPDATE, new object[3] { my_target_score, my_unloaded_target_score, hostile_unloaded_target_score });

    //  //broadcast event
    //  dumplingEventHandler(MultiplayerHandler.GameEvent.PLAYER_ACTION, new object[] {
    //    (int)MultiplayerHandler.GameSubEvent.DUMPLING_UNLOADING, //送至網路
    //    my_target_score,
    //    my_unloaded_target_score });

    //  //dispose dumplings
    //  for (int i = 0; i < numAvailableTarget.Count; ++i) {
    //    if (numAvailableTarget[i].is_attached_by_self == true && numAvailableTarget[i].attraction_done == true) {
    //      //dispose
    //      disableDumpling(numAvailableTarget[i]);
    //    }
    //  }


    //}


    public List<TargetInfo> getCollectedTarget(string guid)
    {
        List<TargetInfo> retList = new List<TargetInfo>();

        for (int i = 0; i < numAvailableTarget.Count; ++i)
        {
            if (numAvailableTarget[i].attracted_player_guid == guid && numAvailableTarget[i].attraction_done == true)
            {
                retList.Add(numAvailableTarget[i]);
            }
        }

        return retList;
    }

    public void setupScore(int available_acc_times, int available_shield_times, int myscore, List<int> otherPlayerScore)
    {
        Debug.Log("1618 - setup shield_available=" + available_shield_times + ", score m_score=" + myscore);

        //accelerator
        sr_available_accelerator[0].sprite = dumpling_counter_digits[available_acc_times / 100];
        sr_available_accelerator[1].sprite = dumpling_counter_digits[available_acc_times % 100 / 10];
        sr_available_accelerator[2].sprite = dumpling_counter_digits[available_acc_times % 10];

        //shield
        sr_available_shield[0].sprite = dumpling_counter_digits[available_shield_times / 100];
        sr_available_shield[1].sprite = dumpling_counter_digits[available_shield_times % 100 / 10];
        sr_available_shield[2].sprite = dumpling_counter_digits[available_shield_times % 10];

        //collected
        sr_localPlayerDumplingCollected[0].sprite = dumpling_counter_digits[myscore / 100];
        sr_localPlayerDumplingCollected[1].sprite = dumpling_counter_digits[myscore % 100 / 10];
        sr_localPlayerDumplingCollected[2].sprite = dumpling_counter_digits[myscore % 10];

        for (int i = 0; i < otherPlayerScore.Count; i++)
        {
            sr_otherPlayerDumplingCollected[i][0].sprite = dumpling_counter_digits[otherPlayerScore[i] / 100];
            sr_otherPlayerDumplingCollected[i][1].sprite = dumpling_counter_digits[otherPlayerScore[i] % 100 / 10];
            sr_otherPlayerDumplingCollected[i][2].sprite = dumpling_counter_digits[otherPlayerScore[i] % 10];
        }
    }
    public void setupTeamScore(int myTeamScore, int hostileTeamScore)
    {
        Debug.Log("myTeamScore= " + myTeamScore + ", hostileTeamScore= " + hostileTeamScore);
        //unloaded
        sr_dumplingUnloaded[0].sprite = dumpling_counter_digits[(int)myTeamScore / 100];
        sr_dumplingUnloaded[1].sprite = dumpling_counter_digits[(int)(myTeamScore % 100 / 10)];
        sr_dumplingUnloaded[2].sprite = dumpling_counter_digits[(int)(myTeamScore % 10)];

        //hostile-unloaded
        sr_dumplingHostileUnloaded[0].sprite = dumpling_counter_digits[(int)hostileTeamScore / 100];
        sr_dumplingHostileUnloaded[1].sprite = dumpling_counter_digits[(int)(hostileTeamScore % 100 / 10)];
        sr_dumplingHostileUnloaded[2].sprite = dumpling_counter_digits[(int)(hostileTeamScore % 10)];
    }


    public void purgeDumpling(GameObject target_go, int unload_pt_idx, List<int> target_idx_arr)
    {
        purgeDumpling(target_go.transform.localPosition, unload_pt_idx, target_idx_arr);
    }

    public void purgeDumpling(Vector3 tgt_pt, int unload_pt_idx, List<int> target_idx_arr)
    {
        //dispose dumplings
        for (int i = 0; i < target_idx_arr.Count; ++i)
        {
            TargetInfo tgid = getTargetInfo(target_idx_arr[i]);
            if (tgid != null)
            {
                //patrial disalbe target
                tgid.curr_status = TargetInfo.Status.DISPOSED;
                if (tgid.radar_pt != null)
                {
                    tgid.radar_pt.SetActive(false);
                }
                tgid.attached_player = null;
                tgid.attracted_player_guid = null;
                tgid.attraction_done = false;
                tgid.is_attached_by_self = false;
                tgid.shift_attraction_posi = Vector3.zero;
                tgid.prev_attraction_posi = new List<Vector3>();
                tgid.prev_attraction_posi_time = new List<float>();
                tgid.obj.transform.Find("bun").GetComponent<Klak.Motion.BrownianMotion>().positionAmplitude = 0.0f;
                tgid.obj.transform.Find("bun").localPosition = new Vector3(0.0f, -0.6952f, 0.0f);

                //setup animation
                //dumpling unload animation (handled by PlayerController)
                dumplingEventHandler(MultiplayerHandler.GameEvent.DUMPLING_PLAY_UNLOADING_ANIM, new object[] { tgid, tgt_pt, unload_pt_idx });

            }
        }

    }

    public void purgeSpecificDumpling(TargetInfo ti)
    {
        disableDumpling(ti);
    }

    public void genRadarSpawnPt(Vector3 w_player_spawn_pt, Vector3 w_hostile_spawn_pt)
    {

        if (mPlayerSpawnPt != null)
        {
            GameObject.Destroy(mPlayerSpawnPt);
            mPlayerSpawnPt = null;
        }

        if (mHostileSpwanPt != null)
        {
            GameObject.Destroy(mHostileSpwanPt);
            mHostileSpwanPt = null;
        }

        Debug.Log("1484 - spawn radar home position at " + w_player_spawn_pt);
        GameObject tmp_rm = GameObject.Instantiate(radar_SpawnPrefab);
        tmp_rm.transform.SetParent(radarOrientation.transform, false);
        tmp_rm.name = "Spawn_pt1";
        tmp_rm.transform.localScale = Vector3.one * 0.075f;
        tmp_rm.transform.localRotation = Quaternion.identity;
        tmp_rm.transform.localPosition = translateToRadarSpace(w_player_spawn_pt - transform.localPosition);
        Color b = new Color(0.0f, 160.0f / 255.0f, 1.0f);
        tmp_rm.transform.Find("Home_01").GetComponent<MeshRenderer>().material.SetColor("_TintColor", b);
        tmp_rm.transform.Find("Home_02").GetComponent<MeshRenderer>().material.SetColor("_TintColor", b);
        tmp_rm.transform.Find("Home_03").GetComponent<MeshRenderer>().material.SetColor("_TintColor", b);

        mPlayerSpawnPt = tmp_rm;

        Debug.Log("2063 - spawn radar home position at " + w_hostile_spawn_pt);
        GameObject tmp_rm2 = GameObject.Instantiate(radar_SpawnPrefab);
        tmp_rm2.transform.SetParent(radarOrientation.transform, false);
        tmp_rm2.name = "Spawn_pt2";
        tmp_rm2.transform.localScale = Vector3.one * 0.075f;
        tmp_rm2.transform.localRotation = Quaternion.identity;
        tmp_rm2.transform.localPosition = translateToRadarSpace(w_hostile_spawn_pt - transform.localPosition);
        Color r = new Color(1.0f, 0.0f, 0.0f);
        tmp_rm2.transform.Find("Home_01").GetComponent<MeshRenderer>().material.SetColor("_TintColor", r);
        tmp_rm2.transform.Find("Home_02").GetComponent<MeshRenderer>().material.SetColor("_TintColor", r);
        tmp_rm2.transform.Find("Home_03").GetComponent<MeshRenderer>().material.SetColor("_TintColor", r);

        mHostileSpwanPt = tmp_rm2;
    }

    public void setHostileShield(int hostile_id, bool tmp_shield_enable, float duration = 0.0f)
    {
        Debug.Log("1609 - set Hostile Shield status : " + tmp_shield_enable);
        if (tmp_shield_enable == true)
        {
            Debug.Log("2251 - shield duration : " + duration);
        }

        TargetInfo tgid = mTargetInfo_NameMapper["Hostile_" + hostile_id];
        tgid.obj.transform.Find("Shield").GetComponent<SphereCollider>().radius = tmp_shield_enable ? 6.0f : 0.5f;

        RemoteShieldStatus rss = null;
        if (remote_shield_status.ContainsKey(hostile_id) == false)
        {
            remote_shield_status.Add(hostile_id, new RemoteShieldStatus(hostile_id));
        }
        rss = remote_shield_status[hostile_id];

        //animation
        if (tmp_shield_enable == true)
        {
            //tgid.obj.transform.Find("Model/p").gameObject.GetComponent<Animator>().SetTrigger("Protection");
            tgid.obj.transform.Find("ShieldEffect").gameObject.SetActive(true);

            rss.activiated = true;
            rss.counter = duration;

        }
        else
        {
            //tgid.obj.transform.Find("Model/p").gameObject.GetComponent<Animator>().SetTrigger("Fly");
            tgid.obj.transform.Find("ShieldEffect").gameObject.SetActive(false);

            rss.activiated = false;
            rss.counter = 0.0f;

        }
    }

    public void playHostileDeadAnimation(int hostile_id, int team_id, int my_team_id)
    {
        Debug.Log("1580 - play player dead animation !");

        TargetInfo tgid = mTargetInfo_NameMapper["Hostile_" + hostile_id];

        //instantiate a dummy role for dead animation playing
        GameObject dummyHostile = null;
        dummyHostile = GameObject.Instantiate(ChooseNPCPrefab(team_id));
        dummyHostile.name = "DeadPlayer";
        dummyHostile.transform.position = tgid.obj.transform.position;
        dummyHostile.transform.rotation = tgid.obj.transform.rotation;
        dummyHostile.transform.localScale = tgid.obj.transform.localScale;

        //add controller
        HostileDeadAnimation cc = dummyHostile.AddComponent<HostileDeadAnimation>();
        Vector3 vel = tgid.obj.GetComponent<Rigidbody>().velocity;
        cc.setVelocity(vel);

        Color color = teamColor[team_id];

        PlayerModelController modelController = dummyHostile.GetComponent<PlayerModelController>();
        modelController.InitPlayerModel(team_id, color);
        modelController.SetBearState(PlayerModelController.BearState.dead);
    }

    public string getHostileNetworkUID(string hostile_go_name)
    {
        //有可能炸到道具等不是敌人的东西
        TargetInfo info = null;
        mTargetInfo_NameMapper.TryGetValue(hostile_go_name, out info);
        if (info != null)
            return info.player_guid;
        else
            return null;
    }

    GameObject ChooseNPCPrefab(int team_id)
    {
        if (MatchmakingController.instance.gameMode == MatchmakingController.GameMode.FreeFight)
        {
            return npcPrefab1;
        }
        else
        {
            if (team_id == 0)
                return npcPrefab1;
            else
                return npcPrefab2;
        }
    }

    Color ChooseTeamColor(int team_id)
    {
        if (MatchmakingController.instance.gameMode == MatchmakingController.GameMode.FreeFight)
        {
            return teamColor[team_id];
        }
        else
        {
            int myGroupId = (int)MatchmakingController.instance.getMatchmakingInfo().getMyPlayerInfo().groupid;

            if (team_id == myGroupId)
                return teamColor[0];
            else
                return teamColor[1];
        }
    }
}
