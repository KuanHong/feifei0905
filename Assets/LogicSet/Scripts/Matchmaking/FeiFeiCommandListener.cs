﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System;

public class FeiFeiCommandListener : MonoBehaviour
{
    const int listenerRecievePort = 11225;
    const int commandRevievePort = 11226;
    IPEndPoint commandIPEndPoint;
    UDPBroadcast.UDPBroadcast udpBroadcast;
    UdpClient udpSend;
    BeaconAPLayer.MatchmakingRoomStatus matchMakingRoomStatus;

    // Use this for initialization
    void Start()
    {
        udpBroadcast = new UDPBroadcast.UDPBroadcast();
        udpBroadcast.StartRecieve(RecieveCallBack, listenerRecievePort);

        udpSend = new UdpClient();
    }

    // Update is called once per frame
    void Update()
    {
        udpBroadcast.Update();
    }

    void RecieveCallBack(string data, IPEndPoint senderIPEndPoint)
    {
        if (data.Contains("Searching Client"))
        {
            commandIPEndPoint = senderIPEndPoint;
            IPEndPoint commandRevieveIPEndPoint = new IPEndPoint(senderIPEndPoint.Address, commandRevievePort);
            string guid = MatchmakingController.instance.getBeaconAPLayer().getMyGUID();
            SendDataToIPEndPoint("Found Client:" + guid, commandRevieveIPEndPoint);
        }
        else if (data.Contains("RoomStatus:"))
        {
            PorcessRoomStatusData(data.Substring(11));
            MatchmakingController.instance.StartGame(matchMakingRoomStatus);
        }
        else if (data.Contains("GameMode:"))
        {
            var gamemode = Enum.Parse(typeof(MatchmakingController.GameMode), data.Substring(9));
            MatchmakingController.instance.gameMode = (MatchmakingController.GameMode)gamemode;
        }
    }

    void PorcessRoomStatusData(string data)
    {
        matchMakingRoomStatus = new BeaconAPLayer.MatchmakingRoomStatus();

        string[] allPlayerData = data.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
        int playerNum = allPlayerData.Length;
        matchMakingRoomStatus.totalPlayers = playerNum;
        matchMakingRoomStatus.myGuid = MatchmakingController.instance.getBeaconAPLayer().getMyGUID();

        for (int i = 0; i < playerNum; i++)
        {
            Debug.Log(allPlayerData[i]);
            string[] playerData = allPlayerData[i].Split(',');
            BeaconAPLayer.PlayerInfo info = new BeaconAPLayer.PlayerInfo();
            info.guid = playerData[1];
            int groupid;
            int.TryParse(playerData[2].Substring(4), out groupid);
            info.groupid = (BeaconAPLayer.Group)(groupid - 1);
            matchMakingRoomStatus.groupChoosenPlayerList.Add(info);
            if (i == 0)
                matchMakingRoomStatus.gameLeaderGuid = info.guid;
        }
    }

    void SendDataToIPEndPoint(string text, IPEndPoint ip)
    {
        try
        {
            var data = Encoding.UTF8.GetBytes(text);
            udpSend.Send(data, data.Length, ip);
        }
        catch (Exception e)
        {
            Console.WriteLine("[UDPBroadcast][SendBroadcast] : " + e.Message);
        }
    }

    void Dispose()
    {
        udpBroadcast.Dispose();

        if (udpSend != null)
            udpSend.Close();
        udpSend = null;
    }

    void OnDestroy()
    {
        Dispose();
    }
}
