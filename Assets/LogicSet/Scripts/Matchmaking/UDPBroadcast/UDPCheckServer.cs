﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace UDPBroadcast
{
    public abstract class UDPCheckServer : IDisposable
    {
        public abstract void DebugLog(string text);

        UDPBroadcast udpBroadcast;

        public const string ServerReadyWord = "Sever Ready!!!";
        public const string AskingNetwork = "Is anybody there?";
        public delegate void CanBeServer();
        CanBeServer _CanBeServerCallback;
        public delegate void CanConnectServer(System.Net.IPAddress hostIP, int hostPort);
        CanConnectServer _CanConnectServerCallback;
        int _timeCount, _timeRec;
        bool _canBeServer, _isChecking;

        public void SetCallback(CanBeServer canBeServer, CanConnectServer canConnectServer)
        {
            _CanBeServerCallback = canBeServer;
            _CanConnectServerCallback = canConnectServer;
        }

        const int StartCheckPort = 11223;
        public void StartCheck(float checkSecond)
        {
            //網路廣播10秒等待其他玩家
            Dispose();

            udpBroadcast = new UDPBroadcast();
            udpBroadcast.StartRecieve(_recieveUDP, StartCheckPort);

            _timeCount = (int)(checkSecond * 1000f);
            _timeRec = Environment.TickCount;
            _isChecking = true;
            DebugLog("[UDPCheckServer][CheckStart_Click]");
        }

        public void Update()
        {
            if (udpBroadcast == null)
                return;
            udpBroadcast.Update();

            if (!_isChecking)
                return;

            //一直送封包告訴大家我按連線了，一直不斷的送....
            udpBroadcast.SendBroadcast(AskingNetwork, StartCheckPort);

            _checkCanBeServer();

            if (_timeCount > 0)
            {
                int elipseMillis = Environment.TickCount - _timeRec;
                _timeRec = Environment.TickCount;
                _timeCount -= elipseMillis;
            }
            else
            {
                //偵測時間到，如果統計結果我是IP最大，我就是server，
                //其他人繼續等待收到開房間封包
                if (_canBeServer)
                {
                    if (_CanBeServerCallback != null)
                        _CanBeServerCallback();
                    _isChecking = false;
                    DebugLog("[UDPCheckServer][time up] can Be Server");
                }
            }
        }

        const bool USE_MAX = true;
        void _checkCanBeServer()
        {
            //檢查自己的Address是否為最大
            if (checkIPList.Count > 0)
            {
                IPEndPoint finalIP = null;
                if (USE_MAX)
                {
                    long maxAddress = -1;                    
                    for (int a = 0; a < checkServerRecieve.Count; a++)
                    {
                        if (!checkServerRecieve[a].Contains(AskingNetwork))
                            continue;

                        long addID = checkIPList[a].Address.Address;
                        if (maxAddress < addID)
                        {
                            maxAddress = addID;
                            finalIP = checkIPList[a];
                        }
                    }
                }
                else
                {
                    long minAddress = long.MaxValue;
                    for (int a = 0; a < checkServerRecieve.Count; a++)
                    {
                        if (!checkServerRecieve[a].Contains(AskingNetwork))
                            continue;

                        long addID = checkIPList[a].Address.Address;
                        if (minAddress > addID)
                        {
                            minAddress = addID;
                            finalIP = checkIPList[a];
                        }
                    }
                }

                if (UDPBroadcast.GetSelfIPAddress().Address == finalIP.Address.Address)
                {
                    if (!_canBeServer)
                    {
                        DebugLog("[UDPCheckServer] _canBeServer = true");
                        _canBeServer = true;

                        //將目前的清單印出來看
                        for (int a = 0; a < checkServerRecieve.Count; a++)
                        {
                            if (!checkServerRecieve[a].Contains(AskingNetwork))
                                continue;
                            DebugLog("ip : " + checkIPList[a].ToString());
                        }
                    }
                }
                else
                {
                    if (_canBeServer)
                    {
                        DebugLog("[UDPCheckServer] _canBeServer = false");
                        _canBeServer = false;

                        //將目前的清單印出來看
                        for (int a = 0; a < checkServerRecieve.Count; a++)
                        {
                            if (!checkServerRecieve[a].Contains(AskingNetwork))
                                continue;
                            DebugLog("ip : " + checkIPList[a].ToString());
                        }
                    }
                }
            }
        }

        List<string> checkServerRecieve = new List<string>();
        List<IPEndPoint> checkIPList = new List<IPEndPoint>();
        void _recieveUDP(string recieve, IPEndPoint ip)
        {
            if (!_isChecking)
                return;

            //如果有人開房間就直接去連了
            if (recieve.Contains(ServerReadyWord))
            {
                if (_CanConnectServerCallback != null)
                {
                    int dd = recieve.IndexOf(":");
                    string pp = recieve.Remove(0, dd + 1);

                    int dd2 = pp.LastIndexOf(":");
                    int hostPort = int.Parse(pp.Remove(0, dd2 + 1));

                    pp = pp.Remove(dd2, pp.Length - dd2);
                    IPAddress hostIP = IPAddress.Parse(pp);
                    _CanConnectServerCallback(hostIP, hostPort);
                    _isChecking = false;
                    DebugLog("[UDPCheckServer][ServerReadyWord] can join Server : " + hostIP + " , port : " + hostPort);
                }
                return;
            }

            string combineString = recieve + ip.Address.ToString();
            if (checkServerRecieve.IndexOf(combineString) < 0)
            {
                checkServerRecieve.Add(combineString);
                checkIPList.Add(ip);
            }
        }

        /// <summary>
        /// 如果比較結果IP是最大的，才呼叫這個FUNCTION去廣播告訴大家結果
        /// </summary>
        public void BroadCastSeverReady(int targetPort)
        {
            string iipp = UDPBroadcast.GetSelfIPAddress().ToString();

            udpBroadcast.SendBroadcast(ServerReadyWord + " :" + iipp + ":" + targetPort.ToString(), StartCheckPort);
        }

        public void Dispose()
        {
            if (udpBroadcast != null)
                udpBroadcast.Dispose();
            udpBroadcast = null;

            checkServerRecieve.Clear();
            checkIPList.Clear();

            _canBeServer = false;
        }
    }
}
