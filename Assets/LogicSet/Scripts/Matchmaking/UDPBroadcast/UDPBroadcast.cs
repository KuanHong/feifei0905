﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace UDPBroadcast
{
    /// <summary>
    /// http://stackoverflow.com/questions/40616911/c-sharp-udp-broadcast-and-receive-example
    /// </summary>
    public class UDPBroadcast : IDisposable
    {
        public List<string> RecieveText = new List<string>();
        public List<IPEndPoint> RecieveIPEndPoint = new List<IPEndPoint>();

        //const int PORT = 11223;
        int _recievePort;

        const string TAG = "[UDPBroadcast]";
        public const string StopRecieveWord = "Stop Recieve!!!";

        //object udpRecieveLock = new object();        
        bool recieveThreadExitDone;

        UdpClient udpRecieve;
        UdpClient udpSend;

        public UDPBroadcast()
        {
            udpSend = new UdpClient();
        }

        public void Dispose()
        {
            _closeRecieveThread();

            if (udpRecieve != null)
                udpRecieve.Close();
            udpRecieve = null;

            if (udpSend != null)
                udpSend.Close();
            udpSend = null;
        }

        public void StartRecieve(RecieveCallback callback, int port)
        {
            _recievePort = port;
            if (udpRecieve != null)
                return;

            udpRecieve = new UdpClient();
            udpRecieve.Client.Bind(new IPEndPoint(IPAddress.Any, _recievePort));
            _recieveThread = new Thread(_recieve);
            _recieveThread.Start();
            Console.WriteLine(TAG + "[Start]");

            recieveCallback = callback;
        }

        Thread _recieveThread;
        void _recieve()
        {
            recieveThreadExitDone = false;
            while (true)
            {
                //  lock (udpRecieveLock)
                {
                    // if (recieveThreadExit)
                    //    break;

                    if (udpRecieve != null)
                    {
                        byte[] recvBuffer = null;
                        IPEndPoint RemoteIpEndPoint = null;
                        try
                        {
                            RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
                            recvBuffer = udpRecieve.Receive(ref RemoteIpEndPoint);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[UDPBroadcast][_recieve] : " + e.Message);
                            //在不同多直行續直接跳出會靠腰，使用傳送StopWord給自己去跳出
                        }

                        string recvText = Encoding.UTF8.GetString(recvBuffer);
                        if (recvText == StopRecieveWord)
                        {
                            Console.WriteLine(TAG + "recieve : " + recvText);
                            break;
                        }

                        lock (RecieveText)
                        {
                            RecieveIPEndPoint.Add(RemoteIpEndPoint);
                            RecieveText.Add(recvText);
                        }
                    }
                }
                Thread.Sleep(0);
            }
            recieveThreadExitDone = true;
        }

        void _closeRecieveThread()
        {
            //使用傳送StopWord給自己去跳出
            if (_recieveThread != null)
            {
                //傳送跳出的訊息給自己
                IPEndPoint BroadcastIpEndPoint = new IPEndPoint(GetSelfIPAddress(), _recievePort);

                Byte[] sendBytes = Encoding.ASCII.GetBytes(StopRecieveWord);
                udpSend.Send(sendBytes, sendBytes.Length, BroadcastIpEndPoint);
            }
            while (!recieveThreadExitDone && _recieveThread != null)
            {
                Thread.Sleep(1);
            }

            //if (_recieveThread != null)
            //_recieveThread.Abort();
            _recieveThread = null;
        }

        IPEndPoint BroadcastIpEndPoint;
        public void SendBroadcast(string text, int port)
        {
            //Console.WriteLine("[UDPBroadcast][SendBroadcast] : " + text);
            if (BroadcastIpEndPoint == null)
                BroadcastIpEndPoint = new IPEndPoint(IPAddress.Broadcast, port);

            try
            {
                var data = Encoding.UTF8.GetBytes(text);
                udpSend.Send(data, data.Length, BroadcastIpEndPoint);
            }
            catch (Exception e)
            {
                Console.WriteLine("[UDPBroadcast][SendBroadcast] : " + e.Message);
            }
        }

        public delegate void RecieveCallback(string recvText, IPEndPoint ip);
        public RecieveCallback recieveCallback;

        public void Update()
        {
            if (recieveCallback != null)
            {
                lock (RecieveText)
                {
                    while (RecieveText.Count > 0)
                    {
                        recieveCallback(RecieveText[0], RecieveIPEndPoint[0]);
                        RecieveText.RemoveAt(0);
                        RecieveIPEndPoint.RemoveAt(0);
                    }
                }
            }
        }

        public static IPAddress GetSelfIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip;
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        public static bool IsHaveNetwork()
        {
            //偵測是否有接網路線。
            IPHostEntry selfIP = Dns.GetHostEntry(Dns.GetHostName());
            if (selfIP.AddressList[0].ToString() != "127.0.0.1")
                return true;
            return false;
        }
    }
}
