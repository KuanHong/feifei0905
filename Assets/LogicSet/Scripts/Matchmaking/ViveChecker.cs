﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViveChecker : MonoBehaviour {
  public enum VIVE_ControllingMode {
    FOLLOW_DIALOG,
    CONTROLLER,
    TRACKER
  }
  [HideInInspector]
  public int[] trackerMapSeq = new int[2] { 0, 1 };
  public VIVE_ControllingMode currCM = VIVE_ControllingMode.CONTROLLER;

  public bool forceViveOff = false;
  bool viveMode = false;
  bool viveChecked = false;

  static private ViveChecker _instance;
  static public ViveChecker instance {
    get {
      if (_instance == null) {
        _instance = GameObject.FindObjectOfType<ViveChecker>();
      }
      return _instance;
    }
  }

  void OnDestroy() {
    _instance = null;
  }


  void Start() {

    if (currCM== VIVE_ControllingMode.FOLLOW_DIALOG) {
      if (QualitySettings.names[QualitySettings.GetQualityLevel()]== "TRACKER") {
        currCM = VIVE_ControllingMode.TRACKER;
        Debug.Log("39 - change controller mode to " + currCM.ToString());
      } else
      if (QualitySettings.names[QualitySettings.GetQualityLevel()] == "CONTROLLER") {
        currCM = VIVE_ControllingMode.CONTROLLER;
        Debug.Log("42 - change controller mode to " + currCM.ToString());
      }
    }
    Debug.Log("44 - vive controller mode = " + currCM.ToString());

    checkVive();
    DontDestroyOnLoad(gameObject);
  }

  // Update is called once per frame
  void Update() {

  }

  void checkVive() {
    //if (viveChecked == true)
    //  return;

    //vive installed ?
    if (Application.platform == RuntimePlatform.OSXEditor) {
      viveMode = false;
    } else {

      if (UnityEngine.VR.VRDevice.isPresent && forceViveOff == false /*&& error == Valve.VR.EVRInitError.None*/) {
        //Debug.Log("vive mode enabled");
        viveMode = true;
      } else {
        //Debug.Log("vive mode disabled");
        viveMode = false;
      }
    }

    //viveChecked = true;

  }

  public bool getViveMode() {
    checkVive();
    return viveMode;
  }

  //public bool isViveChecked() {
  //  return viveChecked;
  //}


}
