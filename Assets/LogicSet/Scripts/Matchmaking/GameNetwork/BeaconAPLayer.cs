﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeaconAPLayer : MonoBehaviour
{
    BeaconLinker bl = null;

    public enum Group
    {
        TEAM1,
        TEAM2,
        TEAM3,
        TEAM4,
        TEAM5,
        TEAM6,
        TEAM7,
        TEAM8,
        UNKNOWN
    }
    public class PlayerInfo : IComparable<PlayerInfo>
    {
        public string guid = "";
        public Group groupid = Group.UNKNOWN;
        public int inGameId;

        public int CompareTo(PlayerInfo other)
        {
            return guid.CompareTo(other.guid);
        }
    }
    public class MatchmakingRoomStatus
    {
        public string gameLeaderGuid = null;
        public string myGuid = null;

        public List<PlayerInfo> groupChoosenPlayerList = new List<PlayerInfo>();
        public int totalPlayers = 0;

        public bool isHost()
        {
            return myGuid == gameLeaderGuid;
        }

        public PlayerInfo getMyPlayerInfo()
        {
            for (int i = 0; i < groupChoosenPlayerList.Count; ++i)
            {
                if (groupChoosenPlayerList[i].guid == myGuid)
                {
                    return groupChoosenPlayerList[i];
                }
            }

            return null;
        }

        public PlayerInfo getPlayerInfoByGUID(string guid)
        {
            for (int i = 0; i < groupChoosenPlayerList.Count; ++i)
            {
                if (groupChoosenPlayerList[i].guid == guid)
                {
                    return groupChoosenPlayerList[i];
                }
            }
            return null;
        }

        public List<byte> Serialized(MiniMessagePack.MiniMessagePacker msgpack)
        {
            Dictionary<string, object> rdata = new Dictionary<string, object>();
            List<Dictionary<string, object>> plist = new List<Dictionary<string, object>>();
            for (int i = 0; i < groupChoosenPlayerList.Count; ++i)
            {
                Dictionary<string, object> pidic = new Dictionary<string, object>();
                pidic.Add("guid", groupChoosenPlayerList[i].guid);
                pidic.Add("groupid", (int)groupChoosenPlayerList[i].groupid);
                plist.Add(pidic);
            }
            rdata.Add("groupChoosenPlayerList", plist);
            rdata.Add("myGuid", myGuid);
            rdata.Add("gameLeaderGuid", gameLeaderGuid);
            rdata.Add("totalPlayers", totalPlayers);

            List<byte> retarr = new List<byte>();
            retarr.AddRange(msgpack.Pack(rdata));
            return retarr;
        }
        public static MatchmakingRoomStatus Deserialized(MiniMessagePack.MiniMessagePacker msgpack, byte[] rdata)
        {
            Dictionary<string, object> dicdata = (Dictionary<string, object>)msgpack.Unpack(rdata);
            MatchmakingRoomStatus tmp_mrs = new MatchmakingRoomStatus();
            tmp_mrs.gameLeaderGuid = (string)dicdata["gameLeaderGuid"];
            tmp_mrs.myGuid = (string)dicdata["myGuid"];
            tmp_mrs.totalPlayers = (int)(System.Int64)dicdata["totalPlayers"];
            tmp_mrs.groupChoosenPlayerList = new List<PlayerInfo>();
            List<object> tmpgcpl = (List<object>)dicdata["groupChoosenPlayerList"];
            for (int i = 0; i < tmpgcpl.Count; ++i)
            {
                PlayerInfo pi = new PlayerInfo();
                Dictionary<string, object> raw_item = (Dictionary<string, object>)tmpgcpl[i];
                pi.guid = (string)raw_item["guid"];
                pi.groupid = (Group)(int)(System.Int64)raw_item["groupid"];
                tmp_mrs.groupChoosenPlayerList.Add(pi);
            }

            return tmp_mrs;
        }
    }

    MatchmakingRoomStatus mCurrMRS = null;

    MiniMessagePack.MiniMessagePacker msgpack = null;

    public delegate void APEventFunction(string src_guid, Dictionary<string, object> payload);
    APEventFunction mAEFunction = null;

    void BeaconEventHandler(string src_guid, Dictionary<string, object> payload)
    {
        string type = (string)payload["MSG_TYPE"];

        //CONNECTED_AS_SERVER
        //CONNECTED_AS_CLIENT
        //RES_INIT
        //GROUP_CHOOSEN
        //START_GAME
        //Debug.Log("48 - "+src_guid+" : "+type);
        if (type == "AP_EVENT")
        {
            mAEFunction(src_guid, (Dictionary<string, object>)payload["PAYLOAD"]);
        }
    }

    // Use this for initialization
    public void Initialize()
    {

        msgpack = new MiniMessagePack.MiniMessagePacker();

        bl = gameObject.AddComponent<BeaconLinker>();
        bl.Initialize(BeaconEventHandler);

        if (bl.StartBeacon())
        {
            DontDestroyOnLoad(gameObject);

            mCurrMRS = new MatchmakingRoomStatus();

        }
    }

    public void publishAPEvent(Dictionary<string, object> payload, bool reliable)
    {
        bl.publishData(new Dictionary<string, object>() {
            {"MSG_TYPE", "AP_EVENT" },
            {"PAYLOAD", payload }
        }, reliable);
    }

    public bool publishAPEvent(string target_guid, Dictionary<string, object> payload, bool reliable)
    {
        return bl.publishData(target_guid, new Dictionary<string, object>() {
      {"MSG_TYPE", "AP_EVENT" },
      {"PAYLOAD", payload }
    }, reliable);
    }

    public void registerAPEventHandler(APEventFunction pHandler)
    {
        mAEFunction = pHandler;
    }

    public int getConnectedNode()
    {
        if (bl != null)
        {
            return bl.getKnownPeerCount();
        }

        return 0;
    }

    public string getMyGUID()
    {
        return bl.getMyGUID();
    }

}
