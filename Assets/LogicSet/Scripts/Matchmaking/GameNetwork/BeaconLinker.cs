﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using MiniMessagePack;

//
// DEFINITION
//
public partial class BeaconLinker : MonoBehaviour
{

    class StatisticsInfo
    {
        public string peer_guid;

        public int snd_packets_counter;
        public int rcv_packets_counter;

        public float statistics_duration;
    }
    Dictionary<string, StatisticsInfo> dic_statistics = new Dictionary<string, StatisticsInfo>();

    public delegate void NetworkEventHandler(string src_guid, Dictionary<string, object> payload);
    public NetworkEventHandler mNetworkEventHandler = null;
    const int kMaxBroadcastMsgSize = 1024;

    const int serverPort = 7272;
    const int broadcastPort = 47777;

    // config data
    int m_BroadcastKey = 1000;
    int m_BroadcastVersion = 1;
    int m_BroadcastSubVersion = 1;

    // runtime data
    int beacon_snd_hostId = -1;
    int beacon_rcv_host_id = -1;
    int peerHostId = -1;

    bool running = false;

    MiniMessagePacker msgpack = null;
    byte[] msgOutBuffer = null;
    byte[] msgInBuffer = null;
    HostTopology beaconTopology;
    HostTopology peerlistTopology;

    int publishTargetPriority = -1;
    string myPublishTarget = null;

    string my_guid = "";
    int priority_id = -1;

    byte reliable_channel_id = 0;
    byte unreliable_channel_id = 0;

    public enum PeerStatus
    {
        UNKNOWN = 0,
        CONNECTING,
        CONNECTED,
        NO_SIGNAL,
        SZ
    }

    class Peer
    {
        public string guid = "";
        public string ip = "";
        public int port = 0;
        public PeerStatus status = PeerStatus.UNKNOWN;

        //connection info
        public bool client_peer = false;
        public int hostId = -1;
        public int connectionId = -1;

        public int priority_id = -1;

    }
    Dictionary<string, Peer> peerList = new Dictionary<string, Peer>(); //key:guid
    Dictionary<int, Dictionary<int, string>> peerGUIDList = new Dictionary<int, Dictionary<int, string>>(); //hostid, connectionId -> guid

}

//
// UTILITY
//
public partial class BeaconLinker : MonoBehaviour
{


    byte[] StringToBytes(string str)
    {
        byte[] bytes = new byte[str.Length * sizeof(char)];
        System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
        return bytes;
    }

    string BytesToString(byte[] bytes, int valid_sz)
    {
        char[] chars = new char[valid_sz / sizeof(char)];
        System.Buffer.BlockCopy(bytes, 0, chars, 0, valid_sz);
        return new string(chars);
    }

    public int getKnownPeerCount()
    {
        return peerList.Count;
    }

    public string getMyGUID()
    {
        return my_guid;
    }

    public string getMostPriorityGUID()
    {
        return myPublishTarget;
    }

    public bool isMostPriority()
    {
        return (my_guid == myPublishTarget);
    }
}


public partial class BeaconLinker : MonoBehaviour
{

    //
    // initialize network service
    //
    public void Initialize(NetworkEventHandler pHandler)
    {
        msgpack = new MiniMessagePacker();

        priority_id = UnityEngine.Random.Range(0, 80000);
        publishTargetPriority = priority_id;
        my_guid = System.Guid.NewGuid().ToString();
        myPublishTarget = my_guid;
        Debug.Log("57 - MY GUID IS " + my_guid + ", PRIORITY IS " + priority_id);

        mNetworkEventHandler = pHandler;
    }

    //
    // start network service
    //
    public bool StartBeacon()
    {
        if (running == true)
        {
            Debug.LogWarning("NetworkDiscovery StartAsServer already started");
            return false;
        }

        Dictionary<string, object> raw = new Dictionary<string, object>(){
            {"MSG_TYPE", "SEARCH_PEER"},
            {"SRC_GUID", my_guid},
            {"SRC_PRIORITY", priority_id}
        };
        msgOutBuffer = msgpack.Pack(raw);
        if (msgOutBuffer.Length >= kMaxBroadcastMsgSize)
        {
            Debug.LogError("NetworkDiscovery Initialize - data too large. max is " + kMaxBroadcastMsgSize);
            return false;
        }

        if (!NetworkTransport.IsStarted)
        {
            NetworkTransport.Init();
        }

        msgInBuffer = new byte[kMaxBroadcastMsgSize];


        //
        // PEER PART
        ConnectionConfig cc2 = new ConnectionConfig();
        byte channel_id2 = cc2.AddChannel(QosType.ReliableSequenced);
        byte channel_id3 = cc2.AddChannel(QosType.UnreliableSequenced);
        Debug.Log("453 - HostTopology channel id (reliable) : " + channel_id2 + ", (unreliable) : " + channel_id3);
        peerlistTopology = new HostTopology(cc2, 10);

        reliable_channel_id = channel_id2;
        unreliable_channel_id = channel_id3;

        peerHostId = NetworkTransport.AddHost(peerlistTopology, serverPort);


        //
        //BEACON PART
        ConnectionConfig cc = new ConnectionConfig();
        byte channel_id1 = cc.AddChannel(QosType.ReliableSequenced);
        Debug.Log("447 - beaconTopology channel id : " + channel_id1);
        beaconTopology = new HostTopology(cc, 1);

        beacon_snd_hostId = NetworkTransport.AddHost(beaconTopology, 0);
        if (beacon_snd_hostId == -1)
        {
            Debug.LogError("159 - NetworkDiscovery StartAsServer - addHost failed");
            return false;
        }

        byte err;
        if (!NetworkTransport.StartBroadcastDiscovery(beacon_snd_hostId, broadcastPort, m_BroadcastKey, m_BroadcastVersion, m_BroadcastSubVersion, msgOutBuffer, msgOutBuffer.Length, 1000, out err))
        {
            Debug.LogError("NetworkDiscovery StartBroadcastDiscovery failed err: " + err);
            return false;
        }

        beacon_rcv_host_id = NetworkTransport.AddHost(beaconTopology, broadcastPort);
        if (beacon_rcv_host_id == -1)
        {
            Debug.LogError("171 - NetworkDiscovery StartAsServer - addHost failed");
            return false;
        }

        NetworkTransport.SetBroadcastCredentials(beacon_rcv_host_id, m_BroadcastKey, m_BroadcastVersion, m_BroadcastSubVersion, out err);

        Debug.Log("120 - Start beacon broadcasting...");

        running = true;
        return true;

    }

    //
    // beacon topology
    //
    public void connectToBrocaster(string fromAddress, string guid, string type, int val)
    {
        if (peerList.ContainsKey(guid) == false && val > priority_id)
        {

            //try-connect
            Peer p = new Peer();
            p.guid = guid;
            p.ip = fromAddress;
            p.status = PeerStatus.CONNECTING;
            p.hostId = peerHostId;
            p.priority_id = val;
            p.client_peer = true;

            byte error;
            p.connectionId = NetworkTransport.Connect(p.hostId, fromAddress, serverPort, 0, out error);

            //found new peer
            Debug.Log("448 - connect to [" + fromAddress + "] " + guid + ";" + type + ";" + val + ", connectionId:" + p.connectionId);

            peerList.Add(guid, p);
            Debug.Log("452 - add Peer " + guid + ", peer count=" + peerList.Count);

            //add peer to peer guid mapper
            if (peerGUIDList.ContainsKey(p.hostId))
            {
                Dictionary<int, string> conn_dic = peerGUIDList[p.hostId];
                conn_dic.Add(p.connectionId, guid);
            }
            else
            {
                peerGUIDList.Add(p.hostId, new Dictionary<int, string>(){
                    {p.connectionId, guid}
                });
            }
        }
    }

    //
    // process network event
    //
    void LateUpdate()
    {
        if (running == false)
            return;

        int connectionId;
        int channelId;
        int receivedSize;
        int outHostId;
        byte error;
        NetworkEventType networkEvent = NetworkEventType.DataEvent;

        do
        {
            networkEvent = NetworkTransport.Receive(out outHostId, out connectionId, out channelId, msgInBuffer, kMaxBroadcastMsgSize, out receivedSize, out error);

            if (networkEvent == NetworkEventType.BroadcastEvent && outHostId == beacon_rcv_host_id)
            {
                NetworkTransport.GetBroadcastConnectionMessage(beacon_rcv_host_id, msgInBuffer, kMaxBroadcastMsgSize, out receivedSize, out error);

                string senderAddr;
                int senderPort;
                NetworkTransport.GetBroadcastConnectionInfo(beacon_rcv_host_id, out senderAddr, out senderPort, out error);
                string[] raddr = senderAddr.Split(':');

                Dictionary<string, object> raw = (Dictionary<string, object>)msgpack.Unpack(msgInBuffer);
                string src_guid = (string)raw["SRC_GUID"];
                string msg_type = (string)raw["MSG_TYPE"];
                int src_priority = (int)(Int64)raw["SRC_PRIORITY"];
                if (src_guid != my_guid)
                {
                    connectToBrocaster(raddr[raddr.Length - 1], src_guid, msg_type, src_priority);
                }

            }
            else
            if (networkEvent == NetworkEventType.ConnectEvent)
            {
                Debug.Log("176 - rcv connected event (hostId:" + outHostId + ", connectionId:" + connectionId + ")");

                //...
                foreach (KeyValuePair<string, Peer> entry in peerList)
                {
                    Peer tmpPeer = entry.Value;
                    if (tmpPeer.hostId == outHostId &&
                        tmpPeer.connectionId == connectionId)
                    {
                        //sync status
                        Debug.Log("184 - found connection (hostId:" + tmpPeer.hostId + "), waiting for sync. status...");

                        if (tmpPeer.status == PeerStatus.CONNECTING)
                        {
                            tmpPeer.status = PeerStatus.CONNECTED;

                            if (tmpPeer.priority_id > publishTargetPriority)
                            {
                                myPublishTarget = tmpPeer.guid;
                                publishTargetPriority = tmpPeer.priority_id;
                                Debug.Log("202 - update publish target to " + myPublishTarget + ", priority is " + publishTargetPriority);

                            }

                            //send connection established info.
                            // byte[] d =StringToBytes(my_guid+";CONN_ESTABLISHED;"+priority_id); //established as client
                            byte[] d = msgpack.Pack(new Dictionary<string, object>(){
                      {"MSG_TYPE", "CONN_ESTABLISHED"},
                      {"SRC_GUID", my_guid},
                      {"SRC_PRIORITY", priority_id}
                    });
                            byte e;
                            NetworkTransport.Send(tmpPeer.hostId, tmpPeer.connectionId, 0, d, d.Length, out e);

                            if (mNetworkEventHandler != null)
                            {
                                mNetworkEventHandler(tmpPeer.guid, new Dictionary<string, object>(){
                        {"MSG_TYPE", "CONNECTED_AS_SERVER"},
                        {"SRC_GUID", tmpPeer.guid}
                      });
                            }
                        }
                        else
                        {
                            Debug.LogWarning("198 - peer has already set as CONNECTED (peer:" + tmpPeer.guid + ")");
                        }

                        break;
                    }
                }

            }
            else
            if (networkEvent == NetworkEventType.DisconnectEvent)
            {
                Debug.Log("207 - rcv disconnected event (hostId:" + outHostId + ", connectionId:" + connectionId + ")");

                //清除 peerlist
                string foundPeerGuid = "";
                foreach (KeyValuePair<string, Peer> entry in peerList)
                {
                    Peer tmpPeer = entry.Value;
                    if (tmpPeer.hostId == outHostId &&
                        tmpPeer.connectionId == connectionId)
                    {
                        Debug.Log("214 - process host disconnected event (hostId:" + tmpPeer.hostId + ", connectionId:" + tmpPeer.connectionId + ")...");

                        //clear peerlist
                        entry.Value.status = PeerStatus.NO_SIGNAL;

                        peerList.Remove(tmpPeer.guid);
                        Debug.Log("261 - remove Peer " + tmpPeer.guid + ", peer count=" + peerList.Count);

                        peerGUIDList[tmpPeer.hostId].Remove(tmpPeer.connectionId);

                        foundPeerGuid = tmpPeer.guid;


                        //dispatch disconnect event
                        if (mNetworkEventHandler != null)
                        {
                            mNetworkEventHandler(tmpPeer.guid, new Dictionary<string, object>(){
                      {"MSG_TYPE", "DISCONNECTED"},
                      {"SRC_GUID", tmpPeer.guid}
                    });
                        }

                        break;
                    }
                }

                if (foundPeerGuid == "")
                {
                    Debug.LogWarning("274 - peer not found (hostId:" + outHostId + ", connectionId:" + connectionId + ")");
                }

                if (myPublishTarget == foundPeerGuid)
                {
                    //update new publish target
                    publishTargetPriority = priority_id;
                    myPublishTarget = my_guid;
                    foreach (KeyValuePair<string, Peer> entry in peerList)
                    {
                        Peer tmpPeer = entry.Value;
                        if (tmpPeer.priority_id > publishTargetPriority)
                        {
                            publishTargetPriority = tmpPeer.priority_id;
                            myPublishTarget = tmpPeer.guid;
                        }
                    }

                    Debug.Log("244 - update publish target to " + myPublishTarget + ", priority is " + publishTargetPriority);
                }

            }
            else
            if (networkEvent == NetworkEventType.DataEvent)
            {
                //Debug.Log("278 - rcv data event (hostId:" + outHostId + ", connectionId:" + connectionId + ")");

                Dictionary<string, object> raw = (Dictionary<string, object>)msgpack.Unpack(msgInBuffer);
                string msg_type = (string)raw["MSG_TYPE"];

                if (msg_type == "CONN_ESTABLISHED")
                {
                    string src_guid = (string)raw["SRC_GUID"];
                    int src_priority = (int)(Int64)raw["SRC_PRIORITY"];
                    Debug.Log("235 - Connection established, peer:" + src_guid + ", hostId:" + outHostId + ", priority id:" + src_priority);

                    if (peerList.ContainsKey(src_guid) == true)
                    {
                        Debug.LogWarning("305 - peer has already created (peer:" + src_guid + ")");

                        //remove current peer
                        Peer tmpPeer = peerList[src_guid];
                        peerList.Remove(src_guid);
                        Debug.Log("310 - remove Peer " + src_guid + ", peer count=" + peerList.Count);

                        peerGUIDList[tmpPeer.hostId].Remove(tmpPeer.connectionId);

                        //dispatch disconnect event
                        if (mNetworkEventHandler != null)
                        {
                            mNetworkEventHandler(tmpPeer.guid, new Dictionary<string, object>(){
                      {"MSG_TYPE", "DISCONNECTED"},
                      {"SRC_GUID", tmpPeer.guid}
                    });
                        }
                    }

                    if (peerList.ContainsKey(src_guid) == false)
                    {
                        //CREATE NEW PEER
                        Peer p = new Peer();
                        p.guid = src_guid;
                        p.status = PeerStatus.CONNECTED;
                        p.hostId = outHostId;
                        p.priority_id = src_priority;
                        p.connectionId = connectionId;
                        p.client_peer = false;


                        peerList.Add(p.guid, p);
                        Debug.Log("307 - add Peer " + p.guid + ", peer count=" + peerList.Count);


                        if (peerGUIDList.ContainsKey(p.hostId))
                        {
                            Dictionary<int, string> conn_dic = peerGUIDList[p.hostId];
                            conn_dic.Add(p.connectionId, p.guid);
                        }
                        else
                        {
                            peerGUIDList.Add(p.hostId, new Dictionary<int, string>(){
                      {p.connectionId, p.guid}
                    });
                        }

                        //SEND SYNC. STATUS
                        //SYNC CLOCK ????
                        if (mNetworkEventHandler != null)
                        {
                            mNetworkEventHandler(p.guid, new Dictionary<string, object>(){
                      {"MSG_TYPE", "CONNECTED_AS_CLIENT"},
                      {"SRC_GUID", p.guid}
                    });
                        }
                    }

                }
                else
                {
                    //Debug.Log("316 - process " + msg_type + " message...");
                    if (dic_statistics.ContainsKey(peerGUIDList[outHostId][connectionId]) == false)
                    {
                        dic_statistics.Add(peerGUIDList[outHostId][connectionId], new StatisticsInfo());
                    }
                    dic_statistics[peerGUIDList[outHostId][connectionId]].rcv_packets_counter++;

                    //relay message or process message ?
                    bool dRelay = false;
                    if (raw.ContainsKey("MSG_DONT_RELAY"))
                    {
                        dRelay = (bool)raw["MSG_DONT_RELAY"];
                    }
                    if (dRelay == true)
                    {
                        //process message
                        if (mNetworkEventHandler != null)
                        {
                            mNetworkEventHandler(peerGUIDList[outHostId][connectionId], raw);
                        }
                    }
                    else
                    {
                        //relay data
                        publishData(raw);
                        if (myPublishTarget == my_guid)
                        {
                            //process message
                            if (mNetworkEventHandler != null)
                            {
                                mNetworkEventHandler(peerGUIDList[outHostId][connectionId], raw);
                            }
                        }
                    }

                }

            }

        } while (networkEvent != NetworkEventType.Nothing);

        //refresh statistics counter
        foreach (KeyValuePair<string, StatisticsInfo> entry in dic_statistics)
        {
            entry.Value.statistics_duration += Time.deltaTime;

            if (entry.Value.statistics_duration >= 5.0f)
            {
                Debug.Log("393 - rcv packet times = " + entry.Value.rcv_packets_counter + ", snd packet times = " + entry.Value.snd_packets_counter);
                entry.Value.snd_packets_counter = 0;
                entry.Value.rcv_packets_counter = 0;
                entry.Value.statistics_duration = 0.0f;
            }
        }

    }


    public void publishData(Dictionary<string, object> payload, bool reliable = true)
    {

        //check message type and source(sender) guid
        if (payload.ContainsKey("MSG_TYPE") == false)
        {
            payload.Add("MSG_TYPE", "DATA");
        }

        if (payload.ContainsKey("MSG_SRC") == false)
        {
            payload.Add("MSG_SRC", my_guid);
        }

        if (myPublishTarget != my_guid)
        { //直接 relay data 給 publisher
          //relay data
            sendDataToPeer(myPublishTarget, msgpack.Pack(payload), reliable);
            Debug.Log("402 - relay message to peer : " + myPublishTarget + " (hostId: " + peerList[myPublishTarget].hostId + ", connectionId: " + peerList[myPublishTarget].connectionId + ")");
        }
        else
        {
            //doing real publish
            //doing real publish
            if (payload.ContainsKey("MSG_DONT_RELAY") == false)
            {
                payload.Add("MSG_DONT_RELAY", true);
            }
            else
            {
                payload["MSG_DONT_RELAY"] = true;
            }

            string msg_src_guid = null;
            if (payload.ContainsKey("MSG_TYPE") && ((string)payload["MSG_TYPE"]) == "DATA" && payload.ContainsKey("MSG_SRC"))
            {
                msg_src_guid = (string)payload["MSG_SRC"];
            }

            foreach (KeyValuePair<string, Peer> entry in peerList)
            {
                bool skipPeer = false;

                if (msg_src_guid != null && msg_src_guid == entry.Value.guid)
                    skipPeer = true;

                if (skipPeer == true)
                {
                    Debug.Log("434 - skip publish peer : " + entry.Value.guid);
                    continue;
                }

                Peer tmpPeer = entry.Value;
                sendDataToPeer(tmpPeer.guid, msgpack.Pack(payload), reliable);

                Debug.Log("416 - publish message to peer : " + tmpPeer.guid + " (hostId: " + tmpPeer.hostId + ", connectionId: " + tmpPeer.connectionId + ")");
            }

        }
    }

    public bool publishData(string target_guid, Dictionary<string, object> payload, bool reliable = true)
    {
        if (target_guid == getMyGUID())
            return true;

        //check message type and source(sender) guid
        if (payload.ContainsKey("MSG_TYPE") == false)
        {
            payload.Add("MSG_TYPE", "DATA");
        }

        if (payload.ContainsKey("MSG_SRC") == false)
        {
            payload.Add("MSG_SRC", my_guid);
        }
        payload.Add("MSG_DONT_RELAY", true);

        return sendDataToPeer(target_guid, msgpack.Pack(payload), reliable);
    }

    bool sendDataToPeer(string peer_guid, byte[] rawbytes, bool reliable)
    {
        if (peerList.ContainsKey(peer_guid) == false)
            return false;

        Peer p = peerList[peer_guid];
        byte e;
        NetworkTransport.Send(p.hostId, p.connectionId, reliable ? reliable_channel_id : unreliable_channel_id, rawbytes, rawbytes.Length, out e);

        if (dic_statistics.ContainsKey(peer_guid) == false)
        {
            dic_statistics.Add(peer_guid, new StatisticsInfo());
        }
        dic_statistics[peer_guid].snd_packets_counter++;

        return true;
    }
}
