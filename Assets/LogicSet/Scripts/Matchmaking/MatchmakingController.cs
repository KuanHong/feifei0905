﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchmakingController : MonoBehaviour
{
    public static MatchmakingController instance { get; private set; }

    BeaconAPLayer bap = null;
    BeaconAPLayer.MatchmakingRoomStatus currMRS = null;

    public enum GameMode
    {
        TeamFight,
        FreeFight
    }

    public GameMode gameMode = GameMode.FreeFight;

    void Awake()
    {
        if (instance != null)
        {
            enabled = false;
            DestroyImmediate(this);
            return;
        }

        instance = this;
    }

    void Start()
    {
        bap = GetComponent<BeaconAPLayer>();
        bap.Initialize();
        bap.registerAPEventHandler(aPEventHandler);
    }

    public void StartGame(BeaconAPLayer.MatchmakingRoomStatus mrs)
    {
        currMRS = mrs;
        UnityEngine.SceneManagement.SceneManager.LoadScene("game_scene");
    }

    public BeaconAPLayer.MatchmakingRoomStatus getMatchmakingInfo()
    {
        return currMRS;
    }

    public BeaconAPLayer getBeaconAPLayer()
    {
        return bap;
    }

    void aPEventHandler(string src_guid, Dictionary<string, object> payload)
    {
        //DELIVER AP EVENT
        if (pAPHandler != null)
        {
            pAPHandler(src_guid, payload);
        }
    }

    public void publishAPEvent(string dest_guid, Dictionary<string, object> payload, bool reliable)
    {
        if (currMRS == null || currMRS.myGuid == dest_guid)
        {
            return;
        }

        for (int i = 0; i < currMRS.groupChoosenPlayerList.Count; ++i)
        {
            if (currMRS.groupChoosenPlayerList[i].guid == dest_guid)
            {
                bap.publishAPEvent(currMRS.groupChoosenPlayerList[i].guid, payload, reliable);
                break;
            }
        }
    }

    public void publishAPEvent(Dictionary<string, object> payload, bool reliable)
    {
        if (currMRS == null)
        {
            Debug.LogError("114 - currMRS is null");
            return;
        }

        for (int i = 0; i < currMRS.groupChoosenPlayerList.Count; ++i)
        {
            if (currMRS.groupChoosenPlayerList[i].guid != currMRS.myGuid)
            {
                bap.publishAPEvent(currMRS.groupChoosenPlayerList[i].guid, payload, reliable);
            }
        }

    }

    public delegate void APEventHandler(string src_guid, Dictionary<string, object> payload);
    APEventHandler pAPHandler = null;
    public void registerAPEventHandler(APEventHandler handler)
    {
        pAPHandler = handler;
    }
}
