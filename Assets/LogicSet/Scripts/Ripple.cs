﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ripple : MonoBehaviour {
  List<IAnimation> mAnimationList = new List<IAnimation>();

  // Use this for initialization
  void Start () {
    counter = 2.0f;

  }

  float counter = 0.0f;
	// Update is called once per frame
	void Update () {
    //animation
    for (int i = 0; i < mAnimationList.Count;/*++i*/) {
      mAnimationList[i].updateAnimation(Time.deltaTime);

      if (mAnimationList[i].isAnimationDone() && mAnimationList[i].isAutoDismiss()) {
        mAnimationList.RemoveAt(i);
      } else {
        ++i;
      }
    }


    counter += Time.deltaTime;
    if (counter >= 2.0f) {

      {
        ScaleAnimation sa = new ScaleAnimation();
        sa.initAnimation(CurveUtil.CircEaseOut, 0.0f, 1.5f, new GameObject[] { gameObject }, true);
        sa.setupScaleVal(Vector3.zero, Vector3.one);
        mAnimationList.Add(sa);
      }
      {
        ColorAnimation ca = new ColorAnimation();
        ca.initAnimation(CurveUtil.ExpoEaseIn, 0.0f, 1.5f, new SpriteRenderer[] { gameObject.GetComponent<SpriteRenderer>() }, true);
        ca.setupColorVal(new Color(1.0f, 1.0f, 1.0f, 1.0f), new Color(1.0f, 1.0f, 1.0f, 0.0f));
        mAnimationList.Add(ca);
      }

      counter = 0.0f;
    }
  }
}
