﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gyroscope : MonoBehaviour {

  public bool enable = false;
  Vector3 _forwardingDir = Vector3.zero;
  Vector3 _cacheDir = Vector3.zero;

  float shift_angle = 0.0f;

  public bool rotateHead = false;
  public float rotateHeadRatio = 0.75f;

  public Vector3 forwardingDir {
    get {
      return _forwardingDir;
    }
    set {
      updateForwardingDir(value);

    }

  }

  public void updateForwardingDir(Vector3 new_forwarding, float pre_apply_feedback_ang_scale =1.0f) {
    if (rotateHead == false || _forwardingDir == Vector3.zero) {
      _forwardingDir = new_forwarding;

    } else {
      //cache dir
      //...
      if (_cacheDir == Vector3.zero) {
        Vector3 f = new_forwarding;
        float ang = Vector3.Angle(new Vector3(_forwardingDir.x, 0.0f, _forwardingDir.z), new Vector3(f.x, 0.0f, f.z));
        if (Mathf.Abs(ang) > 0.0f) {
          if (Vector3.Cross(_forwardingDir, new_forwarding).y < 0) {
            ang = -ang;
          }
          shift_angle += ang* pre_apply_feedback_ang_scale;

          _cacheDir = new_forwarding;
        }

      } else {
        Vector3 f = new_forwarding;
        float ang = Vector3.Angle(new Vector3(_cacheDir.x, 0.0f, _cacheDir.z), new Vector3(f.x, 0.0f, f.z));
        if (Mathf.Abs(ang) > 0.0f) {
          if (Vector3.Cross(_cacheDir, new_forwarding).y < 0) {
            ang = -ang;
          }
          shift_angle += ang*pre_apply_feedback_ang_scale;

          _cacheDir = new_forwarding;
        }

      }

    }
  }

  // Use this for initialization
  void Start() {

  }

  public void reset() {
    //Debug.Log("reset gyroscope rotation");
    transform.localRotation = Quaternion.identity;
    _forwardingDir = Vector3.zero;

    _cacheDir = Vector3.zero;
    shift_angle = 0.0f;
  }


  // Update is called once per frame
  void Update() {

    //achieve chache direction
    Vector3 fd = _forwardingDir;

    if (rotateHead == true) {
      fd = Quaternion.AngleAxis(shift_angle * rotateHeadRatio, Vector3.up) * fd;
    } else {
      fd = _forwardingDir;
    }

    if (forwardingDir != Vector3.zero && enable == true) {
      transform.rotation = Quaternion.identity;
      transform.forward = new Vector3(fd.x, 0.0f, fd.z);
    } else {
      transform.localRotation = Quaternion.identity;
    }

  }
}
