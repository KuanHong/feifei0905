﻿using UnityEngine;
using System.Collections;

public class Screencapture : MonoBehaviour {

    public Camera camera;
    private Texture2D ScreenShot;
    private RenderTexture rt;
    private int resWidth = 4096;
    private int resHeight = 4096*9/16;

    void Start() {
        rt = new RenderTexture(resWidth, resHeight, 24);
        ScreenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
    }

    void Update() {
        if (Input.GetKeyDown(KeyCode.P)) {
            camera.targetTexture = rt;
            camera.Render();
            RenderTexture.active = rt;
            ScreenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
            RenderTexture.active = null; //can help avoid errors 
            camera.targetTexture = null;
            var bytes = ScreenShot.EncodeToPNG();
            System.DateTime now = System.DateTime.Now;
            string date = now.Year + "-" + now.Month + "-" + now.Day + "-" + now.Hour + "-" + now.Minute + "-" + now.Second;
            string FILE_NAME = "BandData-" + date + ".png";
            System.IO.File.WriteAllBytes("C:/Users/user/Documents/" + FILE_NAME, bytes);
            Debug.Log("C:/Users/user/Documents/" + FILE_NAME);
        }
    }
}