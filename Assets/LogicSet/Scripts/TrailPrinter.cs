﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailPrinter : MonoBehaviour {
    float counter = 0.0f;
    float stride = 0.1f;

    public GameObject trailObj = null;
    LineRenderer lr = null;

    List<Vector3> record_pts = new List<Vector3>();

	// Use this for initialization
	void Start () {
        lr = GetComponent<LineRenderer>();
        lr.positionCount = 0;
	}
	
	// Update is called once per frame
	void Update () {
        
        if (trailObj == null) {
            GameObject.Destroy(gameObject);
            return;
        }

        counter += Time.deltaTime;

        if (counter >= stride) {
            counter = 0.0f;

            record_pts.Add(trailObj.transform.localPosition);
            if (record_pts.Count > 100) {
                record_pts.RemoveAt(0);
            }

            lr.positionCount = record_pts.Count;
            lr.SetPositions(record_pts.ToArray());
        }
		
	}
}
