﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleProgressLine : MonoBehaviour {
  LineRenderer lr = null;
  GameObject flash = null;

  public Sprite greenSight = null;
  public Sprite redSight = null;

	// Use this for initialization
	void Start () {
    lr = GetComponent<LineRenderer>();
    flash = transform.Find("Flash").gameObject;
    flash.SetActive(false);

	}

  //0~1
  public void setProgress(float percent, bool hostile=false) {
    int ct = (int)(360.0f/10.0f);
    lr.positionCount = ct;
    Vector3[] num_pts = new Vector3[ct];
    for (int i = 0; i < ct; ++i) {
      num_pts[i] = Quaternion.AngleAxis(i*10*percent, -Vector3.forward)*(new Vector3(0.0f, -0.32f, 0.0f));
    }

    lr.SetPositions(num_pts);

    Color c = Color.green;
    if (hostile) {
      c = Color.red;
    }
    lr.startColor = c;
    lr.endColor = c;

    if (percent >= 0.9f) {
      
      if (hostile) {
        flash.GetComponent<SpriteRenderer>().sprite = redSight;
      } else {
        flash.GetComponent<SpriteRenderer>().sprite = greenSight;
      }
      flash.SetActive(true);
    } else {
      flash.SetActive(false);
    }

  }
}
