﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthIndicatorEffect : MonoBehaviour {

    bool showing = true;
    enum Status {
        FLASHING,
        FADEOUT,
        IDLE
    }
    Status curr_status = Status.IDLE;

	// Use this for initialization
	void Start () {
		
	}

    // Update is called once per frame
    float flash_stride = 0.05f;
    bool flash_showing = true;

    float flash_count = 0.0f;
    float count = 0.0f;
    float fade_alpha = 1.0f;
	void Update () {
        if (curr_status == Status.FLASHING) {
            count += Time.deltaTime;
            flash_count += Time.deltaTime;
            if (count > 1.0f) {
                curr_status = Status.FADEOUT;
                count = 0.0f;
                flash_count = 0.0f;
                fade_alpha = 1.0f;
                GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
                return;
            }

            if (flash_count >= flash_stride) {
                flash_count = 0.0f;
                flash_showing = !flash_showing;
                GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, flash_showing ? 1.0f : 0.0f);
            }

        }else
        if (curr_status == Status.FADEOUT) {
            count += Time.deltaTime;
            if (count >= 1.0f) {
                count = 0.0f;
                curr_status = Status.IDLE;
                GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
                return;
            }

            //FADE-ALPHA
            fade_alpha = (float)CurveUtil.QuintEaseOut(count, 1.0f, -1.0f, 1.0f);
            GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, fade_alpha);
        }
		
	}

    public void setActive(bool active) {
        //GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, active ? 1.0f : 0.0f);

        if (active == true) {
            curr_status = Status.IDLE;
            GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
            showing = true;
        } else {
            if (showing == true) {
                showing = false;
                flash_count = 0.0f;
                count = 0.0f;
                curr_status = Status.FLASHING;
            }

        }
    }

}
