﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarningViewDisplayer : MonoBehaviour {

    public GameObject eyeObj = null;
    GameObject warnObj = null;
    GameObject timeObj = null;

    GameObject sec = null;
    GameObject[] msec = null;

    public Sprite[] sprite_arr = null;

    // Use this for initialization
    void Start() {
        warnObj = transform.Find("Root").gameObject;
        warnObj.SetActive(false);

        timeObj = transform.Find("Time").gameObject;
        timeObj.SetActive(false);

        sec = timeObj.transform.Find("SEC").gameObject;
        msec = new GameObject[2];
        msec[0] = timeObj.transform.Find("MILLSEC0").gameObject;
        msec[1] = timeObj.transform.Find("MILLSEC1").gameObject;
    }

    // Update is called once per frame
    void Update() {
        if (eyeObj != null) {
            transform.position = eyeObj.transform.position/* + eyeObj.transform.rotation * new Vector3(0.0f, 0.0f, 0.0f)*/;
            transform.rotation = eyeObj.transform.rotation;
        }

        if (showing) {
            counter += Time.deltaTime;
            if (counter < duration) {
                int c = (int)(duration - counter);
                c = c % 10;

                //sec
                sec.GetComponent<SpriteRenderer>().sprite = sprite_arr[c];

                //msec
                float ms = duration - counter;
                ms *= 100.0f;
                int nms = (int)ms % 100;
                msec[0].GetComponent<SpriteRenderer>().sprite = sprite_arr[nms / 10];
                msec[1].GetComponent<SpriteRenderer>().sprite = sprite_arr[nms % 10];
            }
        }
    }

    float counter = 0.0f;
    float duration = 10.0f;

    bool showing = false;
    public void showWarningView(bool show) {
        if (show != showing) {
            showing = show;
            warnObj.SetActive(show);
            timeObj.SetActive(show);

            if (show == true) {
                counter = 0.0f;
            }
        }
    }

    public bool isTimesUp() {
        if (counter >= duration) {
            return true;
        }
        return false;
    }

    public void resetTimer() {
        counter = 0.0f;
    }
}
