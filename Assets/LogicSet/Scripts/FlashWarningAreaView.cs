﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashWarningAreaView : MonoBehaviour {
    public GameObject[] objs = null;

    float base_rate = 2.0f;

    float rate = 0.0f;
    float curr_alpha = 1.0f;

	// Use this for initialization
	void Start () {
        rate = base_rate;
	}
	
	// Update is called once per frame
	void Update () {
        if (objs == null)
            return;

        curr_alpha += Time.deltaTime * rate;
        if (curr_alpha >= 1.0f) {// || curr_alpha <= 0.0f) {
            rate = -1.0f*base_rate;
        }else
        if (curr_alpha <= 0.0f) {
            rate = base_rate;
        }

        for (int i = 0; i < objs.Length; ++i) {
            objs[i].GetComponent<MeshRenderer>().material.SetColor("_Color", new Color(1.0f, 1.0f, 1.0f, curr_alpha));
        }
		
	}
}
