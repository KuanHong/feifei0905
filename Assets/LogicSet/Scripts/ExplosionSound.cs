﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionSound : MonoBehaviour {

    AudioSource asund =null;
    float vol = 1.0f;

    float counter = 0.0f;
    float duration = 1.2f;

	// Use this for initialization
	void Start () {
        asund =GetComponent<AudioSource>();
        vol = asund.volume;
	}
	
	// Update is called once per frame
	void Update () {
        counter += Time.deltaTime;
        if (counter <= duration) {
            asund.volume = vol-(counter / duration * vol);
        }
		
	}
}
