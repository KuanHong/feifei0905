﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedLinePlayer : MonoBehaviour {

  public Texture[] speedLineTex = null;
  public GameObject speedLineBasePrefab = null;

  public int maxLineNum = 10;
  public float lineWidth = 0.35f;
  public float lineLength = 20.0f;

  public float freqRatio = 1.0f;
  public float topSpeed = 300.0f;

  public float radius = 10.0f;

  Vector3 cam_posi = Vector3.zero;
  Vector3 cam_dir = Vector3.forward;
  float cam_speed = 0.0f;


  // Use this for initialization
  int tint_color_id = -1;
  void Start() {
    tint_color_id = Shader.PropertyToID("_TintColor");
  }

  // Update is called once per frame
  void Update() {
    speedLineGenerator();
    for (int i = 0; i < speed_line_list.Count; ++i) {
      speedLineAnimPlayer(speed_line_list[i]);
    }

    removeObsoletedSpeedLine();
  }

  enum SpeedLineType {
    STATIC,
    ANIMATED
  }
  enum SpeedLineObjectStatus {
    NULL,
    INIT,
    PLAYING,
    EXIT
  }
  enum SpeedLinePlayingStatus {
    IDLE,
    PLAYING
  }
  SpeedLinePlayingStatus slps = SpeedLinePlayingStatus.IDLE;

  class Animation {
    //public float counter = 0.0f;
    public float duration = 0.0f;

    public float startVal = 0.0f;
    public float targetVal = 0.0f;
    public float currVal = 0.0f;
  }

  class SpeedLine {
    public GameObject obj = null;

    public Vector3 ori_posi = Vector3.zero;
    public Vector3 ori_center = Vector3.zero;
    public float wideScale = 1.0f;
    public float lengthScale = 1.0f;

    public int curr_tex_no = -1;

    public SpeedLineType slt = SpeedLineType.STATIC;
    public SpeedLineObjectStatus slps = SpeedLineObjectStatus.NULL;

    //into animation
    public Animation into_lenAnim = null;
    public Animation into_fadeAnim = null;

    //playing animation
    //...

    //exit animation
    public Animation exit_lenAnim = null;
    public Animation exit_fadeAnim = null;


    //life
    public float counter = 0.0f;
    public float duration = 0.0f;

    public float texCounter = 0.0f;

  }

  List<SpeedLine> speed_line_list = new List<SpeedLine>();

  void speedLineGenerator() {
    if (slps == SpeedLinePlayingStatus.IDLE) {
      return;
    }

    //generate line ?
    float speed = 1.0f;
    //if (cam_speed >= topSpeed) {
    //    speed = 1.0f;
    //} else {
      speed = cam_speed / topSpeed;
    //}

    float lw = lineWidth * speed;

    float rad_adj = speed - 1.0f;
    if (rad_adj <= 0.0f)
      rad_adj = 0.0f;

    int ln = (int)(maxLineNum * speed);
    if (speed_line_list.Count > ln)
      return;

    if (speed_line_list.Count > 10 || UnityEngine.Random.Range(0, 100) < 70)
      return;

    //generate speedline
    SpeedLine sl_inst = new SpeedLine();
    sl_inst.duration = UnityEngine.Random.Range(0.3f, 1.6f);
    sl_inst.wideScale = UnityEngine.Random.Range(0.3f, 2.25f);
    sl_inst.lengthScale = UnityEngine.Random.Range(0.3f, 2.5f) * (sl_inst.wideScale + 0.7f);

    GameObject obj = GameObject.Instantiate(speedLineBasePrefab);
    obj.transform.SetParent(transform, true);
    obj.transform.localPosition = Vector3.zero;
    obj.transform.localRotation = Quaternion.identity;
    obj.name = "SpeedLine";


    //random position/scale
    obj.transform.localPosition = Quaternion.AngleAxis(UnityEngine.Random.Range(0, 360), Vector3.up) * (Vector3.forward * (radius-rad_adj*radius*0.5f) );
    obj.transform.localPosition = new Vector3(obj.transform.localPosition.x * 1.5f, obj.transform.localPosition.y, obj.transform.localPosition.z); //span width
    obj.transform.localPosition += Vector3.up * 15.0f;
    obj.transform.localScale = new Vector3(lw * sl_inst.wideScale, lineLength * sl_inst.lengthScale, 1.0f);

    //
    //setup init animation
    //fade
    sl_inst.into_fadeAnim = new Animation();
    sl_inst.into_fadeAnim.startVal = 0.0f;
    sl_inst.into_fadeAnim.targetVal = UnityEngine.Random.Range(0.01f, 0.05f*speed*speed);
    sl_inst.into_fadeAnim.currVal = sl_inst.into_fadeAnim.startVal;
    sl_inst.into_fadeAnim.duration = sl_inst.duration * UnityEngine.Random.Range(0.4f, 0.5f);

    //scale
    sl_inst.into_lenAnim = new Animation();
    sl_inst.into_lenAnim.startVal = 0.0f;
    sl_inst.into_lenAnim.targetVal = sl_inst.lengthScale;
    sl_inst.into_lenAnim.currVal = sl_inst.into_lenAnim.startVal;
    sl_inst.into_lenAnim.duration = sl_inst.into_fadeAnim.duration;

    //
    //setup exit animation
    sl_inst.exit_fadeAnim = new Animation();
    sl_inst.exit_fadeAnim.startVal = sl_inst.into_fadeAnim.targetVal;
    sl_inst.exit_fadeAnim.targetVal = 0.0f;
    sl_inst.exit_fadeAnim.currVal = sl_inst.exit_fadeAnim.startVal;
    sl_inst.exit_fadeAnim.duration = sl_inst.duration * UnityEngine.Random.Range(0.4f, 0.5f);

    //scale
    sl_inst.exit_lenAnim = new Animation();
    sl_inst.exit_lenAnim.startVal = sl_inst.lengthScale;
    sl_inst.exit_lenAnim.targetVal = 0.0f;
    sl_inst.exit_lenAnim.currVal = sl_inst.exit_lenAnim.startVal;
    sl_inst.exit_lenAnim.duration = sl_inst.exit_fadeAnim.duration;

    //
    //setup speedline inst
    sl_inst.slps = SpeedLineObjectStatus.INIT;

    sl_inst.obj = obj;
    sl_inst.ori_posi = obj.transform.localPosition;
    sl_inst.ori_center = Vector3.up * 15.0f;

    sl_inst.curr_tex_no = UnityEngine.Random.Range(0, speedLineTex.Length);
    sl_inst.obj.GetComponent<MeshRenderer>().material.mainTexture = speedLineTex[sl_inst.curr_tex_no];

    Color tc = new Color(1.0f, 1.0f-rad_adj, 1.0f-rad_adj);
    sl_inst.obj.GetComponent<MeshRenderer>().material.SetColor("_TintColor", tc);

    speed_line_list.Add(sl_inst);

  }

  void removeObsoletedSpeedLine() {
    for (int i = 0; i < speed_line_list.Count;/*++i*/) {
      if (speed_line_list[i].counter >= speed_line_list[i].duration) {
        //dispose speedline
        GameObject.Destroy(speed_line_list[i].obj);
        speed_line_list.RemoveAt(i);
      } else {
        ++i;
      }
    }

  }


  void speedLineAnimPlayer(SpeedLine sl) {

    //facing camera
    sl.obj.transform.localRotation = Quaternion.LookRotation(sl.ori_posi - sl.ori_center);

    //tex
    sl.texCounter += Time.deltaTime;
    if (sl.texCounter >= 0.025f) {
      sl.texCounter = 0.0f;
      sl.curr_tex_no++;
      if (sl.curr_tex_no >= speedLineTex.Length) {
        sl.curr_tex_no = 0;
      }

      sl.obj.GetComponent<MeshRenderer>().material.mainTexture = speedLineTex[sl.curr_tex_no];
    }

    sl.counter += Time.deltaTime;
    if (sl.slps == SpeedLineObjectStatus.INIT) {
      if (sl.counter >= sl.into_fadeAnim.duration) {
        sl.slps = SpeedLineObjectStatus.PLAYING;

        //FADE
        Color ce = sl.obj.GetComponent<MeshRenderer>().material.GetColor(tint_color_id);
        ce.a = sl.into_fadeAnim.targetVal;
        sl.obj.GetComponent<MeshRenderer>().material.SetColor(tint_color_id, ce);

        //SCALE
        sl.obj.transform.localScale = new Vector3(sl.obj.transform.localScale.x, lineLength * sl.into_lenAnim.targetVal, sl.obj.transform.localScale.z);
        return;
      }

      //FADE
      sl.into_fadeAnim.currVal = (float)CurveUtil.QuintEaseOut(sl.counter, sl.into_fadeAnim.startVal, sl.into_fadeAnim.targetVal - sl.into_fadeAnim.startVal, sl.into_fadeAnim.duration);
      Color c = sl.obj.GetComponent<MeshRenderer>().material.GetColor(tint_color_id);
      c.a = sl.into_fadeAnim.currVal;
      sl.obj.GetComponent<MeshRenderer>().material.SetColor(tint_color_id, c);

      //SCALE
      sl.into_lenAnim.currVal = (float)CurveUtil.QuintEaseOut(sl.counter, sl.into_lenAnim.startVal, sl.into_lenAnim.targetVal - sl.into_lenAnim.startVal, sl.into_lenAnim.duration);
      float len = sl.into_lenAnim.currVal * lineLength;
      Vector3 s = sl.obj.transform.localScale;
      s.y = len;
      sl.obj.transform.localScale = s;

      //translate position depends on its scale value
      float ratio = sl.into_lenAnim.currVal / (sl.into_lenAnim.targetVal - sl.into_lenAnim.startVal);
      float total_length = lineLength * sl.lengthScale;

      sl.obj.transform.localPosition = sl.ori_posi + (Vector3.up * total_length * 0.5f) - (Vector3.up * total_length * 0.5f) * ratio;

    } else
    if (sl.slps == SpeedLineObjectStatus.PLAYING) {
      if (sl.counter >= (sl.duration - (sl.into_fadeAnim.duration + sl.exit_fadeAnim.duration))) {
        sl.slps = SpeedLineObjectStatus.EXIT;
        return;
      }

    } else
    if (sl.slps == SpeedLineObjectStatus.EXIT) {
      float startCounter = sl.duration - sl.exit_fadeAnim.duration;

      //correct duration problem
      float t = sl.counter - startCounter;
      if (t < 0.0f) {
        // Debug.Log(t);
        t = 0.0f;

      } else
      if (t > sl.exit_lenAnim.duration) {
        // Debug.Log(t);
        t = sl.exit_lenAnim.duration;

      }

      //fade
      sl.exit_fadeAnim.currVal = (float)CurveUtil.QuintEaseIn(t, sl.exit_fadeAnim.startVal, sl.exit_fadeAnim.targetVal - sl.exit_fadeAnim.startVal, sl.exit_fadeAnim.duration);
      Color c = sl.obj.GetComponent<MeshRenderer>().material.GetColor(tint_color_id);
      c.a = sl.exit_fadeAnim.currVal;
      sl.obj.GetComponent<MeshRenderer>().material.SetColor(tint_color_id, c);

      //scale
      sl.exit_lenAnim.currVal = (float)CurveUtil.QuintEaseIn(t, sl.exit_lenAnim.startVal, sl.exit_lenAnim.targetVal - sl.exit_lenAnim.startVal, sl.exit_lenAnim.duration);
      float len = sl.exit_lenAnim.currVal * lineLength;
      Vector3 s = sl.obj.transform.localScale;
      s.y = len;
      sl.obj.transform.localScale = s;

      //translate position depends on its scale value
      float ratio = sl.exit_lenAnim.currVal / (sl.exit_lenAnim.targetVal - sl.exit_lenAnim.startVal);
      float total_length = lineLength * sl.lengthScale;

      sl.obj.transform.localPosition = sl.ori_posi - (Vector3.up * total_length * 0.5f) - (Vector3.up * total_length * 0.5f) * ratio;
    }


  }

  public void updateCameraInfo(Vector3 position, Vector3 forward_dir, float speed, float top_speed) {
    cam_posi = position;
    cam_dir = forward_dir;
    cam_speed = speed;
    topSpeed = top_speed;

    transform.localPosition = cam_posi;
    if (forward_dir != Vector3.zero)
      transform.forward = forward_dir;
    transform.localRotation *= Quaternion.AngleAxis(90.0f, Vector3.right);

    slps = SpeedLinePlayingStatus.PLAYING;
  }

}
