﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//目前只有在基地有用到, 偵測 player collider 之後回收包子用
public class UnloadPtCollideReporter : MonoBehaviour
{

    public delegate void ReportFunc(bool hostile_spawn_pt);
    bool mHostilePt = false;
    ReportFunc rfunc = null;
    public void registerReportFunc(ReportFunc rf, bool hostile_pt = false)
    {
        mHostilePt = hostile_pt;
        rfunc = rf;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "PlayerCollider")
        {
            Debug.Log("28 - unloadPoint OnTriggerEnter (collide with :" + other.name + ")...");

            //report to player controller
            if (rfunc != null)
            {
                rfunc(mHostilePt);
            }
        }
    }
}
