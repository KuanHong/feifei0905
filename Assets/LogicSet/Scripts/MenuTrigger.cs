﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other) {
        if (other.name != "anchor")
            return;

        transform.parent.GetComponent<GameMenuController>().setUIEvent(GameMenuController.UIEvent.ENTER, gameObject.name, other);
    }

    void OnTriggerExit(Collider other) {
        if (other.name != "anchor")
            return;

        transform.parent.GetComponent<GameMenuController>().setUIEvent(GameMenuController.UIEvent.EXIT, gameObject.name, other);

    }
}
