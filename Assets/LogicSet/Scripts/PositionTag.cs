﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionTag : MonoBehaviour {

  float fixed_dist = -20.0f;

  // Use this for initialization
  void Start() {

  }

  // Update is called once per frame
  void LateUpdate() {
    Vector3 cam_pt = Camera.main.transform.position;
    Vector3 dir = cam_pt - (transform.parent.position+Vector3.up*30.0f);
    transform.position = Camera.main.transform.position + (dir.normalized * fixed_dist);

    //orientation
    Vector3 cc =Vector3.Normalize(cam_pt - transform.parent.position);
    if (cc != Vector3.zero){
      transform.forward =cc;
    }else{
      transform.forward =Vector3.forward;
    }

  }
}
