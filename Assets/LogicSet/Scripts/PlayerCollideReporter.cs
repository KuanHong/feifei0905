﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollideReporter : MonoBehaviour {
  public PlayerController player_controller = null;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

  public void OnCollisionStay(Collision collision) {
    if (collision.gameObject.layer == LayerMask.NameToLayer("Missile")) {
      return;
    }

    //Debug.Log("19 - player collide with " + collision.gameObject.name);
    processCollision(collision);
  }

  public void OnCollisionEnter(Collision collision) {
    if (collision.gameObject.layer == LayerMask.NameToLayer("Missile")) {
      return;
    }

    //Debug.Log("24 - player collide with " + collision.gameObject.name);
    //player_controller.playDizzyView();
    processCollision(collision);
  }

  void processCollision(Collision collision) {
    Vector3 normal = Vector3.zero;
    ContactPoint[] cp = collision.contacts;
    for (int i = 0; i < cp.Length; ++i) {
      normal += cp[i].normal;
    }
    normal.Normalize();

    float bounce_ratio = -2.0f;

    //calculate react force
    Vector3 tmp_curr_vel = player_controller.getCurrVelocity();
    if (Vector3.Dot(tmp_curr_vel, normal) >= 0) { //由內而外撞擊
      return;
    }

    //if ( Mathf.Abs(Vector3.Dot(tmp_curr_vel, normal)) <= 0.1f){ //垂直撞擊
    //  tmp_curr_vel += (Vector3.Cross(normal, Vector3.up));
    //}

    Vector3 outVelocity = 1.0f * (bounce_ratio * (Vector3.Dot(tmp_curr_vel, normal) * normal) + tmp_curr_vel);
    player_controller.setReactForce(outVelocity);
  }

  public void OnTriggerEnter(Collider other) {
    //Debug.Log("18 - player trigger collider bound with " + other.name);
    //player_controller.playDizzyView();

    //find surface normal
    //...



  }

  
}
