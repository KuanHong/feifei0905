﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBtnCollideReporter : MonoBehaviour
{
    public enum BtnType
    {
        SHIELD,
        ACCELERATOR
    }
    public BtnType type = BtnType.SHIELD;

    public delegate void ReportFunc(BtnType report_type);
    ReportFunc rfunc = null;
    public void registerReportFunc(ReportFunc rf)
    {
        rfunc = rf;
    }


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("23 - " + other.name + " collide with " + type.ToString());

        if (!other.CompareTag("Player"))
            return;
        //report to player controller
        if (rfunc != null)
        {
            rfunc(type);
        }
    }
}
