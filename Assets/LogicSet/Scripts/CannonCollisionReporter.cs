﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonCollisionReporter : MonoBehaviour {
  public CannonBall.CannonBallInfo parentBall = null;

  // Use this for initialization
  void Start() {

  }

  // Update is called once per frame
  void Update() {

  }

  private void OnTriggerEnter(Collider other) {
    Debug.Log("Cannon collide with " + other.gameObject.name);
    parentBall.collideWithGameObject(other.gameObject.name);

  }

  private void OnCollisionEnter(Collision collision) {
    Debug.Log("Cannon collide with " + collision.gameObject.name);
    parentBall.collideWithGameObject(collision.gameObject.name);


  }
}
