﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    public Transform[] freeFightSpawnPoints = null;
    public Transform[] team1FightSpawnPoints = null;
    public Transform[] team2FightSpawnPoints = null;
    public GameObject[] unload_points = null;
    public bool forceViveOff = false;
    bool viveMode = false;
    //bool mIsHost = false;

    public GameObject[] trackerSrcObj = null;
    public GameObject[] trackerDstObj = null;

    public GameObject[] ui_btn_obj = null;

    public enum VIVE_ControllingMode
    {
        FOLLOW_VIVE_CHECKER,
        CONTROLLER,
        TRACKER
    }
    public VIVE_ControllingMode controlling_mode = VIVE_ControllingMode.FOLLOW_VIVE_CHECKER;
    public GameObject[] hands_array = null;

    bool bGameSceneOnlyMode = false;

    public enum GamePhase
    {
        PRE_INIT = 0,
        INIT,
        //MENU,
        //NETWORK_LINKING,
        SCENE_READY_SYNC,
        PLAYING,
        END,
        SZ
    }
    GamePhase current_game_phase = GamePhase.PRE_INIT;
    FSMSystem fsm_vault = new FSMSystem();

    [HideInInspector]
    public PlayerController pc = null;
    [HideInInspector]
    public GameMenuController gmc = null;
    //[HideInInspector]
    //public MultiplayerHandler mph = null;
    [HideInInspector]
    public MatchmakingController mmc = null;

    // Use this for initialization
    void Start()
    {

        //locate controllers
        string ppname = transform.parent.parent.name;
        pc = GameObject.Find(ppname + "/Controller/PlayerController").GetComponent<PlayerController>();
        for (int i = 0; i < unload_points.Length; ++i)
        {
            pc.addUnloadPt(i, unload_points[i]);
        }

        gmc = GameObject.Find(ppname + "/Controller/GameMenuController").GetComponent<GameMenuController>();
        //mph = GameObject.Find(ppname + "/Controller/NetworkManager").GetComponent<MultiplayerHandler>();

        if (GameObject.Find("ViveInit") == null)
        {
            //mock vivechecker state
            Debug.LogWarning("44 - use mock vive status...");
            GameObject gg = new GameObject();
            gg.name = "ViveInit";
            gg.AddComponent<ViveChecker>();

            viveMode = ViveChecker.instance.getViveMode();
            if (forceViveOff)
            {
                viveMode = false;
            }
        }
        else
        {
            viveMode = ViveChecker.instance.getViveMode();
        }
        Debug.Log("63 - ViveMode =" + viveMode);

        mmc = GameObject.Find("Matchmaking").GetComponent<MatchmakingController>();

        //prepare fsm
        fsm_vault.allocFSM((int)GamePhase.SZ);
        fsm_vault.registerFSMBase(new FSM_Init(this, (int)GamePhase.INIT));
        fsm_vault.registerFSMBase(new FSM_Playing(this, (int)GamePhase.PLAYING));
        fsm_vault.registerFSMBase(new FSM_End(this, (int)GamePhase.END));

        current_game_phase = GamePhase.INIT;
        fsm_vault.gotoFSM((int)current_game_phase);

        if (controlling_mode == VIVE_ControllingMode.FOLLOW_VIVE_CHECKER)
        {
            controlling_mode = (VIVE_ControllingMode)(((int)ViveChecker.instance.currCM));
            mapper = ViveChecker.instance.trackerMapSeq;
        }

        //setup controller
        if (controlling_mode == VIVE_ControllingMode.TRACKER)
        {
            hands_array[0].transform.localRotation = Quaternion.Euler(-20.0f, -90.0f, -90.0f);
            hands_array[0].transform.localPosition = new Vector3(-0.0356f, 0.0189f, -0.03f);

            hands_array[1].transform.localRotation = Quaternion.Euler(-20.0f, 90.0f, 90.0f);
            hands_array[1].transform.localPosition = new Vector3(0.0356f, 0.0189f, -0.03f);

        }

    }

    int[] mapper = new int[] { 0, 1 };

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.C) && controlling_mode == VIVE_ControllingMode.TRACKER)
        {
            for (int i = 0; i < 2; ++i)
            {
                mapper[i] = (mapper[i] + 1) % 2;
            }
        }

        //proceed FSM
        fsm_vault.runFSM();

    }

    public void FixedUpdate()
    {

        //sync tracker index
        if (controlling_mode == VIVE_ControllingMode.TRACKER)
        {
            for (int i = 0; i < 2; ++i)
            {
                if (trackerSrcObj[mapper[i]].transform.Find("Model") != null)
                {
                    trackerDstObj[i].GetComponent<SteamVR_TrackedObject>().SetDeviceIndex((int)trackerSrcObj[mapper[i]].transform.Find("Model").GetComponent<SteamVR_RenderModel>().index);
                }
            }
        }
    }

    public abstract class FSM : FSMBase
    {
        public GameController gc_inst = null;
        public FSM(GameController gc, int id) : base(id)
        {
            gc_inst = gc;
        }
    }

    public class FSM_Init : FSM
    {
        int[] playerInGameIDList = new int[] { 1, 2, 3, 4, 5, 6, 7, 8 };
        public FSM_Init(GameController gc, int id) : base(gc, id) { }
        public override void init(object extra_param) { }
        public override bool run(object ex_param)
        {
            Debug.Log("FSM_Init().run()...");

            //choice spawn position
            Transform rnd_spawn = GetSpawnPoint();

            SetTargetNum(gc_inst.mmc.getMatchmakingInfo().totalPlayers);
            DumplingManager.instance.SpawnTarget();

            //spawn user
            gc_inst.pc.spawn(rnd_spawn.position, rnd_spawn.rotation, gc_inst.viveMode, gc_inst.controlling_mode == VIVE_ControllingMode.TRACKER); //the call also set playercontroller status to PRE_LAUNCH 
            gc_inst.pc.resetDumpling();
            gc_inst.pc.resetHostile();

            SetPlayerInGameID(gc_inst.mmc.getMatchmakingInfo().groupChoosenPlayerList);

            Debug.Log("102 - total players = " + gc_inst.mmc.getMatchmakingInfo().totalPlayers);
            if (gc_inst.mmc.getMatchmakingInfo().totalPlayers == 1)
            {

                //initiate single player mode
                gc_inst.pc.startGame(false, gc_inst.mmc.getMatchmakingInfo()); //false:single player

                gc_inst.current_game_phase = GameController.GamePhase.PLAYING;
                gc_inst.fsm_vault.gotoFSM((int)gc_inst.current_game_phase);

            }
            else
            {

                //initiate multiplayer mode
                bool isHost = gc_inst.mmc.getMatchmakingInfo().isHost();
                if (isHost)
                {
                    //host
                    Debug.Log("119 - process host tasks (leader guid=" + gc_inst.mmc.getMatchmakingInfo().gameLeaderGuid + ", myguid=" + gc_inst.mmc.getMatchmakingInfo().myGuid + ")...");

                    //...
                    //等待其它所有玩家載入 scene 的部分放在 playController
                    //同步 scene 裡所有資料的部分放在 playController

                }

                gc_inst.pc.startGame(true, gc_inst.mmc.getMatchmakingInfo()); //true:multi player

                Debug.Log("netowrk connected (host:" + isHost + ")");
                gc_inst.current_game_phase = GameController.GamePhase.PLAYING;
                gc_inst.fsm_vault.gotoFSM((int)gc_inst.current_game_phase);

            }

            return true;
        }

        void SetTargetNum(int playerNum)
        {
            if (playerNum <= 4)
            {
                DumplingManager.instance.numTarget /= 3;
                DumplingManager.instance.numShield /= 3;
                DumplingManager.instance.numAccelerator /= 3;
            }
            else if (playerNum > 4 && playerNum <= 6)
            {
                DumplingManager.instance.numTarget /= (3 / 2);
                DumplingManager.instance.numShield /= (3 / 2);
                DumplingManager.instance.numAccelerator /= (3 / 2);
            }
        }

        /// <summary>
        /// FreeFight出生点在地图上八个区域，TeamFight出生点在中间两个区域内，每个区域有4个出生点，根据guid排序分配位置
        /// </summary>
        Transform GetSpawnPoint()
        {
            BeaconAPLayer.MatchmakingRoomStatus mrs = MatchmakingController.instance.getMatchmakingInfo();
            BeaconAPLayer.Group myGroupId = mrs.getMyPlayerInfo().groupid;
            if (MatchmakingController.instance.gameMode == MatchmakingController.GameMode.FreeFight)
            {
                return gc_inst.freeFightSpawnPoints[(int)myGroupId];
            }
            else
            {
                List<BeaconAPLayer.PlayerInfo> myTeamPlayers = new List<BeaconAPLayer.PlayerInfo>();

                for (int i = 0; i < mrs.groupChoosenPlayerList.Count; i++)
                {
                    if (mrs.groupChoosenPlayerList[i].groupid == mrs.getMyPlayerInfo().groupid)
                        myTeamPlayers.Add(mrs.groupChoosenPlayerList[i]);
                }

                myTeamPlayers.Sort();

                for (int i = 0; i < myTeamPlayers.Count; i++)
                {
                    if (myTeamPlayers[i].guid == mrs.myGuid)
                    {
                        if ((int)myGroupId == 0)
                            return gc_inst.team1FightSpawnPoints[i];
                        else
                            return gc_inst.team2FightSpawnPoints[i];
                    }
                }

                return gc_inst.team1FightSpawnPoints[0];
            }
        }

        /// <summary>
        /// 根据guid的排序分配游戏内id
        /// </summary>
        void SetPlayerInGameID(List<BeaconAPLayer.PlayerInfo> playerInfoList)
        {
            playerInfoList.Sort();
            for (int i = 0; i < playerInfoList.Count; i++)
            {
                playerInfoList[i].inGameId = playerInGameIDList[i];
            }
        }
    }

    public class FSM_Playing : FSM
    {

        int my_unload_pt_idx = -1;
        int hostile_unload_pt_idx = -1;

        public FSM_Playing(GameController gc, int id) : base(gc, id) { }
        public override void init(object extra_param)
        {
            Debug.Log("FSM_Playing().run()...");

            SetUpHandler();

            SetUpUIEvent();

            if (MatchmakingController.instance.gameMode == MatchmakingController.GameMode.TeamFight)
                SetUpUnloadPoint();
        }

        void SetUpHandler()
        {
            gc_inst.pc.registerGameHandler1(player_event_handler1); //broadcast to all players
            gc_inst.pc.registerGameHandler2(player_event_handler2); //send to specific player
            gc_inst.mmc.registerAPEventHandler(network_event_handler_converter); //event from network 
        }

        void SetUpUIEvent()
        {
            for (int i = 0; i < gc_inst.ui_btn_obj.Length; ++i)
            {
                gc_inst.ui_btn_obj[i].GetComponent<UIBtnCollideReporter>().registerReportFunc(ui_btn_event_handler);
            }
        }

        void SetUpUnloadPoint()
        {
            for (int i = 0; i < gc_inst.unload_points.Length; ++i)
            {
                if (i == (int)gc_inst.mmc.getMatchmakingInfo().getMyPlayerInfo().groupid)
                {
                    gc_inst.unload_points[i].SetActive(true);
                    gc_inst.unload_points[i].transform.Find("ballEffect").gameObject.SetActive(true);
                    gc_inst.unload_points[i].GetComponent<UnloadPtCollideReporter>().registerReportFunc(unload_pt_event_handler, false);

                    my_unload_pt_idx = i;
                }
                else
                {
                    gc_inst.unload_points[i].SetActive(true);
                    gc_inst.unload_points[i].transform.Find("ballEffect").gameObject.SetActive(false);

                    //prepare big-bun
                    gc_inst.unload_points[i].transform.Find("bun/bun_root").gameObject.layer = LayerMask.NameToLayer("Target");

                    //change ray color
                    ParticleSystem.MainModule mm = gc_inst.unload_points[i].transform.Find("CFX3 Rays").GetComponent<ParticleSystem>().main;
                    mm.startColor = new Color(1.0f, 0.0f, 0.0f, 139.0f / 255.0f);


                    hostile_unload_pt_idx = i;
                }
            }
        }

        void ui_btn_event_handler(UIBtnCollideReporter.BtnType type)
        {
            Debug.Log("247 - receive event (" + type.ToString() + ")");

            if (type == UIBtnCollideReporter.BtnType.ACCELERATOR)
            {
                gc_inst.pc.processNetworkMsg(gc_inst.mmc.getMatchmakingInfo().myGuid, MultiplayerHandler.GameEvent.PLAYER_ACCELERATING_ENABLED_FROM_COLLIDER, null);
            }

            if (type == UIBtnCollideReporter.BtnType.SHIELD)
            {
                gc_inst.pc.processNetworkMsg(gc_inst.mmc.getMatchmakingInfo().myGuid, MultiplayerHandler.GameEvent.PLAYER_SHIELD_ENABLED_FROM_COLLIDER, null);
            }

        }

        void unload_pt_event_handler(bool hostile_pt)
        {
            Debug.Log("202 - unload_pt rcv event (" + (hostile_pt ? "hostile" : "alliance") + ")");

            if (hostile_pt == false)
            {
                //unload dumpling
                gc_inst.pc.processNetworkMsg(gc_inst.mmc.getMatchmakingInfo().myGuid, MultiplayerHandler.GameEvent.DUMPLING_UNLOADING_FROM_COLLIDER, new List<object>() {
          (hostile_pt?hostile_unload_pt_idx:my_unload_pt_idx)
        }); //送至本機處理 (player controller)

            }

        }

        public override bool run(object ex_param)
        {
            return false;
        }

        //
        //event from local player
        //
        void player_event_handler2(string dest_guid, MultiplayerHandler.GameEvent e, List<object> param, bool reliable)
        {
            gc_inst.mmc.publishAPEvent(dest_guid, new Dictionary<string, object>() {
        {"GameEvent", (int)e},
        {"Parameters", param}
      }, reliable);
        }

        void player_event_handler1(MultiplayerHandler.GameEvent e, List<object> param, bool reliable)
        {
            if (e == MultiplayerHandler.GameEvent.END_GAME)
            {
                end_game_from_host();
                if (gc_inst.mmc.getMatchmakingInfo().isHost() == true)
                {
                    gc_inst.mmc.publishAPEvent(new Dictionary<string, object>() {
            {"GameEvent", (int)e },
            {"Parameters", param}
          }, reliable);
                }
            }
            else
            {
                gc_inst.mmc.publishAPEvent(new Dictionary<string, object>() {
          {"GameEvent", (int)e },
          {"Parameters", param}
        }, reliable);
            }

        }

        //
        //event from other network player
        //
        void network_event_handler_converter(string src_guid, Dictionary<string, object> payload)
        {
            MultiplayerHandler.GameEvent e = (MultiplayerHandler.GameEvent)(System.Int64)payload["GameEvent"];
            network_event_handler(src_guid, e, (List<object>)payload["Parameters"]);
        }
        void network_event_handler(string src_guid, MultiplayerHandler.GameEvent e, List<object> param)
        {
            if (e == MultiplayerHandler.GameEvent.END_GAME)
            {

                end_game_from_remote();
                return;
            }
            else
            {

                gc_inst.pc.processNetworkMsg(src_guid, e, param);
            }

        }

        void end_game_from_host()
        {
            Debug.Log("end game event received (host)");

            Debug.Log("382 - calling endMatchmaking()...");
            //gc_inst.mph.endMatchmaking();

            Debug.Log("385 - calling goto END");
            gc_inst.current_game_phase = GameController.GamePhase.END;
            gc_inst.fsm_vault.gotoFSM((int)gc_inst.current_game_phase);
        }

        void end_game_from_remote()
        {
            Debug.Log("end game event received (remote)");

            gc_inst.pc.rcv_gameEnd();
        }
    }

    public class FSM_End : FSM
    {
        public FSM_End(GameController gc, int id) : base(gc, id) { }
        public override bool run(object ex_param)
        {
            Debug.Log("FSM_End().run()...");
            //gc_inst.current_game_phase = GameController.GamePhase.INIT;
            //gc_inst.fsm_vault.gotoFSM((int)gc_inst.current_game_phase);
            if (gc_inst.bGameSceneOnlyMode == false)
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene("matchmaking"); //back to matchmaking scene
            }
            else
            {
                gc_inst.current_game_phase = GameController.GamePhase.INIT;
                gc_inst.fsm_vault.gotoFSM((int)gc_inst.current_game_phase);
            }
            return false;
        }
    }

}

public class FSMSystem
{
    FSMBase[] FSMVault = null;
    public void allocFSM(int sz)
    {
        FSMVault = new FSMBase[sz];
    }

    public void registerFSMBase(FSMBase f)
    {
        if (FSMVault == null)
        {
            Debug.LogError("call allocFSM before registerFSMBase !");
            return;
        }

        if (FSMVault.Length <= f.FSM_ID)
        {
            Debug.LogError("FSM ID exceeded the fsm vault size !");
            return;
        }

        if (FSMVault[f.FSM_ID] != null)
        {
            Debug.LogError("FSMVault[" + f.FSM_ID + "] has been occupied by another FSM !");
            return;
        }
        FSMVault[f.FSM_ID] = f;
    }

    public bool runFSM(int no = -1, object extra_param = null)
    {
        return FSMVault[current_fsm_no].run(extra_param);
    }

    int current_fsm_no = -1;
    public void gotoFSM(int no, object extra_param = null)
    {
        FSMVault[no].init(extra_param);
        current_fsm_no = no;
    }
}

public abstract class FSMBase
{
    int my_fsm_id = -1;
    public int FSM_ID
    {
        get
        {
            return my_fsm_id;
        }
    }
    public FSMBase(int fsm_id)
    {
        my_fsm_id = fsm_id;
    }
    public virtual bool run(object extra_param) { return false; }
    public virtual void init(object extra_param) { }
}





