﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TracerFrustum : MonoBehaviour {
    Dictionary<string, GameObject> trackedObject = new Dictionary<string, GameObject>();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //int trackedObj = 0;
    void OnTriggerEnter(Collider other) {
        //trackedObj++;
        //Debug.Log("cn =" + other.name);
        if (other.gameObject.layer == LayerMask.NameToLayer("Target")) {
            Debug.Log("add tracked obj : " + other.gameObject.name);
            trackedObject.Add(other.gameObject.name, other.gameObject);
        }
    }

    void OnTriggerExit(Collider other) {
        //if (other.gameObject.layer != LayerMask.NameToLayer("Target"))
        //    return;

        //trackedObj--;
        if (trackedObject.ContainsKey(other.gameObject.name) == false) {
            //Debug.LogWarning("tracked object " + other.gameObject.name + " missing !");
            return;
        }
        trackedObject.Remove(other.gameObject.name);
    }

    public void objectDestroyed(string name) {
        if (trackedObject.ContainsKey(name)) {
            Debug.Log("remove tracked obj : " + name);
            trackedObject.Remove(name);
        }
    }

    public List<GameObject> getTrackedObject() {
        List<GameObject> v = new List<GameObject>();
        v.AddRange(trackedObject.Values);
        //Debug.Log("tracked obj : " + v.Count);
        return v;
    }

    public void purgeTrackedObj() {
        trackedObject.Clear();
    }
}
