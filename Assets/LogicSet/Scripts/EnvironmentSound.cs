﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class EnvironmentSound : MonoBehaviour {
    public AudioMixer mixer = null;
    AudioSource[] asund = null;
    enum SoundType {
        ENVIRONMENT =0,
        WIND,
        GAME_BGM,
        WARN,
        SZ
    }

	// Use this for initialization
	void Start () {
        asund = GetComponents<AudioSource>();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    float minSpeed = 100.0f;
    float maxSpeed = 300.0f;

    float minVol = 1.0f;
    float maxVol = 10.0f;

    float minPitch = 1.0f;
    float maxPitch = 5.0f;

    public void setSpeed(float speed) {
        if (speed <= minSpeed) {
            asund[(int)SoundType.WIND].volume = minVol;
            asund[(int)SoundType.WIND].pitch = minPitch;
        }else
        if (speed >= maxSpeed) {
            asund[(int)SoundType.WIND].volume = maxVol;
            asund[(int)SoundType.WIND].pitch = maxPitch;
        }else {
            float ratio = (speed-minSpeed) / (maxSpeed - minSpeed);

            float vol = minVol + ((maxVol - minVol) * ratio);
            float pitch = minPitch + ((maxPitch - minPitch) * ratio);

            asund[(int)SoundType.WIND].volume = vol;
            asund[(int)SoundType.WIND].pitch = pitch;
        }
    }

    bool playing = false;
    public void setGamePlaying(bool play) {
        if (play == playing)
            return;

        playing = play;
        if (play) {
            
            mixer.FindSnapshot("GAME").TransitionTo(1.0f);
        }else {
            asund[(int)SoundType.GAME_BGM].timeSamples = 0;
            mixer.FindSnapshot("MENU").TransitionTo(1.0f);
        }
    }

    bool playing_warn = false;
    public void setPlayWarn(bool play) {
        if (play == playing_warn)
            return;

        playing_warn = play;
        if (play) {
            asund[(int)SoundType.WARN].timeSamples = 0;
            asund[(int)SoundType.WARN].Play();
        }else {
            asund[(int)SoundType.WARN].Stop();
        }
    }


}
