﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinePloter : MonoBehaviour {

    public enum HeightType {
        FIXED,
        EXTRACT,
        ADAPTIVE
    }
    HeightType ht = HeightType.EXTRACT;

    public float plot_width = 1.0f;
    public float plot_height = 0.5f;

    LineRenderer lr = null;
    float recordCounter = 0.0f;
    float recordStride = 0.0f;
    float tmpCurrentValue = 0.0f;
    int plotPts = 0;
    bool inited = false;

    List<Vector3> record_list = new List<Vector3>();
    List<Vector3> scaled_record_list = new List<Vector3>();

    // Use this for initialization
    void Start () {
        if (lr==null)
            lr = GetComponent<LineRenderer>();
	}

    // Update is called once per frame
	void Update () {
        if (inited == false)
            return;

        recordCounter += Time.deltaTime;
        if (recordCounter >= recordStride) {
            recordCounter = 0.0f;

            record_list.Add(new Vector3(0.0f, tmpCurrentValue, 0.0f));
            while (record_list.Count > plotPts) {
                record_list.RemoveAt(0);
            }

            //scale value
            List<Vector3> sd =scaleRecordList();

            lr.SetPositions(sd.ToArray());
        }
		
	}

    float min_height = 0.0f;
    float max_height = float.MinValue;
    List<Vector3> scaleRecordList() {
        scaled_record_list.Clear();
        float x_axis_stride = plot_width / (float)plotPts;

        if (ht != HeightType.FIXED) {
            //find min/max height
            if (ht == HeightType.ADAPTIVE) {
                min_height = float.MaxValue;
            }


            max_height = float.MinValue;
            for (int i = 0; i < record_list.Count; ++i) {
                if (record_list[i].y > max_height) {
                    max_height = record_list[i].y;
                }

                if (record_list[i].y < min_height) {
                    min_height = record_list[i].y;
                }
            }
        }

        //scale value
        float max_minus_min = (max_height - min_height);
        if (max_minus_min == 0) {
            max_minus_min = 1;
        }
        float y_axis_scale =  plot_height/ max_minus_min;
        for (int i = 0; i < record_list.Count; ++i) {
            float yval = record_list[i].y - min_height;
            yval *= y_axis_scale;

            //setup x-axis value
            Vector3 v = new Vector3(x_axis_stride * i, yval, 0.0f);
            scaled_record_list.Add(v);
        }

        return scaled_record_list;
    }

    public void setFixedHeight(float min_val, float max_val) {
        ht = HeightType.FIXED;

        min_height = min_val;
        max_height = max_val;
    }

    public void initPlot(int p_plotPts, float plotDuration, HeightType p_ht=HeightType.EXTRACT) {
        if (lr == null)
            lr = GetComponent<LineRenderer>();

        ht = p_ht;
        plotPts = p_plotPts;
        recordStride = plotDuration / (float)p_plotPts;

        lr.positionCount = p_plotPts;

        Vector3[] dummy_array = new Vector3[lr.positionCount];
        for (int i = 0; i < lr.positionCount; ++i) {
            dummy_array[i] = Vector3.zero;
        }
        lr.SetPositions(dummy_array);

        inited = true;
    }

    public void setupValue(float value) {
        tmpCurrentValue = value;
        if (ht == HeightType.FIXED) {
            if (tmpCurrentValue > max_height) {
                tmpCurrentValue = max_height;
            }
            if (tmpCurrentValue < min_height) {
                tmpCurrentValue = min_height;
            }
        }
    }
}
