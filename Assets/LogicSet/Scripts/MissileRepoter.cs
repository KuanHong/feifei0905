﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace MissileBehaviours.Actions {
  public class MissileRepoter : ActionBase {
    float counter = 0.0f;
    public float missile_life = 10.0f;
    public GameObject explosionPrefab = null;

    public DumplingManager dm = null;
    public DumplingManager.TargetInfo ti = null;

    bool targetMissing = false;

    internal override void Update() {
      if (wasTriggered == true)
        return;

      if (ti != null && targetMissing == false) {
        if (ti.curr_status == DumplingManager.TargetInfo.Status.DISPOSED ||
            ti.curr_status == DumplingManager.TargetInfo.Status.NULL) {
          GetComponent<MissileBehaviours.Controller.MissileController>().Target = null;
          counter = missile_life - ((missile_life - counter) * 0.25f);

          targetMissing = true;
        }
      }

      counter += Time.deltaTime;
      if (counter >= missile_life) {
        internalExplosion();

      }
    }


    internal override void OnTrigger(object sender, EventArgs e) {
      Collider c = (Collider)sender;
      if (c.gameObject.layer == LayerMask.NameToLayer("StaticScene") ||
          c.gameObject.layer == LayerMask.NameToLayer("Terrain")) {
        internalExplosion();
        return;
      }
      if (!wasTriggered && this.enabled) {
        destroyTarget(c.name);
      }
    }

    void destroyTarget(bool reachTarget) {
      wasTriggered = true;
      if (dm != null) {
        if (ti != null) {
          dm.targetDestroyed(this.gameObject, reachTarget ? ti : null);
        } else {
          dm.targetDestroyed(this.gameObject, (DumplingManager.TargetInfo)null);
        }
      }
    }

    void internalExplosion() {
      Instantiate(explosionPrefab, transform.position, Quaternion.identity);
      destroyTarget(false);
    }

    void destroyTarget(string name) {
      wasTriggered = true;
      if (dm != null) {
        dm.targetDestroyed(this.gameObject, name);
      }
    }
  }
}