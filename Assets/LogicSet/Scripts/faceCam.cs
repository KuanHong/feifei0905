﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class faceCam : MonoBehaviour {
  public float adj_angle = 90.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
    transform.localRotation = Quaternion.identity;
    transform.forward = Camera.main.transform.position - transform.position;

    transform.localRotation *= Quaternion.Euler(0.0f, adj_angle, 0.0f);

  }
}
