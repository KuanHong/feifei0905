﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransGateway : MonoBehaviour {
  List<IAnimation> mAnimationList = new List<IAnimation>();

  public GameObject enterence = null;
  public GameObject exitance = null;

  bool gateway_enabled = true;

  public int gateway_id = -1;

  float scale = 3.0f;
  float delay_contract = 5.0f;

	// Use this for initialization
	void Start () {
    gateway_enabled = false;
    currStatus = Status.CLOSED;

    //hide geometry
    enterence.SetActive(false);
    enterence.transform.parent.position = GetComponent<GatewayPlacement>().prepareNextGateway();
	}

  float sine_time = 0.0f;
	
	// Update is called once per frame
	void Update () {

    cooling_down_counter -= Time.deltaTime;

    for (int i = 0; i < mAnimationList.Count;/*++i*/) {
      mAnimationList[i].updateAnimation(Time.deltaTime);

      if (mAnimationList[i].isAnimationDone() && mAnimationList[i].isAutoDismiss()) {
        mAnimationList.RemoveAt(i);
      } else {
        ++i;
      }
    }

    if (currStatus == Status.OPENING) {
      enterence.SetActive(true);
      currStatus = Status.OPENED;

      {
        ScaleAnimation sa = new ScaleAnimation();
        sa.initAnimation(CurveUtil.BackEaseOut, 0.0f, 1.0f, new GameObject[] { enterence }, true);
        sa.setupScaleVal(Vector3.zero, Vector3.one* scale);
        mAnimationList.Add(sa);
      }


    } else
    if (currStatus == Status.CLOSING) {

      //五秒後關閉
      {
        ScaleAnimation sa = new ScaleAnimation();
        sa.initAnimation(CurveUtil.BackEaseIn, 5.0f, 1.0f, new GameObject[] { enterence }, true, delegate(object[] p) {
          //MirrorCamera.enabled = false;
          Debug.Log("57 - set TransGateway position (" + new_position + ")");
          enterence.transform.parent.position =new_position;

          currStatus = Status.CLOSED;
          gateway_enabled = false;

          cooling_down_counter = delay_contract; //

        });
        sa.setupScaleVal(Vector3.one* scale, Vector3.zero);
        mAnimationList.Add(sa);
      }

      currStatus = Status.WAIT_CLOSING;

    }else
    if (currStatus == Status.WAIT_CLOSING) {
      //beating effect
      if (sine_time < delay_contract) {
        sine_time += Time.deltaTime;
        float sv = scale + Mathf.Sin(sine_time*25.0f)*0.125f;
        enterence.transform.localScale = Vector3.one * sv;
      }
    }

		
	}

  enum Status {
    NULL,
    CLOSED,
    CLOSING,
    WAIT_CLOSING,
    OPENED,
    OPENING,
    WAIT_OPENING
  }
  Status currStatus = Status.NULL;

  Vector3 new_position;
  public bool setClose(Vector3 new_posi, bool force_close =false) { //renew position after gateway closed
    if (force_close == false && (gateway_enabled == false || currStatus == Status.CLOSING || currStatus == Status.WAIT_CLOSING))
      return false;

    new_position = new_posi;
    currStatus = Status.CLOSING;

    return true;
  }

  float cooling_down_counter = 0.0f;

  public bool setOpen(int tint_color_ref, bool force_open =false) {

    if (force_open ==false && (gateway_enabled == true))
      return false;

    if (cooling_down_counter > 0.0f) {
      return false;
    }

    currStatus = Status.OPENING;
    gateway_enabled = true;

    //tint color
    if (tint_color_ref == gateway_id) {
      //red
      transform.Find("Sphere/Model/RemoteViewSphereModel").GetComponent<MeshRenderer>().material.SetColor("_Tint", new Color(1.0f, 178.0f / 255.0f, 178.0f / 255.0f));
      transform.Find("Sphere/Model/ReflectionViewSphereModel").GetComponent<MeshRenderer>().material.SetColor("_Tint", new Color(1.0f, 0.0f, 0.0f));

    } else {
      //blue
      transform.Find("Sphere/Model/RemoteViewSphereModel").GetComponent<MeshRenderer>().material.SetColor("_Tint", new Color(195.0f / 255.0f, 245.0f / 255.0f, 1.0f));
      transform.Find("Sphere/Model/ReflectionViewSphereModel").GetComponent<MeshRenderer>().material.SetColor("_Tint", new Color(0.0f, 213.0f / 255.0f, 1.0f));

    }

    return true;
  }

  public bool isEnabled() {
    return gateway_enabled;
  }

}
