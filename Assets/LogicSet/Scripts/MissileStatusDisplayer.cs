﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileStatusDisplayer : MonoBehaviour {

  public GameObject eyeObj = null;
  GameObject icon = null;

  // Use this for initialization
  void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
    if (preparing == false)
      return;

    counter += Time.deltaTime;
    if (counter >= duration) {
      GetComponent<SpriteRenderer>().color = new Color(253.0f / 255.0f, 1.0f, 135.0f / 255.0f, 1.0f);
      preparing = false;
    }

  }

  float counter = 0.0f;
  float duration = 0.0f;
  bool preparing = false;

  public void missilePreparing(float d) {
    duration = d;
    counter = 0.0f;
    preparing = true;

    GetComponent<SpriteRenderer>().color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
  }
}
