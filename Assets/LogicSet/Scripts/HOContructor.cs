﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HOContructor : MonoBehaviour {

    public GameObject lineSegment = null;
    List<GameObject> lineSegGroup = new List<GameObject>(); 

	// Use this for initialization
	void Start () {
        for (int i = 0; i < 36; ++i) {
            GameObject s = GameObject.Instantiate(lineSegment);
            s.transform.SetParent(transform, false);
            s.name = "LineSeg_" + i;
            s.transform.localPosition = Quaternion.AngleAxis(i * 10, Vector3.up) * Vector3.forward*20.0f;
            s.transform.forward = Vector3.Cross(Vector3.up, s.transform.position - transform.position);
            s.transform.localRotation = s.transform.localRotation*Quaternion.AngleAxis(90.0f, new Vector3(1.0f, 0.0f, 0.0f));

            s.transform.localPosition -= transform.parent.transform.localPosition + new Vector3(0.0F, 3.0F, 0.0F);

            lineSegGroup.Add(s);
        }

        for (int i = 0; i < 36; ++i) {
            GameObject s = GameObject.Instantiate(lineSegment);
            s.transform.SetParent(transform, false);
            s.name = "ShortLineSeg_" + i;
            s.transform.localPosition = Quaternion.AngleAxis(5+(i * 10), Vector3.up) * Vector3.forward * 20.0f;
            s.transform.forward = Vector3.Cross(Vector3.up, s.transform.position - transform.position);
            s.transform.localRotation = s.transform.localRotation * Quaternion.AngleAxis(90.0f, new Vector3(1.0f, 0.0f, 0.0f));
            s.transform.localScale = new Vector3(1.0f, 0.1f, 1.0f);

            s.transform.localPosition -= transform.parent.transform.localPosition+new Vector3(0.0F, 3.0F, 0.0F);

            lineSegGroup.Add(s);
        }

        optimization();
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 c = transform.parent.forward;
        float rotAng = Vector3.Angle(Vector3.forward, new Vector3(c.x, 0.0f, c.z));
        if (Vector3.Cross(Vector3.forward, new Vector3(c.x, 0.0f, c.z)).y > 0.0f) {
            rotAng = -rotAng;
        }
        //Debug.Log(c + ", " + Vector3.forward + ", ang=" + rotAng);
        transform.localRotation = Quaternion.AngleAxis(rotAng, Vector3.up);

    }

    //mesh optimization
    void optimization() {
        MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
        CombineInstance[] combine = new CombineInstance[meshFilters.Length];
        int i = 0;
        while (i < meshFilters.Length) {
            combine[i].mesh = meshFilters[i].sharedMesh;
            combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
            meshFilters[i].gameObject.active = false;
            i++;
        }
        transform.GetComponent<MeshFilter>().mesh = new Mesh();
        transform.GetComponent<MeshFilter>().mesh.CombineMeshes(combine);
        transform.gameObject.active = true;
    }
}
