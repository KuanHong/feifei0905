﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatewayPlacement : MonoBehaviour {
  
  [System.Serializable]
  public class PlaceBound {
    public Vector3 center;
    public Vector3 size;
  }

  public PlaceBound[] placeBounds = null;

  // Use this for initialization
  void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

  private void OnDrawGizmos() {
    for (int i = 0; i < placeBounds.Length; ++i) {
      Gizmos.color = Color.yellow;
      Gizmos.DrawWireCube(placeBounds[i].center, placeBounds[i].size);

    }
  }


  public Vector3 prepareNextGateway() {

    int choose_bound = UnityEngine.Random.Range(0, placeBounds.Length);
    float rnd_x = UnityEngine.Random.Range(-placeBounds[choose_bound].size.x * 0.5f, placeBounds[choose_bound].size.x * 0.5f);
    float rnd_y = UnityEngine.Random.Range(-placeBounds[choose_bound].size.y * 0.5f, placeBounds[choose_bound].size.y * 0.5f);
    float rnd_z = UnityEngine.Random.Range(-placeBounds[choose_bound].size.z * 0.5f, placeBounds[choose_bound].size.z * 0.5f);

    Vector3 rnd_posi = placeBounds[choose_bound].center + (new Vector3(rnd_x, rnd_y, rnd_z));

    return rnd_posi;
  }
}
