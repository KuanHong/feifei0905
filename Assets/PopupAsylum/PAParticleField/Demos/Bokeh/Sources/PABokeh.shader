﻿Shader "PA/Bokeh" {
	Properties {
		_MainTex("Main Texture", 2D) = "white"
		_FocusDistance("Focus Distance", Float) = 0
	}
	SubShader {		
	
	Tags{
			"Queue"="Transparent"
			"RenderType"="Transparent"
			"IgnoreProjector"="True"
		}

        Pass {

        	Blend One One
        	ZWrite Off
        	Cull Off
        	
			CGPROGRAM
			
			//use the vertex program defined in "ParticleField.cginc"
			#pragma vertex vertParticleFieldCube
			//define the fragment program
			#pragma fragment frag 
			#pragma target 3.0
			
			//include the default Unity functions/variables etc
			#include "UnityCG.cginc"	
			//include PA Particle Field		
			#include "Assets/PopupAsylum/PAParticleField/Shaders/ParticleField.cginc"
			
			//include the required pa particle features for Bokeh
			#pragma shader_feature _ WORLDSPACE_ON
			
			float _FocusDistance;
			sampler2D _MainTex;
			
			//the fragment program using "v2fParticleField" data struct
			fixed4 frag(v2fParticleField i) : COLOR {
				fixed4 c = tex2Dbias(_MainTex, float4(i.tex.xy, 1, _FocusDistance));
				c *= i.color;
				c.rgb *= c.a;
				return c;
			}
		
			ENDCG
		}		
	}
}