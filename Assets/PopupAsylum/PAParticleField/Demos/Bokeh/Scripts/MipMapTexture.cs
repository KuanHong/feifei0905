﻿using UnityEngine;
using System.Collections;

namespace PA.ParticleField.Samples
{
	public class MipMapTexture : MonoBehaviour
	{
		const float MIN_BOKEH = -7;
		const float MAX_BOKEH = 2;

		public Texture2D[] textures;
		public Material bokehMaterial;

		void Start(){
			bokehMaterial.SetTexture ("_MainTex", GenerateTexture ());
		}

		public void SetBokeh(float value){
			value = Mathf.Clamp01 (value);
			float bokehOffset = Mathf.Lerp (MIN_BOKEH, MAX_BOKEH, value);
			bokehMaterial.SetFloat ("_FocusDistance", bokehOffset);
		}

		Texture2D GenerateTexture(){
			int width = 0;
			int height = 0;

			for (int i = 0; i < textures.Length; i++) {
				if (textures[i].width > width){
					width = textures[i].width;
				}
				if (textures[i].height > height){
					height = textures[i].height;
				}
			}

			Texture2D newTexture = new Texture2D (width, height, TextureFormat.ARGB32, true);

			for (int i = 0; i < newTexture.mipmapCount; i++) {
				Texture2D mipTexture = textures[Mathf.Clamp(i, 0, textures.Length-1)];
				newTexture.SetPixels(mipTexture.GetPixels(i), i);
			}

			newTexture.filterMode = FilterMode.Trilinear;
			newTexture.Apply (false);

			return newTexture;
		}
	}
}