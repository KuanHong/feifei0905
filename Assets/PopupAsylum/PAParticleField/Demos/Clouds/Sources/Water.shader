﻿Shader "Custom/Water" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_BumpMap("Normal Map", 2D) = "bump" {}
		_BumpMap1("Normal Map", 2D) = "bump" {}
		_BumpMap2("Normal Map", 2D) = "bump" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _BumpMap;
		sampler2D _BumpMap1;
		sampler2D _BumpMap2;

		struct Input {
			float2 uv_MainTex;
			float2 uv_BumpMap;
			float2 uv_BumpMap1;
			float2 uv_BumpMap2;
			float3 worldPos;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			IN.worldPos *= 0.1;
			
			fixed4 c = tex2D (_MainTex, IN.worldPos.xz * 0.04) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = 0.1;
			o.Smoothness = 1;
			o.Alpha = c.a;
			
			o.Normal = normalize(UnpackNormal(tex2D (_BumpMap, IN.worldPos.xz * 0.04)/3 + tex2D (_BumpMap1, IN.worldPos.xz * 0.03)/3 + tex2D (_BumpMap2, IN.worldPos.xz * 0.02)/3));
			o.Albedo += c.a * (o.Normal.y, 0, 1) * 1;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
