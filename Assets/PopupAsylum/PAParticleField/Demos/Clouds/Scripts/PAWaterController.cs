﻿//
// PAWaterController
// Mark Hogan
// @markeahogan
// www.popupasylum.co.uk
//

using UnityEngine;
using System.Collections;

namespace PA.ParticleField.Samples {

    /// <summary>
    /// Updates a transfoms position so that it stays directly beneath the targetTransform at its original Y height
    /// </summary>
    public class PAWaterController : MonoBehaviour {

        public Transform targetTransform;
        public float yPosition;

        void Start() {
            yPosition = transform.position.y;
        }

        void Update() {
            Vector3 newPosition = targetTransform.position;
            newPosition.y = yPosition;
            transform.position = newPosition;
        }
    }
}
