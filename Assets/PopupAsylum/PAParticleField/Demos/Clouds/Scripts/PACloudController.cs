﻿//
// PA cloud controller
// Mark Hogan
// @markeahogan
// www.popupasylum.co.uk
//

using UnityEngine;
using System.Collections;

namespace PA.ParticleField.Samples {
    /// <summary>
    /// Moves clouds so they are centered around the player, fading them on and off when the move
    /// </summary>
    public class PACloudController : MonoBehaviour {

        /// <summary>
        /// The target the clouds will be cenetered around
        /// </summary>
        public Transform followTarget;

        /// <summary>
        /// The cloud fields that are children of this object
        /// </summary>
        PAParticleField[] cloudFields;

        /// <summary>
        /// The size of each cell/distances at which a field will be moved
        /// </summary>
        Vector3 area;

        void Start() {
            //Get all the chirld fields
            cloudFields = GetComponentsInChildren<PAParticleField>();

            //Calculate the maximum distances between fields
            Bounds bounds = cloudFields[0].GetBounds();
            Vector3 min = bounds.center;
            Vector3 max = bounds.center;

            int result = 0;

            for (int i = 1; i < cloudFields.Length; i++) {
                bounds = cloudFields[i].GetBounds();
                min = Vector3.Min(bounds.center, min);
                max = Vector3.Max(bounds.center, max);
                result += cloudFields[i].particleCount;
            }

            min = transform.InverseTransformPoint(min);
            max = transform.InverseTransformPoint(max);
            min.y = 0;
            max.y = 0;

            area = max - min;
        }

        void Update() {
            UpdateCloudPositions();
        }

        void UpdateCloudPositions() {
            Vector3 targetPosition = followTarget.position;

            for (int i = 0; i < cloudFields.Length; i++) {

                //Determine if the fields position should be changed and reposition it
                Vector3 newPosition = cloudFields[i].transform.position;

                if (targetPosition.x - cloudFields[i].transform.position.x > area.x * 0.5f) {
                    newPosition.x += area.x;
                }
                if (targetPosition.x - cloudFields[i].transform.position.x < -area.x * 0.5f) {
                    newPosition.x -= area.x;
                }
                if (targetPosition.z - cloudFields[i].transform.position.z > area.z * 0.5f) {
                    newPosition.z += area.z;
                }
                if (targetPosition.z - cloudFields[i].transform.position.z < -area.z * 0.5f) {
                    newPosition.z -= area.z;
                }

                cloudFields[i].transform.position = newPosition;

                //Fade clouds near edges
                Vector3 cloudDiff = targetPosition - newPosition;
                cloudDiff.y = 0;

                float cloudAlpha = Mathf.Clamp01((cloudDiff.magnitude / (area.magnitude * 0.5f)) * 2f);
                cloudAlpha = Mathf.Pow(cloudAlpha, 6);
                cloudAlpha = 1f - cloudAlpha;

                Color col = cloudFields[i].color;
                col.a = cloudAlpha;
                cloudFields[i].color = col;
            }
        }
    }
}