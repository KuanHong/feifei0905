﻿//
// Stardust
// Mark Hogan
// @markeahogan
// www.popupasylum.co.uk
//

using UnityEngine;
using System.Collections;

namespace PA.ParticleField.Samples {

    /// <summary>
    /// Updates a particle field's force to match its current speed vector
    /// </summary>
    public class Stardust : MonoBehaviour {

        public float maxMagnitude = 2f;
        public float lerpiness = 0.8f;

        PAParticleField field;
        Vector3 lastPosition;
        Vector3 lastDifference;

        void Start() {
            field = GetComponent<PAParticleField>();
            lastPosition = transform.position;
            lastDifference = Vector3.zero;
        }

        void FixedUpdate() {
            Vector3 difference = transform.position - lastPosition;
            if (difference.normalized.magnitude > 0f) {
                difference = Vector3.ClampMagnitude(difference, maxMagnitude);
                lastDifference = Vector3.Lerp(difference, lastDifference, lerpiness);
                field.force = lastDifference;
            }
            lastPosition = transform.position;
        }
    }
}