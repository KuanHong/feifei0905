﻿//
// Rotator
// Mark Hogan
// @markeahogan
// www.popupasylum.co.uk
//

using UnityEngine;
using System.Collections;

namespace PA.ParticleField.Samples {

    /// <summary>
    /// Rotates a rigidbody over time
    /// </summary>
	public class Rotator : MonoBehaviour
	{
		const float RANGE = 0.4f;
		Rigidbody r;

		// Use this for initialization
		void Start ()
		{
			r = GetComponent<Rigidbody> ();
			Vector3 angluar = new Vector3 (Random.Range (-RANGE, RANGE), Random.Range (-RANGE, RANGE), Random.Range (-RANGE, RANGE));
			angluar.Normalize ();
			angluar *= RANGE;
			r.angularVelocity = angluar * Random.Range (0.8f, 1.2f);
		}

		void Update ()
		{
			r.angularVelocity = Vector3.ClampMagnitude (r.angularVelocity, RANGE);
		}
	}
}