﻿//
// SmoothCamera
// Based on SmoothFollow2 by Vasilis Christopoulos & Daniel Toliaferro
//
// Mark Hogan
// @markeahogan
// www.popupasylum.co.uk
//
using UnityEngine;
using System.Collections;

namespace PA.ParticleField.Samples
{
    /// <summary>
    /// Smoothly moves a camera to follow behind a target
    /// </summary>
	public class SmoothCamera : MonoBehaviour
	{
		public Transform target;
		public float distance = 3.0f;
		public float height = 3.0f;
		public float damping = 5.0f;
		public bool smoothRotation = true;
		public bool followBehind = true;
		public float rotationDamping = 10.0f;

		void Start(){
			if (target) {
				Vector3 wantedPosition;
				if (followBehind){
					wantedPosition = target.TransformPoint (0, height, -distance);
				}else{
					wantedPosition = target.TransformPoint (0, height, distance);
				}

				transform.position = wantedPosition;
				transform.LookAt (target, target.up);
			}
		}
	
		void FixedUpdate ()
		{
			Vector3 wantedPosition;
			if (followBehind) {
				wantedPosition = target.TransformPoint (0, height, -distance);
			} else {
				wantedPosition = target.TransformPoint (0, height, distance);
			}
		
			transform.position = Vector3.Lerp (transform.position, wantedPosition, Time.deltaTime * damping);
		
			if (smoothRotation) {
				Quaternion wantedRotation = Quaternion.LookRotation (target.position - transform.position, target.up);
				transform.rotation = Quaternion.Slerp (transform.rotation, wantedRotation, Time.deltaTime * rotationDamping);
			} else {
				transform.LookAt (target, target.up);
			}
		}
	}
}