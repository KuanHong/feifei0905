﻿//
// StarFieldSpaceship
// Mark Hogan
// @markeahogan
// www.popupasylum.co.uk
//

using UnityEngine;
using System.Collections;

namespace PA.ParticleField.Samples
{
    /// <summary>
    /// Applys gravityless movement to a rigidbody given player Input
    /// </summary>
	public class StarFieldSpaceship : MonoBehaviour
	{
		public float turnspeed = 500f;
		public float speed = 1000f;
		public float rollSpeed = 50000f;

		void Start (){
			//Screen.lockCursor = true;
		}

		void Update (){		
			float roll = -Input.GetAxis ("Horizontal") * rollSpeed * Time.deltaTime;
#if (UNITY_ANDROID || UNITY_IPHONE) && !UNITY_EDITOR
			float pitch = 0f;
			float yaw = 0f;
			float power = 0f;
			if(Input.touchCount>0){
				pitch = Input.touches[Input.touchCount-1].deltaPosition.y/(Screen.height*0.005f);
				yaw = Input.touches[Input.touchCount-1].deltaPosition.x/(Screen.width*0.005f);
			}
			if (Input.touchCount>1){
				power = speed * 5f;
			}
#else
			float pitch = (Input.GetAxis ("Mouse Y") + Input.GetAxis ("Vertical")) / 2f;
			float yaw = Input.GetAxis ("Mouse X") * 2;
			float power = Input.GetKey (KeyCode.Space) ? speed * 5f : 0f;
#endif
		
			GetComponent<Rigidbody>().AddRelativeTorque (pitch * turnspeed * Time.deltaTime, yaw * turnspeed * Time.deltaTime, roll * Time.deltaTime);
			GetComponent<Rigidbody>().AddRelativeForce (0, 0, (speed + power) * Time.deltaTime);
		}
	}
}