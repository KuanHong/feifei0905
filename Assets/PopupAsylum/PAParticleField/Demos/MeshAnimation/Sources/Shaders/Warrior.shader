﻿Shader "PA/Warrior" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_MoveSpeed ("Move Speed", Float) = 5
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		
		// Basic set of includes required for effect
		#pragma shader_feature DIRECTIONAL_ON
		#pragma shader_feature _ WORLDSPACE_ON

		// Include PAParticleField cgincs
		#include "Assets/PopupAsylum/PAParticleField/Shaders/ParticleField.cginc"
		#include "Assets/PopupAsylum/PAParticleField/Shaders/ParticleMeshField.cginc"
		
		// PBS with all frills removed for compile speed
		#pragma surface surf Standard noshadow nometa nolightmap nodynlightmap nodirlightmap exclude_path:deferred exclude_path:prepass vertex:vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;		
		half _MoveSpeed;
		fixed4 _Color;
		
		void vert(inout appdata_full v, out Input o){
			UNITY_INITIALIZE_OUTPUT(Input, o);
			
			float sinTime = sin(_Time.w * _MoveSpeed + v.color.g * 4 + v.color.b * 4);			
			v.vertex.z += sinTime * v.color.r * 0.01; 
			
			sinTime = clamp(sin(_Time.w * _MoveSpeed + 1 + v.color.g * 4 + v.color.b * 4), 0, 1);
			v.vertex.y += sinTime * v.color.r * 0.01;
			
			PAParticleMeshField(v);
			v.vertex = PAPositionVertexSurf(v.vertex);
		}

		void surf (Input IN, inout SurfaceOutputStandard o) {
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
