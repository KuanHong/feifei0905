﻿//
// DemoMenu
// Mark Hogan
// @markeahogan
// www.popupasylum.co.uk
//

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

namespace PA.ParticleField.Samples {

    /// <summary>
    /// Opens and closes the demo menu
    /// </summary>
    public class DemosMenu : MonoBehaviour, IPointerClickHandler {

        public string[] names;

        public Text headerButton;
        public Color openColor;
        public Color closedColor;

        public GameObject dropDown;

        void Start() {
            Show(false);
        }

        void Show(bool arg = true) {
            dropDown.SetActive(arg);
            headerButton.color = arg ? openColor : closedColor;
            headerButton.text = arg ? "DEMOS\n-" : "DEMOS\n+";
        }

        #region IPointerClickHandler implementation

        void IPointerClickHandler.OnPointerClick(PointerEventData eventData) {
            Show(!dropDown.activeSelf);
        }

        #endregion
    }
}