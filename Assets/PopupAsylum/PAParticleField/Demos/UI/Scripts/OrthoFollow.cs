﻿//
// OrthoFollow
// Mark Hogan
// @markeahogan
// www.popupasylum.co.uk
//

using UnityEngine;
using System.Collections;

namespace PA.ParticleField.Samples {

    /// <summary>
    /// Moves a transform, an orthographic camera, so that it stays above a target
    /// </summary>
    [ExecuteInEditMode]
    public class OrthoFollow : MonoBehaviour {

        public Transform target;
        public float offset;

        // Update is called once per frame
        void Update() {
            if (target) {
                transform.position = target.position + Vector3.up * offset;
            }
        }
    }
}