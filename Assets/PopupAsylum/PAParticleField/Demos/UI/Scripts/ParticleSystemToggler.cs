﻿//
// ParticleSystemToggler
// Mark Hogan
// @markeahogan
// www.popupasylum.co.uk
//

using UnityEngine;
using System.Collections;

namespace PA.ParticleField.Samples {

    /// <summary>
    /// Enables or diables the assigned 'system' gameobject
    /// </summary>
    public class ParticleSystemToggler : MonoBehaviour {

        public GameObject system;

        public void SetSystemOff(bool arg) {
            if (system) {
                system.SetActive(!arg);
            }
        }
    }
}
