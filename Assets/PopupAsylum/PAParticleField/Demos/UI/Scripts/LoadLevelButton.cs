﻿//
// LoadLevelButton
// Mark Hogan
// @markeahogan
// www.popupasylum.co.uk
//


using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

namespace PA.ParticleField.Samples {

    /// <summary>
    /// Loads a level when clicked
    /// </summary>
    public class LoadLevelButton : MonoBehaviour, IPointerClickHandler {

        #region IPointerClickHandler implementation

        public void OnPointerClick(PointerEventData eventData) {
            Application.LoadLevel(name);
        }

        #endregion
    }
}
