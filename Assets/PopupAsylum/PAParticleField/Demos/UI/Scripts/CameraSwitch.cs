﻿//
// CameraSwitch
// Mark Hogan
// @markeahogan
// www.popupasylum.co.uk
//

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

namespace PA.ParticleField.Samples {

    /// <summary>
    /// Switches cameras between layers and rects to give a "picture in picture" effect
    /// </summary>
    public class CameraSwitch : MonoBehaviour, IPointerClickHandler {

        public Camera cameraA;
        public Camera cameraB;

        #region IPointerClickHandler implementation

        public void OnPointerClick(PointerEventData eventData) {
            SwitchCameras();
        }

        #endregion

        RectTransform rt {
            get {
                if (!m_Rt) {
                    m_Rt = (RectTransform)transform;
                }
                return m_Rt;
            }
        }
        RectTransform m_Rt;

        void SwitchCameras() {
            float width = rt.sizeDelta.x / (float)Screen.width;
            float height = rt.sizeDelta.y / (float)Screen.height;

            Camera modCam = (cameraA.depth > cameraB.depth) ? cameraB : cameraA;

            cameraA.rect = new Rect(0, 0, 1, 1);
            cameraB.rect = new Rect(0, 0, 1, 1);
            cameraA.depth = 0;
            cameraB.depth = 0;

            modCam.rect = new Rect(0f, 1f - height, width, height);
            modCam.depth = 1;
        }

        void Update() {
            if (Input.GetKeyDown(KeyCode.E)) {
                SwitchCameras();
            }
        }
    }
}