﻿//
// DemoMenu
// Mark Hogan
// @markeahogan
// www.popupasylum.co.uk
//

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace PA.ParticleField.Samples {

    /// <summary>
    /// Monitors the time per frame and displays it
    /// </summary>
    public class FPSCounter : MonoBehaviour {

        public float updateInterval = 0.5F;

        private float accum = 0; // FPS accumulated over the interval
        private int frames = 0; // Frames drawn over the interval
        private float timeleft; // Left time for current interval

        string frameString;



        void Start() {
            timeleft = updateInterval;
        }

        void Update() {
            timeleft -= Time.deltaTime;
            accum += Time.timeScale / Time.deltaTime;
            ++frames;

            // Interval ended - update GUI text and start new interval
            if (timeleft <= 0.0) {
                // display two fractional digits (f2 format)
                float fps = accum / frames;
                frameString = ((int)fps).ToString();

                timeleft = updateInterval;
                accum = 0.0F;
                frames = 0;

                GetComponent<Text>().text = frameString;
            }
        }
    }
}