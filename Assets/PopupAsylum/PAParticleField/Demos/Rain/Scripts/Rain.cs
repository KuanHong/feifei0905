﻿//
// Rain
// Mark Hogan
// @markeahogan
// www.popupasylum.co.uk
//

using UnityEngine;
using System.Collections;

namespace PA.ParticleField.Samples {
    /// <summary>
    /// Updates a particle fields position so that it sits at the ground height in front of the target camera
    /// </summary>
    public class Rain : MonoBehaviour {

        public Camera targetCamera;
        public PAParticleField dropField;
        public float groundHeight = 0.05f;

        Vector3 mainOffset;

        void Awake() {
            mainOffset = targetCamera.transform.InverseTransformPoint(transform.position);
        }

        void FixedUpdate() {
            Vector3 pos = targetCamera.transform.TransformPoint(mainOffset);
            pos.y = groundHeight;
            transform.position = pos;
            Vector3 forward = targetCamera.transform.forward;
            forward.y = 0f;

            transform.rotation = Quaternion.LookRotation(forward, Vector3.up);
        }
    }
}